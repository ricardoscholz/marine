package mustic.scholz.marine.test;

import mustic.scholz.marine.core.PerformanceFacade;
import mustic.scholz.marine.core.performance.element.CustomElement;
import mustic.scholz.marine.core.performance.element.builtin.CalibrationAgent;
import mustic.scholz.marine.core.performance.transition.CommandTransition;
import mustic.scholz.marine.core.performance.transition.TimerTransition;

/**
 * This is a test class that creates a performance, with a set of transitions, and tests if the transitions are working as expected. The
 * class is expected to test the behaviour of {@link CommandTransition} and {@link TimerTransition}. See the source code for non-JavaDoc
 * comments.
 * 
 * IMPORTANT: As no flow control is implemented, remember to kill EyesWeb process manually after quitting.
 * 
 * @see mustic.scholz.marine.test.SavePerformanceTest
 * @author Ricardo Scholz
 *
 */
public class FlowTest {

	public static void main(String[] args) {

		PerformanceFacade facade = PerformanceFacade.getInstance();
		facade.setBackgroundColor(0xFFFFFFFF);
		facade.newPerformance();
		
		CalibrationAgent skeleton = new CalibrationAgent();
		CustomElement skeletonElement = new CustomElement(skeleton, skeleton.getName(), 0xFF0000);
		CalibrationAgent calibration = new CalibrationAgent();
		CustomElement calibrationElement = new CustomElement(calibration, calibration.getName(), 0x0000FF);
		
		CommandTransition transition1 = new CommandTransition();
		TimerTransition transition2 = new TimerTransition();
		transition2.setDuration(0, 0, 10, 0);
		TimerTransition transition3 = new TimerTransition();
		transition3.setDuration(0, 1, 0, 0);
		CommandTransition transition4 = new CommandTransition();
		
		//Test Spectacle
		//TRANSITIONS: command  >> timer 10s   >> timer 1m0s  >> command  >> IDLE
		//TRACK 0    : skeleton .............. >> ----------- >> skeleton >> IDLE
		//TRACK 1    : -------- >> calibration .............. >> -------- >> IDLE
		//Subtitle: ---- no element | .... continuation of element
		
		//Track 0
		facade.addElement(skeletonElement, 0, 0, false);
		facade.addElement(skeletonElement, 0, 1, false);
		facade.addElement(skeletonElement, 0, 3, false);
		
		//Track 1
		facade.addElement(calibrationElement, 1, 1, false);
		facade.addElement(calibrationElement, 1, 2, false);
		
		//Transitions
		facade.addTransition(transition1, 0, false);
		facade.addTransition(transition2, 1, false);
		facade.addTransition(transition3, 2, false);
		facade.addTransition(transition4, 3, false);

		System.out.println("Starting...");
		
		facade.play();
		
	}

}
