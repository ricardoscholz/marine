package mustic.scholz.marine.test;

import mustic.scholz.marine.core.PerformanceFacade;
import mustic.scholz.marine.core.performance.element.CustomElement;
import mustic.scholz.marine.core.performance.element.builtin.SkeletonAgent;

/**
 * This is a test class that prints a skeleton on screen, according to the performer position. See the source code for non-JavaDoc comments.
 * 
 * IMPORTANT: As no flow control is implemented, remember to kill EyesWeb process manually after quitting.
 * 
 * @see mustic.scholz.marine.core.performance.element.builtin.SkeletonAgent
 * @author Ricardo Scholz
 *
 */
public class SkeletonTest {

	public static void main(String[] args) {

		mustic.scholz.marine.core.PerformanceFacade facade = PerformanceFacade.getInstance();
		facade.setBackgroundColor(0xFFFFFFFF);
		facade.newPerformance();
		
		SkeletonAgent skeleton = new SkeletonAgent();
		CustomElement skeletonElement = new CustomElement(skeleton, skeleton.getName(), 0x00FFFF);
		facade.addElement(skeletonElement, 1, 0, false);

		System.out.println("Starting...");
		
		facade.play();
		
	}

}
