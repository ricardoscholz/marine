package mustic.scholz.marine.test;

import mustic.scholz.marine.core.PerformanceFacade;

/**
 * This is a test class that reads and loads a performance from a binary file (D:\savePerformanceTest.mpf), for testing purposes. See the 
 * source code for non-JavaDoc comments.
 * 
 * IMPORTANT: As no flow control is implemented, remember to kill EyesWeb process manually after quitting.
 * 
 * @see mustic.scholz.marine.test.SavePerformanceTest
 * @author Ricardo Scholz
 *
 */
public class LoadPerformanceTest {

	public static void main(String[] args) {
		
		PerformanceFacade facade = PerformanceFacade.getInstance();
		facade.setBackgroundColor(0xFFFFFFFF);
		facade.setInputDeviceAngle(0);
		
		facade.openPerformance("D:\\savePerformanceTest.mpf");
		
		System.out.println("Preparing...");
		
		facade.prepareExecution();
		
		System.out.println("Starting...");
		
		facade.play();
		
	}
	
}
