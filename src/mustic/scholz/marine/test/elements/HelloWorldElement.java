package mustic.scholz.marine.test.elements;

import java.util.Random;

import mustic.scholz.marine.core.PerformanceFacade;
import mustic.scholz.marine.core.performance.element.CustomElement;
import mustic.scholz.marine.core.performance.element.Element;

/**
 * This class is a Hello World implementation. Please, see the source code for non-JavaDoc comments. It shows random color centralised 
 * increasing circles on screen for 20 seconds, then exits.
 * 
 * IMPORTANT: If you manually exit before the end of execution, remember to kill EyesWeb process manually.
 * 
 * @author Ricardo Scholz
 *
 */
//A class that extends Element...
public class HelloWorldElement extends Element {
	
	//Speed in which radius get larger
	private static final int SPEED = 15;
	
	//Stores the current radius of the circle
	private int radius;
	
	//The maximum radius to detect that circle is out of screen
	private int maxRadius;
	
	//Stores the current color being used
	private int color;
	
	//Stores the last used color
	private int lastColor;
	
	//We'll need some random behaviours...
	private Random random;
	
	//Some general (optional) information provided within the constructor.
	public HelloWorldElement() {
		System.out.println("Constructor called.");
		super.setName("Hello World Element");
		super.setDescription("This is a Hello World! element.");
		super.setAuthor("Ricardo Scholz");
		super.setVersion("1.0.0");
	}
	
	//Overridden methods:
	
	//This method is called only once, in the beginning of the execution.
	@Override
	public void setup() {
		System.out.println("Setup method called!");
		
		//Initialising initial radius
		this.radius = 0;
		//the hypothenuse from the centre of the screen to a corner
		this.maxRadius = (int) Math.round(Math.sqrt((Math.pow(super.performance.displayWidth, 2)) + 
				(Math.pow(super.performance.displayHeight, 2))));
		//First circle is white
		this.color = 0xFF000000;
		//Background starts black
		this.lastColor = 0xFFFFFFFF;
		//Initialising Random object
		this.random = new Random(System.currentTimeMillis());
		
	}
	
	//This method is called every cycle, before the paint() method.
	@Override
	public void update() {
		System.out.println("Update method called!");
		//Updating variables.
		//Increasing radius
		this.radius += SPEED;
		//Checking if circle has grown outside the screen
		if (this.radius > this.maxRadius) {
			//Restarts the circle radius
			this.radius = 1;
			//Copies the current color to the last used color
			this.lastColor = this.color;
			//Gets a new random color (the | 0xFF000000 guarantees a 100% opaque color)
			this.color = this.random.nextInt(0xFFFFFF) | 0xFF000000;
		}
	}

	//This method is called every cycle, after the update() method.
	//Remember that concurrent elements may change graphics (PApplet) variables (fill, stroke, etc),
	//so make sure you set whatever you need.
	//Also, if you make any changes in perspective, make sure you undo them as this may cause collateral
	//effects on other elements using the same graphics (PApplet) afterwards.
	@Override
	public void paint() {
		System.out.println("Paint method called!");
		//Prepares 2D coordinates system to screen (independent of chosen plane) 
		//[0, 0] is in the middle of the screen, x grows to the right and y grows upward.
		//For 3D painting, no need for preparation, just start drawing.
		super.performance.begin2D();
		//Fills the background with the last color used
		super.performance.background(this.lastColor);
		//Sets the brush to the current color
		super.performance.fill(this.color);
		//Draws a circle with the current color and radius, from the screen centre
		super.performance.ellipse(0, 0, this.radius, this.radius);
		//Restores 3D coordinates system
		//IMPORTANT: every call to "begin2D()" must have a corresponding call to "end2D()".
		super.performance.end2D();
	}

	//This method is called once at the end of the execution of this element. There is no guarantee that
	//this method will be called, but it should be if responsible programmers are using the framework.
	@Override
	public void destroy() {
		System.out.println("Destroy method called!");
		//No resources to free...
	}

	//A main method, which usually stays out of the element class.
	public static void main(String[] args) throws Throwable {

		//Retrieving the PerformanceFacade instance
		PerformanceFacade facade = PerformanceFacade.getInstance();
		
		//Adjusting desired background color
		facade.setBackgroundColor(0xFFFFFFFF);
		
		//Creating a new graphics
		facade.newPerformance();
		
		//Instantiating the Hello World Element
		Element element = new HelloWorldElement();
		
		//Creating a custom Hello World Element with red key
		CustomElement customElement = new CustomElement(element, element.getName(), 0xFF0000);
		
		//Adding hello world element to the graphics
		facade.addElement(customElement, 0, 0, false);
		
		//Loads PApplet, so that future "play()" calls respond quickly
		facade.prepareExecution();
		
		//Waiting 5 seconds on background color...
		Thread.sleep(10000);
		
		//Playing the graphics
		facade.play();
		
		//PGraphics lasts playing for 20 seconds...
		Thread.sleep(20000);
		
		//Then it is stopped.
		facade.stop();
		
		//Killing EywConsole.exe
		facade.destroy();
		
		System.exit(0);
		
	}
	
}
