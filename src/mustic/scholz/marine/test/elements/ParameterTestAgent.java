package mustic.scholz.marine.test.elements;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mustic.scholz.marine.core.entity.JointEnum;
import mustic.scholz.marine.core.feature.Feature;
import mustic.scholz.marine.core.feature.FeatureFactory;
import mustic.scholz.marine.core.performance.element.Element;
import mustic.scholz.marine.core.performance.element.InputTypeEnum;
import mustic.scholz.marine.core.performance.element.parameter.ColorParameter;
import mustic.scholz.marine.core.performance.element.parameter.FlagParameter;
import mustic.scholz.marine.core.performance.element.parameter.NumberParameter;
import mustic.scholz.marine.core.performance.element.parameter.Parameter;
import mustic.scholz.marine.core.performance.element.parameter.SelectionParameter;
import mustic.scholz.marine.core.performance.element.parameter.TextParameter;
import mustic.scholz.marine.core.performance.element.parameter.TimeParameter;
import mustic.scholz.marine.core.persistence.ParameterSerializer;
import mustic.scholz.marine.core.persistence.binary.FeatureSerializer;
import mustic.scholz.marine.core.persistence.binary.JointEnumSerializer;

/**
 * This class defines an element that prints to the console some parameter values. It has been developed for debug purposes. Please, see the
 * source code for non-JavaDoc comments.
 * 
 * @see mustic.scholz.marine.test.SavePerformanceTest
 * @author Ricardo Scholz
 */
public class ParameterTestAgent extends Element implements ParameterSerializer<Integer, DataInputStream, DataOutputStream> {
	
	public static final int PARAM_COLOR 	= 0;
	public static final int PARAM_FLAG 		= 1;
	public static final int PARAM_NUMBER 	= 2;
	public static final int PARAM_SELECTION	= 3;
	public static final int PARAM_FEATURE	= 4;
	public static final int PARAM_JOINT		= 5;
	public static final int PARAM_TEXT 		= 6;
	public static final int PARAM_TIME		= 7;
	
	private String result;
	
	public ParameterTestAgent() {
		
		this.result = "nothing set yet.";
		
		super.parameters = new HashMap<Integer, Parameter>();
		
		//Color parameter. Value red. Default value blue.
		Parameter color = new ColorParameter(InputTypeEnum.COLOR, PARAM_COLOR, "Color", "Color picking parameter.", 0xFFFF0000, 0xFF0000FF);
		
		//Flag parameter. Value false. Default value true.
		Parameter flag = new FlagParameter(InputTypeEnum.SWITCH, PARAM_FLAG, "Flag", "Flag parameter.", false, true);
		
		//Number parameter. Value 10. Default value 20. Range 1 to 100.
		Parameter number = new NumberParameter(InputTypeEnum.INTEGER_SLIDER, PARAM_NUMBER, "Number", "Number parameter.", 10f, 20f, 1f, 100f);
		
		//Selection parameter. Value 3. Default value 1. Elements: 1, 2, 3, 4, 5.
		List<Integer> selectionList = new ArrayList<Integer>();
		selectionList.add(1); selectionList.add(2); selectionList.add(3); selectionList.add(4); selectionList.add(5);
		Parameter selection = new SelectionParameter<Integer>(InputTypeEnum.SELECTION_LIST, PARAM_SELECTION, "Selection", 
				"Selection parameter.", selectionList.get(2), selectionList.get(0), selectionList, "toString", 
				this);
		
		//Selection parameter <Feature>. 
		List<Feature> featuresList = FeatureFactory.buildAllBuiltInFeatures();
		Parameter feature = new SelectionParameter<Feature>(InputTypeEnum.SELECTION_LIST, PARAM_SELECTION, "Features", 
				"Features selection.", featuresList.get(0), featuresList.get(1), featuresList, "name", new FeatureSerializer());
		
		//Selection parameter <JointEnum>.
		List<JointEnum> jointsList = JointEnum.getAsList();
		Parameter joint = new SelectionParameter<JointEnum>(InputTypeEnum.SELECTION_LIST, PARAM_SELECTION, "Joints", 
				"Jointes selection.", jointsList.get(0), jointsList.get(1), jointsList, "name", new JointEnumSerializer());
		
		//Text parameter.
		Parameter text = new TextParameter(InputTypeEnum.TEXT_BOX, PARAM_TEXT, "Text", "Text parameter.", "This is a custom text value.", 
				"This is the default text parameter value.");
		
		//Time parameter. Value 10 seconds. Default value 2 seconds.
		Parameter time = new TimeParameter(InputTypeEnum.TIME, PARAM_TIME, "Time", "Time parameter.", 10000l, 2000l);
		
		super.parameters.put(PARAM_COLOR, color);
		super.parameters.put(PARAM_FLAG, flag);
		super.parameters.put(PARAM_NUMBER, number);
		super.parameters.put(PARAM_SELECTION, selection);
		super.parameters.put(PARAM_FEATURE, feature);
		super.parameters.put(PARAM_JOINT, joint);
		super.parameters.put(PARAM_TEXT, text);
		super.parameters.put(PARAM_TIME, time);
	}
	
	@Override
	public void setup() {
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void update() {
		this.result = "Parameters:\n";
		ColorParameter color 					= (ColorParameter) super.parameters.get(PARAM_COLOR);
		FlagParameter flag 						= (FlagParameter) super.parameters.get(PARAM_FLAG);
		NumberParameter number 					= (NumberParameter) super.parameters.get(PARAM_NUMBER);
		SelectionParameter<Integer> selection 	= (SelectionParameter<Integer>) super.parameters.get(PARAM_SELECTION);
		SelectionParameter<Feature> feature		= (SelectionParameter<Feature>) super.parameters.get(PARAM_FEATURE);
		SelectionParameter<JointEnum> joint 	= (SelectionParameter<JointEnum>) super.parameters.get(PARAM_JOINT);
		TextParameter text 						= (TextParameter) super.parameters.get(PARAM_TEXT);
		TimeParameter time 						= (TimeParameter) super.parameters.get(PARAM_TIME);
		this.result = this.result + "\nColor:\n\tValue: " + color.getValue() + " | Default: " + color.getDefaultValue();
		this.result = this.result + "\nFlag:\n\tValue: " + flag.getValue() + " | Default: " + flag.getDefaultValue();
		this.result = this.result + "\nNumber:\n\tValue: " + number.getValue() + " | Default: " + number.getDefaultValue();
		this.result = this.result + "\nSelection:\n\tValue: " + selection.getValue() + " | Default: " + selection.getDefaultValue();
		this.result = this.result + "\nFeature:\n\tValue: " + feature.getValue() + " | Default: " + feature.getDefaultValue();
		this.result = this.result + "\nJoint:\n\tValue: " + joint.getValue() + " | Default: " + joint.getDefaultValue();
		this.result = this.result + "\nText:\n\tValue: " + text.getValue() + " | Default: " + text.getDefaultValue();
		this.result = this.result + "\nTime:\n\tValue: " + time.getValue() + " | Default: " + time.getDefaultValue();
	}
	
	@Override
	public void paint() {
		if (super.performance.frameCount % 150 == 0) {
			System.out.println(this.result);
		}
	}
	
	@Override
	public Integer readObject(DataInputStream dis)
			throws IOException {
		if (dis.readChar() != NULL_VALUE) {
			return dis.readInt();
		}
		return null;
	}

	@Override
	public void writeObject(Integer object, DataOutputStream dos)
			throws IOException {
		if (object == null) {
			dos.writeChar(NULL_VALUE);
		} else {
			dos.writeChar(NOT_NULL_VALUE);
			dos.writeInt(object);
		}
	}
}
