package mustic.scholz.marine.test;

import java.util.List;

import mustic.scholz.marine.core.PerformanceFacade;
import mustic.scholz.marine.core.performance.element.CustomElement;
import mustic.scholz.marine.core.performance.element.Element;
import mustic.scholz.marine.core.plugin.ElementPlugin;

/**
 * This is a test class that imports a plug-in file (D:\test_plugin.jar) to the facade and plays it, for testing purposes. See the source 
 * code for non-JavaDoc comments.
 * 
 * IMPORTANT: As no flow control is implemented, remember to kill EyesWeb process manually after quitting.
 * 
 * @see PerformanceFacade#importElementPlugin(String)
 * @author Ricardo Scholz
 *
 */
public class PluginTest {

	public static void main(String[] args) throws Exception {
		
		mustic.scholz.marine.core.PerformanceFacade facade = PerformanceFacade.getInstance();
		facade.setBackgroundColor(0xFFFFFFFF);
		facade.newPerformance();
		
		facade.importElementPlugin("D:\\test_plugin.jar");
		List<ElementPlugin> plugins = facade.loadAllElementPlugins();
		
		Element plugin = plugins.get(3).getElement();
		CustomElement customPlugin = new CustomElement(plugin, plugin.getName(), 0xFFFF00);
		plugin.setPerformance(facade.getPerformance());
		
		facade.addElement(customPlugin, 1, 0, false);

		System.out.println("Starting...");
		
		facade.play();

		Thread.sleep(15000);
		
		facade.stop();
		
		facade.destroy();
		
		System.exit(0);
	}

}
