package mustic.scholz.marine.test;

import mustic.scholz.marine.core.PerformanceFacade;
import mustic.scholz.marine.core.performance.element.CustomElement;
import mustic.scholz.marine.test.elements.ParameterTestAgent;

/**
 * This is a test class that creates and saves a performance to a binary file (D:\savePerformanceTest.mpf), for testing purposes. See the 
 * source code for non-JavaDoc comments.
 * 
 * IMPORTANT: As no flow control is implemented, remember to kill EyesWeb process manually after quitting.
 * 
 * @see mustic.scholz.marine.test.elements.ParameterTestAgent
 * @author Ricardo Scholz
 *
 */
public class SavePerformanceTest {

	public static void main(String[] args) {
		
		PerformanceFacade facade = PerformanceFacade.getInstance();
		facade.setBackgroundColor(0xFFFFFFFF);
		facade.setInputDeviceAngle(0);
		facade.newPerformance();

		ParameterTestAgent parameters = new ParameterTestAgent();
		CustomElement parametersElement = new CustomElement(parameters, parameters.getName(), 0x0000FF);
		facade.addElement(parametersElement, 0, 0, false);
		
		facade.savePerformance("D:\\savePerformanceTest.mpf");

		System.out.println("Starting...");
		
		try {
			facade.destroy();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Ended.");
		
		System.exit(0);
	}
	
}
