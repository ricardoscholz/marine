package mustic.scholz.marine.test;

import mustic.scholz.marine.core.PerformanceFacade;
import mustic.scholz.marine.core.performance.element.CustomElement;
import mustic.scholz.marine.core.performance.element.builtin.CalibrationAgent;

/**
 * This class runs the calibrator agent. Its purpose is to allow calibration of input device and projection coordinates when your equipment
 * is positioned on stage.
 * 
 * IMPORTANT: As no flow control is implemented, remember to kill EyesWeb process manually after quitting.
 * 
 * @see mustic.scholz.marine.core.performance.element.builtin.CalibrationAgent
 * @author Ricardo Scholz
 *
 */
public class Calibrator {

	public static void main(String[] args) {

		PerformanceFacade facade = PerformanceFacade.getInstance();
		facade.setBackgroundColor(0xFFFFFFFF);
		facade.setInputDeviceAngle(0);
		facade.newPerformance();
		
		CalibrationAgent calibrationMarks = new CalibrationAgent();
		calibrationMarks.setPerformance(facade.getPerformance());
		
		CustomElement calibrationMarksElement = new CustomElement(calibrationMarks, calibrationMarks.getName(), 0xFF0000);
		
		facade.addElement(calibrationMarksElement, 0, 0, false);

		System.out.println("Starting...");
		
		facade.play();
		
	}

}
