package mustic.scholz.marine.core.plugin;

/**
 * This Business Object is intended to encapsulate all permission information related to a given plug-in. Currently, the three permission
 * flags are: read, write and network.
 * 
 * @author Ricardo Scholz
 */
public class PluginPermissionsBO {

	private boolean read;
	
	private boolean write;
	
	private boolean network;
	
	/**
	 * Builds a new instance of this object.
	 * SECURITY ISSUE: The current version does not use the permission information to allow or deny recourses to plug-ins. This may be 
	 * potentially dangerous. Also, the model based on configurations' file was not deeply evaluated about possible vulnerabilities.
	 * 
	 * @param read			"true" if the plug-in should be able to read from the file system.
	 * @param write			"true" if the plug-in should be able to write to the file system.
	 * @param network		"true" if the plug-in should be able to send/receive information through the network.
	 */
	protected PluginPermissionsBO (boolean read, boolean write, boolean network) {
		this.read = read;
		this.write = write;
		this.network = network;
	}

	/**
	 * @return		<code>true</code> if the plug-in should be able to read from file system.
	 */
	public boolean isRead() {
		return read;
	}

	/**
	 * @return		<code>true</code> if the plug-in should be able to write to file system.
	 */
	public boolean isWrite() {
		return write;
	}

	/**
	 * @return		<code>true</code> if the plug-in should be able to send/receive packages through the network.
	 */
	public boolean isNetwork() {
		return network;
	}
	
}
