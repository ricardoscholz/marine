package mustic.scholz.marine.core.plugin;

import java.util.Properties;

import mustic.scholz.marine.core.performance.element.Element;
import mustic.scholz.marine.core.utils.FilesUtil;

/**
 * This class encapsulates the code to read element plug-in's properties file. The element plug-in's properties file should be placed under
 * the "config" directory (located in the root of the package), and the file name must be "plugin.config". The file has the common Java
 * Properties file format (key=value), and it is expected to contain the following keys, with their default values:
 * name: the plug-in name;
 * id: the plug-in id (usually the main Java package);
 * version: the package version;
 * element.class: the full name of the class which extends {@link Element}, including the package;
 * permissions.filesystem.read: "true" if the plug-in needs permission to read from the file system; "false" otherwise.
 * permissions.filesystem.write: "true" if the plug-in needs permission to write to the file system; "false" otherwise.
 * permissions.network: "true" if the plug-in needs permission to use the network (send/receive packages); "false" otherwise.
 * 
 *  Permission checking is currently not implemented, but future versions of the framework might allow or deny some operations, or warn user
 *  about them, according to the values of these properties.
 * 
 * @author Ricardo Scholz
 *
 */
public class ElementPluginPropertiesReader {

	private static final String PLUGIN_NAME 					= "name";
	private static final String PLUGIN_ID 						= "id";
	private static final String PLUGIN_VERSION					= "version";
	private static final String ELEMENT_CLASS 					= "element.class";
	private static final String FILE_SYSTEM_READ_REQUEST 		= "permissions.filesystem.read";
	private static final String FILE_SYSTEM_WRITE_REQUEST 		= "permissions.filesystem.write";
	private static final String NETWORK_REQUEST 				= "permissions.network";
	
	private Properties prop;
	private String path;
	
	/**
	 * Creates a new instance of the reader, to read a given file.
	 * 
	 * @param configFileUrl		the configurations file to be read by this instance.
	 */
	public ElementPluginPropertiesReader(String configFileUrl) {
		this.path = configFileUrl;
		this.load();
	}
	
	private void load() {
		this.prop = FilesUtil.loadProperties(this.path);
	}
	
	/**
	 * @return		the value of the property {@link #PLUGIN_NAME} ("name").
	 */
	public String getPluginName() {
		return this.prop.getProperty(PLUGIN_NAME);
	}
	
	/**
	 * @return		the value of the property {@link PLUGIN_ID} ("id").
	 */
	public String getPluginID() {
		return this.prop.getProperty(PLUGIN_ID);
	}
	
	/**
	 * @return		the value of the property {@link #PLUGIN_VERSION} ("version").
	 */
	public String getPluginVersion() {
		return this.prop.getProperty(PLUGIN_VERSION);
	}
	
	/**
	 * @return		the value of the property {@link #ELEMENT_CLASS} ("element.class").
	 */
	public String getElementClass() {
		return this.prop.getProperty(ELEMENT_CLASS);
	}
	
	/**
	 * @return		the value of the property {@link #FILE_SYSTEM_READ_REQUEST} ("permissions.filesystem.read").
	 */
	public boolean getFileSystemReadRequest() {
		String str = this.prop.getProperty(FILE_SYSTEM_READ_REQUEST);
		return str != null && str.equalsIgnoreCase("true");
	}
	
	/**
	 * @return		the value of the property {@link #FILE_SYSTEM_WRITE_REQUEST} ("permissions.filesystem.write").
	 */
	public boolean getFileSystemWriteRequest() {
		String str = this.prop.getProperty(FILE_SYSTEM_WRITE_REQUEST);
		return str != null && str.equalsIgnoreCase("true");
	}

	/**
	 * @return		the value of the property {@link #NETWORK_REQUEST} ("permissions.network").
	 */
	public boolean getNetworkRequest() {
		String str = this.prop.getProperty(NETWORK_REQUEST);
		return str != null && str.equalsIgnoreCase("true");
	}
}
