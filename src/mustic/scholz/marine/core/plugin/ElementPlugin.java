package mustic.scholz.marine.core.plugin;

import mustic.scholz.marine.core.performance.element.Element;

/**
 * This class encapsulates a previously loaded plug-in element. The directory in which a plug-in element is loaded is considered to be its 
 * key. Therefore, plug-in files identically named may cause conflicts when imported, as the file name is used to create the plug-in's 
 * internal directory.
 * 
 * @author Ricardo Scholz
 *
 */
public class ElementPlugin {
	
	private Element element;
	private String fileUrl;
	
	/**
	 * The element encapsulated by this plug-in package.
	 * 
	 * @param element	an element encapsulated in the package.
	 * @param fileUrl	the package file path in Marines system directories.
	 */
	public ElementPlugin(Element element, String fileUrl) {
		this.element = element;
		this.fileUrl = fileUrl;
	}
	
	/**
	 * @return	the encapsulated element.
	 */
	public Element getElement() {
		return element;
	}
	
	/**
	 * Modifies the encapsulated element.
	 * 
	 * @param element	the new element to encapsulate.
	 */
	public void setElement(Element element) {
		this.element = element;
	}
	
	/**
	 * @return	the file path of this plug-in.
	 */
	public String getFileUrl() {
		return fileUrl;
	}
	
	/**
	 * Modifies the file path of this plug-in. Calling this method does not actually modifies the file path under the file system. This
	 * method only changes this instance variable. The changes will only be forwarded to the file system if some other code deals with it.
	 *  
	 * @param fileUrl		the new plug-in's file URL.
	 */
	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}
	
	/**
	 * Compares two plug-ins for semantic equality. They are considered to be equal if, and only if, their encapsulated element are equal,
	 * as implemented by {@link Element#equals(Object)}, and, when their file URLs are not null, they are also equal, as implemented by 
	 * {@link String#equalsIgnoreCase(String)}.
	 * 
	 */
	@Override
	public boolean equals(Object otherObj) {
		ElementPlugin other = null;
		if (otherObj instanceof ElementPlugin) {
			other = (ElementPlugin) otherObj;
		}
		if (other != null) {
			if (other.element != null && this.element != null) {
				if (this.fileUrl != null && other.fileUrl != null) {
					return this.element.equals(other.element)
							&& this.fileUrl.equalsIgnoreCase(other.fileUrl);	
				} else {
					return this.element.equals(other.element);
				}
			} else if (this.fileUrl != null && other.fileUrl != null) {
				return this.fileUrl.equalsIgnoreCase(other.fileUrl);
			}
			return false;
		}
		return false;
	}
}
