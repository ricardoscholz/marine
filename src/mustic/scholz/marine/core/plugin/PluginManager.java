package mustic.scholz.marine.core.plugin;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import mustic.scholz.marine.core.ConfigurationManager;
import mustic.scholz.marine.core.classloader.ClassLoaderManager;
import mustic.scholz.marine.core.classloader.PluginURLClassLoader;
import mustic.scholz.marine.core.performance.element.Element;
import mustic.scholz.marine.core.utils.FilesUtil;

/**
 * This Singleton is intended to manage all plug-ins which have been previously loaded into the system. It reads the internal directories
 * looking for all plug-ins imported and load them into memory. It also loads the built-in plug-ins: Calibrator, Skeleton and Joint.
 * 
 * @author Ricardo Scholz
 */
public class PluginManager {
	
	private static final String CONFIG_FILE 		= "plugin.config";
	private static final String CONFIG_DIRECTORY	= "config";
	private static final String LIBS_DIR			= "lib";
	
	private static PluginManager instance;
	
	private ConfigurationManager configuration;
	
	private List<ElementPlugin> plugins;
	
	private PluginManager() {
		this.configuration = ConfigurationManager.getInstance();
		this.plugins = new ArrayList<ElementPlugin>();
		
		//FIXME [Security] Correctly handle security managers for system classes and libraries, and for plug-ins classes.
		//System.setSecurityManager(new PluginSecurityManager(dir));
		this.loadEmbeddedPluginElements();
		this.loadExternalPluginElements();
	}
	
	/**
	 * @return		the Singleton instance.
	 */
	public static PluginManager getInstance() {
		if (instance == null) {
			instance = new PluginManager();
		}
		return instance;
	}
	
	/**
	 * Imports an element plug-in from a given URL. The file is expected to be a JAR file (however developers may prefer to use .MAR 
	 * extension). The file is unpacked into the system plug-ins directory, under a new directory created with the same name of the given 
	 * file. If a conflict occurs (another file with the same name has been previously imported), the method skips the import and prints
	 * a message in the console.
	 * 
	 * @param pluginUrl			the file path of the plug-in package file to import.
	 * @throws IOException		if any file system related exception occurs.
	 */
	public void importElementPlugin (String pluginUrl) throws IOException {
		File pluginJar = new File(pluginUrl);
		File destinyDir = new File(FilesUtil.concatenateURL(configuration.getRootDirectory(), configuration.getPluginsFolder(), 
				FilesUtil.getFileNameWithoutExtension(pluginJar.getName())));
		if (destinyDir.exists()) {
			//TODO [Improvement] Implement appropriate error handling 
			//Adapt to ask about overwriting existing version, or something related to version control.
			System.out.println("[Plugin Manager] Import plug-in element skipped. Plug-in element already imported: " + pluginJar.getName());
			return;
		}
		FilesUtil.unpack(pluginJar.getAbsolutePath(), 
				FilesUtil.concatenateURL(configuration.getRootDirectory(), configuration.getPluginsFolder()));
	}
	
	/**
	 * Reads the plug-in configuration file and encapsulates them into a business object of the type {@link PluginPermissionsBO}. All
	 * "permission.*" properties are read. After reading the information, attempts to delete the temporarily unpacked files from the file 
	 * system.
	 * 
	 * @param pluginJar			the plug-in JAR file to check permissions;
	 * @return					a new instance of a business object, encapsulating all 3 permissions (file system read/write and network).
	 * @throws IOException		if any exception occurs when trying to read the configurations file inside the JAR.
	 * 
	 * @see mustic.scholz.marine.core.plugin.ElementPluginPropertiesReader
	 */
	public PluginPermissionsBO checkPluginRequestedPermissions(File pluginJar) throws IOException {
		String tmpDir = FilesUtil.concatenateURL(configuration.getRootDirectory(), configuration.getTemporaryFolder());
		FilesUtil.unpack(pluginJar.getAbsolutePath(), tmpDir);
		ElementPluginPropertiesReader prop = new ElementPluginPropertiesReader(
				FilesUtil.concatenateURL(tmpDir, FilesUtil.getFileNameWithoutExtension(pluginJar.getAbsolutePath()), 
						CONFIG_FILE));
		
		boolean read = prop.getFileSystemReadRequest();
		boolean write = prop.getFileSystemWriteRequest();
		boolean network = prop.getNetworkRequest();
		PluginPermissionsBO result = new PluginPermissionsBO(read, write, network);
		
		String target = FilesUtil.concatenateURL(tmpDir, pluginJar.getName());
		try {
			FilesUtil.delete(target, false);
		} catch (IOException e) {
			//TODO [Improvement] Implement appropriate error handling
			System.out.println("[File Manager] Could not delete file: " + target);
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * @return		a list of all element plug-ins currently loaded.
	 */
	public List<ElementPlugin> getElementPlugins() {
		return this.plugins;
	}
	
	/**
	 * Attempts to physically delete from the system plug-ins directory a given plug-in.
	 * 
	 * @param plugin			the plug-in to be deleted.
	 * @throws IOException		if any error occurs while accessing or deleting the plug-in.
	 */
	public void deleteElementPlugin(ElementPlugin plugin) throws IOException {
		//TODO [Security] Verify whether it is a URL under plug-ins directory
		if (plugin.getFileUrl().length() > 0) {
			FilesUtil.delete(plugin.getFileUrl(), true);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ElementPlugin loadElementPlugin(String elementBin, String path)
			throws IllegalAccessException, InstantiationException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException,
			SecurityException {
		Class clazz = ClassLoaderManager.getInstance().getPluginsClassLoader().loadClass(path, elementBin, true);

		Class superclass = clazz;
		String superclassName = "";
		
		while (true) {
			superclass = superclass.getSuperclass();
			superclassName = superclass.getName();
			
			if (superclassName.equalsIgnoreCase(Object.class.getName())){
				break;
			}
			
			if (superclassName != null
					&& superclassName.equalsIgnoreCase(Element.class.getName())) {
				//FIXME [Plugin] Verify security. Cannot run empty constructor, possibly due to security issues.
				Constructor constructor = clazz.getConstructor();
				Element element = (Element) constructor.newInstance();
				return new ElementPlugin(element, path);
			}
		}
		throw new InstantiationException("Could not instantiate element: " + path + " " + elementBin);
	}

	private List<File> listConfigurationFiles() {
		List<File> result = new ArrayList<File>();
		String pluginsRootDir = FilesUtil.concatenateURL(this.configuration.getRootDirectory(), this.configuration.getPluginsFolder());
	    File directory = new File(pluginsRootDir);
	    
	    File[] pluginDirs = directory.listFiles();
	    if (pluginDirs != null) {
	    	String configFileStr;
		    for (File pluginDir : pluginDirs) {
		        if (pluginDir.isDirectory()) {
		        	configFileStr = FilesUtil.concatenateURL(pluginDir.getAbsolutePath(), CONFIG_DIRECTORY, CONFIG_FILE);
		        	File configFile = new File(configFileStr);
		        	if (configFile.exists() && configFile.isFile()) {
		        		result.add(configFile);
		        	}
		        }
		    }
	    }
	    return result;
	}
	
	/**
	 * Loads all external plug-in elements to the {@link #plugins} attribute for further reading. All non built-in plug-ins are considered 
	 * external.
	 */
	public void loadExternalPluginElements() {
		List<File> configFiles = this.listConfigurationFiles();
		for (File configFile : configFiles) {
			String pluginDir = null;
			try {
				ElementPluginPropertiesReader prop = new ElementPluginPropertiesReader(configFile.getAbsolutePath());
				
				String configPath = FilesUtil.concatenateURL("", CONFIG_DIRECTORY, CONFIG_FILE);
				pluginDir = configFile.getAbsolutePath().substring(
						0, (int) (configFile.getAbsolutePath().length() - configPath.length()));
				String[] tokens = FilesUtil.splitByFileSeparator(pluginDir);
				
				String rootDir = tokens[tokens.length-1];
				String libDir = FilesUtil.concatenateURL(rootDir, LIBS_DIR);
				
				PluginURLClassLoader classloader = ClassLoaderManager.getInstance().getPluginsClassLoader();
				classloader.addToClassPath(rootDir);
				classloader.addToClassPath(libDir);
				
				ConfigurationManager config = ConfigurationManager.getInstance();
				String libsDirAbsolute = FilesUtil.concatenateURL(
						config.getRootDirectory(), config.getPluginsFolder(), libDir);
				
				File libDirectory = new File(libsDirAbsolute);
				if (libDirectory.exists() && libDirectory.isDirectory()) {
					File[] jars = libDirectory.listFiles();
					for (File jarFile : jars) {
						classloader.addToClassPath(FilesUtil.concatenateURL(libDir, jarFile.getName()));
					}
				}
				ElementPlugin element = this.loadElementPlugin(prop.getElementClass(), pluginDir);
				
				if (!this.plugins.contains(element)) {
					this.plugins.add(element);
				}
			} catch (IllegalAccessException | InstantiationException | ClassNotFoundException | InvocationTargetException |
					NoSuchMethodException | SecurityException e) {
				System.out.println("[Plugin Manager] Could not load external plug-in elements.");
				System.out.println("[Plugin Manager] Plugin Directory: " + pluginDir);
				System.out.println("[Plugin Manager] Exception Type: " + e.getClass().getCanonicalName());
				System.out.println("[Plugin Manager] Exception Message: " + e.getMessage());
				System.out.println("Due to:");
				e.printStackTrace();
			}
		}
	}
	
	private void loadEmbeddedPluginElements() {
//		ElementPlugin calibrator = new ElementPlugin(new CalibrationAgent(), "");
//		ElementPlugin skeleton = new ElementPlugin(new SkeletonAgent(), "");
//		ElementPlugin joint = new ElementPlugin(new JointAgent(), "");
//		this.plugins.add(calibrator);
//		this.plugins.add(skeleton);
//		this.plugins.add(joint);
	}
}
