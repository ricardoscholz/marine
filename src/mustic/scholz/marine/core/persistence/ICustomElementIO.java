package mustic.scholz.marine.core.persistence;

import mustic.scholz.marine.core.performance.element.CustomElement;

/**
 * This interface defines method signatures to save and load custom elements. It assumes that custom elements will be saved in file system, 
 * but it does not bind the actual implementation to any specific technology.
 * 
 * NOTE: Currently, methods in this interface do not throw any non-runtime exceptions. However, this approach will be evaluated in the 
 * future, and future versions of this interface may not be backward compatible.
 * 
 * @author Ricardo Scholz
 *
 */
//FIXME [Exceptions] Deal with specific exceptions for each method
public interface ICustomElementIO {

	/**
	 * Loads a previously saved {@link CustomElement} instance from the file system.
	 * 
	 * @param fileUrl		the URL of the file which contains the custom element to load.
	 * @return				the custom element loaded.
	 */
	public CustomElement loadCustomElement(String fileUrl);
	
	/**
	 * Saves a given {@link CustomElement} instance into the file system.
	 * 
	 * @param element		the custom element to be persisted.
	 * @param fileUrl		the URL of the file which will be used to persist <code>element</code>.
	 */
	public void saveCustomElement(CustomElement element, String fileUrl);
	
}
