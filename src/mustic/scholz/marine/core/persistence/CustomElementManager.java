package mustic.scholz.marine.core.persistence;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mustic.scholz.marine.core.ConfigurationManager;
import mustic.scholz.marine.core.performance.element.CustomElement;
import mustic.scholz.marine.core.performance.element.Element;
import mustic.scholz.marine.core.persistence.binary.BinaryCustomElementIO;
import mustic.scholz.marine.core.utils.FilesUtil;

/**
 * This Singleton is responsible for saving and loading {@link CustomElement} instances to/from files. Internally, it uses a
 * {@link BinaryCustomElementIO} instance for the actual tasks of persisting a custom element and reading a persisted file from file system.
 * 
 */
public class CustomElementManager {
	
	private static CustomElementManager instance;
	
	private ICustomElementIO ioManager;
	
	private CustomElementManager() {
		this.ioManager = new BinaryCustomElementIO();
	}
	
	/**
	 * Instance retrieval method.
	 * 
	 * @return		this Singleton instance.
	 */
	public static CustomElementManager getInstance() {
		if (instance == null) {
			instance = new CustomElementManager();
		}
		return instance;
	}
	
	// Public Methods
	
	/**
	 * Encapsulates a given element into a custom element, and persists it.
	 * 
	 * @param element		the element to be encapsulated and saved.
	 * @param name			the name of the resulting custom element; works as a primary key; therefore, it is not possible to save two 
	 * 						custom elements with the same name.
	 * @param key			the key of the resulting custom element.
	 * @param directory		the original plug-in directory, within system plug-ins' directory.
	 */
	public void saveAsCustomElement(Element element, String name, int key, String directory) {
		CustomElement customElement = new CustomElement(element, name, key, directory);
		this.ioManager.saveCustomElement(customElement, this.getCustomElementFilePath(directory, key));
	}
	
	/**
	 * Persists a list of custom elements, in a single call. This is a shortcut for programmers. Actually, each custom element is persisted
	 * at once, in separate calls to {@link ICustomElementIO#saveCustomElement(CustomElement, String)}.
	 * 
	 * @param customElements		the list of custom elements to be persisted.
	 */
	public void saveCustomElements(List<CustomElement> customElements) {
		for (CustomElement customElement : customElements) {
			this.saveCustomElement(customElement);
		}
	}

	public void saveCustomElement(CustomElement customElement) {
		this.ioManager.saveCustomElement(customElement, this.getCustomElementFilePath(
				customElement.getDirectory(), customElement.getKey()));
	}
	
	/**
	 * Loads a custom element from its name. As custom elements are always saved into a system directory (see configurations file at
	 * "/config/system.config"), it is possible to retrieve a custom element by its name only.
	 *  
	 * @param directory		the directory of the custom element to be loaded.
	 * @param key			the key of the custom element to be loaded.
	 * @return				the custom element with that <code>name</code>; or <code>null</code> if not found.
	 */
	public CustomElement loadCustomElement(String directory, Integer key) {
		return this.ioManager.loadCustomElement(this.getCustomElementFilePath(directory, key));
	}
	
	/**
	 * Loads all custom elements into system default custom elements directory (defined in the configurations file, at 
	 * "/config/system.config").
	 * 
	 * @return			a list containing all system custom elements.
	 */
	public List<CustomElement> loadAllCustomElements() {
		List<File> customElementFiles = FilesUtil.loadFiles(ConfigurationManager.getInstance().getCustomElementsFolder(), 
				FilesUtil.CUSTOM_ELEMENT_EXTENSION);
		List<CustomElement> result = new ArrayList<CustomElement>();
		for (File file : customElementFiles) {
			result.add(this.ioManager.loadCustomElement(file.getAbsolutePath()));
		}
		return result;
	}
	
	/**
	 * Physically deletes a custom element, if it exists in the file system; this operation cannot be undone.
	 * 
	 * @param element		the custom element to be deleted.
	 */
	//FIXME [Exceptions] Properly deal with exceptions.
	public void delete(CustomElement element) {
		if (element != null) {
			String target = this.getCustomElementFilePath(element.getDirectory(), element.getKey());
			try {
				FilesUtil.delete(target, false);
			} catch (IOException e) {
				System.out.println("[Custom Element Manager] Could not delete \"" + target + "\" file.");
				e.printStackTrace();
			}
		}
	}
	
	// Private Methods
	
	private String getCustomElementFilePath(String directory, Integer key) {
		ConfigurationManager config = ConfigurationManager.getInstance();
		return FilesUtil.concatenateURL(config.getRootDirectory(), config.getCustomElementsFolder(), directory) + "_" + key + "." 
				+ FilesUtil.CUSTOM_ELEMENT_EXTENSION;
	}
}
