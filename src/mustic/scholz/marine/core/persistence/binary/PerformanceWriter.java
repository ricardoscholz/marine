package mustic.scholz.marine.core.persistence.binary;

import java.io.DataOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import mustic.scholz.marine.core.performance.ElementTable;
import mustic.scholz.marine.core.performance.Performance;
import mustic.scholz.marine.core.performance.element.CustomElement;
import mustic.scholz.marine.core.performance.element.Element;
import mustic.scholz.marine.core.performance.element.parameter.ColorParameter;
import mustic.scholz.marine.core.performance.element.parameter.FlagParameter;
import mustic.scholz.marine.core.performance.element.parameter.NumberParameter;
import mustic.scholz.marine.core.performance.element.parameter.Parameter;
import mustic.scholz.marine.core.performance.element.parameter.SelectionParameter;
import mustic.scholz.marine.core.performance.element.parameter.TextParameter;
import mustic.scholz.marine.core.performance.element.parameter.TimeParameter;
import mustic.scholz.marine.core.performance.transition.CommandTransition;
import mustic.scholz.marine.core.performance.transition.TimerTransition;
import mustic.scholz.marine.core.performance.transition.Transition;
import mustic.scholz.marine.core.persistence.ParameterSerializer;
import processing.core.PMatrix3D;

/**
 * This class actually implements the {@link Performance} writing protocol. It must write information in the exact order read by 
 * {@link PerformanceReader}. Therefore, if one of these classes change, the other must be updated properly. In addition, objects persisted
 * with different versions of these classes will not be able to be read.
 * 
 * NOTE: Currently, this approach is not dealing with differences in persistence protocol versions; future releases may allow persisted
 * objects to carry a persistence version, so the appropriate reader may be loaded and used after reading the first bytes; such an approach 
 * will probably be incompatible with this implementation. 
 * 
 * @author Ricardo Scholz
 *
 */
class PerformanceWriter implements BinarySymbols {
	
	// Protected Methods
	
	/**
	 * Writes a performance to a {@link DataOutputStream}.  This implementation catches all {@link Exception} into a <code>try-catch</code>
	 * block, printing the stack trace to the default output.
	 * 
	 * @param performance		the performance to be written.
	 * @param dos				the data output stream instance, in which the performance will be written.
	 */
	protected static final void writePerformance(Performance performance, DataOutputStream dos) {
		try {
			if (performance != null) {
				dos.writeInt(performance.getBackgroundColor());
				dos.writeInt(performance.getProjectionScreen());
				dos.writeInt(performance.getPlane().getCameraAxis());
				
				PerformanceWriter.writeDefaultMatrix(performance.getDefaultMatrix(), dos);
				PerformanceWriter.writeElementTable(performance.getElementTable(), dos);
				
				dos.flush();
			} else {
				throw new IllegalArgumentException("Cannot persist a null Performance reference.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Writes a custom element to a {@link DataOutputStream}.  This implementation catches all {@link Exception} into a 
	 * <code>try-catch</code> block, printing the stack trace to the default output.
	 * 
	 * @param element		the custom element to be written.
	 * @param dos			the data output stream instance, in which the custom element will be written.
	 */
	protected static final void writeCustomElement(CustomElement element, DataOutputStream dos) {
		try {
			if (element == null) {
				dos.writeChar(NULL_VALUE);
			} else {
				dos.writeChar(NOT_NULL_VALUE);
				dos.writeUTF(element.getName());
				dos.writeInt(element.getKey());
				dos.writeUTF(element.getDirectory());
				PerformanceWriter.writeElement(element.getElement(), dos);
				
				dos.flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Private Methods
	
	private static final void writeDefaultMatrix(PMatrix3D matrix, DataOutputStream dos) throws Exception {
		if (matrix == null) {
			dos.writeChar(NULL_VALUE);
		} else {
			dos.writeChar(NOT_NULL_VALUE);
			dos.writeFloat(matrix.m00); dos.writeFloat(matrix.m01); dos.writeFloat(matrix.m02); dos.writeFloat(matrix.m03);
			dos.writeFloat(matrix.m10); dos.writeFloat(matrix.m11); dos.writeFloat(matrix.m12); dos.writeFloat(matrix.m13);
			dos.writeFloat(matrix.m20); dos.writeFloat(matrix.m21); dos.writeFloat(matrix.m32); dos.writeFloat(matrix.m23);
			dos.writeFloat(matrix.m30); dos.writeFloat(matrix.m31); dos.writeFloat(matrix.m32); dos.writeFloat(matrix.m33);
		}
	}
	
	private static final void writeElementTable(ElementTable table, DataOutputStream dos) throws Exception {
		if (table == null) {
			dos.writeChar(NULL_VALUE);
		} else {
			dos.writeChar(NOT_NULL_VALUE);
			int maxTracks = table.getMaxTracks();
			int maxElementsPerTrack = table.getMaxElementsPerTrack();
			
			dos.writeInt(maxTracks);
			dos.writeInt(maxElementsPerTrack);
			dos.writeInt(table.getState());
			dos.writeInt(table.getCurrentPosition());
			
			CustomElement[][] elements = table.getElements();
			Transition[] transitions = table.getTransitions();
			
			if (elements == null) {
				dos.writeChar(NULL_VALUE);
			} else {
				dos.writeChar(NOT_NULL_VALUE);
				for (int i = 0; i < maxTracks; i++) {
					for (int j = 0; j < maxElementsPerTrack; j++) {
						PerformanceWriter.writeCustomElement(elements[i][j], dos);
					}
				}
			}
			
			if (transitions == null) {
				dos.writeChar(NULL_VALUE);
			} else {
				dos.writeChar(NOT_NULL_VALUE);
				for (Transition transition : transitions) {
					PerformanceWriter.writeTransition(transition, dos);
				}
			}
		}
	}
	
	private static final void writeElement(Element element, DataOutputStream dos) throws Exception {
		if (element == null) {
			dos.writeChar(NULL_VALUE);
		} else {
			dos.writeChar(NOT_NULL_VALUE);
			dos.writeUTF(element.getClass().getName());
			
			if (element.getDescription() == null) {
				dos.writeUTF("");
			} else {
				dos.writeUTF(element.getDescription());
			}
			if (element.getVersion() == null) {
				dos.writeUTF("");
			} else {
				dos.writeUTF(element.getVersion());
			}
			if (element.getAuthor() == null) {
				dos.writeUTF("");
			} else {
				dos.writeUTF(element.getAuthor());	
			}
			if (element.getName() == null) {
				dos.writeUTF("");
			} else {
				dos.writeUTF(element.getName());	
			}
			
			HashMap<Integer, Parameter> parameters = element.getParameters();
			if (parameters == null) {
				dos.writeChar(NULL_VALUE);
			} else {
				dos.writeChar(NOT_NULL_VALUE);
				Set<Integer> keys = parameters.keySet();
				dos.writeInt(keys.size());
				Parameter param = null;
				for (Integer key : keys) {
					param = parameters.get(key);
					if (param == null) {
						dos.writeChar(NULL_VALUE);
					} else {
						dos.writeChar(NOT_NULL_VALUE);
						dos.writeInt(key);
						PerformanceWriter.writeParameter(param, dos);
					}
				}
			}
		}
	}
	
	private static final void writeParameter(Parameter parameter, DataOutputStream dos) throws Exception {
		
		dos.writeUTF(parameter.getClass().getName());
		
		if (parameter.getInput() == null && parameter.getInput().getId() != null) {
			dos.writeChar(NULL_VALUE);
		} else {
			dos.writeChar(NOT_NULL_VALUE);
			dos.writeInt(parameter.getInput().getId());
		}
		
		if (parameter.getId() == null) {
			dos.writeChar(NULL_VALUE);
		} else {
			dos.writeChar(NOT_NULL_VALUE);
			dos.writeInt(parameter.getId());
		}
		
		if (parameter.getName() != null) {
			dos.writeUTF(parameter.getName());
		} else {
			dos.writeUTF("");
		}
		if (parameter.getDescription() != null) {
			dos.writeUTF(parameter.getDescription());
		} else {
			dos.writeUTF("");
		}
		
		if (parameter instanceof ColorParameter) {
			PerformanceWriter.writeColorParameter((ColorParameter) parameter, dos);
		} else if (parameter instanceof FlagParameter) {
			PerformanceWriter.writeFlagParameter((FlagParameter) parameter, dos);
		} else if (parameter instanceof NumberParameter) {
			PerformanceWriter.writeNumberParameter((NumberParameter) parameter, dos);
		} else if (parameter instanceof SelectionParameter) {
			PerformanceWriter.writeSelectionParameter((SelectionParameter<?>) parameter, dos);
		} else if (parameter instanceof TextParameter) {
			PerformanceWriter.writeTextParameter((TextParameter) parameter, dos);
		} else if (parameter instanceof TimeParameter) {
			PerformanceWriter.writeTimeParameter((TimeParameter) parameter, dos);
		} else {
			throw new Exception("Invalid parameter type found.");
		}
	}
	
	private static final void writeColorParameter(ColorParameter parameter, DataOutputStream dos) throws Exception {
		dos.writeInt(parameter.getValue());
		dos.writeInt(parameter.getDefaultValue());
	}
	
	private static final void writeFlagParameter(FlagParameter parameter, DataOutputStream dos) throws Exception {
		dos.writeBoolean(parameter.getValue());
		dos.writeBoolean(parameter.getDefaultValue());
	}
	
	private static final void writeNumberParameter(NumberParameter parameter, DataOutputStream dos) throws Exception {
		dos.writeFloat(parameter.getValue());
		dos.writeFloat(parameter.getDefaultValue());
		dos.writeFloat(parameter.getMinValue());
		dos.writeFloat(parameter.getMaxValue());
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static final void writeSelectionParameter(SelectionParameter<?> parameter, DataOutputStream dos) throws Exception {
		ParameterSerializer serializer = parameter.getSerializer();
		if (serializer != null) {
			dos.writeChar(NOT_NULL_VALUE);
			dos.writeUTF(serializer.getClass().getName());
			if (parameter.getListLabel() != null) {
				dos.writeChar(NOT_NULL_VALUE);
				dos.writeUTF(parameter.getListLabel());
			} else {
				dos.writeChar(NULL_VALUE);
			}
			List<?> listRange = parameter.getList();
			if (listRange != null) {
				dos.writeChar(NOT_NULL_VALUE);
				dos.writeInt(listRange.size());
				for (Object obj : listRange) {
					serializer.writeObject(obj, dos);
				}
			} else {
				dos.writeChar(NULL_VALUE);
			}
			if (parameter.getValue() != null) {
				dos.writeChar(NOT_NULL_VALUE);
				serializer.writeObject(parameter.getValue(), dos);
			} else {
				dos.writeChar(NULL_VALUE);
			}
			if (parameter.getDefaultValue() != null) {
				dos.writeChar(NOT_NULL_VALUE);
				serializer.writeObject(parameter.getDefaultValue(), dos);
			} else {
				dos.writeChar(NULL_VALUE);
			}
			
		} else {
			dos.writeChar(NULL_VALUE);
		}
	}
	
	private static final void writeTextParameter(TextParameter parameter, DataOutputStream dos) throws Exception {
		if (parameter.getValue() != null) {
			dos.writeUTF(parameter.getValue());
		} else {
			dos.writeUTF("");
		}
		if (parameter.getDefaultValue() != null) {
			dos.writeUTF(parameter.getDefaultValue());
		} else {
			dos.writeUTF("");
		}
	}
	
	private static final void writeTimeParameter(TimeParameter parameter, DataOutputStream dos) throws Exception {
		dos.writeLong(parameter.getValue());
		dos.writeLong(parameter.getDefaultValue());
	}
	
	private static final void writeTransition(Transition transition, DataOutputStream dos) throws Exception {
		if (transition == null) {
			dos.writeChar(NULL_VALUE);
		} else {
			dos.writeChar(NOT_NULL_VALUE);
			dos.writeUTF(transition.getClass().getName());
			if (transition.getType() != null) {
				dos.writeChar(NOT_NULL_VALUE);
				dos.writeInt(transition.getType().getIndex());
			} else {
				dos.writeChar(NULL_VALUE);
			}
			
			if (transition instanceof CommandTransition) {
				PerformanceWriter.writeCommandTransition((CommandTransition) transition, dos);
			} else if (transition instanceof TimerTransition) {
				PerformanceWriter.writeTimerTransition((TimerTransition) transition, dos);
			}
		}
	}
	
	private static final void writeCommandTransition(CommandTransition transition, DataOutputStream dos) throws Exception {
		//nothing to save.
	}
	
	private static final void writeTimerTransition(TimerTransition transition, DataOutputStream dos) throws Exception {
		dos.writeLong(transition.getOriginalDuration());
	}
}
