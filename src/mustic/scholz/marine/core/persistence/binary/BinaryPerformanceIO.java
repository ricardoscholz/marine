package mustic.scholz.marine.core.persistence.binary;

import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import mustic.scholz.marine.core.performance.Performance;
import mustic.scholz.marine.core.persistence.IPerformanceIO;

/**
 * This class is an implementation of {@link IPerformanceIO}. It uses {@link FileOutputStream} and {@link FileInputStream} to read and
 * write in the file system. Therefore, it implements a customised {@link Performance} persistence protocol, in which every operation 
 * executed when writing a performance into a file must be executed when reading a performance from a file.
 *   
 * @author Ricardo Scholz
 *
 */
public class BinaryPerformanceIO implements IPerformanceIO {
	
	/**
	 * Reads a {@link Performance} from the file system.
	 * 
	 * @param performance			the reference in which the read performance will be saved.
	 * @param fileUrl				the URL of the file which contains the performance to be read.
	 * @see IPerformanceIO#openPerformance(Performance, String)
	 */
	@Override
	public void openPerformance(Performance performance, String fileUrl) {
		DataInputStream dis = null;
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(fileUrl);
			dis = new DataInputStream(fis);
			PerformanceReader.readPerformance(performance, dis);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closeStream(fis);
			this.closeStream(dis);
		}
	}

	/**
	 * Writes a {@link Performance} to the file system.
	 * 
	 * @param performance			the performance to be persisted.
	 * @param fileUrl				the URL of the file in which <code>performance</code> will be persisted.
	 * @see IPerformanceIO#savePerformance(Performance, String)
	 */
	@Override
	public void savePerformance(Performance performance, String fileUrl) {
		FileOutputStream fos = null;
		DataOutputStream dos = null;
		try {
			fos = new FileOutputStream(fileUrl);
			dos = new DataOutputStream(fos);
			PerformanceWriter.writePerformance(performance, dos);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closeStream(fos);
			this.closeStream(dos);
		}
	}
	
	private void closeStream(Closeable stream) {
		if (stream != null) {
			try {
				stream.close();
			} catch (Throwable t) {
				t.printStackTrace();
			}
		}
	}

}
