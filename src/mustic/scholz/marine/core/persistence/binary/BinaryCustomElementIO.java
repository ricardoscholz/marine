package mustic.scholz.marine.core.persistence.binary;

import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import mustic.scholz.marine.core.performance.element.CustomElement;
import mustic.scholz.marine.core.persistence.ICustomElementIO;

/**
 * This class is an implementation of {@link ICustomElementIO}. It uses {@link FileOutputStream} and {@link FileInputStream} to read and
 * write in the file system. Therefore, it implements a customised {@link CustomElement} persistence protocol, in which every operation 
 * executed when writing a custom element into a file must be executed when reading a custom element from a file.  
 * 
 * @author Ricardo Scholz
 *
 */
public class BinaryCustomElementIO implements ICustomElementIO {

	/**
	 * Reads a previously saved {@link CustomElement} from the file system.
	 * 
	 * @param fileUrl		the URL of the custom element file.
	 * @return				the custom element read, or <code>null</code> if any error occurs.
	 */
	@Override
	public CustomElement loadCustomElement(String fileUrl) {
		DataInputStream dis = null;
		FileInputStream fis = null;
		CustomElement result = null;
		try {
			fis = new FileInputStream(fileUrl);
			dis = new DataInputStream(fis);
			result = PerformanceReader.readCustomElement(dis);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closeStream(fis);
			this.closeStream(dis);
		}
		return result;
	}

	/**
	 * Writes a {@link CustomElement} to the file system.
	 * 
	 * @param element		the custom element to be persisted.
	 * @param fileUrl		the URL of the file in which the custom element will be saved.
	 */
	@Override
	public void saveCustomElement(CustomElement element, String fileUrl) {
		FileOutputStream fos = null;
		DataOutputStream dos = null;
		try {
			fos = new FileOutputStream(fileUrl);
			dos = new DataOutputStream(fos);
			PerformanceWriter.writeCustomElement(element, dos);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.closeStream(fos);
			this.closeStream(dos);
		}
	}

	private void closeStream(Closeable stream) {
		if (stream != null) {
			try {
				stream.close();
			} catch (Throwable t) {
				t.printStackTrace();
			}
		}
	}
}
