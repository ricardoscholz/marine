package mustic.scholz.marine.core.persistence.binary;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mustic.scholz.marine.core.entity.JointEnum;
import mustic.scholz.marine.core.performance.element.parameter.SelectionParameter;
import mustic.scholz.marine.core.persistence.ParameterSerializer;

/**
 * This class implements a serialization approach for {@link JointEnum} instances. This implementation allows the use of {@link JointEnum} 
 * into {@link SelectionParameter}, with persistence capabilities.
 * 
 * @author Ricardo Scholz
 *
 */
public class JointEnumSerializer implements ParameterSerializer<JointEnum, DataInputStream, DataOutputStream> {
	
	/**
	 * Reads a {@link JointEnum} from a {@link DataInputStream}. Only the index is saved, as a primitive type <code>int</code>. Then,
	 * {@link JointEnum#getByIndex(int)} is used to recover the correct instance. Internal <code>null</code> value control is performed.
	 * 
	 * @param dis			the data input stream that contains the joint information.
	 * @return				the retrieved joint instance; or <code>null</code> if a null instance has been saved.
	 */
	@Override
	public JointEnum readObject(DataInputStream dis) throws IOException {
		JointEnum result = null;
		char nullFlag = dis.readChar();
		if (nullFlag != ParameterSerializer.NULL_VALUE) {
			result = JointEnum.getByIndex(dis.readInt());
		}
		return result;
	}

	/**
	 * Writes a {@link JointEnum} into a {@link DataOutputStream}. Only the index is saved, as a primitive type <code>int</code>. Internal
	 * <code>null</code> value control is performed.
	 * 
	 * @param object		the joint to write.
	 * @param dos			the data output stream in which the joint information will be written.
	 */
	@Override
	public void writeObject(JointEnum object, DataOutputStream dos) throws IOException {
		if (object != null) {
			dos.writeChar(ParameterSerializer.NOT_NULL_VALUE);
			dos.writeInt(object.getIndex());
		} else {
			dos.writeChar(ParameterSerializer.NULL_VALUE);
		}
	}

}
