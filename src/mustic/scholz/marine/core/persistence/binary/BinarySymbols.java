package mustic.scholz.marine.core.persistence.binary;

/**
 * The binary persistence protocol writes a character to identify if a given object is null or not, before persisting a complex object.
 * This interface defines such symbols, so that readers and writers can use the same symbols.
 * 
 * @author Ricardo Scholz
 *
 */
public interface BinarySymbols {

	/**
	 * Symbol used to identify that the next object is <code>null</code>.
	 */
	public static final char NULL_VALUE = '#';
	
	/**
	 * Symbol used to identify that the next object is not <code>null</code>.
	 */
	public static final char NOT_NULL_VALUE = '$';
	
}
