package mustic.scholz.marine.core.persistence.binary;

import java.io.DataInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mustic.scholz.marine.core.performance.ElementTable;
import mustic.scholz.marine.core.performance.Performance;
import mustic.scholz.marine.core.performance.ProjectionPlane;
import mustic.scholz.marine.core.performance.element.CustomElement;
import mustic.scholz.marine.core.performance.element.Element;
import mustic.scholz.marine.core.performance.element.InputTypeEnum;
import mustic.scholz.marine.core.performance.element.parameter.ColorParameter;
import mustic.scholz.marine.core.performance.element.parameter.FlagParameter;
import mustic.scholz.marine.core.performance.element.parameter.NumberParameter;
import mustic.scholz.marine.core.performance.element.parameter.Parameter;
import mustic.scholz.marine.core.performance.element.parameter.SelectionParameter;
import mustic.scholz.marine.core.performance.element.parameter.TextParameter;
import mustic.scholz.marine.core.performance.element.parameter.TimeParameter;
import mustic.scholz.marine.core.performance.transition.CommandTransition;
import mustic.scholz.marine.core.performance.transition.TimerTransition;
import mustic.scholz.marine.core.performance.transition.Transition;
import mustic.scholz.marine.core.performance.transition.TransitionTypeEnum;
import mustic.scholz.marine.core.persistence.ParameterSerializer;
import mustic.scholz.marine.core.utils.ReflectionsUtil;
import processing.core.PMatrix3D;

/**
 * This class actually implements the {@link Performance} reading protocol. It must read information in the exact order written by 
 * {@link PerformanceWriter}. Therefore, if one of these classes change, the other must be updated properly. In addition, objects persisted
 * with different versions of these classes will not be able to be read.
 * 
 * NOTE: Currently, this approach is not dealing with differences in persistence protocol versions; future releases may allow persisted
 * objects to carry a persistence version, so the appropriate reader may be loaded and used after reading the first bytes; such an approach 
 * will probably be incompatible with this implementation. 
 * 
 * @author Ricardo Scholz
 *
 */
class PerformanceReader implements BinarySymbols {

	// Protected Methods
	
	/**
	 * Reads a performance from a {@link DataInputStream}. This implementation catches all {@link Exception} into a <code>try-catch</code>
	 * block, printing the stack trace to the default output.
	 * 
	 * @param performance		the performance to be read.
	 * @param dis				the data input stream instance, from which the performance will be read.
	 */
	protected static final void readPerformance(Performance performance, DataInputStream dis) {
		try {
			performance.setBackgroundColor(dis.readInt());
			performance.setProjectionScreen(dis.readInt());
			performance.setPlane(ProjectionPlane.getByCameraAxis(dis.readInt()));
			
			performance.setDefaultMatrix(PerformanceReader.readDefaultMatrix(dis));
			performance.setElementTable(PerformanceReader.readElementTable(performance, dis));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Reads a custom element from a {@link DataInputStream}.  This implementation catches all {@link Exception} into a 
	 * <code>try-catch</code> block, printing the stack trace to the default output.
	 * 
	 * @param element		the custom element to be read.
	 * @param dis			the data input stream instance, from which the custom element will be read.
	 */
	public static final CustomElement readCustomElement(DataInputStream dis) throws Exception {
		CustomElement result = null;
		try {
			char nullFlag = dis.readChar();
			if (nullFlag == NULL_VALUE) {
				return null;
			} else {
				String name = dis.readUTF();
				int key = dis.readInt();
				String directory = dis.readUTF();
				Element element = PerformanceReader.readElement(null, dis);
				result = new CustomElement(element, name, key, directory);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	// Private Methods
	
	private static final PMatrix3D readDefaultMatrix(DataInputStream dis) throws Exception {
		PMatrix3D result = null;
		char nullFlag = dis.readChar();
		if (nullFlag == NULL_VALUE) {
			return null;
		} else {
			result = new PMatrix3D(dis.readFloat(), dis.readFloat(), dis.readFloat(), dis.readFloat(), 
					dis.readFloat(), dis.readFloat(), dis.readFloat(), dis.readFloat(), 
					dis.readFloat(), dis.readFloat(), dis.readFloat(), dis.readFloat(),
					dis.readFloat(), dis.readFloat(), dis.readFloat(), dis.readFloat());
		}
		return result;
	}
	
	private static final ElementTable readElementTable(Performance performance, DataInputStream dis) 
			throws Exception {
		ElementTable result = null;
		char nullFlag = dis.readChar();
		if (nullFlag == NULL_VALUE) {
			return null;
		} else {
			int maxTracks = dis.readInt();
			int maxElementsPerTrack = dis.readInt();
			result = new ElementTable(maxTracks, maxElementsPerTrack);
			
			result.setState(dis.readInt());
			result.setCurrentPosition(dis.readInt());

			nullFlag = dis.readChar();
			
			if (nullFlag != NULL_VALUE) {
				CustomElement element = null;
				for (int i = 0; i < maxTracks; i++) {
					for (int j = 0; j < maxElementsPerTrack; j++) {
						element = PerformanceReader.readCustomElement(dis);
						if (element != null) {
							result.addElement(element, i, j, false);
						}
					}
				}
			}
			
			nullFlag = dis.readChar();
			if (nullFlag != NULL_VALUE) {
				Transition transition = null;
				for (int i = 0; i < maxElementsPerTrack; i++) {
					transition = PerformanceReader.readTransition(dis);
					if (transition != null) {
						result.addTransition(transition, i, false);
					}
				}
			}
		}
		return result;
	}
	
	private static final Element readElement(Performance performance, DataInputStream dis) throws Exception {
		char nullFlag = dis.readChar();
		Element result = null;
		
		if (nullFlag != NULL_VALUE) {
			String className = dis.readUTF();
			
			result = (Element) ReflectionsUtil.createInstance(className);	
			
			result.setDescription(dis.readUTF());
			result.setVersion(dis.readUTF());
			result.setAuthor(dis.readUTF());
			result.setName(dis.readUTF());
			
			HashMap<Integer, Parameter> parameters = null;
			nullFlag = dis.readChar();
			if (nullFlag != NULL_VALUE) {
				parameters = new HashMap<Integer, Parameter>();
				int numberOfParameters = dis.readInt();
				for (int i = 0; i < numberOfParameters; i++) {
					Parameter param = null;
					nullFlag = dis.readChar();
					if (nullFlag != NULL_VALUE) {
						int key = dis.readInt();
						param = PerformanceReader.readParameter(dis);
						parameters.put(key, param);
					}
				}
			}
			result.setParameters(parameters);
			result.setPerformance(performance);
		}
		return result;
	}
	
	private static final Parameter readParameter(DataInputStream dis) throws Exception {
		
		String className = dis.readUTF();
		Parameter result = (Parameter) ReflectionsUtil.createInstance(className);
		
		char nullFlag = dis.readChar();
		if (nullFlag != NULL_VALUE) {
			result.setInput(InputTypeEnum.getById(dis.readInt()));
		}
		nullFlag = dis.readChar();
		if (nullFlag != NULL_VALUE) {
			result.setId(dis.readInt());
		}
		
		result.setName(dis.readUTF());
		result.setDescription(dis.readUTF());
		
		if (result instanceof ColorParameter) {
			PerformanceReader.readColorParameter((ColorParameter) result, dis);
		} else if (result instanceof FlagParameter) {
			PerformanceReader.readFlagParameter((FlagParameter) result, dis);
		} else if (result instanceof NumberParameter) {
			PerformanceReader.readNumberParameter((NumberParameter) result, dis);
		} else if (result instanceof SelectionParameter) {
			PerformanceReader.readSelectionParameter((SelectionParameter<?>) result, dis);
		} else if (result instanceof TextParameter) {
			PerformanceReader.readTextParameter((TextParameter) result, dis);
		} else if (result instanceof TimeParameter) {
			PerformanceReader.readTimeParameter((TimeParameter) result, dis);
		} else {
			throw new Exception("Invalid parameter type found.");
		}
		return result;
	}
	
	private static final void readColorParameter(ColorParameter parameter, DataInputStream dis) throws Exception {
		parameter.setValue(dis.readInt());
		parameter.setDefaultValue(dis.readInt());
	}
	
	private static final void readFlagParameter(FlagParameter parameter, DataInputStream dis) throws Exception {
		parameter.setValue(dis.readBoolean());
		parameter.setDefaultValue(dis.readBoolean());
	}
	
	private static final void readNumberParameter(NumberParameter parameter, DataInputStream dis) throws Exception {
		parameter.setValue(dis.readFloat());
		parameter.setDefaultValue(dis.readFloat());
		parameter.setMinValue(dis.readFloat());
		parameter.setMaxValue(dis.readFloat());
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static final void readSelectionParameter(SelectionParameter parameter, DataInputStream dis) throws Exception {
		char nullFlag = dis.readChar();
		if (nullFlag != NULL_VALUE) {
			String className = dis.readUTF();
			ParameterSerializer serializer = (ParameterSerializer) ReflectionsUtil.createInstance(className);
			parameter.setSerializer(serializer);
			nullFlag = dis.readChar();
			if (nullFlag != NULL_VALUE) {
				parameter.setListLabel(dis.readUTF());
			}
			nullFlag = dis.readChar();
			if (nullFlag != NULL_VALUE) {
				int listSize = dis.readInt();
				List values = new ArrayList();
				for (int i = 0; i < listSize; i++) {
					values.add(serializer.readObject(dis));
				}
				parameter.setList(values);
			}
			nullFlag = dis.readChar();
			if (nullFlag != NULL_VALUE) {
				parameter.setValue(serializer.readObject(dis));
			}
			nullFlag = dis.readChar();
			if (nullFlag != NULL_VALUE) {
				parameter.setDefaultValue(serializer.readObject(dis));
			}
		}
	}
	
	private static final void readTextParameter(TextParameter parameter, DataInputStream dis) throws Exception {
		parameter.setValue(dis.readUTF());
		parameter.setDefaultValue(dis.readUTF());
	}
	
	private static final void readTimeParameter(TimeParameter parameter, DataInputStream dis) throws Exception {
		parameter.setValue(dis.readLong());
		parameter.setDefaultValue(dis.readLong());
	}
	
	private static final Transition readTransition(DataInputStream dis) throws Exception {
		char nullFlag = dis.readChar();
		Transition result = null;
		if (nullFlag != NULL_VALUE) {
			String className = dis.readUTF();
			result = (Transition) ReflectionsUtil.createInstance(className);
			nullFlag = dis.readChar();
			if (nullFlag != NULL_VALUE) {
				int typeId = dis.readInt();
				result.setType(TransitionTypeEnum.getByIndex(typeId));
			}
			if (result instanceof CommandTransition) {
				PerformanceReader.readCommandTransition((CommandTransition) result, dis);
			} else if (result instanceof TimerTransition) {
				PerformanceReader.readTimerTransition((TimerTransition) result, dis);
			}
		}
		return result;
	}
	
	private static final void readCommandTransition(CommandTransition transition, DataInputStream dis) throws Exception {
		//nothing to read.
	}
	
	private static final void readTimerTransition(TimerTransition transition, DataInputStream dis) throws Exception {
		long duration = dis.readLong();
		transition.setDuration(duration);
		transition.setOriginalDuration(duration);
	}
}
