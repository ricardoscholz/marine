package mustic.scholz.marine.core.persistence.binary;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import mustic.scholz.marine.core.entity.JointEnum;
import mustic.scholz.marine.core.feature.Feature;
import mustic.scholz.marine.core.persistence.ParameterSerializer;

/**
 * This class implements a serialization approach for {@link Feature} instances. This implementation allows the use of {@link Feature}s 
 * into {@link mustic.scholz.marine.core.performance.element.parameter.SelectionParameter}, with persistence capabilities.
 * 
 * @author Ricardo Scholz
 *
 */
public class FeatureSerializer implements ParameterSerializer<Feature, DataInputStream, DataOutputStream> {
	
	/**
	 * Reads a {@link Feature} from a {@link DataInputStream}. Internal <code>null</code> value control is performed.
	 * 
	 * @param dis			the data input stream that contains the feature information.
	 * @return				the retrieved feature; or <code>null</code> if a null instance has been saved.
	 */
	@Override
	public Feature readObject(DataInputStream dis) throws IOException {
		Feature result = null;
		char nullFlag = dis.readChar();
		if (nullFlag != ParameterSerializer.NULL_VALUE) {
			result = new Feature();
			nullFlag = dis.readChar();
			if (nullFlag != ParameterSerializer.NULL_VALUE) {
				result.setId(dis.readUTF());
			}
			nullFlag = dis.readChar();
			if (nullFlag != ParameterSerializer.NULL_VALUE) {
				result.setName(dis.readUTF());
			}
			nullFlag = dis.readChar();
			if (nullFlag != ParameterSerializer.NULL_VALUE) {
				result.setValue(dis.readFloat());
			}
			nullFlag = dis.readChar();
			if (nullFlag != ParameterSerializer.NULL_VALUE) {
				result.setMinValue(dis.readFloat());
			}
			nullFlag = dis.readChar();
			if (nullFlag != ParameterSerializer.NULL_VALUE) {
				result.setMaxValue(dis.readFloat());
			}
			nullFlag = dis.readChar();
			if (nullFlag != ParameterSerializer.NULL_VALUE) {
				result.setJoint(JointEnum.getByIndex(dis.readInt()));
			}
		}
		return result;
	}

	/**
	 * Writes a {@link Feature} into a {@link DataOutputStream}. Internal <code>null</code> value control is performed.
	 * 
	 * @param object		the feature to write.
	 * @param dos			the data output stream in which the feature information will be written.
	 */
	@Override
	public void writeObject(Feature object, DataOutputStream dos) throws IOException {
		if (object != null && object instanceof Feature) {
			dos.writeChar(ParameterSerializer.NOT_NULL_VALUE);
			Feature feature = (Feature) object;
			if (feature.getId() != null) {
				dos.writeChar(ParameterSerializer.NOT_NULL_VALUE);
				dos.writeUTF(feature.getId());
			} else {
				dos.writeChar(ParameterSerializer.NULL_VALUE);
			}
			if (feature.getName() != null) {
				dos.writeChar(ParameterSerializer.NOT_NULL_VALUE);
				dos.writeUTF(feature.getName());
			} else {
				dos.writeChar(ParameterSerializer.NULL_VALUE);
			}
			if (feature.getValue() != null) {
				dos.writeChar(ParameterSerializer.NOT_NULL_VALUE);
				dos.writeFloat(feature.getValue());
			} else {
				dos.writeChar(ParameterSerializer.NULL_VALUE);
			}
			if (feature.getMinValue() != null) {
				dos.writeChar(ParameterSerializer.NOT_NULL_VALUE);
				dos.writeFloat(feature.getMinValue());
			} else {
				dos.writeChar(ParameterSerializer.NULL_VALUE);
			}
			if (feature.getMaxValue() != null) {
				dos.writeChar(ParameterSerializer.NOT_NULL_VALUE);
				dos.writeFloat(feature.getMaxValue());
			} else {
				dos.writeChar(ParameterSerializer.NULL_VALUE);
			}
			if (feature.getJoint() != null) {
				dos.writeChar(ParameterSerializer.NOT_NULL_VALUE);
				dos.writeInt(feature.getJoint().getIndex());
			} else {
				dos.writeChar(ParameterSerializer.NULL_VALUE);
			}
		} else {
			dos.writeChar(ParameterSerializer.NULL_VALUE);
		}
	}

}
