package mustic.scholz.marine.core.persistence;

import mustic.scholz.marine.core.performance.Performance;

/**
 * This interface defines method signatures to save performances and load performances. It assumes that performances will be saved in file
 * system, but it does not bind the actual implementation to any specific technology.
 * 
 * NOTE: Currently, methods in this interface do not throw any non-runtime exceptions. However, this approach will be evaluated in the 
 * future, and future versions of this interface may not be backward compatible.
 * 
 * @author Ricardo Scholz
 *
 */
//FIXME [Exceptions] Deal with specific exceptions for each method
public interface IPerformanceIO {

	/**
	 * Loads a previously saved {@link Performance} instance from the file system.
	 * 
	 * @param performance		the performance reference which will point to the loaded performance; this method could return the read
	 * 							performance reference, however, the performance reference must have some attributes previously set when
	 * 							loading the rest of the information from the file system; therefore, passing the performance reference
	 * 							as a method attribute seemed more appropriate in this case. 
	 * @param fileUrl			the URL of the file which contains the performance to load.
	 */
	public void openPerformance(Performance performance, String fileUrl);
	
	/**
	 * Saves a given {@link Performance} instance to the file system.
	 * 
	 * @param performance		the performance to be persisted.
	 * @param fileUrl			the URL of the file in which the performance will be persisted.
	 */
	public void savePerformance(Performance performance, String fileUrl);
	
}
