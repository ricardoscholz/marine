package mustic.scholz.marine.core.persistence;

import mustic.scholz.marine.core.performance.Performance;
import mustic.scholz.marine.core.persistence.binary.BinaryPerformanceIO;

/**
 * This Singleton is responsible for saving and loading {@link Performance} instances to/from files. Internally, it uses a
 * {@link BinaryPerformanceIO} instance for the actual tasks of persisting a performance and reading a persisted file from file system.
 * 
 */
public class PerformanceManager {

	private static PerformanceManager instance;
	
	private IPerformanceIO ioManager;
	
	private PerformanceManager() {
		this.ioManager = new BinaryPerformanceIO();
	}
	
	/**
	 * Instance retrieval method.
	 * 
	 * @return		this Singleton instance.
	 */
	public static PerformanceManager getInstance() {
		if (instance == null) {
			instance = new PerformanceManager();
		}
		return instance;
	}
	
	/**
	 * Loads a persisted performance from a <code>fileUrl</code> into a <code>performance</code> instance.
	 *  
	 * @param performance		the {@link Performance} instance to which the open performance will be loaded.
	 * @param fileUrl			the URL of the file containing the performance to load.
	 */
	public void openPerformance(Performance performance, String fileUrl) {
		this.ioManager.openPerformance(performance, fileUrl);
	}
	
	/**
	 * Persists a given <code>performance</code> into the file system, on a given file URL (<code>fileUrl</code>).
	 * 
	 * @param performance		the {@link Performance} instance to persist.
	 * @param fileUrl			the URL of the file in which the performance will be persisted.
	 */
	public void savePerformance(Performance performance, String fileUrl) {
		this.ioManager.savePerformance(performance, fileUrl);
	}
	
}
