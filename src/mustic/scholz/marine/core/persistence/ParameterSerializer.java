package mustic.scholz.marine.core.persistence;

import java.io.IOException;
import java.io.Serializable;

import mustic.scholz.marine.core.performance.element.parameter.SelectionParameter;

/**
 * This interface defines the methods which must be implemented by any parameter serializer. Parameter serializers are used by the
 * {@link SelectionParameter} instances, when it is necessary to persist them.
 * 
 * @author Ricardo Scholz
 *
 * @param <T>		the {@link SelectionParameter} generic type.
 * @param <I>		the actual serializer class input stream reader class.
 * @param <O>		the actual serializer class output stream writer class.
 */
public interface ParameterSerializer<T, I, O> extends Serializable {

	/**
	 * A code for null values.
	 */
	public static final char NULL_VALUE = '#';
	
	/**
	 * A code for not null values.
	 */
	public static final char NOT_NULL_VALUE = '$';
	
	/**
	 * Abstract method, which actual implementation is expected to read an object of type 'T', using an input reader of type 'I', which
	 * contains the 'T' object information.
	 * 
	 * @param inputReader		the input reader used to read the object.
	 * @return					the object read from the <code>inputReader</code>.
	 * @throws IOException		if any problem happens when attempting to read the object.
	 */
	public abstract T readObject(I inputReader) throws IOException;
	
	/**
	 * Abstract method, which actual implementation is expected to write an object of type 'T' into an output writer of type 'O'.
	 * 
	 * @param object			the object to be written to <code>outputWriter</code>.
	 * @param outputWriter		the output writer where the object will be written.
	 * @throws IOException		if any problem happens when attempting to write the object.
	 */
	public abstract void writeObject(T object, O outputWriter) throws IOException;
}
