package mustic.scholz.marine.core.utils;


/**
 * This class implements several static methods for time handling.
 * 
 * @author Ricardo Scholz
 */
public class TimeUtil {

	private static int MILISEC_MULT 	= 1000;
	private static int SECONDS_MULT 	= 60*MILISEC_MULT;
	private static int MINUTES_MULT 	= 60*SECONDS_MULT;
	
	private static int HOURS_PER_DAY 		= 24;
	private static int MINUTES_PER_HOUR 	= 60;
	private static int SECONDS_PER_MINUTE 	= 60;
	
	/**
	 * @param time		the time from which the milliseconds part must be retrieved.
	 * @return			the milliseconds part of <code>time</code>.
	 */
	public static int getMilliseconds(long time) {
		return (int) time % MILISEC_MULT;
	}
	
	/**
	 * @param time		the time from which the seconds part must be retrieved.
	 * @return			the seconds part of <code>time</code>.
	 */
	public static int getSeconds(long time) {
		return (int) (time/MILISEC_MULT)%SECONDS_PER_MINUTE;
	}
	
	/**
	 * @param time		the time from which the minutes part must be retrieved.
	 * @return			the minutes part of <code>time</code>.
	 */
	public static int getMinutes(long time) {
		return (int) ((time/MILISEC_MULT)/SECONDS_PER_MINUTE)%MINUTES_PER_HOUR;
	}
	
	/**
	 * @param time		the time from which the hours part must be retrieved.
	 * @return			the hours part of <code>time</code>; value between 0 and 23 hours.
	 */
	public static int getHours(long time) {
		return (int) ((((time/MILISEC_MULT)/SECONDS_PER_MINUTE)/MINUTES_PER_HOUR)%HOURS_PER_DAY);
	}
	
	/**
	 * @param time				the time which the milliseconds part will be overwritten.
	 * @param milliseconds		the amount of milliseconds to set (between 0 and 999).
	 * @return					<code>time</code>, with the milliseconds part set to <code>milliseconds</code>.
	 */
	public static long setMilliseconds(long time, long milliseconds) {
		if (milliseconds > 999 || milliseconds < 0) {
			throw new IllegalArgumentException("Milliseconds must be between 0 and 999.");
		}
		return (time/MILISEC_MULT) + milliseconds;
	}

	/**
	 * @param time		the time which the seconds part will be overwritten.
	 * @param seconds	the amount of seconds to set (between 0 and 59).
	 * @return			<code>time</code>, with the seconds part set to <code>seconds</code>.
	 */
	public static long setSeconds(long time, long seconds) {
		if (seconds > 59 || seconds < 0) {
			throw new IllegalArgumentException("Seconds must be between 0 and 59.");
		}
		return (time - (getSeconds(time)*MILISEC_MULT) + (seconds*MILISEC_MULT));
	}
	
	/**
	 * @param time		the time which the minutes part will be overwritten.
	 * @param minutes	the amount of minutes to set (between 0 and 59).
	 * @return			<code>time</code>, with the minutes part set to <code>minutes</code>.
	 */
	public static long setMinutes(long time, long minutes) {
		if (minutes > 59 || minutes < 0) {
			throw new IllegalArgumentException("Minutes must be between 0 and 59.");
		}
		return (time - (getMinutes(time)*SECONDS_MULT) + (minutes*SECONDS_MULT));
	}
	
	/**
	 * @param time		the time which the hours part will be overwritten.
	 * @param hours		the amount of hours to set (between 0 and 23)
	 * @return			<code>time</code>, with the hours part set to <code>hours</code>; value between 0 and 23 hours.
	 */
	public static long setHours(long time, long hours) {
		if (hours < 0 || hours > 23) {
			throw new IllegalArgumentException("Hours must be an integer between 0 and 23.");
		}
		return (time - (getHours(time)*MINUTES_MULT) + (hours*MINUTES_MULT));
	}

	/**
	 * Computes a time stamp from separate values of hour, minutes, seconds and milliseconds.
	 *  
	 * @param hours				the hours part of the time stamp;
	 * @param minutes			the minutes part of the time stamp;
	 * @param seconds			the seconds part of the time stamp;
	 * @param milliseconds		the milliseconds part of the time stamp;
	 * @return					a time stamp representing the given parameters.
	 */
	public static long getTime(int hours, int minutes, int seconds, int milliseconds) {
		return ((hours*MINUTES_MULT) + (minutes*SECONDS_MULT) + (seconds*MILISEC_MULT) + (milliseconds));
	}

	/**
	 * Formats a time lapse as 'hh:mm:ss'.
	 * 
	 * @param duration		the time lapse, in milliseconds.
	 * @return				a <code>String</code> in the format 'hh:mm:ss'.
	 */
	public static String getFormattedTime(long duration) {
		String format = "%02d";
		return String.format(format, getHours(duration)) + ":" + String.format(format, getMinutes(duration)) + ":" 
				+ String.format(format, getSeconds(duration));
	}
}
