package mustic.scholz.marine.core.utils;

import java.awt.Color;

/**
 * This class implements several static methods for color handling.
 * 
 * @author Ricardo Scholz
 */
public class ColorUtil {

	private final static int ALPHA_MASK = 0xFF000000;
	private final static int RED_MASK 	= 0x00FF0000;
	private final static int GREEN_MASK = 0x0000FF00;
	private final static int BLUE_MASK 	= 0x000000FF;
	
	private final static int ALPHA_MASK_INVERSE = 0x00FFFFFF;
	private final static int RED_MASK_INVERSE 	= 0xFF00FFFF;
	private final static int GREEN_MASK_INVERSE = 0xFFFF00FF;
	private final static int BLUE_MASK_INVERSE 	= 0xFFFFFF00;
	
	private final static int ALPHA_SHIFT 	= 24;
	private final static int RED_SHIFT 		= 16;
	private final static int GREEN_SHIFT 	= 8;
	private final static int BLUE_SHIFT 	= 0;
	
	/**
	 * Retrieves a color as a single <code>int</code>, given the values of alpha, red, green and blue.
	 * 
	 * @param alpha			the alpha value; 255 meaning completely opaque.
	 * @param red			the red value; 255 meaning completely red.
	 * @param green			the green value; 255 meaning completely green.
	 * @param blue			the blue value; 255 meaning completely blue.
	 * @return				an <code>int</code> which represents the given ARGB color.
	 */
	public static int getColor(byte alpha, byte red, byte green, byte blue) {
		int color = setAlpha(0x00000000, alpha);
		color = setRed(color, red);
		color = setGreen(color, green);
		return setBlue(color, blue);
	}

	/**
	 * Builds an integer representing a given color. Alpha is set to 0 (transparent).
	 * 
	 * @param red		red component.
	 * @param green		green component.
	 * @param blue		blue component.
	 * @return			an integer representing the color as 0x00RRGGBB.
	 */
	public static int getColor(int red, int green, int blue) {
		String redString = (red == 0 ? "0" : "") + Integer.toHexString(red);
		String greenString = (green == 0 ? "0" : "") + Integer.toHexString(green);
		String blueString = (blue == 0 ? "0" : "") + Integer.toHexString(blue);	
		return Integer.parseInt(redString + greenString + blueString, 16);
	}
	
	/**
	 * @param color		the color from which the alpha value must be retrieved.
	 * @return			the alpha value of <code>color</code>.
	 */
	public static byte getAlpha(int color) {
		return (byte) ((color & ALPHA_MASK) >>> ALPHA_SHIFT);
	}
	
	/**
	 * @param color		the color in which the alpha value must be changed.
	 * @param alpha		the new alpha value.
	 * @return			the color with the alpha value changed to <code>alpha</code>.
	 */
	public static int setAlpha(int color, byte alpha) {
		return (color & ALPHA_MASK_INVERSE) | (alpha << ALPHA_SHIFT);
	}
	
	/**
	 * @param color		the color from which the red value must be retrieved.
	 * @return			the red value of <code>color</code>.
	 */
	public static byte getRed(int color) {
		return (byte) ((color & RED_MASK) >>> RED_SHIFT);
	}
	
	/**
	 * @param color		the color from which the red value must be retrieved.
	 * @return			the red value of <code>color</code>, as an <code>int</code>.
	 */
	public static int getRedInt(int color) {
		return (color & RED_MASK) >>> RED_SHIFT;
	}
	
	/**
	 * @param color		the color in which the red value must be changed.
	 * @param red		the new red value.
	 * @return			the color with the red value changed to <code>red</code>.
	 */
	public static int setRed(int color, byte red) {
		return (color & RED_MASK_INVERSE) | ((red << RED_SHIFT) & RED_MASK);
	}
	
	/**
	 * @param color		the color from which the green value must be retrieved.
	 * @return			the green value of <code>color</code>.
	 */
	public static byte getGreen(int color) {
		return (byte) ((color & GREEN_MASK) >>> GREEN_SHIFT);
	}
	
	/**
	 * @param color		the color from which the green value must be retrieved.
	 * @return			the green value of <code>color</code>, as an <code>int</code>.
	 */
	public static int getGreenInt(int color) {
		return (color & GREEN_MASK) >>> GREEN_SHIFT;
	}
	
	/**
	 * @param color		the color in which the green value must be changed.
	 * @param green		the new green value.
	 * @return			the color with the green value changed to <code>green</code>.
	 */
	public static int setGreen(int color, byte green) {
		return (color & GREEN_MASK_INVERSE) | ((green << GREEN_SHIFT) & GREEN_MASK);
	}
	
	/**
	 * @param color		the color from which the blue value must be retrieved.
	 * @return			the blue value of <code>color</code>.
	 */
	public static byte getBlue(int color) {
		return (byte) ((color & BLUE_MASK) >>> BLUE_SHIFT);
	}
	
	/**
	 * @param color		the color from which the blue value must be retrieved.
	 * @return			the blue value of <code>color</code>, as an <code>int</code>.
	 */
	public static int getBlueInt(int color) {
		return (color & BLUE_MASK) >>> BLUE_SHIFT;
	}
	
	/**
	 * @param color		the color in which the blue value must be changed.
	 * @param blue		the new blue value.
	 * @return			the color with the blue value changed to <code>blue</code>.
	 */
	public static int setBlue(int color, byte blue) {
		return (color & BLUE_MASK_INVERSE) | ((blue << BLUE_SHIFT) & BLUE_MASK);
	}
	
	/**
	 * Translates from RGB color space to HSB color space.
	 * @param rgb	the RGB color to be translated. Only red, green and blue components are considered.
	 * @return		a float array with three values: hue (index 0), saturation (index 1) and brightness (index 2).
	 * @see Color#RGBtoHSB(int, int, int, float[]) 
	 */
	public static float[] rgbToHsb(int rgb) {
		return Color.RGBtoHSB(getRedInt(rgb), getGreenInt(rgb), getBlueInt(rgb), null);
	}
	
	/**
	 * Translates from HSB color space to RGB color space.
	 * @param hue			the hue value, in HSB color space.
	 * @param saturation	the saturation value, in HSB color space.
	 * @param brightness	the brightness value, in HSB color space.
	 * @return				an integer representing the given color on RGB space.
	 * @see Color#HSBtoRGB(float, float, float)
	 */
	public static int hsbToRgb(float hue, float saturation, float brightness) {
		return Color.HSBtoRGB(hue, saturation, brightness);
	}
	
	//Format ARGB. What about RGBA?
	/**
	 * Formats a given color for web, as '#ARGB'.
	 * 
	 * @param color		the color to format.
	 * @return			a String in the format '#ARGB'.
	 */
	public static String getWebARGB(int color) {
		String hexString;
		if (color == 0) {
			hexString = "000";
		} else {
			hexString = Integer.toHexString(color);
		}
		return "#" + hexString;
	}
	
	/**
	 * @param color		the color to translate to Web RGB format.
	 * 
	 * @return			a <code>String</code> in the format '#FFRRGGBB' (opaque RGB color). 
	 */
	public static String getWebRGB(int color) {
		return "#" + String.format("%06X", (color & ALPHA_MASK_INVERSE));
	}
	
	/**
	 * Compares two colors, ignoring transparency. Assumes colors are coded as 0xAARRGGBB.
	 * 
	 * @param color1		first color to compare.
	 * @param color2		second color to compare.
	 * @return				<code>true</code> if both colors have the same red, green and blue values, regardless of their transparency.
	 */
	public static boolean equalsIgnoreAlpha(int color1, int color2) {
		color1 = setAlpha(color1, (byte) 0);
		color2 = setAlpha(color2, (byte) 0);
		return color1 == color2;
	}

	/**
	 * Translates a color defined by three double values from 0 to 1 each, representing red, green and blue, to an integer, in the format
	 * 0xFFRRGGBB (opaque color).
	 * 
	 * @param red		red value; 0 to 1, proportional to 0 to 255.
	 * @param green		green value; 0 to 1, proportional to 0 to 255.
	 * @param blue		blue value; 0 to 1, proportional to 0 to 255.
	 * @return			a color in the format 0xFFRRGGBB.
	 */
	public static int translateDoubleColor(double red, double green, double blue) {
		int result = 0xFF000000;
		result = ColorUtil.setRed(result, (byte) (red*255));
		result = ColorUtil.setGreen(result, (byte) (green*255));
		result = ColorUtil.setBlue(result, (byte) (blue*255));
		return result;
	}
}
