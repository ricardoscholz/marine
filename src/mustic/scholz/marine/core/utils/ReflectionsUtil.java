package mustic.scholz.marine.core.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import mustic.scholz.marine.core.classloader.ClassLoaderManager;

/**
 * This class implements a series of methods to make the use of reflections easier and less verbose.
 * 
 * @author Ricardo Scholz
 */
public class ReflectionsUtil {

	/**
	 * Finds a method in a given object, by the method name.
	 * 
	 * @param <T>						the type of the object in which the method will be called.
	 * @param object					the object in which it will look for the method.
	 * @param methodName				the method name to look for.
	 * @return							the method instance, if found; <code>null</code> otherwise.
	 * @throws NoSuchFieldException		if the method is not found.
	 * @throws SecurityException		if the method call violates any security rule.
	 */
	public static <T extends Object> Method findMethod(T object, String methodName) throws NoSuchFieldException, SecurityException {
		
		Method result = null;
		
		if (methodName != null && object != null) {
		
			Class<?> clazz = object.getClass();
	
			for (Method method : clazz.getMethods()) {
				if (method.getName().equals(methodName)) {
					result = method;
					break;
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Looks for a getter method, given an object and the corresponding field name.
	 * 
	 * @param <T>						the type of the object in which the getter method will be called.
	 * @param object					the object in which to look for.
	 * @param fieldName					the field name whose getter will be searched.
	 * @return							the getter method of <code>fieldName</code>, or <code>null</code> if not found.
	 * @throws NoSuchFieldException		if the field is not found.
	 * @throws SecurityException		if the field access violates any security rule.
	 */
	public static <T extends Object> Method findGetter(T object, String fieldName) throws NoSuchFieldException, SecurityException {
		
		Method result = null;
		
		if (fieldName != null && object != null) {
		
			Field field = object.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			Class<?> clazz = object.getClass();
	
			for (Method method : clazz.getMethods()) {
				if ((method.getName().startsWith("get")) && (method.getName().length() == (fieldName.length() + 3))) {
					if (method.getName().toLowerCase().endsWith(fieldName.toLowerCase())) {
						result = method;
						break;
					}
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Invokes the getter method of a given object and field name.
	 * 
	 * @param <T>							the type of the object in which the getter method will be called.
	 * @param object						the object in which the method will be invoked.
	 * @param fieldName						the field name whose getter will be invoked.
	 * @return								the field value, resulting from the invocation of the getter.
	 * @throws NoSuchFieldException			exception thrown by Java reflections API.
	 * @throws SecurityException			exception thrown by Java reflections API.
	 * @throws IllegalAccessException		exception thrown by Java reflections API.
	 * @throws IllegalArgumentException		exception thrown by Java reflections API.
	 * @throws InvocationTargetException	exception thrown by Java reflections API.
	 */
	public static <T extends Object> Object invokeGetter(T object, String fieldName) throws NoSuchFieldException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Object result = null;
		Method getterMethod = ReflectionsUtil.findGetter(object, fieldName);
		if (getterMethod != null && object != null) {
			result = getterMethod.invoke(object);
		}
		return result;
	}
	
	/**
	 * Invokes an ordinary method.
	 * 
	 * @param <T>							the type of the object in which the method will be called.
	 * @param object						the object in which the method will be invoked.
	 * @param methodName					the method name.
	 * @param args							the method arguments.
	 * @return								the object returned by the method, or <code>null</code> if the method is not found or the method
	 * 										is <code>void</code>.
	 * @throws NoSuchFieldException			exception thrown by Java reflections API.
	 * @throws SecurityException			exception thrown by Java reflections API.
	 * @throws IllegalAccessException		exception thrown by Java reflections API.
	 * @throws IllegalArgumentException		exception thrown by Java reflections API.
	 * @throws InvocationTargetException	exception thrown by Java reflections API.
	 */
	public static <T extends Object> Object invokeMethod(T object, String methodName, Object... args) throws NoSuchFieldException, 
			SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Object result = null;
		Method method = ReflectionsUtil.findMethod(object, methodName);
		if (method != null && object != null) {
			result = method.invoke(object, args);
		}
		return result;
	}
	
	/**
	 * Retrieves a field value from an object.
	 * 
	 * @param object						the object which field will be retrieved.
	 * @param fieldName						the field name.
	 * @return								the field value.
	 * @throws NoSuchFieldException			exception thrown by Java reflections API.
	 * @throws SecurityException			exception thrown by Java reflections API.
	 * @throws IllegalArgumentException		exception thrown by Java reflections API.
	 * @throws IllegalAccessException		exception thrown by Java reflections API.
	 */
	public static Object getFieldValue(Object object, String fieldName) throws NoSuchFieldException, SecurityException, 
			IllegalArgumentException, IllegalAccessException {
		Object result = null;
		Field field = object.getClass().getDeclaredField(fieldName);
		if (field != null) {
			field.setAccessible(true);
			
			Object fieldValue = field.get(object);
			if (fieldValue != null) {
				result = fieldValue;
			}
		}
		return result;
	}

	/**
	 * Creates an instance of a class, executing the default empty constructor. Searches the default class path. If class is not found,
	 * looks into the plug-ins class path.
	 * 
	 * @param className						the full class name, including the package.
	 * @return								a new instance of the class.
	 * @throws ClassNotFoundException		if this class has not been found in default class path nor in plug-ins class path.
	 * @throws InstantiationException		if any error occurs during constructor call.
	 * @throws IllegalAccessException		if this method attempts to violate access rules.
	 */
	@SuppressWarnings("rawtypes")
	public static Object createInstance(String className) throws ClassNotFoundException,
			InstantiationException, IllegalAccessException {
		Class clazz;
		try {
			clazz = Class.forName(className);
		} catch (ClassNotFoundException e) {
			clazz = ClassLoaderManager.getInstance().getPluginsClassLoader().loadClass(className);
		}
		return clazz.newInstance();
	}
}
