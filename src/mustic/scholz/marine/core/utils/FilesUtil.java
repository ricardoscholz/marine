package mustic.scholz.marine.core.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * This class implements a series of methods to make file handling easier.
 * 
 * @author Ricardo Scholz
 */
public class FilesUtil {

	/**
	 * The performance file extension. MPF stands for "Marine Performance File".
	 */
	public static final String PERFORMANCE_EXTENSION = "mpf";
	
	/**
	 * The custom element file extension. MCE stands for "Marine Custom Element".
	 */
	public static final String CUSTOM_ELEMENT_EXTENSION = "mce";
	
	/**
	 * The plug-in file extension. MAR stands for "Marine".
	 */
	public static final String PLUG_IN_EXTENSION = "mar";
	
	//TODO [Improvement] Implement automatic recognition of charset.
	private static final String CHARSET = "UTF-8";
	
	/**
	 * Saves a given text file to the file system.
	 * 
	 * @param content		the content of the file to be saved.
	 * @param path			the file path.
	 */
	public static void saveFile(String content, String path) {
		Charset charset = Charset.forName(CHARSET);
		Path filePath = Paths.get(path);
		try (BufferedWriter writer = Files.newBufferedWriter(filePath, charset)) {
		    writer.write(content, 0, content.length());
		} catch (IOException e) {
		    System.err.format("IOException: %s%n", e);
		}
	}
	
	/**
	 * Loads a file as a text, from the file system.
	 * 
	 * @param path		the file path.
	 * @return			the file text.
	 */
	public static String loadFile(String path) {
		StringBuffer sb = new StringBuffer();
		Charset charset = Charset.forName(CHARSET);
		Path filePath = Paths.get(path);
		try (BufferedReader reader = Files.newBufferedReader(filePath, charset)) {
			String line = reader.readLine();
			while (line != null) {
				sb.append(line);
				sb.append("\n");
			}
		} catch (IOException e) {
		    System.err.format("IOException: %s%n", e);
		}
		return sb.toString();
	}
	
	/**
	 * Deletes a file or directory from file system, recursively or not.
	 * 
	 * @param filePath		the path of the file or directory.
	 * @param recursive		<code>true</code> to delete sub-directories recursively; <code>false</code> to delete the file/directory only.
	 * @throws IOException	if the file can not be deleted from the file system.
	 */
	public static void delete(String filePath, boolean recursive) throws IOException {
		delete(new File(filePath), recursive);
	}
	
	/**
	 * Deletes a file or directory from file system, recursively or not.
	 * 
	 * @param file			the file or directory to be deleted.
	 * @param recursive		<code>true</code> to delete sub-directories recursively; <code>false</code> to delete the file/directory only.
	 * @throws IOException	if the file cannot be deleted from file system.
	 */
	public static void delete(File file, boolean recursive) throws IOException {
		if (file.isDirectory() && recursive) {
			for (File c : file.listFiles())
				delete(c, recursive);
		}
		if (!file.delete()) {
			throw new IOException("Failed to delete file: " + file);
		}
	}
	
	/**
	 * Copies a file from a source directory to a destiny directory, replacing any conflicting files.
	 * 
	 * @param source	the source file or directory to be copied.
	 * @param destiny	the destination file or directory.
	 */
	public static void copyFileToSystemFolder(String source, String destiny) {
		try {
			Path src = Paths.get(source);
			Path dst = Paths.get(destiny + File.separator + src.getFileName().toString());
			Files.copy(src, dst, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.err.println(e);
		}
	}
	
	/**
	 * Loads all files of a given extension within a directory.
	 * 
	 * @param directoryStr		the path of the directory to look for files with the given <code>extension</code>.
	 * @param extension			the extension of the files that will be retrieved.
	 * @return					the list of all files with a given extension (<code>extension</code>) within a directory 
	 * 							(<code>directoryStr</code>).
	 */
	public static List<File> loadFiles(String directoryStr, String extension) {
		File directory = new File(directoryStr);
		File[] allFiles = directory.listFiles();
		List<File> result = new ArrayList<File>();
		if (allFiles != null && allFiles.length > 0) {
			for (File file : allFiles) {
				if (file != null && file.isFile() && file.getName().endsWith(extension)) {
					result.add(file);
				}
			}
		}
		return result;
	}
	
	/**
	 * Loads a properties file.
	 * 
	 * @param path		the file path.
	 * @return			the properties file, as a {@link Properties} instance.
	 */
	public static Properties loadProperties(String path) {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			File file = new File(path);
			file.createNewFile();
			input = new FileInputStream(path);
			prop.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop;
	}

	/**
	 * Writes a {@link Properties} instance into an actual file.
	 * 
	 * @param prop		the properties instance.
	 * @param path		the actual file path.
	 */
	public static void writeProperties(Properties prop, String path) {
		OutputStream output = null;
		try {
			output = new FileOutputStream(path);
			prop.store(output, null);
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Uncompresses a file (zip compressed) into a destiny directory.
	 * 
	 * @param fileUrl			the file to be uncompressed.
	 * @param destiny			the destination directory.
	 * @throws IOException		if <code>fileUrl</code> cannot be read or unpacked into system temporary directory, or <code>destiny</code>
	 * 							is not writable.
	 */
	public static void unpack(String fileUrl, String destiny) throws IOException {
		JarFile jar = new JarFile(fileUrl);
		try {
			File subfolder = new File(destiny + File.separator + getFileNameWithoutExtension(jar.getName()));
			if (!subfolder.exists()) {
				subfolder.mkdir();
			}
			
			Enumeration<JarEntry> enumEntries = jar.entries();
			while (enumEntries.hasMoreElements()) {
			    JarEntry file = (JarEntry) enumEntries.nextElement();
			    File f = new File(subfolder.getCanonicalPath() + File.separator + file.getName());
			    if (file.isDirectory()) {
			        f.mkdir();
			        continue;
			    }
			    InputStream is = null;
			    FileOutputStream fos = null;
			    try {
			    	f.createNewFile();
				    is = jar.getInputStream(file);
				    fos = new FileOutputStream(f);
				    while (is.available() > 0) {
				        fos.write(is.read());
				    }
			    } catch (IOException e) {
			    	throw e;
			    } finally {
			    	if (fos != null) {
			    		fos.close();
			    	}
				    if (is != null) {
				    	is.close();	
				    }
			    }
			}
		} catch (Exception e) {
			throw e;
		} finally {
			jar.close();
		}
	}
	
	/**
	 * Parses a file name to retrieve the file name without the path, and the extension.
	 * 
	 * @param fileName		the absolute file name.
	 * @return				the file name without the absolute path and the extension.
	 */
	public static String getFileNameWithoutExtension(String fileName) {
		return fileName.substring(fileName.lastIndexOf(File.separatorChar) + 1, fileName.lastIndexOf('.'));
	}
	
	/**
	 * Concatenates a number of <code>String</code> which make a URL, inserting a {@link File#separator} between them.
	 * @param parts		all parts of a URL;
	 * @return	a single <code>String</code> which represents the URL with correct system dependent file separators.
	 */
	public static String concatenateURL(String... parts){
		String result = "";
		if (parts != null) {
			int i;
			for (i = 0; i < parts.length-1; i++) {
				result = result + parts[i] + File.separator;
			}
			result = result + parts[i];
		}
		return result;
	}

	/**
	 * Splits a directory by system file separator.
	 * 
	 * @param path		the path to be split.
	 * @return			an array of <code>String</code> with each piece, excluding file separators themselves.
	 */
	public static String[] splitByFileSeparator(String path) {
		path = path.replace(File.separatorChar, ':');
		return path.split(":");
	}
	
}
