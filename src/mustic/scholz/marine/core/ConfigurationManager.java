package mustic.scholz.marine.core;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import mustic.scholz.marine.core.utils.FilesUtil;

/**
 * This class is responsible for system.config file reading and handling.
 * 
 * @author Ricardo Scholz
 */
public class ConfigurationManager {

	/**
	 * Plug-ins icons directory, within each plug-in root directory. 
	 */
	public static final String PLUGIN_IMG_DIRECTORY					= "img";
	
	/**
	 * Plug-ins default icon file, expected to be within {@link #PLUGIN_IMG_DIRECTORY}.
	 */
	public static final String PLUGIN_ICON_DEFAULT_PATH				= "icon.png";
	
	/**
	 * Plug-in large icon file, expected to be within {@link #PLUGIN_IMG_DIRECTORY}.
	 */
	public static final String PLUGIN_ICON_LARGE_DEFAULT_PATH		= "icon-large.png";
	
	private static final String SYS_CONFIG_DIRECTORY				= "config";
	private static final String SYS_CONFIG_FILE 					= "system.config";
	
	private static final String EYES_WEB_EXTERNAL 					= "eyesweb.external";
	private static final String EYES_WEB_INSTALL_DIRECTORY 			= "eyesweb.install.directory";
	private static final String EYES_WEB_EXECUTABLE	 				= "eyesweb.executable.file";
	private static final String EYES_WEB_FEATURES_PATCH 			= "eyesweb.patch.file";
	private static final String SYSTEM_FEATURES_SAMPLING_TIMEOUT	= "system.features.sampling.timeout";
	private static final String PLUGINS_DIRECTORY 					= "system.plugins.directory";
	private static final String CUSTOM_ELEMENTS_DIRECTORY 			= "system.customelements.directory";
	private static final String TEMPORARY_DIRECTORY 				= "system.temp.directory";
	private static final String SYSTEM_CALIBRATION_FILE				= "system.calibration.file";
	private static final String SYSTEM_TRANSITION_UPDATE_INTERVAL	= "system.transitions.update.interval";
	
	private static ConfigurationManager instance;
	
	private Properties prop;
	private String root;
	private String propertiesFilePath;
	
	private ConfigurationManager() {
		Path currentRelativePath = Paths.get("");
		this.root = currentRelativePath.toAbsolutePath().toString();
		this.propertiesFilePath = FilesUtil.concatenateURL(this.root, SYS_CONFIG_DIRECTORY, SYS_CONFIG_FILE);
		this.load();
	}
	
	/**
	 * Singleton instance retrieval method.
	 * @return	the singleton instance.
	 */
	public static ConfigurationManager getInstance() {
		if (instance == null) {
			instance = new ConfigurationManager();
		}
		return instance;
	}
	
	private void load() {
		this.prop = FilesUtil.loadProperties(this.propertiesFilePath);
	}
	
	/**
	 * Saves the current properties to filesystem.
	 */
	public void save() {
		FilesUtil.writeProperties(this.prop, this.propertiesFilePath);
	}
	
	/**
	 * @return	<code>true</code> if EyesWeb directory information is absolute (external EyesWeb installation).
	 * 			<code>false</code> if embedded EyesWeb installation will be used.
	 */
	public Boolean isEyesWebExternal() {
		String ext = this.prop.getProperty(EYES_WEB_EXTERNAL);
		return "true".equalsIgnoreCase(ext) || "1".equalsIgnoreCase(ext);
	}
	
	/**
	 * Modifies {@link #EYES_WEB_EXTERNAL} property value. This property indicates whether an external install of EyesWeb will be used,
	 * or the default package install should be used. Mainly for testing and development purposes.
	 * 
	 * @param external		<code>true</code> if an external instance of EyesWeb will be used; <code>false</code> otherwise.
	 */
	public void setEyesWebExternal(boolean external) {
		if (external) {
			this.prop.setProperty(EYES_WEB_EXTERNAL, "true");
		} else {
			this.prop.setProperty(EYES_WEB_EXTERNAL, "false");
		}
	}
	
	/**
	 * @return	EyesWeb install directory, as in the configurations file.
	 */
	public String getEyesWebInstallFolder() {
		return this.prop.getProperty(EYES_WEB_INSTALL_DIRECTORY);
	}
	
	/**
	 * Modifies the value of EyesWeb install directory in the configuration file.
	 * It is not persisted until {@link ConfigurationManager#save()} is called.
	 * 
	 * @param folder	the new absolute propertiesFilePath to EyesWeb install folder.
	 */
	public void setEyesWebInstallFolder(String folder) {
		this.prop.setProperty(EYES_WEB_INSTALL_DIRECTORY, folder);
	}
	
	/**
	 * @return	EyesWeb executable file name.
	 */
	public String getEyesWebExecutable() {
		return this.prop.getProperty(EYES_WEB_EXECUTABLE);
	}
	
	/**
	 * Modifies the value of EyesWeb executable file name in the configuration file.
	 * It is not persisted until {@link ConfigurationManager#save()} is called.
	 * 
	 * @param filename	the new EyesWeb executable file name.
	 */
	public void setEyesWebExecutable(String filename) {
		this.prop.setProperty(EYES_WEB_EXECUTABLE, filename);
	}
	
	/**
	 * @return	EyesWeb features patch file name.
	 */
	public String getEyesWebFeaturesPatch() {
		return this.prop.getProperty(EYES_WEB_FEATURES_PATCH);
	}
	
	/**
	 * Modifies the value of EyesWeb features patch file name in the configuration file.
	 * It is not persisted until {@link ConfigurationManager#save()} is called.
	 * 
	 * @param filename	the new EyesWeb features patch file name (expected to be a "*.eywx" file).
	 */
	public void setEyesWebFeaturesPatch(String filename) {
		this.prop.setProperty(EYES_WEB_FEATURES_PATCH, filename);
	}
	
	/**
	 * Retrieves the system features sampling timeout. System will not update a given feature
	 * before it is older than this timeout (in milliseconds).
	 * 
	 * @return	system features sampling timeout.
	 */
	public long getSystemFeaturesSamplingTimeout() {
		return this.getLongValue(SYSTEM_FEATURES_SAMPLING_TIMEOUT);
	}
	
	/**
	 * Modifies the value of system sampling timeout in the configuration file.
	 * It is not persisted until {@link ConfigurationManager#save()} is called.
	 * 
	 * @param samplingRate	sampling rate, in milliseconds.
	 */
	public void setSystemFeaturesSamplingTimeout(long samplingRate) {
		this.prop.setProperty(SYSTEM_FEATURES_SAMPLING_TIMEOUT, Long.toString(samplingRate));
	}
	
	/**
	 * @return	directory where where all third party plug-ins are extracted.
	 */
	public String getPluginsFolder() {
		return this.prop.getProperty(PLUGINS_DIRECTORY);
	}
	
	/**
	 * Modifies the plug-ins directory. This modification does not transfer plug-ins extracted to
	 * the previous plug-ins directory to the new one, what means previous imported plug-ins will
	 * not be found anymore. It is not persisted until {@link ConfigurationManager#save()} is called.
	 * 
	 * @param folder		the new plug-ins directory URL.
	 */
	public void setPluginsFolder(String folder) {
		this.prop.setProperty(PLUGINS_DIRECTORY, folder);
	}
	
	/**
	 * @return	directory where all custom elements are saved.
	 */
	public String getCustomElementsFolder() {
		return this.prop.getProperty(CUSTOM_ELEMENTS_DIRECTORY);
	}
	
	/**
	 * Modifies the directory where custom elements are saved. This modification does not transfer 
	 * custom elements saved within previous directory to the new one, what means previously 
	 * saved custom elements will not be found anymore. It is not persisted until
	 * {@link ConfigurationManager#save()} is called.
	 * 
	 * @param folder		the new custom elements directory URL.
	 */
	public void setCustomElementsFolder(String folder) {
		this.prop.setProperty(CUSTOM_ELEMENTS_DIRECTORY, folder);
	}
	
	/**
	 * Retrieves the system temporary directory, used for temporary files. Files in this directory
	 * may be deleted after each program run, with no collateral effects.
	 * 
	 * @return	system temporary directory.
	 */
	public String getTemporaryFolder() {
		return this.prop.getProperty(TEMPORARY_DIRECTORY);
	}
	
	/**
	 * Modifies system temporary folder.
	 * 
	 * @param folder		the new system temporary directory URL.
	 */
	public void setTemporaryFolder(String folder) {
		this.prop.setProperty(TEMPORARY_DIRECTORY, folder);
	}
	
	/**
	 * Retrieves the default calibration file propertiesFilePath, in which built-in calibration element
	 * saves information.
	 * 
	 * @return	default calibration file propertiesFilePath.
	 */
	public String getDefaultCalibrationFile() {
		return this.prop.getProperty(SYSTEM_CALIBRATION_FILE);
	}
	
	/**
	 * Modifies default calibration file. It is not persisted until 
	 * {@link ConfigurationManager#save()} is called.
	 * 
	 * @param file	the new default calibration file.
	 */
	public void setDefaultCalibrationFile(String file) {
		this.prop.setProperty(SYSTEM_CALIBRATION_FILE, file);
	}
	
	/**
	 * Retrieves the default calibration file absolute file propertiesFilePath, assuming it is under configurations folder.
	 * 
	 * @return an absolute file propertiesFilePath of the default calibration file.
	 */
	public String getDefaultCalibrationFileAbsolutePath() {
		return FilesUtil.concatenateURL(this.root, SYS_CONFIG_DIRECTORY, this.getDefaultCalibrationFile());
	}
	
	/**
	 * Retrieves the transition checking thread update interval.
	 * 
	 * @return	the time interval between two consecutive transitions firing check, in milliseconds.
	 */
	public long getTransitionUpdateInterval() {
		return this.getLongValue(SYSTEM_TRANSITION_UPDATE_INTERVAL);
	}
	
	/**
	 * Modifies the transition checking thread update interval. It is not persisted until {@link ConfigurationManager#save()} is called.
	 * 
	 * @param updateInterval		the new update interval in milliseconds.
	 */
	public void setTransitionUpdateInterval(long updateInterval) {
		this.prop.setProperty(SYSTEM_TRANSITION_UPDATE_INTERVAL, Long.toString(updateInterval));
	}
	
	/**
	 * @return	the root directory (installation directory), from where the JAR is being executed. 
	 */
	public String getRootDirectory() {
		return this.root;
	}
	
	/**
	 * @return	the default icon path, within a plug-in root directory.
	 */
	public String getDefaultIconPath() {
		return FilesUtil.concatenateURL(PLUGIN_IMG_DIRECTORY, PLUGIN_ICON_DEFAULT_PATH);
	}
	
	/**
	 * @return	the default large icon path, within a plug-in root directory.
	 */
	public String getDefaultLargeIconPath() {
		return FilesUtil.concatenateURL(PLUGIN_IMG_DIRECTORY, PLUGIN_ICON_LARGE_DEFAULT_PATH);
	}
	
	private long getLongValue(String property) {
		long result = 0;
		String str = this.prop.getProperty(property);
		try {
			result = Long.parseLong(str);
		} catch (Exception e) {
			//do nothing.
		}
		return result;
	}
}
