package mustic.scholz.marine.core;

import java.io.IOException;
import java.util.List;

import mustic.scholz.marine.core.feature.FeatureExtractor;
import mustic.scholz.marine.core.feature.IFeatureProcessor;
import mustic.scholz.marine.core.feature.ew.EyesWebProcessorManager;
import mustic.scholz.marine.core.input.IInputReceiver;
import mustic.scholz.marine.core.input.kinect.J4KKinectListener;
import mustic.scholz.marine.core.movement.MovementManagerFactory;
import mustic.scholz.marine.core.performance.CalibrationPropertiesManager;
import mustic.scholz.marine.core.performance.FeaturesPool;
import mustic.scholz.marine.core.performance.Performance;
import mustic.scholz.marine.core.performance.ProjectionPlane;
import mustic.scholz.marine.core.performance.element.CustomElement;
import mustic.scholz.marine.core.performance.element.Element;
import mustic.scholz.marine.core.performance.transition.Transition;
import mustic.scholz.marine.core.persistence.CustomElementManager;
import mustic.scholz.marine.core.persistence.PerformanceManager;
import mustic.scholz.marine.core.plugin.ElementPlugin;
import mustic.scholz.marine.core.plugin.PluginManager;
import processing.core.PApplet;

/**
 * This class is intended to provide all functionalities UI implementations need, by delegating to adequate managers.
 * 
 * @author Ricardo Scholz
 */
public class PerformanceFacade {

	private static final int INITIAL 	= 0;
	private static final int PAPPLET 	= 1;
	private static final int PREPARED 	= 2;
	private static final int PLAYING 	= 3;
	private static final int PAUSED 	= 4;
	private static final int STOPPED 	= 5;
	
	/**
	 * Internal facade state.
	 */
	private int state = INITIAL;
	
	/**
	 * The Singleton instance.
	 */
	private static PerformanceFacade instance;
	
	/**
	 * PGraphics thread, asynchronously running. This thread is responsible for allowing the creation and execution of a graphics and 
	 * provides methods to setup the elements' grid, as well as play back methods.
	 */
	private Performance performance;
	
	/**
	 * Custom element manager runs synchronously with this thread, and provides methods to manage custom elements.
	 */
	private CustomElementManager customElementManager;
	
	/**
	 * Plug-in manager runs synchronously with this thread, and provides methods to manage plug-in elements.
	 */
	private PluginManager pluginManager;
	
	/**
	 * PGraphics manager runs synchronously with this thread, providing methods to manage graphics as files.
	 */
	private PerformanceManager performanceManager;
	
	/**
	 * Features processor is responsible for silently starting EyesWeb program and execute the features
	 * computation patch. EyesWeb is executed as an independent process, called through command line. To end EyesWeb
	 * process, it is necessary to call its <code>stop()</code> method.
	 */
	private IFeatureProcessor featureProcessor;
	
	/**
	 * Input receiver runs asynchronously with this thread, processing input received from EyesWeb process through OSC.
	 */
	private IInputReceiver inputReceiver;
	
	/**
	 * Calibration Properties Manager, holds the calibration properties file.
	 */
	private CalibrationPropertiesManager calibrationManager;
	
	/**
	 * Configuration Manager, holds the system configuration properties file.
	 */
	private ConfigurationManager configurationManager;
	
	/**
	 * Local variable to control when elements table has been changed before a new call to {@link PerformanceFacade#play()}.
	 */
	private boolean elementsTableChanged;
	
	/*
	 * PGraphics Startup Methods
	 */
	
	/**
	 * Singleton instance retrieval.
	 * 
	 * @return	the singleton instance.
	 */
	public static PerformanceFacade getInstance() {
		if (instance == null) {
			instance = new PerformanceFacade();
		}
		return instance;
	}
	
	private PerformanceFacade() {
		this.customElementManager = CustomElementManager.getInstance();
		this.pluginManager = PluginManager.getInstance();
		this.performanceManager = PerformanceManager.getInstance();
		this.featureProcessor = EyesWebProcessorManager.getInstance();
		this.inputReceiver = MovementManagerFactory.createMovementManager(true, true);
		
		this.prepareProjection(0x00000000, 2, ProjectionPlane.WALL);
		this.startInputReceiver();
		this.startFeatureProcessor();
		
		this.configurationManager = ConfigurationManager.getInstance();
		this.elementsTableChanged = false;
	}

	/*
	 * Init Methods
	 */
	
	/**
	 * Resets the PGraphics element, defining the background color and a projection screen. All changes to the
	 * graphics will be lost, as well as any element's table configurations.
	 * 
	 * @param backgroundColor		the default background color.
	 * @param projectionScreen		the projection screen index (0 for SPAN, 1 for main system screen).
	 * @param plane					the plane in which projection will take place.
	 */
	public void prepareProjection(int backgroundColor, int projectionScreen, ProjectionPlane plane) {
		if (this.state == INITIAL) {
			if (this.performance == null) {
				this.performance = new Performance(backgroundColor, projectionScreen, plane);
			} else {
				this.performance.setBackgroundColor(backgroundColor);
				this.performance.setProjectionScreen(projectionScreen);
				this.performance.setPlane(plane);
			}
		} else {
			throw new IllegalStateException("The method 'prepareProjection(int, int, ProjectionPlane)' must be called before the PApplet"
					+ " is started.");
		}
	}
	
	private void startInputReceiver() {
		Thread inputReceiverThread = new Thread(this.inputReceiver);
		inputReceiverThread.start();
	}
	
	private void startFeatureProcessor() {
		if (!this.featureProcessor.isRunning()) {
			try {
				this.featureProcessor.start();
			} catch (Exception e) {
				//TODO [Improvement] Implement appropriate error handling.
				e.printStackTrace();
			}
		}
	}

	/*
	 * State Machine Management Methods
	 */
	
	/**
	 * Starts the PApplet instance. Must be called from INITIAL state, and returns in PAPPLET state.
	 */
	public void startPApplet() {
		if (this.state == INITIAL) {
			PApplet.runSketch(new String[] {Performance.class.getName()}, this.performance);
		} else {
			throw new IllegalStateException("The method 'startPApplet()' must be called before the PApplet is started.");
		}
		this.state = PAPPLET;
	}
	
	/**
	 * Sets default background color (black), starts PApplet (if not yet started), and prepares elements' table.
	 * Must be called from INITIAL, PAPPLET or STOPPED state, and returns in PREPARED state.
	 */
	public void prepareExecution() {
		this.prepareExecution(this.performance.getBackgroundColor());
	}
	
	/**
	 * Sets the background color, starts PApplet (if not yet started), and prepares elements' table.
	 * Must be called from INITIAL, PAPPLET or STOPPED state, and returns in PREPARED state.
	 * 
	 * @param	bkgColor				the background color of the projection screen;
	 * @throws IllegalStateException	if facade is not in INITIAL, PAPPLET or PREPARED states.
	 */
	public void prepareExecution(int bkgColor) {
		switch(this.state) {
			case INITIAL:
				this.startPApplet();
			case PAPPLET:
			case STOPPED:
				this.performance.setBackgroundColor(bkgColor);
				this.performance.prepareElementsTable(this.inputReceiver);
			case PREPARED:
				break;
			default:
				throw new IllegalStateException("The method 'prepareExecution(int)' must not be called from a PLAYING, PAUSED or PREPARED "
						+ "state. Only INITIAL, PAPPLET and STOPPED are allowed.");
		}
		this.state = PREPARED;
	}
	
	/**
	 * Starts the performance loop. Calls all previous state machine methods, if they were not called yet. May be called from any state.
	 * Returns in PLAYING state.
	 * 
	 * @throws IllegalStateException	if the performance instance is null.
	 */
	public void play() {
		if (this.canPlay()) {
			if (this.state == STOPPED) {
				if (elementsTableChanged) {
					this.state = PAPPLET;
				}
			}
			switch(this.state) {
			case INITIAL:
				this.startPApplet();
			case PAPPLET:
				this.prepareExecution();
			case PREPARED:
			case PAUSED:
			case STOPPED:
				this.performance.play();
			}
			this.state = PLAYING;
			this.elementsTableChanged = false;
		} else {
			throw new IllegalStateException("The method 'play()' cannot be called when the performance instance is null.");
		}
	}
	
	/**
	 * @return	<code>true</code> if the current performance instance is in a state in which it can be started.
	 */
	public boolean canPlay() {
		return this.performance != null;
	}
	
	/**
	 * Pauses the performance loop. Indexes remain unchanged, agents stop being updated and painted, and features pool stops being 
	 * updated. Returns in PAUSED state.
	 * 
	 * @throws IllegalStateException	if facade is not in PLAYING state or performance instance is <code>null</code>.
	 */
	public void pause() {
		if (this.canPause()) {
			this.performance.pause();
		} else if (this.state != PAUSED || this.performance == null) {
			throw new IllegalStateException("The method 'pause()' must be called from PLAYING state, and performance instance must not be"
					+ " null.");
		}
		this.state = PAUSED;
	}
	
	/**
	 * @return	<code>true</code> if the current performance instance is in a state in which it can be paused.
	 */
	public boolean canPause() {
		return (this.state == PLAYING && this.performance != null);
	}

	/**
	 * Stops the performance execution. All indexes go back to the start. PApplet process keeps running, but not loop is executed. 
	 * Returns in STOPPED state.
	 * 
	 * @throws IllegalStateException	if facade is not in PLAYING or PAUSE states or performance instance is <code>null</code>.
	 */
	public void stop() {
		if (this.canStop()) {
			this.performance.stop();
		} else {
			throw new IllegalStateException("The method 'stop()' must be called from PLAYING or PAUSED state, and performance instance must"
					+ " not be null.");
		}
		this.state = STOPPED;
	}
	
	/**
	 * @return	<code>true</code> if the current performance instance is in a state in which it can be stopped.
	 */
	public boolean canStop() {
		return (this.performance != null && (this.state == PLAYING || this.state == PAUSED));
	}
	
	/**
	 * Stops feature processor and execute elements' <code>destroy()</code> method to free memory. May be called from any state.
	 * If PApplet has already been initialised, stops it as well. When PApplet instance is destroyed, application exits. Even external
	 * applications will exit if this method is called. No workaround has been found, till now, to avoid main application exiting.
	 */
	public void destroy() {
		try {
			this.featureProcessor.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.performance.destroy(this.state != INITIAL);
	}
	
	/*
	 * Execution time methods.
	 */
	
	/**
	 * Creates a new element table, discarding the previous one. Automatically stops the current performance.
	 */
	public void newPerformance() {
		if (this.state == PLAYING || this.state == PAUSED) {
			this.stop();
		}
		this.performance.newPerformance();
		this.loadCalibrationFile();
	}

	/**
	 * Creates a new element table, discarding the previous one. Automatically stops the current performance.
	 * 
	 * @param maxTracks					the number of tracks of the new performance.
	 * @param maxElementsPerTrack		the number of elements within each track in the new performance.
	 */
	public void newPerformance(int maxTracks, int maxElementsPerTrack) {
		if (this.state == PLAYING || this.state == PAUSED) {
			this.stop();
		}
		this.performance.newPerformance(maxTracks, maxElementsPerTrack);
		this.loadCalibrationFile();
	}
	
	/**
	 * Creates a new element table, discarding the previous one. Automatically stops the current performance.
	 * 
	 * @param color		the background color of the new graphics.
	 */
	public void newPerformance(int color) {
		this.newPerformance();
		this.performance.setBackgroundColor(color);
		this.loadCalibrationFile();
	}
	
	/**
	 * Allows setting the background color of the current graphics.
	 * 
	 * @param color		the new background color.
	 */
	public void setBackgroundColor(int color) {
		this.performance.setBackgroundColor(color);
	}
	
	/**
	 * During graphics execution, it forces a transition forward.
	 */
	public void moveForward() {
		if (this.canMoveForward()) {
			this.performance.moveForward();
		}
	}
	
	/**
	 * @return	<code>true</code> if the current performance instance is in a state in which it can be moved forward.
	 */
	public boolean canMoveForward() {
		return this.performance != null && this.performance.getElementTable().getCurrentPosition() <= this.getLastTransitionIndex();
	}
	
	/**
	 * During a graphics execution, it forces a transition backward.
	 */
	public void moveBackward() {
		if (this.canMoveBackward()) {
			this.performance.moveBackward();
		}
	}
	
	/**
	 * @return	<code>true</code> if the current performance instance is in a state in which it can be moved backward.
	 */
	public boolean canMoveBackward() {
		return this.performance != null && this.performance.getElementTable().getCurrentPosition() > 0;
	}
	
	/*
	 * Creation time methods
	 */
	
	/**
	 * Adds an element to the grid, in the specified track and position, shifting or not elements at its right.
	 * 
	 * @param element		the element to include in the grid.
	 * @param track			the track in which the element must be included (row).
	 * @param position		the position in which the element must be included (column).
	 * @param shift			if <code>true</code>, shifts right all elements to the right of the inserted element;
	 * 						otherwise, overwrites the element at that position, if any.
	 */
	public void addElement(CustomElement element, int track, int position, boolean shift) {
		this.elementsTableChanged = true;
		this.performance.addElement(element, track, position, shift);
	}
	
	/**
	 * Removes an element from the specified track and position of the grid, shifting or not elements at its right.
	 * 
	 * @param track			the track from which the element must be withdrawn (row).
	 * @param position		the position from which the element must be withdrawn (column).
	 * @param shift			if <code>true</code>, shifts left all elements to the right of the removed element;
	 * 						otherwise, leave the position blank.
	 * @return				the element instance removed.
	 */
	public CustomElement removeElement(int track, int position, boolean shift) {
		this.elementsTableChanged = true;
		return this.performance.removeElement(track, position, shift);
	} 
	
	/**
	 * Shifts all elements right of a given track and position. After its execution, the position will be a blank space in the grid.
	 * 
	 * @param track		the track where the shift will take place.
	 * @param position	the position, from which the shift will take place, inclusive.
	 */
	public void shiftRightFromElement(int track, int position) {
		this.elementsTableChanged = true;
		this.performance.shiftRightFromElement(track, position);
	}
	
	/**
	 * Shifts all elements left from a given track and position. The element at the given position will be overwritten by 
	 * its right neighbor and the last element of the track will be null, after executing the shift.
	 * 
	 * @param track		the track where the shift will take place.
	 * @param position	the position, from which the shift will take place, inclusive.
	 */
	public void shiftLeftFromElement(int track, int position) {
		this.elementsTableChanged = true;
		this.performance.shiftLeftFromElement(track, position);
	}

	/**
	 * Adds a transition in a given position, shifting or not the transitions to the right of it.
	 * 
	 * @param transition	the transition to be added.
	 * @param position		the position in which the transition will be added.
	 * @param shift			if <code>true</code>, the transitions to the right of the added transition will be shifted right;
	 * 						otherwise, the new transition will overwrite the existing transition at that position.
	 */
	public void addTransition(Transition transition, int position, boolean shift) {
		this.elementsTableChanged = true;
		this.performance.addTransition(transition, position, shift);
	}
	
	/**
	 * Adds a transition in the next available position.
	 * 
	 * @param transition	the transition to be added.
	 */
	public void addTransition(Transition transition) {
		this.elementsTableChanged = true;
		this.performance.addTransition(transition);
	}
	
	/**
	 * Removes a transition in a given position. Always shift other transitions left after removing the transition, as transitions
	 * sequence must not have empty spaces.
	 * 
	 * @param position		the position from which the transition must be removed.
	 * @return				the removed transition instance.
	 */
	public Transition removeTransition(int position) {
		this.elementsTableChanged = true;
		return this.performance.removeTransition(position);
	}
	
	/*
	 * Library Management
	 */
	
	// Custom Elements Management Methods
	
	/**
	 * Saves an element within system folders as a custom element.
	 * 
	 * @param element	the element to save, already with its properties customised.
	 * @param name		the custom element name (identifier).
	 * @param key		the custom element key; "core" package does not use this property; it is meant for UI usage.
	 * @param directory	the original plug-in element directory within plug-ins' directory.
	 */
	public void saveAsCustomElement(Element element, String name, int key, String directory) {
		this.customElementManager.saveAsCustomElement(element, name, key, directory);
	}
	
	/**
	 * Saves a list of elements within system folders as custom elements.
	 * 
	 * @param customElements	the elements to save, already with their properties customised.
	 */
	public void saveCustomElements(List<CustomElement> customElements) {
		this.customElementManager.saveCustomElements(customElements);
	}

	/**
	 * Saves a single instance of {#@link CustomElement}.
	 * 
	 * @param customElement		the element to save, with customised properties.
	 */
	public void saveCustomElement(CustomElement customElement) {
		this.customElementManager.saveCustomElement(customElement);
	}
	
	/**
	 * Retrieves from file system a previously saved custom element.
	 * 
	 * @param directory		the custom element directory (identifier).
	 * @param key			the custom element key (identifier).
	 * @return				the retrieved custom element instance.
	 */
	public CustomElement loadCustomElement(String directory, Integer key) {
		return this.customElementManager.loadCustomElement(directory, key);
	}
	
	/**
	 * Loads all custom elements within the system folder. Useful to populate the custom elements area of UI.
	 * 
	 * @return	a list of custom elements.
	 */
	public List<CustomElement> loadAllCustomElements() {
		return this.customElementManager.loadAllCustomElements();
	}
	
	/**
	 * Removes from file system a given custom element.
	 * 
	 * @param element	the element to remove.
	 */
	public void delete(CustomElement element) {
		this.customElementManager.delete(element);
	}
	
	/*
	 * Plug-in Elements Management Methods
	 */
	
	/**
	 * Imports a given plug-in to the system. Copies the file to system plug-ins folder.
	 * 
	 * @param fileUrl			the complete path of the file to import.
	 * @throws IOException		when there is a problem to read from <code>fileUrl</code> or to write into system plug-ins directory. 
	 */
	public void importElementPlugin(String fileUrl) throws IOException {
		this.pluginManager.importElementPlugin(fileUrl);
	}
	
	/**
	 * Loads all plug-ins within system plug-ins folder. Useful to populate plug-ins area of UI.
	 * 
	 * @return	a list of plug-ins.
	 */
	public List<ElementPlugin> loadAllElementPlugins() {
		this.pluginManager.loadExternalPluginElements();
		return this.pluginManager.getElementPlugins();
	}
	//TODO [Improvement] Provide method to find plug-in by ID and/or NAME
	
	/**
	 * Removes from file system a given plug-in.
	 * 
	 * @param plugin			the plug-in to remove
	 * @throws IOException		when there is a problem to delete <code>plugin</code> from the file system. 
	 */
	public void delete(ElementPlugin plugin) throws IOException {
		this.pluginManager.deleteElementPlugin(plugin);
	}
	
	/*
	 * Input Receivers Management Methods 
	 */
	
	/**
	 * Adds a custom feature to the internal pool, so every performer position message received will recompute the added feature and put it 
	 * in the <code>FeaturesPool</code>.
	 * 
	 * @param featureExtractor		the <code>FeatureExtractor</code> instance, which implements the feature's computation algorithm.
	 */
	public void addFeatureExtractor(FeatureExtractor featureExtractor) {
		this.inputReceiver.addFeatureExtractor(featureExtractor);
	}
	
	/**
	 * Removes a given custom feature from the internal pool. This feature will not be updated by future performer position messages 
	 * received by this instance. However, this method does not withdraw the last updated feature from the <code>FeaturesPool</code>.
	 * Therefore, its value will be kept constant until the end of the execution, or the custom feature extractor is added again to the 
	 * pool.
	 * 
	 * @param featureExtractor		the feature extractor that implements the computation algorithm.
	 */
	public void removeFeatureExtractor(FeatureExtractor featureExtractor) {
		this.inputReceiver.removeFeatureExtractor(featureExtractor);
	}
	
	/**
	 * Computes the equivalent, in Marine units, of a given distance in device units.
	 * 
	 * @param value		the distance, in device units.
	 * @return			the same distance, Marine units.
	 */
	public float deviceUnitsToMarineUnits(float value) {
		return this.inputReceiver.getPositionListenerPixelPerUnitRatio() * value;
	}
	
	/*
	 * PGraphics Management
	 */
	
	/**
	 * Opens a performance file, as a {@link Performance} instance, loading it to the local variable.
	 * 
	 * @param fileUrl	the complete path of the graphics to open.
	 */
	public void openPerformance(String fileUrl) {
		this.newPerformance();
		this.performanceManager.openPerformance(this.performance, fileUrl);
		this.elementsTableChanged = true;
	}
	
	/**
	 * Persists the current performance to the given file name.
	 * 
	 * @param fileUrl	the complete path of the file to persist the current graphics in.
	 */
	public void savePerformance(String fileUrl) {
		this.performanceManager.savePerformance(this.performance, fileUrl);
	}
	
	/*
	 * Calibration Methods
	 * 
	 * Calibration parameters are divided into two groups: input device parameters and projection parameters. For input device, parameters
	 * are:
	 * 
	 * 1. Angle: the angle of the input device in relation to the horizontal line between the downstage and the upstage; sometimes it is
	 * called input device alignment.
	 * 
	 * 2. Translation: any translation to be applied to the default coordinates of the input device; this allows to move
	 * the (0,0,0) point (from which the projection planes are defined) forward, backward, upward and downward.
	 * 
	 * 3. Pixels per Unit Ratio (PPU):  the relation between the input device unit and screen unit; the amount of pixels for each input
	 * device measurement unit; Kinect measurement unit is equivalent to 1 meter, so PPU represents the amount of pixels to use for each
	 * meter in real world coordinates.
	 * 
	 * Two projection parameters can be adjusted, which are:
	 * 
	 * 4. Camera Distance: the camera distance between the projection plane (0,0,0) and the camera; moving the camera towards the projection plane
	 * may be similar to zooming in, due to perspective effect, but it will not change the objects size actually; the same applies for
	 * moving the camera away from the projection plane, which may be similar to zooming out.
	 * 
	 * 5. Projection Plane: the projection plane defines which plane will be projected on screen, by seting the axis in which the camera 
	 * is positioned and its direction; currently, only 'wall' and 'floor' projection planes are available.
	 */
	
	/**
	 * This method sets the input device angle, in radians, relative to the performer. When the input device is aligned
	 * to the performer in the horizontal axe (z axe), use 0 (zero); if the input device is aligned to the performer 
	 * in the vertical axe (y axe), use PI/2.
	 * 
	 * @param angleRad		the angle, in radians, between the performer and the device, considering that horizontal
	 * 						alignment is equivalent to 0 radians.
	 */
	/*                                                 ___
	 *                                                 ___  kinect
	 * 		    								        ^
	 *                                                  |
	 *                                                  |  90 degrees
	 *                                                  | 
	 *       _O_    0 degrees    kinect                _O_
	 *  _ _ _ |  _ _ _ _ _ _ _ _  >||         _ _ _ _ _ | _ _ _ _ _ _ _ _ _
	 *       / \    base axis                          / \    base axis
	 * 
	 */
	public void setInputDeviceAngle(float angleRad) {
		this.inputReceiver.setInputDeviceAngle(angleRad);
	}
	
	/**
	 * @return		the angle, in radians, between the performer and the device, considering that horizontal alignment is equivalent to 0 
	 * 				radians.
	 */
	public float getInputDeviceAngle() {
		return this.inputReceiver.getInputDeviceAngle();
	}
	
	/**
	 * This methods provides access to the position input device coordinates translation parameters.
	 * The framework assumes that all input devices (or the listeners implemented for them) will provide
	 * coordinates following the camera space coordinates system used by Microsoft Kinect SDK 2.0, but with
	 * X axis not mirrored (meaning X axis grows to the left of the performer, when she is looking to the device).
	 * See {@link J4KKinectListener} for a detailed explanation about default translation values. MS Kinect v2
	 * distance unit is meters. 
	 * 
	 * @param x		the x translation, in the actual device unit.
	 * @param y		the y translation, in the actual device unit.
	 * @param z		the z translation, in the actual device unit.
	 */
	public void setPositionDeviceTranslationCoordinates(float x, float y, float z) {
		this.inputReceiver.setPositionListenerTranslationCoordinates(x, y, z);
	}
	
	/**
	 * This method provides access to the position input device unit to pixels ratio.
	 * See {@link J4KKinectListener} for a detailed explanation about default ratio values.
	 * 
	 * @param pixelsPerMeter	the amount of pixels each unit will be mapped to; in practice,
	 * 							this value will be multiplied by the device unit.
	 */
	public void setPositionDevicePixelsPerUnitRatio(float pixelsPerMeter) {
		this.inputReceiver.setPositionListenerPixelsPerUnitRatio(pixelsPerMeter);
	}
	
	/**
	 * This method allows to set the position input device unit to pixels ratio by computing
	 * the ratio between the actual projection length in pixels and the equivalent device capture length.
	 *  
	 * @param projectionLength			a given projection length, in pixels;
	 * @param realWorldLength			the equivalent device capture length, in device's unit; 
	 */
	public void setInputDevicePPURatio(int projectionLength, float realWorldLength) {
		this.inputReceiver.setPositionListenerPixelsPerUnitRatio(((float)projectionLength)/realWorldLength);
	}
	
	/**
	 * Directly sets the distance of the camera to the projection plane, in the camera axis. Only positive values are accepted, as the
	 * <code>PGraphics</code> takes care of adjustments (multiplying by -1), according to the projection plan chosen.
	 *  
	 * @param distance		the distance of the camera to the projection plan, in pixels.
	 */
	public void setCameraDistance(float distance) {
		this.performance.setCameraDistance(distance);
	}
	
	/**
	 * Allows for choosing which plan will be projected (actually, the projector position, towards the floor or towards the wall).
	 * 
	 * @param plane		One of {@link ProjectionPlane#FLOOR} or {@link ProjectionPlane#WALL}, meaning where the projector is looking 
	 * 					towards.
	 */
	public void setProjectionPlane(ProjectionPlane plane) {
		this.performance.setPlane(plane);
	}
	
	/*
	 * Shortcut methods to reset, increase, decrease and set to commonly used values for all calibration parameters.
	 */
	
	
	/*
	 * 1. Angle Methods
	 */
	
	/**
	 * Shortcut to <code>setPositionDeviceAngle(0)</code>. Should be used when performer and input device are aligned in the
	 * horizontal axis (z axis).
	 */
	public void setInputDeviceHorizontalAlignment() {
		this.setInputDeviceAngle(0);
	}
	
	/**
	 * Increases input device angle by a number of radians. Device angle starts from 0 radians (horizontal alignment, device pointing to the
	 * backstage) and goes until 2*PI/4 radians. PI/4 radians indicate the input device is in the ceiling, pointing to the floor. PI radians
	 * means the input device is upside-down, in the backstage, pointing to the audience (considering an Italian stage).
	 * 
	 * @param radians		the angle to be added to the current input device angle.
	 */
	public void increaseInputDeviceAngle(float radians) {
		radians = this.inputReceiver.getInputDeviceAngle() + radians;
		if (radians < 0) radians = 0;
		if (radians > Performance.TWO_PI) radians = Performance.TWO_PI;
		this.inputReceiver.setInputDeviceAngle(radians);
	}
	
	/**
	 * Shortcut to <code>setPositionDeviceAngle(0)</code>. Should be used when performer and input device are aligned in the
	 * vertical axis (y axis).
	 */
	public void setInputDeviceVerticalAlignment() {
		this.setInputDeviceAngle((float)Math.PI/2);
	}
	
	/*
	 * 2. Translation Methods
	 */
	
	/**
	 * Shortcut for input device translation reset to default coordinates.
	 */
	public void resetProjectionPlaneCoordinates() {
		this.inputReceiver.resetPositionListenerTranslationCoordinates();
	}
	
	/**
	 * Shortcut for input device translation, which moves the scene center point forward (towards the downstage).
	 * @param value		in device units (for MS Kinect, meters)
	 */
	public void moveProjectionPlaneForward(float value) {
		float x = this.inputReceiver.getPositionListenerTranslationCoordinateX();
		float y = this.inputReceiver.getPositionListenerTranslationCoordinateY();
		float z = this.inputReceiver.getPositionListenerTranslationCoordinateZ();
		z += value;
		this.inputReceiver.setPositionListenerTranslationCoordinates(x, y, z);
	}
	
	/**
	 * Shortcut for input device translation, which moves the scene center point backward (towards the upstage), if a positive value
	 * is provided.
	 * @param value		in device units (for MS Kinect, meters)
	 */
	public void moveProjectionPlaneBackward(float value) {
		float x = this.inputReceiver.getPositionListenerTranslationCoordinateX();
		float y = this.inputReceiver.getPositionListenerTranslationCoordinateY();
		float z = this.inputReceiver.getPositionListenerTranslationCoordinateZ();
		z -= value;
		this.inputReceiver.setPositionListenerTranslationCoordinates(x, y, z);
	}
	
	/**
	 * Shortcut for input device translation, which moves the scene center point downward (towards the ground), if a positive value is 
	 * provided. 
	 * @param value		in device units (for MS Kinect, meters)
	 */
	public void moveProjectionPlaneDownward(float value) {
		float x = this.inputReceiver.getPositionListenerTranslationCoordinateX();
		float y = this.inputReceiver.getPositionListenerTranslationCoordinateY();
		float z = this.inputReceiver.getPositionListenerTranslationCoordinateZ();
		y += value;
		this.inputReceiver.setPositionListenerTranslationCoordinates(x, y, z);
	}
	
	/**
	 * Shortcut for input device translation, which moves the scene center point upward (towards the sky). 
	 * @param value		in device units (for MS Kinect, meters)
	 */
	public void moveProjectionPlaneUpward(float value) {
		float x = this.inputReceiver.getPositionListenerTranslationCoordinateX();
		float y = this.inputReceiver.getPositionListenerTranslationCoordinateY();
		float z = this.inputReceiver.getPositionListenerTranslationCoordinateZ();
		y -= value;
		this.inputReceiver.setPositionListenerTranslationCoordinates(x, y, z);
	}
	
	/*
	 * 3. Pixels per Unit Ratio Methods
	 */
	
	/**
	 * Resets the pixel per unit ratio to the default value.
	 */
	public void resetPixelPerUnitRatio() {
		this.inputReceiver.resetPositionListenerPixelsPerUnitRatio();
	}
	
	/**
	 * Shortcut to increase position listener pixels per unit ratio by a given value. Performs validation, so that the resulting
	 * value is never less than 0. If <code>value</code> results in a PPU ratio lower than zero, the PPU ratio is set to 1. To decrease
	 * PPU ratio, use negative numbers as <code>value</code>.
	 * 
	 * @param value		the value to be added to current pixel per unit ratio.
	 */
	public void increasePixelPerUnitRatio(float value) {
		float ppu = this.inputReceiver.getPositionListenerPixelPerUnitRatio();
		ppu = ppu + value;
		if (ppu <= 0) {
			ppu = 1;
		}
		this.inputReceiver.setPositionListenerPixelsPerUnitRatio(ppu);
	}
	
	/*
	 * 4. Camera Distance Methods
	 */
	
	/**
	 * Resets camera position in the current plane. Also resets the camera angle to its default value.
	 */
	public void resetCamera() {
		this.performance.resetCamera();
	}
	
	/**
	 * Shortcut method which moves the camera backwards, away from the projection plane. Although it should look like a zoom out, there is 
	 * no scale of the coordinates' system. Allows calibration of the camera distance to the projection plan.
	 * 
	 * @param value		the amount of pixels the camera must be moved in its axis.
	 */
	public void moveCameraFurther(float value) {
		this.performance.moveCameraFurther(value);
	}

	/**
	 * Shortcut method which moves the camera forwards, closer to the projection plane. Although it should look like a zoom in, there is 
	 * no scale of the coordinates' system. Allows calibration of the camera distance to the projection plan.
	 * 
	 * @param value		the amount of pixels the camera must be moved in its axis.
	 */
	public void moveCameraCloser(float value) {
		this.performance.moveCameraCloser(value);
	}
	
	/*
	 * 5. Projection Plane Methods
	 */
	
	/**
	 * Shortcut to set the projection plan to be the wall behind the performer (in the upstage). Remember that projection plane always
	 * cross the (0,0,0) coordinate, and the {@link ProjectionPlane#WALL} uses the X and Y axes. Also, the (0, 0, 0) coordinate is always 
	 * in the middle of the projection plane, no matter which plane was selected.
	 * 
	 * To move projection plane upward, downward, forward and backward, it it necessary to move the input device translation parameters.
	 * Check methods marked with the "@see" tag.
	 * 
	 * @see PerformanceFacade#moveProjectionPlaneUpward(float)
	 * @see PerformanceFacade#moveProjectionPlaneDownward(float)
	 * @see PerformanceFacade#moveProjectionPlaneBackward(float)
	 * @see PerformanceFacade#moveProjectionPlaneForward(float)
	 * 
	 */
	public void projectionPlaneWall() {
		this.performance.setPlane(ProjectionPlane.WALL);
	}
	
	/**
	 * Shortcut to set the projection plan to be parallel to the stage floor. Remember that projection plane always cross the (0,0,0) 
	 * coordinate, and the {@link ProjectionPlane#FLOOR} uses the X and Z axes. Also, the (0, 0, 0) coordinate is always 
	 * in the middle of the projection plane, no matter which plane was selected.
	 * 
	 * To move projection plane upward, downward, forward and backward, it it necessary to move the input device translation parameters.
	 * Check methods marked with the "@see" tag.
	 * 
	 * @see PerformanceFacade#moveProjectionPlaneUpward(float)
	 * @see PerformanceFacade#moveProjectionPlaneDownward(float)
	 * @see PerformanceFacade#moveProjectionPlaneBackward(float)
	 * @see PerformanceFacade#moveProjectionPlaneForward(float)
	 *  
	 */
	public void projectionPlaneFloor() {
		this.performance.setPlane(ProjectionPlane.FLOOR);
	}
	
	/*
	 * 6. Camera Perspective Methods
	 */
	
	/**
	 * Modifies the projection mode. Options are perspective projection (closer objects get bigger, more realistic), or orthogonal projection.
	 * 
	 * @param value		<code>true</code> if perspective projection should be used;
	 * 					<code>false</code> if orthogonal projection should be used;
	 */
	public void usePerspective(boolean value) {
		this.performance.setUsePerspective(value);
	}
	
	/*
	 * 7. Camera Angle Methods
	 */
	
	/**
	 * Modifies the camera wideness lens angle.
	 * 
	 * @param value		the new camera angle (in radians).
	 */
	public void setCameraLensAngle(float value) {
		this.performance.setCameraLensAngle(value);
	}
	
	/**
	 * Makes the camera angle wider.
	 * 
	 * @param value		the value to add to current camera angle; the angle will get narrower if the value is negative.
	 */
	public void widenCameraLensAngle(float value) {
		this.performance.widenCameraLensAngle(value);
	}
	
	
	/**
	 * Makes the camera angle narrower.
	 * 
	 * @param value		the value to add to current camera angle; the angle will get wider if the value is negative.
	 */
	public void narrowCameraLensAngle(float value) {
		this.performance.narrowCameraLensAngle(value);
	}
	
	/*
	 * 8. Load and Save Configuration Files
	 */
	
	private void loadCalibrationFile() {
		this.loadCalibrationFile(ConfigurationManager.getInstance().getDefaultCalibrationFileAbsolutePath());
	}
	
	/**
	 * Calibration files are properties files which contains a pair (key, value) in the format "key=value"
	 * on each line. This method reads a calibration file and applies the values found to the current calibration.
	 * 
	 * @param path		the path of the calibration file.
	 * @see java.util.Properties
	 * @see mustic.scholz.marine.core.performance.CalibrationPropertiesManager
	 */
	public void loadCalibrationFile(String path) {
		this.calibrationManager = new CalibrationPropertiesManager(path);
		this.inputReceiver.setInputDeviceAngle(this.calibrationManager.getDeviceAngle());
		float[] translation = this.calibrationManager.getTranslationCoordinates();
		this.inputReceiver.setPositionListenerTranslationCoordinates(translation[0], translation[1], translation[2]);
		this.inputReceiver.setPositionListenerPixelsPerUnitRatio(this.calibrationManager.getDevicePPURatio());
		this.performance.setCameraDistance(this.calibrationManager.getCameraDistance());
		this.performance.setCameraLensAngle(this.calibrationManager.getCameraAngle());
		this.performance.setPlane(this.calibrationManager.getProjectionPlane());
		this.performance.setUsePerspective(this.calibrationManager.isUsingPerspective());
	}
	
	/**
	 * Calibration files are properties files which contains a pair (key, value) in the format "key=value"
	 * on each line. This method saves the current calibration values to a file. In case the file does not
	 * exist, a new file is created.
	 * 
	 * @param path		the path of the calibration file.
	 * @see java.util.Properties
	 * @see mustic.scholz.marine.core.performance.CalibrationPropertiesManager
	 */
	public void saveCurrentCalibrationToFile(String path) {
		CalibrationPropertiesManager cpm = new CalibrationPropertiesManager(path);
		cpm.setDeviceAngle(this.inputReceiver.getInputDeviceAngle());
		cpm.setTranslationCoordinates(this.inputReceiver.getPositionListenerTranslationCoordinateX(), 
				this.inputReceiver.getPositionListenerTranslationCoordinateY(),
				this.inputReceiver.getPositionListenerTranslationCoordinateZ());
		cpm.setDevicePPURatio(this.inputReceiver.getPositionListenerPixelPerUnitRatio());
		cpm.setCameraDistance(this.performance.getCameraDistance());
		cpm.setCameraAngle(this.performance.getCameraLensAngle());
		cpm.setProjectionPlane(this.performance.getPlane());
		cpm.setUsingPerspective(this.performance.isUsingPerspective());
		cpm.save();
		this.calibrationManager = cpm;
	}
	
	/**
	 * @return	the calibration properties manager, so external users may have access to the calibration parameter values.
	 */
	public CalibrationPropertiesManager getCalibrationManager() {
		return this.calibrationManager;
	}
	
	/**
	 * @return	the system configurations manager, so external users may have access to the system configurations.
	 */
	public ConfigurationManager getConfigurationManager() {
		return this.configurationManager;
	}
	
	/*
	 * Getters and Setters
	 */
	
	/**
	 * @return	the instance of the current loaded graphics.
	 */
	public Performance getPerformance() {
		return this.performance;
	}
	
	/**
	 * @return the instance of the current input receiver.
	 */
	public IInputReceiver getInputReceiver() {
		return this.inputReceiver;
	}
	
	/**
	 * @return		<code>true</code> if the current state is PLAYING.
	 */
	public boolean isPlaying() {
		return this.state == PLAYING;
	}
	
	/**
	 * @return		<code>true</code> if the current state is PAUSED.
	 */
	public boolean isPaused() {
		return this.state == PAUSED;
	}
	
	/**
	 * @return		<code>true</code> if the current state is STOPPED, PREPARED, PAPPLET or IDDLE.
	 */
	public boolean isStoped() {
		return this.state == STOPPED || this.state == PREPARED || this.state == PAPPLET || this.state == INITIAL;
	}
	
	/**
	 * Checks whether the skeleton is being properly tracked by the system.
	 * 
	 * @param lastInfoTimeout		if last information about skeleton is older than <code>lastInfoTimeout</code> milliseconds, skeleton is 
	 * 								considered out of reach (not tracked).
	 * @return						<code>true</code> if recent information about the skeleton is available. <code>false</code> otherwise.
	 */
	public boolean isSkeletonTracked(long lastInfoTimeout) {
		return FeaturesPool.getInstance().getPerformerPosition(lastInfoTimeout) != null;
	}
	
	/**
	 * @return		the current "frames per second" (FPS) rate; if performance instance is null, returns 0 (zero).
	 */
	public float getFPS() {
		if (this.performance != null) {
			return this.performance.frameRate;
		}
		return 0;
	}
	
	/**
	 * Searches for the last transition in the transitions sequence. This implementation must be optimised for performance in the future.
	 * 
	 * @return	the last transition position (rightmost) in the transitions' sequence.
	 */
	//TODO Optimise for performance.
	public int getLastTransitionIndex() {
		int result = -1;
		if (this.performance != null && this.performance.getElementTable() != null 
				&& this.performance.getElementTable().getTransitions() != null) {
			Transition[] transitions = this.performance.getElementTable().getTransitions();
			if (transitions != null) {
				for (int i = transitions.length-1; i >= 0; i--) {
					if (transitions[i] != null) {
						result = i;
						break;
					}
				}
			}
		}
		return result;
	}
	
	/**
	 * Searches for the last element in the elements' table. This implementation must be optimised for performance in the future.
	 * 
	 * @return	the last element position (rightmost), in the elements' table.
	 */
	//TODO Optimise for performance.
	public int getLastElementIndex() {
		int result = -1;
		if (this.performance != null && this.performance.getElementTable() != null 
				&& this.performance.getElementTable().getElements() != null) {
			CustomElement[][] elements = this.performance.getElementTable().getElements();
			int maxElements = this.performance.getElementTable().getMaxElementsPerTrack();
			int maxTracks = this.performance.getElementTable().getMaxTracks();
			if (elements != null) {
				elementsLoop:
				for (int pos = maxElements - 1; pos >= 0; pos--) {
					for (int track = 0; track < maxTracks; track++) {
						if (elements[track][pos] != null) {
							result = pos;
							break elementsLoop;
						}
					}
				}
			}
		}
		return result;
	}
}
