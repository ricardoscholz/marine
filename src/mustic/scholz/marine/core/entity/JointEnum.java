package mustic.scholz.marine.core.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Enumeration of joints in the human body. Joints have been mapped and named in accordance with Microsoft Kinect v2 convention.
 * JointEnums are saved into a hash map, so retrieval by string id tends to be faster.
 * 
 * @author Ricardo Scholz
 */
public enum JointEnum {

	LEFT_TOE ("LeftToe", "Left Toe", 0),
	RIGHT_TOE ("RightToe", "Right Toe", 1),
	LEFT_ANKLE ("LeftAnkle", "Left Ankle", 2),
	RIGHT_ANKLE ("RightAnkle", "Right Ankle", 3),
	LEFT_KNEE ("LeftKnee", "Left Knee", 4),
	RIGHT_KNEE ("RightKnee", "Right Knee", 5),
	LEFT_HIP ("LeftHip", "Left Hip", 6),
	RIGHT_HIP ("RightHip", "Right Hip", 7),
	HIPS ("Hips", "Hips", 8),
	ABDOMEN ("Abdomen", "Abdomen", 9),
	LEFT_COLLAR ("LeftCollar", "Left Collar", 10),
	RIGHT_COLLAR ("RightCollar", "Right Collar", 11),
	LEFT_SHOULDER ("LeftShoulder", "Left Shoulder", 12),
	RIGHT_SHOULDER ("RightShoulder", "Right Shoulder", 13),
	LEFT_ELBOW ("LeftElbow", "Left Elbow", 14),
	RIGHT_ELBOW ("RightElbow", "Right Elbow", 15),
	LEFT_WRIST ("LeftWrist", "Left Wrist", 16),
	RIGHT_WRIST ("RightWrist", "Right Wrist", 17),
	LEFT_HAND ("LeftHand", "Left Hand", 18),
	RIGHT_HAND ("RightHand", "Right Hand", 19),
	CHEST ("Chest", "Chest", 20),
	NECK ("Neck", "Neck", 21),
	HEAD ("Head", "Head", 22);
	
	/**
	 * Total number of joints mapped.
	 */
	public static final int NUMBER_OF_JOINTS = 23;

	private static HashMap<String, JointEnum> map;
	
	private String strId;
	private String name;
	
	private Integer index;
	
	static {
		map = new HashMap<String, JointEnum>();
		map.put(LEFT_TOE.strId, LEFT_TOE);
		map.put(RIGHT_TOE.strId, RIGHT_TOE);
		map.put(LEFT_ANKLE.strId, LEFT_ANKLE);
		map.put(RIGHT_ANKLE.strId, RIGHT_ANKLE);
		map.put(LEFT_KNEE.strId, LEFT_KNEE);
		map.put(RIGHT_KNEE.strId, RIGHT_KNEE);
		map.put(LEFT_HIP.strId, LEFT_HIP);
		map.put(RIGHT_HIP.strId, RIGHT_HIP);
		map.put(HIPS.strId, HIPS);
		map.put(ABDOMEN.strId, ABDOMEN);
		map.put(LEFT_COLLAR.strId, LEFT_COLLAR);
		map.put(RIGHT_COLLAR.strId, RIGHT_COLLAR);
		map.put(LEFT_SHOULDER.strId, LEFT_SHOULDER);
		map.put(RIGHT_SHOULDER.strId, RIGHT_SHOULDER);
		map.put(LEFT_ELBOW.strId, LEFT_ELBOW);
		map.put(RIGHT_ELBOW.strId, RIGHT_ELBOW);
		map.put(LEFT_WRIST.strId, LEFT_WRIST);
		map.put(RIGHT_WRIST.strId, RIGHT_WRIST);
		map.put(LEFT_HAND.strId, LEFT_HAND);
		map.put(RIGHT_HAND.strId, RIGHT_HAND);
		map.put(CHEST.strId, CHEST);
		map.put(NECK.strId, NECK);
		map.put(HEAD.strId, HEAD);
	
	}

	private JointEnum(String strId, String name, Integer index) {
		this.strId = strId;
		this.name = name;
		this.index = index;
	}

	/*
	 * Getters and Setters
	 */
	
	/**
	 * @return		this joint's textual ID.
	 */
	public String getStrId() {
		return strId;
	}
	
	/**
	 * @return		this joint's name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return		this joint's index.
	 */
	public Integer getIndex() {
		return index;
	}

	/*
	 * Utilitary Methods
	 */
	
	/**
	 * Finds a joint by its textual ID, by looking into a hash map with
	 * all joints for faster retrieval.
	 * 
	 * @param strId		the textual ID to look for.
	 * @return			<code>null</code> if no joint is found; the joint found otherwise.
	 */
	public static JointEnum findByStrId(String strId) {
		if(strId != null) {
			return map.get(strId);
		}
		return null;
	}
	
	/**
	 * Compares two joints for equality.
	 * 
	 * @param other	the other joint to compare with this instance.
	 * @return	<code>true</code> if both joints are equal; <code>false</code> otherwise.
	 */
	public boolean equals(JointEnum other) {
		return other.getName().equalsIgnoreCase(this.name);
	}

	/**
	 * Retrieves a list of five "basic" joints: LEFT_TOE, RIGHT_TOE, LEFT_HAND, RIGHT_HAND and HEAD.
	 * 
	 * @return	a list with five basic joints.
	 */
	public static List<JointEnum> getBasicJoints() {
		
		List<JointEnum> result = new ArrayList<JointEnum>();
		
		result.add(LEFT_TOE);
		result.add(RIGHT_TOE);
		result.add(LEFT_HAND);
		result.add(RIGHT_HAND);
		result.add(HEAD);
		
		return result;
	}
	
	/**
	 * Retrieves all joints as a list.
	 * 
	 * @return	all joints, ordered bottom-up, left-right.
	 */
	public static List<JointEnum> getAsList() {
		List<JointEnum> result = new ArrayList<JointEnum>();
		
		result.add(LEFT_TOE);
		result.add(RIGHT_TOE);
		result.add(LEFT_ANKLE);
		result.add(RIGHT_ANKLE);
		result.add(LEFT_KNEE);
		result.add(RIGHT_KNEE);
		result.add(LEFT_HIP);
		result.add(RIGHT_HIP);
		result.add(HIPS);
		result.add(ABDOMEN);
		result.add(LEFT_COLLAR);
		result.add(RIGHT_COLLAR);
		result.add(LEFT_SHOULDER);
		result.add(RIGHT_SHOULDER);
		result.add(LEFT_ELBOW);
		result.add(RIGHT_ELBOW);
		result.add(LEFT_WRIST);
		result.add(RIGHT_WRIST);
		result.add(LEFT_HAND);
		result.add(RIGHT_HAND);
		result.add(CHEST);
		result.add(NECK);
		result.add(HEAD);
		
		return result;
	}
	
	/**
	 * Retrieves a joint by its numerical index. This is a sequential search and may be slower than retrieving by textual ID.
	 * 
	 * @param index		the joint index.
	 * @return			the joint with the given index; or <code>null</code> if not found.
	 */
	public static JointEnum getByIndex(int index) {
		List<JointEnum> allJoints = JointEnum.getAsList();
		for (JointEnum joint : allJoints) {
			if (joint.getIndex() == index) {
				return joint;
			}
		}
		return null;
	}
}
