package mustic.scholz.marine.core.entity;

import mustic.scholz.marine.core.feature.JointPosition;

/**
 * Lightweight class to hold a performer position. This version takes into account only the three-dimensional coordinates of each joint 
 * position.
 * 
 * @author Ricardo Scholz
 */
public class PerformerPosition {

	private JointPosition[] positions;
	private long timestamp;
	
	private JointPosition leftmost;
	private JointPosition rightmost;
	private JointPosition foremost;
	private JointPosition backmost;
	private JointPosition uppermost;
	private JointPosition lowermost;
	
	private float bodyOrientation;
	
	/**
	 * Default constructor. Creates the internal <code>JointPosition</code> array.
	 */
	public PerformerPosition() {
		positions = new JointPosition[JointEnum.NUMBER_OF_JOINTS];
	}

	/**
	 * Overwrites a joint three-dimensional position with the given new data.
	 * 
	 * @param position	the new 3D position of the joint.
	 */
	public void setPosition(JointPosition position) {
		if (position != null) {
			this.positions[position.getJoint().getIndex()] = position;
			this.updateBoundaries(position);
		}
	}
	
	/**
	 * Retrieves a given joint position.
	 * 
	 * @param joint		the joint to retrieve the position.
	 * @return			the three-dimensional position of the joint.
	 */
	public JointPosition getPosition(JointEnum joint) {
		if (joint != null) {
			return this.positions[joint.getIndex()];
		}
		return null;
	}
	
	/**
	 * @return an array with all joints positions of this performer.
	 */
	public JointPosition[] getPositions() {
		return positions;
	}

	/**
	 * Modifies a set of joint positions with a single method call. Shortcut to multiple calls of
	 * {@link PerformerPosition#setPosition(JointPosition)}.
	 * 
	 * @param positions	an array with the new positions to overwrite.
	 */
	public void setPositions(JointPosition[] positions) {
		for (JointPosition position : positions) {
			this.setPosition(position);
		}
	}
	
	/**
	 * Modifies the body orientation of the performer. Values must be between -180 and 180, in degrees. The negative z axis direction 
	 * (performer looking to the audience) is taken as 0 degrees. The positive x axis direction is taken as 90 degrees, while the negative x
	 * axis direction is taken as -90 degrees.
	 * 
	 * @param bodyOrientation	the new body orientation, in degrees.
	 */
	public void setBodyOrientation(float bodyOrientation) {
		this.bodyOrientation = bodyOrientation;
	}
	
	/**
	 * @return		the body orientation of the performer, in degrees, between -180 and 180.
	 */
	public float getBodyOrientation() {
		return this.bodyOrientation;
	}
	
	/**
	 * Modifies the time stamp of the data in this instance.
	 * @param timestamp		the timestamp of this position information.
	 */
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	/**
	 * @return	the time stamp of the data in this instance.
	 */
	public long getTimestamp() {
		return this.timestamp;
	}
	
	/**
	 * @return	this performer leftmost position (lower X axis joint position);
	 */
	public JointPosition getLeftmost() {
		return leftmost;
	}

	/**
	 * @return	this performer rightmost position (greater X axis joint position);
	 */
	public JointPosition getRightmost() {
		return rightmost;
	}

	/**
	 * @return	this performer foremost position (lower Z axis joint position);
	 */
	public JointPosition getForemost() {
		return foremost;
	}

	/**
	 * @return	this performer backmost position (greater Z axis joint position);
	 */
	public JointPosition getBackmost() {
		return backmost;
	}

	/**
	 * @return	this performer uppermost position (greater Y axis joint position);
	 */
	public JointPosition getUppermost() {
		return uppermost;
	}

	/**
	 * @return	this performer lowermost position (lower Y axis joint position);
	 */
	public JointPosition getLowermost() {
		return lowermost;
	}

	private void updateBoundaries(JointPosition position) {
		this.updateLeftmostPosition(position);
		this.updateRightmostPosition(position);
		this.updateForetmostPosition(position);
		this.updateBackmostPosition(position);
		this.updateUppermostPosition(position);
		this.updateLowermostPosition(position);
	}
	
	private void updateLeftmostPosition(JointPosition position) {
		if (this.leftmost == null || position.getX() < this.leftmost.getX()) {
			this.leftmost = position;
		}
	}
	
	private void updateRightmostPosition(JointPosition position) {
		if (this.rightmost == null ||position.getX() > this.rightmost.getX()) {
			this.rightmost = position;
		}	
	}
	
	private void updateForetmostPosition(JointPosition position) {
		if (this.foremost == null ||position.getZ() < this.foremost.getZ()) {
			this.foremost = position;
		}
	}
	
	private void updateBackmostPosition(JointPosition position) {
		if (this.backmost == null ||position.getZ() > this.backmost.getZ()) {
			this.backmost = position;
		}
	}
	
	private void updateUppermostPosition(JointPosition position) {
		if (this.uppermost == null ||position.getY() > this.uppermost.getY()) {
			this.uppermost = position;
		}
	}
	
	private void updateLowermostPosition(JointPosition position) {
		if (this.lowermost == null ||position.getY() < this.lowermost.getY()) {
			this.lowermost = position;
		}
	}
}
