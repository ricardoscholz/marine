package mustic.scholz.marine.core.entity;

/**
 * Lightweight object used to hold a three-dimensional position (x, y, z).
 * 
 * @author Ricardo Scholz
 *
 */
public class PositionVector {

	private float x;
	
	private float y;
	
	private float z;
	
	/**
	 * Default constructor. Returns a new instance in the position (0, 0, 0).
	 */
	public PositionVector() {
		this(0, 0, 0);
	}
	
	/**
	 * Returns a new instance in the given position.
	 * 
	 * @param x		the x axis position.
	 * @param y		the y axis position.
	 * @param z		the z axis position.
	 */
	public PositionVector(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * @return		this instance's x position.
	 */
	public float getX() {
		return x;
	}

	/**
	 * Modifies this instance's x position.
	 * @param x		the new x position value.
	 */
	public void setX(float x) {
		this.x = x;
	}

	/**
	 * @return		this instance's y position.
	 */
	public float getY() {
		return y;
	}

	/**
	 * Modifies this instance's y position.
	 * @param y		the new y position value.
	 */
	public void setY(float y) {
		this.y = y;
	}

	/**
	 * @return		this instance's z position.
	 */
	public float getZ() {
		return z;
	}

	/**
	 * Modifies this instance's z position.
	 * @param z		the new z position value.
	 */
	public void setZ(float z) {
		this.z = z;
	}
	
}
