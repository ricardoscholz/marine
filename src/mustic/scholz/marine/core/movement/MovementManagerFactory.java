package mustic.scholz.marine.core.movement;

import mustic.scholz.marine.core.feature.custom.FeetHeadAngle;
import mustic.scholz.marine.core.feature.custom.HighestPoint;
import mustic.scholz.marine.core.feature.custom.HipsHeadAngle;
import mustic.scholz.marine.core.feature.custom.LowestPoint;
import mustic.scholz.marine.core.input.InputListener;
import mustic.scholz.marine.core.input.kinect.IPKinectListener;
import mustic.scholz.marine.core.input.kinect.J4KKinectListener;
import mustic.scholz.marine.core.input.kinect.KinectInputListener;
import mustic.scholz.marine.core.input.osc.OSCInputListener;
import mustic.scholz.marine.core.input.osc.ew.EyesWebFullBodyListener;
import mustic.scholz.marine.core.input.osc.ew.EyesWebJointListener;

/**
 * Factory pattern was applied to the movement manager, as its creation demands some external objects creation and subscription to the new
 * <code>MovementManager</code> instance.
 * 
 * @author Ricardo Scholz
 */
public class MovementManagerFactory {

	/**
	 * Creates an instance of <code>MovementManager</code>, adding some default input listeners and custom Marine features to it. The custom
	 * Marine features added are: "Feet Head Angle", "Hips Head Angle", "Highest Point" and "Lowest Point".  
	 * 
	 * @param skeletonPrepared		if set to <code>true</code>, adds a <code>J4KKinectListener</code> as input listener.
	 * @param eyesWebPrepared		if set to <code>true</code>, adds <code>EyesWebFullBodyListener</code> and 
	 * 								<code>EyesWebJointListener</code> as input listeners.
	 * @return						the new <code>MovementManager</code> instance.
	 * 
	 * @see mustic.scholz.marine.core.feature.custom.FeetHeadAngle
	 * @see mustic.scholz.marine.core.feature.custom.HipsHeadAngle
	 * @see mustic.scholz.marine.core.feature.custom.HighestPoint
	 * @see mustic.scholz.marine.core.feature.custom.LowestPoint
	 */
	public static MovementManager createMovementManager(boolean skeletonPrepared, boolean eyesWebPrepared) {
		MovementManager instance = new MovementManager();
		if (skeletonPrepared) {
			addKinectInputListener(instance);
		}
		if (eyesWebPrepared) {
			addEyesWebInputListener(instance);
		}
		instance.addFeatureExtractor(new FeetHeadAngle());
		instance.addFeatureExtractor(new HighestPoint());
		instance.addFeatureExtractor(new HipsHeadAngle());
		instance.addFeatureExtractor(new LowestPoint());
		return instance;
	}
	
	private static void addKinectInputListener(MovementManager instance) {
		InputListener inputListener = new KinectInputListener();
		
		IPKinectListener kinectListener = new J4KKinectListener();
		kinectListener.setInputListener(inputListener);
		inputListener.addListener(kinectListener);
		
		instance.addInputListener(inputListener);
		
		instance.resetPositionListenerTranslationCoordinates();
		instance.resetPositionListenerPixelsPerUnitRatio();
		instance.resetInputDeviceAngle();
	}
	
	private static void addEyesWebInputListener(MovementManager instance) {
		InputListener inputListener = new OSCInputListener();
		EyesWebFullBodyListener fullBodyListener = new EyesWebFullBodyListener();
		EyesWebJointListener jointListener = new EyesWebJointListener();
		fullBodyListener.setInputListener(inputListener);
		jointListener.setInputListener(inputListener);
		inputListener.addListener(fullBodyListener);
		inputListener.addListener(jointListener);
		
		instance.addInputListener(inputListener);
	}
}
