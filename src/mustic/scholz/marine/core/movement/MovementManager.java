package mustic.scholz.marine.core.movement;

import java.util.ArrayList;
import java.util.List;

import mustic.scholz.marine.core.entity.PerformerPosition;
import mustic.scholz.marine.core.entity.PositionVector;
import mustic.scholz.marine.core.feature.Feature;
import mustic.scholz.marine.core.feature.FeatureExtractor;
import mustic.scholz.marine.core.feature.JointPosition;
import mustic.scholz.marine.core.input.IInputReceiver;
import mustic.scholz.marine.core.input.InputListener;
import mustic.scholz.marine.core.performance.FeaturesPool;

/**
 * This implementation of <code>IInputReader</code> is initially tuned for Microsoft Kinect version 2. The default translation values and 
 * pixels per unit ratio are set to this device needs. Besides that, apparently any device can make use of this implementation.
 * 
 * @see mustic.scholz.marine.core.input.IInputReceiver
 * @author Ricardo Scholz
 */
public class MovementManager implements IInputReceiver {

	/**
	 * The default translation of input coordinates, in the x axis. Set to zero, so that point where x = 0 is in front of sensor.
	 */
	public static final float DEFAULT_TRANSLATE_X = 0f;
	
	/**
	 * The default translation of input coordinates, in the y axis. Set to zero, so that point where y = 0 is in front of sensor.
	 */
	public static final float DEFAULT_TRANSLATE_Y = 0f;
	
	/**
	 * The default translation of input coordinates, in the z axis. Set to -3.2, so that point where z = 0 is 3,2 input device units of
	 * measure away from the sensor, towards upstage (one MS Kinect v2 unit of measure corresponds to one meter).
	 */
	public static final float DEFAULT_TRANSLATE_Z = -3.2f;
	
	/**
	 * The default pixels per unit ratio. Its value is 190, meaning that, for MS Kinect v2 sensor (one unit of measure corresponding to one
	 * meter), each meter of real world coordinates will map to 190 pixels in Marine coordinates system.
	 */
	protected static final float DEFAULT_PPU_RATIO = 190;
	
	private List<InputListener> inputListeners;
	private List<FeatureExtractor> featureExtractors;
	
	private float translateX;
	private float translateY;
	private float translateZ;
	private float pixelsPerUnit;
	
	private float inputDeviceAngle;
	
	/**
	 * Protected constructor, so new instances of this class must be created by the <code>MovementManagerFactory</code>.
	 */
	protected MovementManager() {
		this.inputListeners = new ArrayList<InputListener>();
		this.featureExtractors = new ArrayList<FeatureExtractor>();
		this.pixelsPerUnit = DEFAULT_PPU_RATIO;
	}
	
	/**
	 * Callback method which processes a performer position, by computing the actual position coordinates (in pixels), then updating  
	 * performer position at <code>FeaturesPool</code>. Also, for each custom feature extractor, updates the feature computation.
	 * 
	 * @param performer		the performer 
	 * @param index			the index of the performer recognized
	 */
	@Override
	public void processPerformerPosition(PerformerPosition performer, int index) {
		if (performer != null) {
			this.updateToActualPositions(performer);
			FeaturesPool.getInstance().updatePerformerPosition(performer, index);
			if (index == FeaturesPool.getInstance().getFirstPerformerIndex()) { //TODO  == 0 instead?
				for (FeatureExtractor featureExtractor : this.featureExtractors) {
					FeaturesPool.getInstance().updateFeature(featureExtractor.computeFeature(performer));
				}
			}
		}
	}
	
	public void setFirstPerformer(int index) {
		FeaturesPool.getInstance().setFirstPerformer(index);
	}
	
	/**
	 * Callback method which processes a feature message received, by updating the feature at <code>FeaturesPool</code> if it is not null. 
	 */
	@Override
	public void processFeatureMessage(Feature feature) {
		if (feature != null) {
			FeaturesPool.getInstance().updateFeature(feature);
		}
	}
	
	/**
	 * Every <code>IInputReceiver</code> implementation must be prepared to run asynchronously. This method starts the input listeners and
	 * keeps this thread in an endless empty loop, so its callback methods may be called when necessary.
	 */
	@Override
	public void run() {
		this.startInputListeners();
		while(true){}
	}
	
	/**
	 * Resets the default translation coordinates. This method sets this value to ({@link MovementManager#DEFAULT_TRANSLATE_X},
	 * {@link MovementManager#DEFAULT_TRANSLATE_Y}, {@link MovementManager#DEFAULT_TRANSLATE_Z}).
	 * 
	 * @see MovementManager#DEFAULT_TRANSLATE_X
	 * @see MovementManager#DEFAULT_TRANSLATE_Y
	 * @see MovementManager#DEFAULT_TRANSLATE_Z
	 */
	@Override
	public void resetPositionListenerTranslationCoordinates() {
		this.setPositionListenerTranslationCoordinates(
				DEFAULT_TRANSLATE_X, 
				DEFAULT_TRANSLATE_Y, 
				DEFAULT_TRANSLATE_Z);
	}
	
	/**
	 * Modifies the translation coordinates to be applied to every performer position information received by this implementation of 
	 * <code>IInputReceiver</code>. All values must be in device's units of measure.
	 * 
	 * @param x		the x-axis translation value.
	 * @param y		the y-axis translation value.
	 * @param z		the z-axis translation value.
	 */
	@Override
	public void setPositionListenerTranslationCoordinates(float x, float y, float z) {
		this.translateX = x;
		this.translateY = y;
		this.translateZ = z;
	}
	
	/**
	 * @return the x-axis translation value, in device's unit of measure.
	 */
	@Override
	public float getPositionListenerTranslationCoordinateX() {
		return this.translateX;
	}
	
	/**
	 * @return the y-axis translation value, in device's unit of measure.
	 */
	@Override
	public float getPositionListenerTranslationCoordinateY() {
		return this.translateY;
	}
	
	/**
	 * @return the z-axis translation value, in device's unit of measure.
	 */
	@Override
	public float getPositionListenerTranslationCoordinateZ() {
		return this.translateZ;
	}

	/**
	 * This method sets the pixels per unit ratio, which is the amount of pixels each device's unit of measure will be mapped into, to 
	 * {@link MovementManager#DEFAULT_PPU_RATIO}. 
	 */
	@Override
	public void resetPositionListenerPixelsPerUnitRatio() {
		this.pixelsPerUnit = DEFAULT_PPU_RATIO;
	}
	
	/**
	 * Modifies the pixels per unit ratio value.
	 * 
	 * @see MovementManager#DEFAULT_PPU_RATIO
	 */
	@Override
	public void setPositionListenerPixelsPerUnitRatio(float pixelsPerUnit) {
		this.pixelsPerUnit = pixelsPerUnit;
	}
	
	/**
	 * @return the pixels per unit ratio, which is the amount of pixels each device's unit of measure is currently being mapped into.
	 */
	@Override
	public float getPositionListenerPixelPerUnitRatio() {
		return this.pixelsPerUnit;
	}
	
	/**
	 * Resets the input device angle to zero radians. This is the angle between the device z axis and Marine z axis, when their (z, y) planes 
	 * are overlapping.
	 */
	@Override
	public void resetInputDeviceAngle() {
		this.setInputDeviceAngle(0);
	}
	
	/**
	 * Modifies the input device angle.
	 * 
	 * @param angleRad		the angle (in radians) between the device z axis and Marine z axis, when their (z, y) planes are overlapping.
	 */
	@Override
	public void setInputDeviceAngle(float angleRad) {
		this.inputDeviceAngle = angleRad;
	}
	
	/**
	 * @return	the angle (in radians) between the device z axis and Marine z axis, when their (z, y) planes are overlapping.
	 */
	@Override
	public float getInputDeviceAngle() {
		return this.inputDeviceAngle;
	}
	
	/**
	 * Adds a custom feature to the internal pool, so every performer position message received will recompute the added feature and put it 
	 * in the <code>FeaturesPool</code>.
	 * 
	 * @param extractor		the <code>FeatureExtractor</code> instance, which implements the feature's computation algorithm.
	 */
	public void addFeatureExtractor(FeatureExtractor extractor) {
		this.featureExtractors.add(extractor);
	}
	
	/**
	 * Removes a given custom feature from the internal pool. This feature will not be updated by future performer position messages 
	 * received by this instance. However, this method does not withdraw the last updated feature from the <code>FeaturesPool</code>.
	 * Therefore, its value will be kept constant until the end of the execution, or the custom feature extractor is added again to the 
	 * pool.
	 * 
	 * @param extractor		the feature extractor that implements the computation algorithm.
	 */
	public void removeFeatureExtractor(FeatureExtractor extractor) {
		this.featureExtractors.remove(extractor);
	}
	
	/**
	 * Includes a new input listener in the internal input listeners' pool, with this instance as the callback object.
	 * 
	 * @param inputListener		the input listener to be included.
	 */
	public void addInputListener(InputListener inputListener) {
		this.inputListeners.add(inputListener);
		inputListener.setCallbackProvider(this);
	}
	
	/**
	 * Removes a given input listener from the internal input listeners' pool, in case it is found.
	 * 
	 * @param inputListener		the input listener to be removed.
	 */
	public void removeInputListener(InputListener inputListener) {
		this.inputListeners.remove(inputListener);
	}
	
	private void startInputListeners() {
		for (InputListener inputListener : this.inputListeners) {
			inputListener.startListening();
		}
	}
	
	private void updateToActualPositions(PerformerPosition performer) {
		JointPosition[] joints = performer.getPositions();
		for(JointPosition joint : joints) {
			if (joint != null) {
				this.updateToActualPosition(joint.getPosition());
			}
		}
	}
	
	/*
	 * See about rotation (in portuguese): https://www.youtube.com/watch?v=kTxnH1dO2aw
	 * O = Origin
	 * Current plane: zOy
	 * New plane: z1Oy1
	 * A = angle of rotation from xOy to x1Oy1
	 * Identities:				z1		y1
	 * 					z	   cosA	  -sinA
	 * 					y	   sinA    cosA   
	 */
	private void updateToActualPosition(PositionVector position) {
		
		float x = (position.getX() + this.translateX) * this.pixelsPerUnit;
		float y = (position.getY() + this.translateY) * this.pixelsPerUnit;
		float z = (position.getZ() + this.translateZ) * this.pixelsPerUnit;
		
		float sin = (float) Math.sin(this.inputDeviceAngle);
		float cos = (float) Math.cos(this.inputDeviceAngle);
		
		float y1 = (y * cos) - (z * sin);
		float z1 = (y * sin) + (z * cos);
			
		position.setX(x);
		position.setY(y1);
		position.setZ(z1);
	}
}
