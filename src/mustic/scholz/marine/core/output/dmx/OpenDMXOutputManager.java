package mustic.scholz.marine.core.output.dmx;

import com.juanjo.openDmx.OpenDmx;

//TODO Javadoc methods.

/**
 * This class is responsable for handling and sending simple DMX messages. Usage is as follows:
 * 
 * <code>
 * OpenDMXOutputManager manager = OpenDMXOutputManager.init(universe, channels, sleepInterva);
 * ...
 * manager.setUniverse(...);
 * manager.setData(...);
 * manager.setChannels(...);
 * ...
 * manager.exit();
 * </code>
 * 
 * Universes are 0-indexed (first universe is 0). Channels are 1-indexed (first channel is 1). Sleep interval is in milliseconds.
 * 
 * @author Ricardo Scholz
 *
 */
public class OpenDMXOutputManager implements Runnable {

	public static final int MAX_CHANNELS = 512;
	private static final int DEFAULT_UNIVERSE = 0;
	
	private int channels;
	private long sleepInterval;
	private int[] data;
	private int universe;
	
	private boolean running;
	
	private OpenDMXOutputManager(int universe, int channels, long sleepInterval) {
		if(!OpenDmx.connect(OpenDmx.OPENDMX_TX)){
			throw new RuntimeException("Open Dmx widget not detected.");
		}
		this.setChannels(channels);
		this.setUniverse(universe);
		this.setSleepInterval(sleepInterval);
		this.running = true;
	}
	
	public static final OpenDMXOutputManager init(int channels, long sleepInterval) {
		return init(DEFAULT_UNIVERSE, channels, sleepInterval);
	}
	
	public static final OpenDMXOutputManager init(int universe, int channels, long sleepInterval) {
		OpenDMXOutputManager result = new OpenDMXOutputManager(universe, channels, sleepInterval);
		Thread thread = new Thread(result);
		thread.start();
		return result;
	}
	
	public void setChannels(int channels) {
		if (channels <= 0 || channels > MAX_CHANNELS) {
			throw new IllegalArgumentException("Number of channels must be between 1 and " + MAX_CHANNELS);
		}
		this.channels = channels;
		this.data = new int[this.channels];
	}
	
	public void setUniverse(int universe) {
		if (universe < 0) {
			throw new IllegalArgumentException("Universes must be equal or greater than 0.");
		}
		this.universe = universe;
	}
	
	public void setSleepInterval(long sleepInterval) {
		if (sleepInterval < 0) {
			throw new IllegalArgumentException("Sleep interval cannot be less than 0.");
		}
		this.sleepInterval = sleepInterval;
	}
	
	public void setData(int channel, int data) {
		if (channel <= 0 || channel > this.channels) {
			throw new IllegalArgumentException("Non existing channel: " + channel);
		}
		this.data[channel-1] = data;
	}
	
	public void exit() {
		this.running = false;
	}
	
	private void send() {
		try {
			//Sets up connection information
			int startChannel = this.universe * 512;
			//Fill all DMX channels
			for(int i = 0; i < this.channels; i++) {
				OpenDmx.setValue(startChannel + i, this.data[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		try {
			while(this.running) {
				this.send();
				try {
					Thread.sleep(this.sleepInterval);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} finally {
			OpenDmx.disconnect();
		}
	}

	/**
	 * Unitary test method.
	 * @throws InterruptedException
	 */
	public static final void main(String[] args) throws InterruptedException {
		OpenDMXOutputManager manager = OpenDMXOutputManager.init(0, 1, 1);
		System.out.println("Sending data to channel 1...");
		for (int i = 1; i <= 255; i++) {
			manager.setData(1, i);
			manager.send();
			Thread.sleep(200);
		}
		System.out.println("Data sent!");
		Thread.sleep(5000);
		System.out.println("Turn off channel 1...");
		manager.setData(1, 0);
		manager.send();
		Thread.sleep(1000);
		manager.exit();
		System.out.println("Exited!");
	}
}
