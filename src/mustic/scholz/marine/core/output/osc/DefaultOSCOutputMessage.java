package mustic.scholz.marine.core.output.osc;

import com.illposed.osc.OSCMessage;

/**
 * This is a simple OSC message implementation. It sends the address selector only, with no associated value.
 * 
 * @author Ricardo Scholz
 */
public class DefaultOSCOutputMessage implements OSCOutputMessage {

	/**
	 * The address selector of this message.
	 */
	protected String addressSelector;

	/**
	 * Constructor.
	 * 
	 * @param addressSelector	the address selector to be used in this message.
	 */
	public DefaultOSCOutputMessage(String addressSelector) {
		this.addressSelector = addressSelector;
	}
	
	/**
	 * @return	the address selector of this message.
	 */
	@Override
	public String getAddressSelector() {
		return this.addressSelector;
	}

	/**
	 * @return	and specific <code>OSCMessage</code> object, with no values associated but the address selector. 
	 */
	@Override
	public OSCMessage packMessage() {
		OSCMessage param = new OSCMessage(this.addressSelector); 
	    return param;
	}
}
