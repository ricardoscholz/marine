package mustic.scholz.marine.core.output.osc;

import mustic.scholz.marine.core.output.osc.DefaultOSCOutputMessage;

import com.illposed.osc.OSCMessage;

/**
 * This is a simple OSC message implementation. It sends the address selector and a float value associated.
 * 
 * @author Ricardo Scholz
 */
public class DefaultOSCFloatOutputMessage extends DefaultOSCOutputMessage {

	/**
	 * The value associated with this message.
	 */
	private float value;
	
	/**
	 * Constructor.
	 * 
	 * @param addressSelector		the address selector of this message.
	 * @param value					the value associated with the message.
	 */
	public DefaultOSCFloatOutputMessage(String addressSelector, float value) {
		super(addressSelector);
		this.value = value;
	}
	
	/**
	 * @return	the value associated with this message.
	 */
	public float getValue() {
		return this.value;
	}
	
	/**
	 * @return	and specific <code>OSCMessage</code> object, with the address selector filled and the value added as an argument.
	 */
	public OSCMessage packMessage() {
		OSCMessage param = new OSCMessage(super.addressSelector); 
	    param.addArgument(this.value); 
	    return param;
	}
	
}
