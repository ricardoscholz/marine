package mustic.scholz.marine.core.output.osc;

import com.illposed.osc.OSCMessage;

/**
 * This interface defines the behaviours a Marine OSC message must implement. Custom OSC Messages may be implemented and will work with
 * <code>OSCOutputManager</code> if they implement this interface.
 * 
 * @author Ricardo Scholz
 *
 */
public interface OSCOutputMessage {
	
	/**
	 * @return	the address selector of the message.
	 */
	public String getAddressSelector();

	/**
	 * Packs a message into an <code>OSCMessage</code> so that <code>OSCOutputManager</code> can send it. 
	 * 
	 * @return		a packed OSC message, ready to be sent.
	 */
	public OSCMessage packMessage();
}
