package mustic.scholz.marine.core.output.osc;

import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import com.illposed.osc.OSCBundle;
import com.illposed.osc.OSCPortOut;

/**
 * This class is intended to make easier to send OSC messages from Marine, encapsulating some of the logic of OSC messages building and
 * sending. If you want more control over your OSC messages, you may want to directly use the library used here, or another library. But if
 * your needs are basic, using this class might be a good approach.
 * 
 * @author Ricardo Scholz
 */
public class OSCOutputManager implements Runnable {

	private long sleepInterval;
	
	private OSCPortOut portOut;

	private List<OSCOutputMessage> messages;
	
	private String host;
	private Integer port;

	/**
	 * Builds a new instance of this OSC Output Manager.
	 */
	public OSCOutputManager() {
		super();
		this.messages = new ArrayList<OSCOutputMessage>();
		
	}
	
	/**
	 * For performance reasons, one may want to enqueue OSC messages to be sent from times to times. The sleep interval guarantees that no
	 * message will be sent before the interval is over. Messages added during the sleep interval will be enqueued to deliver in the next 
	 * execution of the method {@link OSCOutputManager#sendMessages()}, either manually or automatically (within <code>run()</code>).
	 * 
	 * @param interval		the interval, in milliseconds, between consecutive message submissions.
	 */
	public void setSleepInterval(long interval) {
		this.sleepInterval = interval;
	}
	
	/**
	 * Sends the messages in the pool and empties the pool.
	 */
	public void sendMessages() {
		try {
			OSCBundle bundle = new OSCBundle();
			for (OSCOutputMessage message : this.messages) {
			    bundle.addPacket(message.packMessage());
			}
			if (this.portOut == null) {
				this.initLocalPortOut();
			}
		    this.portOut.send(bundle);
		    this.removeMessages();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adds a message to the pool. This message will be sent in the next execution of {@link OSCOutputManager#sendMessages()}, which will
	 * normally occur after every sleep interval. However, manual calls to {@link OSCOutputManager#sendMessages()} may force messages 
	 * submission.
	 * 
	 * @param message	the message to be enqueued.
	 */
	public void addMessage(OSCOutputMessage message) {
		this.messages.add(message);
	}
	
	/**
	 * Removes all messages from the queue. After a call to {@link OSCOutputManager#sendMessages()}, this method is always called, so that
	 * messages just sent are not sent again.
	 */
	public void removeMessages() {
		this.messages = new ArrayList<OSCOutputMessage>();
	}
	
	/**
	 * Configures the target host and port to which the OSC messages will be sent.
	 * 
	 * @param host		the IP of the target machine; use 127.0.0.1 for local host.
	 * @param port		the port to which the target machine will listen.
	 */
	public void setTarget(String host, Integer port) {
		try {
			if (this.host == null || this.port == null || !this.host.equalsIgnoreCase(host) || !this.port.equals(port)) {
				this.host = host;
				this.port = port;
				this.portOut = new OSCPortOut(InetAddress.getByName(host), port);
			}
		} catch (SocketException | UnknownHostException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Runs the OSC messages submission thread. Executes an infinite loop, calling {@link OSCOutputManager#sendMessages()} and sleeping
	 * for {@link OSCOutputManager#sleepInterval} (in milliseconds).
	 */
	@Override
	public void run() {
		while(true) {
			this.sendMessages();
			try {
				Thread.sleep(this.sleepInterval);
			} catch (InterruptedException e) { }
		}
	}

	/**
	 * Free all resources.
	 */
	public void destroy() {
		this.portOut.close();
	}
	
	// Utilitary methods
	
	private void initLocalPortOut() {
		try {
			this.host = InetAddress.getLocalHost().getHostName();
			this.port = OSCPortOut.defaultSCOSCPort() + 1;
			this.portOut = new OSCPortOut(InetAddress.getLocalHost(), this.port);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
