package mustic.scholz.marine.core.output.midi;

import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;

/**
 * This class is intended to make easier to send MIDI messages from Marine, encapsulating some of the logic of MIDI messages building and
 * sending. If you want more control over your MIDI messages, you may want to directly use the library used here, or another library. But if
 * your needs are basic, using this class might be a good approach.
 * 
 * See https://docs.oracle.com/javase/tutorial/sound/MIDI-messages.html
 * @author Ricardo Scholz
 */
public class MidiOutputManager implements Runnable {
	
	private long sleepInterval;
	private Receiver receiver;
	private List<MidiMessage> messages;
	private boolean useTimestamp;
	
	/**
	 * Builds a new instance of this MIDI Output Manager.
	 * 
	 * @param useTimestamp		if <code>true</code>, uses timestamp information in every message;
	 */
	public MidiOutputManager(boolean useTimestamp) {
		try {
			this.receiver = MidiSystem.getReceiver();
		} catch (MidiUnavailableException e) {
			e.printStackTrace();
		}
		this.messages = new ArrayList<MidiMessage>();
	}
	
	/**
	 * For performance reasons, one may want to enqueue MIDI messages to be sent from times to times. The sleep interval guarantees that no
	 * message will be sent before the interval is over. Messages added during the sleep interval will be enqueued to deliver in the next 
	 * execution of the method {@link MidiOutputManager#sendMessages()}, either manually or automatically (within <code>run()</code>).
	 * 
	 * @param interval		the interval, in milliseconds, between consecutive message submissions.
	 */
	public void setSleepInterval(long interval) {
		this.sleepInterval = interval;
	}
	
	/**
	 * Adds a message to the pool. This message will be sent in the next execution of {@link MidiOutputManager#sendMessages()}, which will
	 * normally occur after every sleep interval. However, manual calls to {@link MidiOutputManager#sendMessages()} may force messages 
	 * submission.
	 * 
	 * @param message	the message to be enqueued.
	 */
	public void addMessage(MidiMessage message) {
		this.messages.add(message);
	}
	
	/**
	 * Removes all messages from the queue. After a call to {@link MidiOutputManager#sendMessages()}, this method is always called, so that
	 * messages just sent are not sent again.
	 */
	public void removeMessages() {
		this.messages.clear();
	}
	
	/**
	 * Sends the messages in the pool and empties the pool. If <code>useTimestamp</code> is true, uses the 
	 * <code>System.currentTimeMillis()</code> as time stamp for the messages sent.
	 */
	public void sendMessages() {
		if (this.receiver != null) {
			long timestamp = -1;
			if (this.useTimestamp) {
				timestamp = System.currentTimeMillis();
			}
			for (MidiMessage message : messages) {
				this.receiver.send(message, timestamp);
			}
			this.removeMessages();
		}
	}
	
	/**
	 * Runs the MIDI messages submission thread. Executes an infinite loop, calling {@link MidiOutputManager#sendMessages()} and sleeping
	 * for {@link MidiOutputManager#sleepInterval} (in milliseconds).
	 */
	@Override
	public void run() {
		while(true) {
			this.sendMessages();
			try {
				Thread.sleep(this.sleepInterval);
			} catch (InterruptedException e) { }
		}
	}
	
	/**
	 * Adds a short message to the pool. This message will be sent in the next execution of {@link MidiOutputManager#sendMessages()}, which 
	 * will normally occur after every sleep interval. However, manual calls to {@link MidiOutputManager#sendMessages()} may force messages 
	 * submission.
	 * 
	 * The example bellow sends a short message to start playing a Middle C (60), moderately loud (93), on channel 0:
	 * 
	 * <code>addShortMessage(ShortMessage.NOTE_ON, 0, 60, 93);</code>
	 * 
	 * @param status		the status of the message, as in {@link ShortMessage}.
	 * @param channel		the channel that message is directed to.
	 * @param data1			the first field of data, as in MIDI protocol.
	 * @param data2			the second field of data, as in MIDI protocol.
	 */
	public void addShortMessage(int status, int channel, int data1, int data2) {
		try {
			ShortMessage message = new ShortMessage();
			message.setMessage(status, channel, data1, data2);		
			this.messages.add(message);
		} catch (InvalidMidiDataException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Free all resources.
	 */
	public void destroy() {
		if (this.receiver != null) {
			try {
				this.receiver.close();
			} catch (Exception e) {
				//do nothing.
			}
		}
		this.receiver = null;
	}

}
