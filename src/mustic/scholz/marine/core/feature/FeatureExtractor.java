package mustic.scholz.marine.core.feature;

import mustic.scholz.marine.core.entity.PerformerPosition;

/**
 * This interface defines the contract which every custom feature must implement. Custom features are seem as feature extractors, as they
 * use input device data to extract features (semantically powerful information).
 * 
 * @author Ricardo Scholz
 *
 */
//TODO [Improvement] Upgrade to allow feature extractors look into a time window of performer positions or built-in features.
public interface FeatureExtractor {

	/**
	 * Computes a feature, according to the current performer position. A drawback of the current implementation is that feature extractors
	 * cannot look into a time window of performer positions, nor to current built-in features (for derivate features computation).
	 * 
	 * @param performerPosition		the performer position, from which the feature will be computed.
	 * @return	the feature computed.
	 */
	public Feature computeFeature(PerformerPosition performerPosition);
	
}
