package mustic.scholz.marine.core.feature.ew;

import java.io.File;
import java.io.IOException;

import mustic.scholz.marine.core.ConfigurationManager;
import mustic.scholz.marine.core.feature.IFeatureProcessor;
import mustic.scholz.marine.core.utils.FilesUtil;

/**
 * This singleton class manages the EyesWeb process execution.
 * 
 * @author Ricardo Scholz
 *
 */
public class EyesWebProcessorManager implements IFeatureProcessor {
	
	private static EyesWebProcessorManager instance;

	private Process process;
	
	private EyesWebProcessorManager() { }
	
	/**
	 * @return the singleton instance.
	 */
	public static EyesWebProcessorManager getInstance() {
		if (instance == null) {
			instance = new EyesWebProcessorManager();
		}
		return instance;
	}
	
	/**
	 * Starts EyesWeb process, using java Runtime API. EyesWeb process is started similarly to a command line start, and set to process
	 * the features patch file set in the system configuration file.
	 */
	@Override
	public void start() throws IOException {
		this.stop();
		Runtime runtime = Runtime.getRuntime();
		ConfigurationManager config = ConfigurationManager.getInstance();
		boolean externalEyesWebInstall = config.isEyesWebExternal();
		String eyesWebDir = config.getEyesWebInstallFolder();
		if (!externalEyesWebInstall) {
			eyesWebDir = FilesUtil.concatenateURL(config.getRootDirectory(), eyesWebDir);
		}
		File f = new File(eyesWebDir, config.getEyesWebExecutable());
		String commandline = "\"" + f.getAbsolutePath() + "\" \"" + config.getEyesWebFeaturesPatch() + "\"";
		this.process = runtime.exec(commandline);
	}

	/**
	 * If this instance still has the reference to the EyesWeb process, it tries to stop/destroy it.
	 */
	@Override
	public void stop() {
		if (this.process != null) {
			this.process.destroy();
		}
		this.process = null;
	}
	
	/**
	 * Considers that EyesWeb process is still running if the reference to the process is not <code>null</code>.
	 */
	@Override
	public boolean isRunning() {
		return this.process != null;
	}

}
