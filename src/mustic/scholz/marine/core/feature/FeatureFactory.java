package mustic.scholz.marine.core.feature;

import java.util.ArrayList;
import java.util.List;

import mustic.scholz.marine.core.entity.JointEnum;

/**
 * As building a feature is a very sensitive task, regarding consistency of information provided, a factory has been created to build
 * all built-in features. 
 * 
 * @author Ricardo Scholz
 *
 */
//TODO [Internationalisation] Create constants into BuiltInFeatures, as KINECT_ENERGY_NAME, to avoid direct String use in this class. 
//Study a way for internationalisation is this case: a core-lang properties file?
public class FeatureFactory {

	/*
	 * EyesWeb Features
	 */
	
	// Full Body Features
	
	/**
	 * Builds a Kinetic Energy feature instance.
	 * 
	 * @param value	the feature current value.
	 * @return	the feature instance.
	 */
	public static Feature buildKineticEnergyFeature(Float value) {
		return new Feature("Kinetic Energy", BuiltInFeatures.KINETIC_ENERGY, 0f, 1f, value, null);
	}

	/**
	 * Builds a Points Density feature instance.
	 * 
	 * @param value	the feature current value.
	 * @return	the feature instance.
	 */
	public static Feature buildPointsDensityFeature(Float value) {
		return new Feature("Points Density", BuiltInFeatures.POINTS_DENSITY, 0f, 1f, value, null);
	}
	
	/**
	 * Builds a Impulsivity Index feature instance.
	 * 
	 * @param value	the feature current value.
	 * @return	the feature instance.
	 */
	public static Feature buildImpulsivityIndexFeature(Float value) {
		return new Feature("Impulsivity Index", BuiltInFeatures.IMPULSIVITY_INDEX, 0f, 1f, value, null);
	}

	/**
	 * Builds a Contraction Index feature instance.
	 * 
	 * @param value	the feature current value.
	 * @return	the feature instance.
	 */
	public static Feature buildContractionIndexFeature(Float value) {
		return new Feature("Contraction Index", BuiltInFeatures.CONTRACTION_INDEX, 0f, 1f, value, null);
	}
	
	//FIXME [Feature] Centroid 3D: treat as a joint? include into performer?
//	public static PositionFeature buildCentroid3DFeature(Float x, Float y, Float z) {
//		return new PositionFeature("Centroid 3D", "centroid_3d", x, y, z, null);
//	}
	
	// Joint Features

	/**
	 * Builds a Smoothness Magnitude feature instance.
	 * 
	 * @param value			the feature current value.
	 * @param joint			the feature joint name.
	 * @return	the feature instance.
	 */
	public static Feature buildSmoothnessMagnitudeFeature(Float value, String joint) {
		return new Feature("Joint Smoothness Magnitude", BuiltInFeatures.JOINT_SMOOTHNESS_MAGNITUDE, 0f, 1f, value, JointEnum.findByStrId(joint));
	}
	
	/**
	 * Builds a Tangential Velocity feature instance.
	 * 
	 * @param value			the feature current value.
	 * @param joint			the feature joint name.
	 * @return	the feature instance.
	 */
	public static Feature buildTangentialVelocityFeature(Float value, String joint) {
		return new Feature("Joint Tangential Velocity", BuiltInFeatures.JOINT_TANGENTIAL_VELOCITY, 0f, 1f, value, JointEnum.findByStrId(joint));
	}
	
	/**
	 * Builds a Tangential Acceleration feature instance.
	 * 
	 * @param value			the feature current value.
	 * @param joint			the feature joint name.
	 * @return	the feature instance.
	 */
	public static Feature buildTangentialAccelerationFeature(Float value, String joint) {
		return new Feature("Joint Tangential Acceleration", BuiltInFeatures.JOINT_TANGENTIAL_ACCELERATION, 0f, 1f, value, JointEnum.findByStrId(joint));
	}
	
	/**
	 * Builds a Tangential Jerk feature instance.
	 * 
	 * @param value			the feature current value.
	 * @param joint			the feature joint name.
	 * @return	the feature instance.
	 */
	public static Feature buildTangentialJerkFeature(Float value, String joint) {
		return new Feature("Joint Tangential Jerk", BuiltInFeatures.JOINT_TANGENTIAL_JERK, 0f, 1f, value, JointEnum.findByStrId(joint));
	}
	
	/**
	 * Builds a Curvature Index 3D feature instance.
	 * 
	 * @param value			the feature current value.
	 * @param joint			the feature joint name.
	 * @return	the feature instance.
	 */
	public static Feature buildCurvatureIndex3DFeature(Float value, String joint) {
		return new Feature("Joint 3D Curvature Index", BuiltInFeatures.JOINT_CURVATURE_INDEX_3D, 0f, 1f, value, JointEnum.findByStrId(joint));
	}
	
	/**
	 * Builds a Smoothness Index feature instance.
	 * 
	 * @param value			the feature current value.
	 * @param joint			the feature joint name.
	 * @return	the feature instance.
	 */
	public static Feature buildSmoothnessIndexFeature(Float value, String joint) {
		return new Feature("Joint Smoothness Index", BuiltInFeatures.JOINT_SMOOTHNESS_INDEX, 0f, 1f, value, JointEnum.findByStrId(joint));
	}
	
	/**
	 * Builds a Directness Index feature instance.
	 * 
	 * @param value			the feature current value.
	 * @param joint			the feature joint name.
	 * @return	the feature instance.
	 */
	public static Feature buildDirectnessIndexFeature(Float value, String joint) {
		return new Feature("Joint Directness Index", BuiltInFeatures.JOINT_DIRECTNESS_INDEX, 0f, 1f, value, JointEnum.findByStrId(joint));
	}
	
	/**
	 * Builds a Directness Length feature instance.
	 * 
	 * @param value			the feature current value.
	 * @param joint			the feature joint name.
	 * @return	the feature instance.
	 */
	public static Feature buildDirectnessLengthFeature(Float value, String joint) {
		return new Feature("Joint Directness Length", BuiltInFeatures.JOINT_DIRECTNESS_LENGTH, 0f, 1f, value, JointEnum.findByStrId(joint));
	}
	
	/**
	 * Builds a Curvature 3D feature instance.
	 * 
	 * @param value			the feature current value.
	 * @param joint			the feature joint name.
	 * @return	the feature instance.
	 */
	public static Feature buildCurvature3DFeature(Float value, String joint) {
		return new Feature("Joint Curvature 3D", BuiltInFeatures.JOINT_CURVATURE_3D, 0f, 1f, value, JointEnum.findByStrId(joint));
	}
	
	/*
	 * Custom Features
	 */
	
	/**
	 * Builds a Feet-Head Angle feature instance.
	 * 
	 * @param value			the feature current value.
	 * @return	the feature instance.
	 */
	public static Feature buildFeetHeadAngleFeature(Float value) {
		return new Feature("Feet Head Angle", BuiltInFeatures.FEET_HEAD_ANGLE, 0f, 1f, value, null);
	}
	
	/**
	 * Builds a Hips-Head Angle feature instance.
	 * 
	 * @param value			the feature current value.
	 * @return	the feature instance.
	 */
	public static Feature buildHipsHeadAngleFeature(Float value) {
		return new Feature("Hips Head Angle", BuiltInFeatures.HIPS_HEAD_ANGLE, 0f, 1f, value, null);
	}
	
	/**
	 * Builds a Highest Point feature instance.
	 * 
	 * @param value	the feature current value.
	 * @return	the feature instance.
	 */
	public static Feature buildHighestPointFeature(Float value) {
		return new Feature("Highest Point", BuiltInFeatures.HIGHEST_POINT, 0f, 1f, value, null);
	}
	
	/**
	 * Builds a Lowest Point feature instance.
	 * 
	 * @param value	the feature current value.
	 * @return	the feature instance.
	 */
	public static Feature buildLowestPointFeature(Float value) {
		return new Feature("Lowest Point", BuiltInFeatures.LOWEST_POINT, 0f, 1f, value, null);
	}

	/**
	 * @return	a list of all seventeen features, built with default values (zero) and <code>null</code> joints.
	 */
	public static List<Feature> buildAllBuiltInFeatures() {
		List<Feature> result = new ArrayList<Feature>();
		result.add(buildKineticEnergyFeature(0f));
		result.add(buildPointsDensityFeature(0f));
		result.add(buildImpulsivityIndexFeature(0f));
		result.add(buildContractionIndexFeature(0f));
		result.add(buildSmoothnessMagnitudeFeature(0f, null));
		result.add(buildTangentialVelocityFeature(0f, null));
		result.add(buildTangentialAccelerationFeature(0f, null));
		result.add(buildTangentialJerkFeature(0f, null));
		result.add(buildCurvatureIndex3DFeature(0f, null));
		result.add(buildSmoothnessIndexFeature(0f, null));
		result.add(buildDirectnessIndexFeature(0f, null));
		result.add(buildDirectnessLengthFeature(0f, null));
		result.add(buildCurvature3DFeature(0f, null));
		result.add(buildFeetHeadAngleFeature(0f));
		result.add(buildHipsHeadAngleFeature(0f));
		result.add(buildHighestPointFeature(0f));
		result.add(buildLowestPointFeature(0f));
		return result;
	}
	
	/**
	 * Builds a feature by its textual ID, with a given value and a given joint.
	 * 
	 * @param id		the textual ID of the desired feature.
	 * @param value		the value of the feature.
	 * @param joint		the joint associated with this feature, if any. 
	 * @return			a new feature instance.
	 */
	public static Feature buildById(String id, float value, String joint) {
		Feature result = null;
		if (id != null) {
			if (BuiltInFeatures.KINETIC_ENERGY.equals(id)) {
				result = buildKineticEnergyFeature(value);
			} else if (BuiltInFeatures.POINTS_DENSITY.equals(id)) {
				result = buildPointsDensityFeature(value);
			} else if (BuiltInFeatures.IMPULSIVITY_INDEX.equals(id)) {
				result = buildImpulsivityIndexFeature(value);
			} else if (BuiltInFeatures.CONTRACTION_INDEX.equals(id)) {
				result = buildContractionIndexFeature(value);
			} else if (BuiltInFeatures.JOINT_SMOOTHNESS_MAGNITUDE.equals(id)) {
				result = buildSmoothnessMagnitudeFeature(value, joint);
			} else if (BuiltInFeatures.JOINT_TANGENTIAL_VELOCITY.equals(id)) {
				result = buildTangentialVelocityFeature(value, joint);
			} else if (BuiltInFeatures.JOINT_TANGENTIAL_ACCELERATION.equals(id)) {
				result = buildTangentialAccelerationFeature(value, joint);
			} else if (BuiltInFeatures.JOINT_TANGENTIAL_JERK.equals(id)) {
				result = buildTangentialJerkFeature(value, joint);
			} else if (BuiltInFeatures.JOINT_CURVATURE_INDEX_3D.equals(id)) {
				result = buildCurvatureIndex3DFeature(value, joint);
			} else if (BuiltInFeatures.JOINT_SMOOTHNESS_INDEX.equals(id)) {
				result = buildSmoothnessIndexFeature(value, joint);
			} else if (BuiltInFeatures.JOINT_DIRECTNESS_INDEX.equals(id)) {
				result = buildDirectnessIndexFeature(value, joint);
			} else if (BuiltInFeatures.JOINT_DIRECTNESS_LENGTH.equals(id)) {
				result = buildDirectnessLengthFeature(value, joint);
			} else if (BuiltInFeatures.JOINT_CURVATURE_3D.equals(id)) {
				result = buildCurvature3DFeature(value, joint);
			} else if (BuiltInFeatures.FEET_HEAD_ANGLE.equals(id)) {
				result = buildFeetHeadAngleFeature(value);
			} else if (BuiltInFeatures.HIPS_HEAD_ANGLE.equals(id)) {
				result = buildHipsHeadAngleFeature(value);
			} else if (BuiltInFeatures.HIGHEST_POINT.equals(id)) {
				result = buildHighestPointFeature(value);
			} else if (BuiltInFeatures.LOWEST_POINT.equals(id)) {
				result = buildLowestPointFeature(value);
			}
		}
		return result;
	}
}
