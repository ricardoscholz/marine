package mustic.scholz.marine.core.feature;

import mustic.scholz.marine.core.entity.JointEnum;

/**
 * This class maps a feature in a generic way. Some of its attributes may take null values, as they will not be useful for all features,
 * such as <code>minValue</code>, <code>maxValue</code> and <code>JointEnum</code>.
 * 
 * @author Ricardo Scholz
 *
 */
public class Feature {

	private String name;
	private String id;
	private Float minValue;
	private Float maxValue;
	private Float value;

	private JointEnum joint;
	
	//TODO [Improvement] Check whether this is necessary for Java Reflections or can be discarded.
	/**
	 * Empty constructor. Used by reflections API to build a feature from a binary file.
	 */
	public Feature() {
	}
	
	/**
	 * Complete constructor.
	 * 
	 * @param name			the feature name (human readable).
	 * @param id			the feature ID.
	 * @param minValue		the minimum value this feature may take, if any.
	 * @param maxValue		the maximum value this feature may reach, if any.
	 * @param value			the actual value of this feature.
	 * @param joint			the joint this feature relates to, if any.
	 */
	public Feature(String name, String id, Float minValue, Float maxValue, Float value, JointEnum joint) {
		this.name = name;
		this.id = id;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.value = value;
		this.joint = joint;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public Float getMinValue() {
		return minValue;
	}
	
	public void setMinValue(Float minValue) {
		this.minValue = minValue;
	}
	
	public Float getMaxValue() {
		return maxValue;
	}
	
	public void setMaxValue(Float maxValue) {
		this.maxValue = maxValue;
	}
	
	public Float getValue() {
		return value;
	}
	
	public void setValue(Float value) {
		this.value = value;
	}
	
	public JointEnum getJoint() {
		return joint;
	}
	
	public void setJoint(JointEnum joint) {
		this.joint = joint;
	}
	
	/**
	 * Compares two feature instances for logical equality. Instances are considered to be equal if <code>other</code> is not null,
	 * <code>other</code> is an instance of <code>Feature</code>, it's ID and it's name are equal (ignoring case) to the current instance's 
	 * ID and name, it's maximum value, minimum value and actual value are equal to the current instance's maximum value, minimum value and 
	 * actual value.
	 */
	@Override
	public boolean equals(Object other) {
		boolean result = false;
		if (other != null && other instanceof Feature) {
			Feature otherFeature = (Feature) other;
			return this.id.equalsIgnoreCase(otherFeature.id)
					&& this.name.equalsIgnoreCase(otherFeature.name)
					&& this.maxValue.equals(otherFeature.maxValue)
					&& this.minValue.equals(otherFeature.minValue)
					&& this.value.equals(otherFeature.value);
		}
		return result;
	}
}
