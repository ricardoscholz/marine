package mustic.scholz.marine.core.feature.custom;

import mustic.scholz.marine.core.entity.JointEnum;
import mustic.scholz.marine.core.entity.PerformerPosition;
import mustic.scholz.marine.core.feature.Feature;
import mustic.scholz.marine.core.feature.FeatureExtractor;
import mustic.scholz.marine.core.feature.FeatureFactory;
import mustic.scholz.marine.core.feature.JointPosition;

/**
 * This custom feature computes the angle between the head and the performer hips. Returns a normalised value.
 * 
 * @author Ricardo Scholz
 *
 */
public class HipsHeadAngle implements FeatureExtractor {
	
	/**
	 * Computes the angle between hip position and head position.
	 * Returns a value between zero and 1. If zero is returned, it means the person is exactly straight in the vertical position.
	 * Values next to 1 mean the person is upside down. Values next to 0.5 mean an horizontal position. 
	 */
	@Override
	public Feature computeFeature(PerformerPosition performerPosition) {

		JointPosition hip = performerPosition.getPosition(JointEnum.HIPS);
		JointPosition head = performerPosition.getPosition(JointEnum.HEAD);
		
		if (hip == null || head == null) {
			return FeatureFactory.buildHipsHeadAngleFeature(0f);
		}
		
		float hipX = hip.getX();
		float hipY = hip.getY();
		float hipZ = hip.getZ();
		float headX = head.getX();
		float headY = head.getY();
		float headZ = head.getZ();
		
		double sideX = headX - hipX;
		double sideZ = headZ - hipZ;
		double sideXZsqr = (sideX * sideX) + (sideZ * sideZ);
		double sideY = headY - hipY;
		double sideXYZ = Math.sqrt((sideY * sideY) + sideXZsqr);

		double angle = Math.acos(sideY/sideXYZ);
		
		if (angle > Math.PI) {
			angle = -angle;
		}
		angle %= (Math.PI*2);
		angle /= Math.PI;
		
		return FeatureFactory.buildHipsHeadAngleFeature((float) angle);
	}

}
