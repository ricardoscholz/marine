package mustic.scholz.marine.core.feature.custom;

import mustic.scholz.marine.core.entity.JointEnum;
import mustic.scholz.marine.core.entity.PerformerPosition;
import mustic.scholz.marine.core.feature.Feature;
import mustic.scholz.marine.core.feature.FeatureExtractor;
import mustic.scholz.marine.core.feature.FeatureFactory;
import mustic.scholz.marine.core.feature.JointPosition;

/**
 * This custom feature computes the angle between the head and the performer feet. If only one feet is visible by the input device, it is 
 * used to compute the final value. If both feet are visible, the average of both points is taken into the computation. Returns a normalised
 * value.
 * 
 * @author Ricardo Scholz
 *
 */
public class FeetHeadAngle implements FeatureExtractor {
	
	/**
	 * Computes the angle between average feet position ((left + right) / 2) and head position.
	 * @return 	a value between zero and 1. If zero is returned, it means the person is exactly straight in the vertical position.
	 * 			Values next to 1 mean the person is upside down. Values next to 0.5 mean an horizontal position. 
	 */
	@Override
	public Feature computeFeature(PerformerPosition performerPosition) {

		JointPosition leftFeet = performerPosition.getPosition(JointEnum.LEFT_TOE);
		JointPosition rightFeet = performerPosition.getPosition(JointEnum.RIGHT_TOE);
		JointPosition head = performerPosition.getPosition(JointEnum.HEAD);
		
		float feetX = 0f;
		float feetY = 0f;
		float feetZ = 0f;
		
		if ((leftFeet == null && rightFeet == null) || head == null) {
			return FeatureFactory.buildHipsHeadAngleFeature(0f);
		}
		
		if (leftFeet == null && rightFeet != null) {
			feetX = rightFeet.getX();
			feetY = rightFeet.getY();
			feetZ = rightFeet.getZ();
		} else if (leftFeet != null && rightFeet == null) {
			feetX = leftFeet.getX();
			feetY = leftFeet.getY();
			feetZ = leftFeet.getZ();
		} else {
			feetX = (leftFeet.getX() + rightFeet.getX()) / 2;
			feetY = (leftFeet.getY() + rightFeet.getY()) / 2;
			feetZ = (leftFeet.getZ() + rightFeet.getZ()) / 2;
		}
		float headX = head.getX();
		float headY = head.getY();
		float headZ = head.getZ();
		
		double sideX = headX - feetX;
		double sideZ = headZ - feetZ;
		double sideXZsqr = (sideX * sideX) + (sideZ * sideZ);
		double sideY = headY - feetY;
		double sideXYZ = Math.sqrt((sideY * sideY) + sideXZsqr);

		double angle = Math.acos(sideY/sideXYZ);
		
		if (angle > Math.PI) {
			angle = -angle;
		}
		angle %= (Math.PI*2);
		angle /= Math.PI;
		
		return FeatureFactory.buildFeetHeadAngleFeature((float) angle);
	}
	
}
