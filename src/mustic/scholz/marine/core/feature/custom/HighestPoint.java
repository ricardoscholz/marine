package mustic.scholz.marine.core.feature.custom;

import mustic.scholz.marine.core.entity.PerformerPosition;
import mustic.scholz.marine.core.feature.Feature;
import mustic.scholz.marine.core.feature.FeatureExtractor;
import mustic.scholz.marine.core.feature.FeatureFactory;
import mustic.scholz.marine.core.feature.JointPosition;

/**
 * Returns the maximum joint position y-axis value, as a normalised value, taken into account a minimum value and a maximum value.
 * 
 * @author Ricardo Scholz
 *
 */
public class HighestPoint implements FeatureExtractor {

	//TODO [Calibration] Highest Point custom feature min and max values. Tests with Heitor: min = -357, max = 208
	private static float MAX_VALUE = 500;
	private static float MIN_VALUE = -500;
	
	/**
	 * @return the normalised maximum y-axis value received, considering {@link HighestPoint#MIN_VALUE} and {@link HighestPoint#MAX_VALUE}.
	 */
	@Override
	public Feature computeFeature(PerformerPosition performerPosition) {
		JointPosition[] positions = performerPosition.getPositions();
		float highestPoint = MIN_VALUE;
		for (JointPosition position : positions) {
			if (position != null && position.getY() > highestPoint) {
				highestPoint = position.getY();
				if (highestPoint >= MAX_VALUE) {
					highestPoint = MAX_VALUE;
					break;
				}
			}
		}
		highestPoint = (highestPoint - MIN_VALUE) / (MAX_VALUE - MIN_VALUE);

		return FeatureFactory.buildHighestPointFeature(highestPoint);
	}

}
