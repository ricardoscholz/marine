package mustic.scholz.marine.core.feature.custom;

import mustic.scholz.marine.core.entity.PerformerPosition;
import mustic.scholz.marine.core.feature.Feature;
import mustic.scholz.marine.core.feature.FeatureExtractor;
import mustic.scholz.marine.core.feature.FeatureFactory;
import mustic.scholz.marine.core.feature.JointPosition;

/**
 * Returns the minimum joint position y-axis value, as a normalised value, taken into account a minimum value and a maximum value.
 * 
 * @author Ricardo Scholz
 *
 */
public class LowestPoint implements FeatureExtractor {

	//TODO [Calibration] Lowest Point min and max values.
	private static float MAX_VALUE = 600f;
	private static float MIN_VALUE = 0f;
	
	/**
	 * @return the normalised minimum y-axis value received, considering {@link LowestPoint#MIN_VALUE} and {@link LowestPoint#MAX_VALUE}.
	 */
	@Override
	public Feature computeFeature(PerformerPosition performerPosition) {
		JointPosition[] positions = performerPosition.getPositions();
		float lowestPoint = Float.MAX_VALUE;
		for (JointPosition position : positions) {
			if (position != null && position.getY() < lowestPoint) {
				lowestPoint = position.getY();
			}
		}
		lowestPoint = ((lowestPoint - MIN_VALUE) / (MAX_VALUE - MIN_VALUE));
		
		return FeatureFactory.buildLowestPointFeature(lowestPoint);
	}
	
}
