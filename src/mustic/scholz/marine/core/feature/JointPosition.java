package mustic.scholz.marine.core.feature;

import mustic.scholz.marine.core.entity.JointEnum;
import mustic.scholz.marine.core.entity.PositionVector;

/**
 * Entity that maps a three-dimensional joint position, related to a joint and containing a time stamp.
 * 
 * @author Ricardo Scholz
 *
 */
public class JointPosition {

	private PositionVector position;
	
	private JointEnum joint;
	
	private long timestamp; 
	
	/**
	 * Returns a new instance of joint position, with the given values. Time stamp, in this case, is set to zero.
	 * 
	 * @param index		the index of the joint this instance refers to.
	 * @param x			the x axis position.
	 * @param y			the y axis position.
	 * @param z			the z axis position.
	 */
	public JointPosition (int index, float x, float y, float z) {
		this.joint = JointEnum.getByIndex(index);
		this.position = new PositionVector(x, y, z);
	}
	
	/**
	 * Returns a new instance of joint position, with the given values.
	 * 
	 * @param position	the three-dimensional position of this joint.
	 * @param joint		the index of the joint this instance refers to.
	 * @param timestamp	the time stamp of the information.
	 */
	public JointPosition(PositionVector position, JointEnum joint, long timestamp) {
		this.position = position;
		this.joint = joint;
		this.timestamp = timestamp;
	}

	public PositionVector getPosition() {
		return position;
	}

	public void setPosition(PositionVector position) {
		this.position = position;
	}

	public JointEnum getJoint() {
		return joint;
	}

	public void setJoint(JointEnum joint) {
		this.joint = joint;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	/*
	 * Shortcut methods
	 */
	
	/**
	 * Shortcut method to access this instance's position x value.
	 * 
	 * @return		this instance's position x value.
	 */
	public float getX() {
		return this.position.getX();
	}
	
	/**
	 * Shortcut method to access this instance's position y value.
	 * 
	 * @return		this instance's position y value.
	 */
	public float getY() {
		return this.position.getY();
	}
	
	/**
	 * Shortcut method to access this instance's position z value.
	 * 
	 * @return		this instance's position z value.
	 */
	public float getZ() {
		return this.position.getZ();
	}
	
}
