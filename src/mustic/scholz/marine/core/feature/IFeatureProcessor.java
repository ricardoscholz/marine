package mustic.scholz.marine.core.feature;

/**
 * This interface defines the methods a feature processor may implement. As different feature processors may be used (not only EyesWeb),
 * this is useful to allow easier extensibility of the framework for custom feature processors.
 * 
 * @author Ricardo Scholz
 *
 */
public interface IFeatureProcessor {

	/**
	 * Expected to start the feature processor thread.
	 * @throws Exception		if actual implementations need to throw any exception.
	 */
	public void start() throws Exception;
	
	/**
	 * Expected to stop the feature processor thread, killing any related process.
	 * @throws Exception		if actual implementations need to throw any exception.
	 */
	public void stop() throws Exception;
	
	/**
	 * @return <code>true</code> if the <code>start()</code> method has previously succeeded in this run, i.e., if the feature processor
	 * is still running.
	 */
	public boolean isRunning();
}
