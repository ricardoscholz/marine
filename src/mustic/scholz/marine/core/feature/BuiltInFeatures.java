package mustic.scholz.marine.core.feature;

/**
 * This abstract class holds the constants used to map all built-in features.
 * 
 * @author Ricardo Scholz
 *
 */
public abstract class BuiltInFeatures {

	/*
	 * Full Body Features - EyesWeb
	 */

	/**
	 * Full body EyesWeb feature Kinetic Energy.
	 */
	public static final String KINETIC_ENERGY = "kinetic_energy";
	
	/**
	 * Full body EyesWeb feature Points Density.
	 */
	public static final String POINTS_DENSITY = "points_density";
	
	/**
	 * Full body EyesWeb feature Impulsivity Index.
	 */
	public static final String IMPULSIVITY_INDEX = "impulsivity_index";
	
	/**
	 * Full body EyesWeb feature Contraction Index.
	 */
	public static final String CONTRACTION_INDEX = "contraction_index";
	
	//FIXME [Feature] Centroid 3D: how to map it?
	//public static final String CENTROID_3D = "centroid_3d";
		
	/*
	 * Full Body Features - Custom
	 */
	
	/**
	 * Full body Marine feature Lowest Point.
	 */
	public static final String LOWEST_POINT = "lowest_point";
	
	/**
	 * Full body Marine feature Highest Point.
	 */
	public static final String HIGHEST_POINT = "highest_point";
	
	/**
	 * Full body Marine feature Feet-Head Angle.
	 */
	public static final String FEET_HEAD_ANGLE = "feet_head_angle";
	
	/**
	 * Full body Marine feature Hips-Head Angle.
	 */
	public static final String HIPS_HEAD_ANGLE = "hips_head_angle";
		
	/*
	 * Joint Based Features - EyesWeb
	 */
	
	/**
	 * Joint based EyesWeb feature Smoothness Magnitude.
	 */
	public static final String JOINT_SMOOTHNESS_MAGNITUDE = "joint_smoothness_magnitude";
	
	/**
	 * Joint based EyesWeb feature Tangential Velocity.
	 */
	public static final String JOINT_TANGENTIAL_VELOCITY = "joint_tangential_velocity";
	
	/**
	 * Joint based EyesWeb feature Tangential Acceleration.
	 */
	public static final String JOINT_TANGENTIAL_ACCELERATION = "joint_tangential_acceleration";
	
	/**
	 * Joint based EyesWeb feature Tangential Jerk.
	 */
	public static final String JOINT_TANGENTIAL_JERK = "joint_tangential_jerk";
	
	/**
	 * Joint based EyesWeb feature Curvature Index 3D.
	 */
	public static final String JOINT_CURVATURE_INDEX_3D = "joint_3d_curvature_index";
	
	/**
	 * Joint based EyesWeb feature Smoothness Index.
	 */
	public static final String JOINT_SMOOTHNESS_INDEX = "joint_smoothness_index";
	
	/**
	 * Joint based EyesWeb feature Directness Index.
	 */
	public static final String JOINT_DIRECTNESS_INDEX = "joint_directness_index";
	
	/**
	 * Joint based EyesWeb feature Directness Length.
	 */
	public static final String JOINT_DIRECTNESS_LENGTH = "joint_directness_length";
	
	/**
	 * Joint based EyesWeb feature Curvature 3D.
	 */
	public static final String JOINT_CURVATURE_3D = "joint_curvature_3d";
	
}
