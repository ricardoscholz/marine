package mustic.scholz.marine.core.classloader;

import java.io.File;

import mustic.scholz.marine.core.ConfigurationManager;
import mustic.scholz.marine.core.utils.FilesUtil;

/**
 * This class is a Singleton which manages runtime classpath. Due to plug-ins extensibility, classpath may change in runtime. This object
 * contains a {@link PluginURLClassLoader} instance, though. Private constructor loads the plug-ins directory, as well as the current 
 * execution root directory.
 * 
 * @author Ricardo Scholz
 *
 */
public class ClassLoaderManager {

	private static ClassLoaderManager instance;
	
	private PluginURLClassLoader pluginsClassLoader;
	
	private ClassLoaderManager() {
		ConfigurationManager config = ConfigurationManager.getInstance();
		String dir = FilesUtil.concatenateURL(config.getRootDirectory(), config.getPluginsFolder());
		File pluginsDir = new File(dir);
		this.pluginsClassLoader = new PluginURLClassLoader(pluginsDir);
	}
	
	/**
	 * Singleton instance retrieval.
	 * 
	 * @return	the singleton instance.
	 */
	public static ClassLoaderManager getInstance() {
		if (instance == null) {
			instance = new ClassLoaderManager();
		}
		return instance;
	}
	
	/**
	 * @return		the plug-in class loader.
	 */
	public PluginURLClassLoader getPluginsClassLoader() {
		return this.pluginsClassLoader;
	}
}
