package mustic.scholz.marine.core.performance;

import java.util.Properties;

import mustic.scholz.marine.core.utils.FilesUtil;

/**
 * This class manages a calibration properties file. For each calibration properties file, a different instance of this class may be
 * created. This class provides methods to load the calibration properties file, as well as reading and changing its values.
 * 
 * @author Ricardo Scholz
 */
public class CalibrationPropertiesManager {

	private static final String DEVICE_ANGLE 					= "input.device.angle";
	private static final String DEVICE_PPU_RATIO 				= "input.device.pointsperunitratio";
	private static final String DEVICE_TRANSLATION_X 			= "input.device.translation.x";
	private static final String DEVICE_TRANSLATION_Y 			= "input.device.translation.y";
	private static final String DEVICE_TRANSLATION_Z 			= "input.device.translation.z";
	private static final String PROJECTION_PLANE 				= "projection.plane";
	private static final String PROJECTION_PERSPECTIVE 			= "projection.perspective";
	private static final String CAMERA_DISTANCE 				= "camera.distance";
	private static final String CAMERA_ANGLE 					= "camera.angle";
	
	private static final String PLANE_FLOOR_STR 				= "floor";
	private static final String PLANE_WALL_STR 					= "wall";
	
	private Properties prop;
	private String path;
	
	/**
	 * Constructor.
	 * 
	 * @param path		the absolute path of the calibration properties file.
	 */
	public CalibrationPropertiesManager(String path) {
		this.path = path;
		this.load();
	}
	
	/**
	 * Loads the file located at {@link #path}.
	 */
	private void load() {
		this.prop = FilesUtil.loadProperties(this.path);
	}
	
	/**
	 * Saves the file located at {@link #path}.
	 */
	public void save() {
		FilesUtil.writeProperties(this.prop, this.path);
	}
	
	/**
	 * @return		the device angle property ("input.device.angle") in the calibration file.
	 * @see CalibrationPropertiesManager#DEVICE_ANGLE
	 */
	public float getDeviceAngle() {
		return Float.parseFloat(this.prop.getProperty(DEVICE_ANGLE));
	}
	
	/**
	 * Modifies the device angle property ("input.device.angle") in the calibration file.
	 * 
	 * @param deviceAngle	the new device angle value.
	 * @see CalibrationPropertiesManager#DEVICE_ANGLE
	 */
	public void setDeviceAngle(float deviceAngle) {
		this.prop.setProperty(DEVICE_ANGLE, Float.toString(deviceAngle));
	}
	
	/**
	 * @return		the device pixels per unit of measurement property ("input.device.pointsperunitratio") in the calibration file.
	 * @see CalibrationPropertiesManager#DEVICE_PPU_RATIO
	 */
	public float getDevicePPURatio() {
		return Float.parseFloat(this.prop.getProperty(DEVICE_PPU_RATIO));
	}
	
	/**
	 * Modifies the device pixels per unit of measurement ratio property ("input.device.pointsperunitratio") in the calibration file.
	 * 
	 * @param ppuRatio		the new pixels per unit of measurement ratio.
	 * @see CalibrationPropertiesManager#DEVICE_PPU_RATIO
	 */
	public void setDevicePPURatio(float ppuRatio) {
		this.prop.setProperty(DEVICE_PPU_RATIO, Float.toString(ppuRatio));
	}
	
	/**
	 * @return		the translation coordinates property ("input.device.translation.x", "input.device.translation.y" and 
	 * "input.device.translation.z"), in this order) of the calibration file; therefore, the resulting array has always three elements,
	 * (x, y, z) on indexes (0, 1, 2).
	 * 
	 * @see CalibrationPropertiesManager#DEVICE_TRANSLATION_X
	 * @see CalibrationPropertiesManager#DEVICE_TRANSLATION_Y
	 * @see CalibrationPropertiesManager#DEVICE_TRANSLATION_Z
	 */
	public float[] getTranslationCoordinates() {
		float[] result = new float[3];
		result[0] = Float.parseFloat(this.prop.getProperty(DEVICE_TRANSLATION_X));
		result[1] = Float.parseFloat(this.prop.getProperty(DEVICE_TRANSLATION_Y));
		result[2] = Float.parseFloat(this.prop.getProperty(DEVICE_TRANSLATION_Z));
		return result;
	}
	
	/**
	 * Modifies the translation coordinates in the configuration file, ("input.device.translation.x", "input.device.translation.y" and 
	 * "input.device.translation.z").
	 *  
	 * @param x		the x axis translation coordinate.
	 * @param y		the y axis translation coordinate.
	 * @param z		the z axis translation coordinate.
	 * @see CalibrationPropertiesManager#DEVICE_TRANSLATION_X
	 * @see CalibrationPropertiesManager#DEVICE_TRANSLATION_Y
	 * @see CalibrationPropertiesManager#DEVICE_TRANSLATION_Z
	 */
	public void setTranslationCoordinates(float x, float y, float z) {
		this.prop.setProperty(DEVICE_TRANSLATION_X, Float.toString(x));
		this.prop.setProperty(DEVICE_TRANSLATION_Y, Float.toString(y));
		this.prop.setProperty(DEVICE_TRANSLATION_Z, Float.toString(z));
	}
	
	/**
	 * @return		the projection plane property ("projection.plane") in the calibration file.
	 * 
	 * @see CalibrationPropertiesManager#PROJECTION_PLANE
	 */
	public ProjectionPlane getProjectionPlane() {
		String plane = this.prop.getProperty(PROJECTION_PLANE);
		if (plane != null && plane.equalsIgnoreCase(PLANE_FLOOR_STR)) {
			return ProjectionPlane.FLOOR;
		}
		return ProjectionPlane.WALL;
	}
	
	/**
	 * Modifies the projection plane property ("projection.plane") in the calibration file.
	 * 
	 * @param plane		the new plane.
	 * @see CalibrationPropertiesManager#PROJECTION_PLANE
	 */
	public void setProjectionPlane(ProjectionPlane plane) {
		String planeStr = PLANE_WALL_STR;
		if (plane.equals(ProjectionPlane.FLOOR)) {
			planeStr = PLANE_FLOOR_STR;
		}
		this.prop.setProperty(PROJECTION_PLANE, planeStr);
	}

	/**
	 * @return		the perspective property ("projection.perspective") in the calibration file.
	 * @see CalibrationPropertiesManager#PROJECTION_PERSPECTIVE
	 */
	public boolean isUsingPerspective() {
		String perspective = this.prop.getProperty(PROJECTION_PERSPECTIVE);
		if (perspective != null && perspective.equalsIgnoreCase("true")) {
			return true;
		}
		return false;
	}
	
	/**
	 * Modifies the perspective property ("projection.perspective") in the calibration file.
	 * 
	 * @param perspective		the new perspective.
	 * @see CalibrationPropertiesManager#PROJECTION_PERSPECTIVE
	 */
	public void setUsingPerspective(boolean perspective) {
		if (perspective) {
			this.prop.setProperty(PROJECTION_PERSPECTIVE, "true");
		} else {
			this.prop.setProperty(PROJECTION_PERSPECTIVE, "false");
		}
	}
	
	/**
	 * @return		the camera distance property ("camera.distance") in the calibration file.
	 * 
	 * @see CalibrationPropertiesManager#CAMERA_DISTANCE
	 */
	public float getCameraDistance() {
		return Float.parseFloat(this.prop.getProperty(CAMERA_DISTANCE));
	}
	
	/**
	 * Modifies the camera distance property ("camera.distance") in the calibration file.
	 * 
	 * @param distance		the new camera distance.
	 * @see CalibrationPropertiesManager#CAMERA_DISTANCE
	 */
	public void setCameraDistance(float distance) {
		this.prop.setProperty(CAMERA_DISTANCE, Float.toString(distance));
	}
	
	/**
	 * @return		the device angle property ("camera.angle") in the calibration file.
	 * 
	 * @see CalibrationPropertiesManager#CAMERA_ANGLE
	 */
	public float getCameraAngle() {
		return Float.parseFloat(this.prop.getProperty(CAMERA_ANGLE));
	}

	/**
	 * Modifies the camera angle property ("camera.angle") in the calibration file.
	 * 
	 * @param angle		the new angle.
	 * @see CalibrationPropertiesManager#CAMERA_ANGLE
	 */
	public void setCameraAngle(float angle) {
		this.prop.setProperty(CAMERA_ANGLE, Float.toString(angle));
	}
}
