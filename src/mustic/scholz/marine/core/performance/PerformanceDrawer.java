package mustic.scholz.marine.core.performance;

import processing.core.PApplet;

/**
 * This class implements a number of static methods for painting, camera and coordinates management. It is intended to allow some level of
 * encapsulation of code related to these matters. 
 * 
 * @author Ricardo Scholz
 *
 */
public class PerformanceDrawer {

	private static float MAX_DEPTH_CAMERA_Z 	= -1000f;
	
	private static float DEFAULT_EYE 			= 940;
	private static float DEFAULT_LENS_ANGLE 	= PApplet.PI/6;
	
	private static float eye;
	private static float lensAngle;
	private static boolean usePerspective;
	
	static {
		init();
		usePerspective = true;
	}
	
	private static void init() {
		eye = DEFAULT_EYE;
		lensAngle = DEFAULT_LENS_ANGLE;
	}
	
	/**
	 * Method that prepares for a drawing cycle. Should be called before any drawing method, every cycle. A cycle is intended to be
	 * related to one execution of {@link PApplet#draw()}.
	 * 
	 * @param performance	the {@link Performance} instance, in which the changes will be applied.
	 */
	protected static void prePaint(Performance performance) {
		if (usePerspective) {
			performance.perspective();
			performance.lights();
		} else {
			performance.ortho();
			performance.noLights();
		}
		adjustCamera(performance);
		performance.background(performance.getBackgroundColor());
	}
	
	/*
	 * Camera Control Methods 
	 */
	
	/**
	 * @return		 a flag indicating whether <code>PApplet</code> graphic engine is using perspectives.
	 */
	public static boolean isUsingPerspective() {
		return usePerspective;
	}
	
	/**
	 * Modifies the flag which indicates whether the <code>PApplet</code> rendering algorithm should use perspectives or not.
	 * 
	 * @param value		<code>true</code> to indicate <code>PApplet</code> that perspectives should be used; <code>false</code> otherwise.
	 */
	protected static void setUsePerspective(boolean value) {
		usePerspective = value;
	}
	
	/**
	 * @return		the camera lens angle. For further details on camera lens angle, check 
	 * 				{@link PApplet#perspective(float, float, float, float)}.
	 */
	public static float getLensAngle() {
		return lensAngle;
	}
	
	/**
	 * Sets the camera lens angle. For further details on camera lens angle, check {@link PApplet#perspective(float, float, float, float)}.
	 * 
	 * @param angle		the camera lens angle.
	 */
	protected static void setLensAngle(float angle) {
		lensAngle = angle;
	}
	
	/**
	 * Sums a given value to the current camera lens angle value. The inverse behaviour happens if the value is negative.
	 * 
	 * @param value		the value to be added to the current camera lens angle.
	 */
	protected static void widenLensAngle(float value) {
		lensAngle += value;
	}
	
	/**
	 * Subtracts a given value from the current camera lens angle value. The inverse behaviour happens if the value is negative.
	 * 
	 * @param value		the value to be subtracted from the current camera lens angle.
	 */
	protected static void narrowLensAngle(float value) {
		lensAngle -= value;
	}
	
	/**
	 * @return		the current distance between the camera and the screen. For further details on eye variable, check
	 * 				{@link PApplet#camera(float, float, float, float, float, float, float, float, float)}.
	 */
	public static float getEye() {
		return eye;
	}
	
	/**
	 * Modifies the current distance between the camera and the screen. For further details on eye variable, check
	 * {@link PApplet#camera(float, float, float, float, float, float, float, float, float)}.
	 * 
	 * @param eyeDistance		the distance between the camera and the screen; only the absolute value is used; therefore, negative sign
	 * 							does not affect the result.
	 */
	protected static void setEye(float eyeDistance) {
		eye = Math.abs(eyeDistance);
	}
	
	/**
	 * Shortcut method, which moves the camera position away from the screen plane.
	 * 
	 * @param value		a number of pixels which will be summed to the current eye variable value.
	 */
	protected static void moveCameraFurther(float value) {
		eye += value;
	}
	
	/**
	 * Shortcut method, which moves the camera position closer to the screen plane.
	 * 
	 * @param value		a number of pixels which will be subtracted from the current eye variable value.
	 */
	protected static void moveCameraCloser(float value) {
		eye -= value;
	}
	
	/**
	 * Sets up the camera perspective, by calling {@link PApplet#perspective()} with current variables values.
	 *  
	 * @param performance		the performance which camera perspective will be set up.
	 * 
	 * @see PerformanceDrawer#lensAngle
	 * @see PerformanceDrawer#eye
	 * @see PerformanceDrawer#MAX_DEPTH_CAMERA_Z
	 * @see PApplet#width
	 * @see PApplet#height
	 */
	protected static void initCameraPerspective(Performance performance) {
		int camAxis = performance.getPlane().getCameraAxis();
		performance.perspective(
				lensAngle, 
				(float) performance.width / (float) performance.height, 
				eye * camAxis,
				MAX_DEPTH_CAMERA_Z * camAxis);
	}
	
	/**
	 * Resets the camera position to the default values.
	 * 
	 * @param performance		the performance to be reset.
	 * @param plane				the plane to use for reset; one of {@link ProjectionPlane#WALL} or {@link ProjectionPlane#FLOOR}.
	 */
	protected static void resetCameraPosition(Performance performance, ProjectionPlane plane) {
		init();
		if (ProjectionPlane.WALL.equals(plane)) {
			adjustCoordinatesSystemWall(performance);
		} else if (ProjectionPlane.FLOOR.equals(plane)) {
			adjustCoordinateSystemFloor(performance);
		}
	}
	
	/*
	 * Private Methods
	 */
	
	private static void adjustCamera(Performance performance) {
		
		performance.beginCamera();
		
		performance.translate(performance.displayWidth/2, performance.displayHeight/2, 0);
		
		camera(performance);
		
		if (ProjectionPlane.WALL.equals(performance.getPlane())) {
			adjustCoordinatesSystemWall(performance);
		} else if (ProjectionPlane.FLOOR.equals(performance.getPlane())) {
			adjustCoordinateSystemFloor(performance);
		} else {
			throw new IllegalArgumentException("Projection plan must be one of WALL or FLOOR.");
		}
		
		performance.endCamera();
	}
	
	private static void camera(Performance performance) {
		int camAxis = performance.getPlane().getCameraAxis();
		performance.camera(0, 0, eye * camAxis, // camera eye position
						   0, 0, 0, 			// scene centre point
						   0, 1, 0); 			// camera upward axis
	}
	
	private static void adjustCoordinatesSystemWall(Performance performance) {
		performance.rotateZ(Performance.PI);
		
	}
	
	private static void adjustCoordinateSystemFloor(Performance performance) {
		performance.rotateX(Performance.PI/2);
	}
}
