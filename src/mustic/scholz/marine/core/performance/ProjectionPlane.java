package mustic.scholz.marine.core.performance;

/**
 * A projection plane represents where the projection will take place. Currently, two options are available: floor and wall. These
 * concepts are based on Marine ordinary coordinates system, which considers the floor to be the plane (x, z) and the "wall" (an allusion
 * to an imaginary wall or screen behind or in front of the performer) to be in the plane (x, y). 
 * 
 * @author Ricardo Scholz
 *
 */
public enum ProjectionPlane {

	/**
	 * Represents the projection in an imaginary screen or "wall" in front or behind the performer, in vertical position, over the (x, y)
	 * plane.
	 */
	WALL (-1),
	
	/**
	 * Represents the projection in the floor, or in an imaginary screen parallel to it, in horizontal position, over the (x, z) plane.
	 */
	FLOOR (1);
	
	private int cameraAxis;
	
	private ProjectionPlane(int axis) {
		this.cameraAxis = axis;
	}
	
	/**
	 * @return		the side of the axis in which the camera is positioned; ordinarily it is multiplied by the 'eye' variable, in
	 * 				{@link PerformanceDrawer}.
	 */
	public int getCameraAxis() {
		return this.cameraAxis;
	}

	/**
	 * Retrieves the projection plane, given a camera axis value.
	 * 
	 * @param axis		the axis used in the query.
	 * @return			the projection plane which uses that axis; or <code>null</code> if no known projection plane uses the axis.
	 */
	public static ProjectionPlane getByCameraAxis(int axis) {
		if (axis == WALL.getCameraAxis()) {
			return WALL;
		} else if (axis == FLOOR.getCameraAxis()) {
			return FLOOR;
		}
		return null;
	}
}
