package mustic.scholz.marine.core.performance;

import mustic.scholz.marine.core.entity.PositionVector;
import mustic.scholz.marine.core.input.IInputReceiver;
import mustic.scholz.marine.core.performance.element.CustomElement;
import mustic.scholz.marine.core.performance.element.Element;
import mustic.scholz.marine.core.performance.transition.Transition;
import processing.core.PApplet;
import processing.core.PMatrix3D;

/**
 * This is one of the main classes of Marine model. It extends the <code>PApplet</code> class, which is responsible for Processing graphic
 * capabilities. It models a performance (spectacle), holding an <code>ElementTable</code>, as well as some spectacle or playback dependent 
 * variables, such as background color, projection screen or projection plane.
 * 
 * @author Ricardo Scholz
 *
 */
public class Performance extends PApplet {
	
	//TODO [Improvement] Evaluate dynamically set these variables or put them into the configuration file.
	// Ideally, table size should adapt to user needs dynamically, by itself, with no need for previous setting.
	private final int MAX_FPS 					= 30;
	private final int MAX_TRACKS 				= 4;
	private final int MAX_ELEMENTS_PER_TRACK 	= 10;
	
	private ElementTable elementsTable;
	
	private int backgroundColor;
	
	private int projectionScreen;
	
	private ProjectionPlane plane;
	
	private PMatrix3D defaultMatrix = new PMatrix3D();
	
	/**
	 * Public default constructor. Calls the custom constructor, with white background, drawing on second screen (if one is available) and
	 * using the "wall" projection plane.
	 * 
	 * <code>Performance(0xFFFFFFFF, 2, ProjectionPlane.WALL)</code>
	 */
	public Performance() {
		this(0xFFFFFFFF, 2, ProjectionPlane.WALL);
	}
	
	/**
	 * Builds a new instance of a Performance, with some custom information provided.
	 * 
	 * @param backgroundColor		the background color, in the format 0xAARRGGBB.
	 * @param projectionScreen		the projection screen (screens index start in 1).
	 * @param plane					the plane of projection ({@link ProjectionPlane#WALL} or {@link ProjectionPlane#FLOOR}).
	 */
	public Performance(int backgroundColor, int projectionScreen, ProjectionPlane plane) {
		this.elementsTable = new ElementTable(MAX_TRACKS, MAX_ELEMENTS_PER_TRACK);
		this.backgroundColor = backgroundColor;
		this.projectionScreen = projectionScreen;
		this.plane = plane;
		this.defaultMatrix = new PMatrix3D();
	}
	
	/*
	 * Applet Setup and Control Methods
	 */
	
	/**
	 * Override of <code>PApplet</code> method, it is automatically called. Executes once at the beginning of the execution. It is 
	 * important that this method does not perform any heavy processing, as its execution time has a limit.
	 */
	@Override
	public void setup() {
		super.setup();
		super.getMatrix(this.defaultMatrix);
		super.background(this.backgroundColor);
		this.elementsTable.setup();
		PerformanceDrawer.initCameraPerspective(this);
		super.frameRate(MAX_FPS);
	}
	
	/**
	 * Override of <code>PApplet</code> method, it is automatically called. . This method sets up some screen features, such as full screen 
	 * capabilities and rendered used.
	 */
	@Override
	public void settings() {
		super.fullScreen(P3D, this.projectionScreen);
	}
	
	/**
	 * Override of <code>PApplet</code> method, with a different behaviour. As original {@link PApplet#stop()} method is not guaranteed to 
	 * be called, this method is often explicitly called by the facade when appropriate. It just calls {@link Performance#reset()} and 
	 * {@link Performance#noLoop()}.
	 */
	@Override
	public void stop() {
		this.reset();
		this.fill(this.backgroundColor);
	}
	
	/**
	 * This method should be called when a performance will no longer be played, in order to free resources. It allows the element table to
	 * free its resources and, if <code>stop</code> parameter is set to <code>true</code>, it also performs a <code>PApplet</code> clean 
	 * exit.
	 * 
	 * @param stop	boolean flag indicating weather this method should also stop the <code>PApplet</code>, by calling its 
	 * 				{@link PApplet#exit()} method.
	 */
	public void destroy(boolean stop) {
		this.elementsTable.destroy();
		if (stop) {
			try {
				super.exit();
			} catch (Throwable t) {
				//OK, we're about to leave anyway...
				t.printStackTrace();
			}
		}
	}
	
	/*
	 * Animation Control Methods
	 */
	
	/**
	 * Override of <code>PApplet</code> method, it is automatically called. This method is called at every cycle, indefinitely, until 
	 * program exits or an interruption happens. Inside it, the elements are updated and painted.
	 */
	@Override
	public void draw() {
		PerformanceDrawer.prePaint(this);
		this.elementsTable.update();
	}
	
	/*
	 * User Interaction Control Methods
	 */
	
	/**
	 * Override of <code>PApplet</code> method, it is automatically called. It processes a key pressed, forwarding it to the UI controller,
	 * through {@link UIController#keyPressed(char, int)} and to the elements table, through {@link ElementTable#keyPressed(char, int)}.
	 */
	@Override
	public void keyPressed() {
		UIController.getInstance().keyPressed(super.key, super.keyCode);
		this.elementsTable.keyPressed(super.key, super.keyCode);
	}
	
	/**
	 * Commands are associated to numbers (actual {@link mustic.scholz.marine.core.PerformanceFacade} implementation associates them with numeric keys). Every element
	 * must implement the {@link Element#fireCommand(int, Object...)} method. However, some elements may ignore commands.
	 * 
	 * NOTE: this is not a good approach to allow element's runtime control, as a given command goes to all active elements, what will
	 * possibly not be the desired behaviour, when more than one command is active simultaneously. Therefore, this approach might change
	 * drastically in the next versions of Marine core library, possibly with no guarantee of backward compatibility.
	 * 
	 * @param command		the numeric command to be executed by the running elements.
	 */
	public void fireCommand(int command) {
		this.elementsTable.fireCommand(command);
	}
	
	/*
	 * Elements table delegation methods
	 */
	
	/**
	 * This method creates a new instance of the element's table, with {@link Performance#MAX_TRACKS} rows and
	 * {@link Performance#MAX_ELEMENTS_PER_TRACK} columns. Probably, future versions of this class will allow dynamically set the element's
	 * table size.
	 */
	public void newPerformance() {
		this.elementsTable = new ElementTable(MAX_TRACKS, MAX_ELEMENTS_PER_TRACK);
	}
	
	/**
	 * Creates a new instance of a performance, with custom maximum number of tracks and maximum number of elements per track. It actually
	 * creates a new {@link ElementTable} instance.
	 * 
	 * @param maxTracks					the maximum number of tracks.
	 * @param maxElementsPerTrack		the maximum number of elements per track.
	 * 
	 */
	public void newPerformance(int maxTracks, int maxElementsPerTrack) {
		this.elementsTable = new ElementTable(maxTracks, maxElementsPerTrack);
	}
	
	/**
	 * Moves the active elements column pointer by one to the right.
	 */
	public void moveForward() {
		if (super.g != null) super.noTint();
		this.elementsTable.moveForward();
	}
	
	/**
	 * Moves the active elements column pointer by one to the left.
	 */
	public void moveBackward() {
		if (super.g != null) super.noTint();
		this.elementsTable.moveBackward();
	}
	
	/**
	 * Moves the active elements column pointer to the initial position (far left). Also, sets the element's table state to "paused".
	 */
	public void reset() {
		super.tint(255, 255);
		this.elementsTable.reset();
	}
	
	/**
	 * Starts the execution of the elements which are in the active column.
	 */
	public void play() {
		this.elementsTable.play();
		this.loop();
	}
	
	/**
	 * Pauses the execution of all elements (no loop, no update, no draw).
	 */
	public void pause() {
		this.elementsTable.pause();
		this.noLoop();
	}
	
	/**
	 * Adds an element in the element's table.
	 * 
	 * @param element		the element instance to be added to the table.
	 * @param track			the track in which the element will be added (row); lower tracks are processed and painted before higher tracks.
	 * @param position		the position (column) where the element will be placed. 
	 * @param shift			a flag indicating whether the current elements in the track will be shifted right (if <code>true</code>) or any 
	 * 						element which is in the target position will be otherwise overwritten (if <code>false</code>).
	 */
	public void addElement(CustomElement element, int track, int position, boolean shift) {
		this.elementsTable.addElement(element, track, position, shift);
	}
	
	/**
	 * Removes an element from the element's table.
	 * 
	 * @param track			the track (row) of the target element.
	 * @param position		the position (column) of the target element.
	 * @param shift			a flag indicating whether the removed element's place will be kept empty (if <code>false</code>), or if the
	 * 						elements to the right of the target element will be shifted left to fill its space (if <code>true</code>).
	 * @return				the removed element, or <code>null</code> if the space was already empty.
	 */
	public CustomElement removeElement(int track, int position, boolean shift) {
		return this.elementsTable.removeElement(track, position, shift);
	} 
	
	/**
	 * Shifts elements right in a given track, from a given position (inclusive). The element at the given position will be null and last 
	 * element of the track will be lost, after executing the shift.
	 * 
	 * @param track		the track in which shift will occur.
	 * @param position	the position from which the shift starts (elements at the left of this position won't be affected).
	 */
	public void shiftRightFromElement(int track, int position) {
		this.elementsTable.shiftRightFromElement(track, position);
	}
	
	/**
	 * Shifts elements left in a given track, from a given position (inclusive). The element at the given position will be overwritten by 
	 * its right neighbor and the last element of the track will be null, after executing the shift.
	 * 
	 * @param track		the track in which shift will occur.
	 * @param position	the position from which the shift starts (elements at the left of this position won't be affected).
	 */
	public void shiftLeftFromElement(int track, int position) {
		this.elementsTable.shiftLeftFromElement(track, position);
	}

	/**
	 * Adds a transition in a given position.
	 * 
	 * @param transition	the transition to be added.
	 * @param position		the position in which the transition will be added.
	 * @param shift			if <code>true</code>, shift other transitions right after adding this transition;
	 * 						otherwise, overwrites previous transition at that position.
	 */
	public void addTransition(Transition transition, int position, boolean shift) {
		this.elementsTable.addTransition(transition, position, shift);
	}
	
	/**
	 * Adds a transition at the first null position in the element table, without shifting other transitions.
	 * 
	 * @param transition		the transition to be added.
	 */
	public void addTransition(Transition transition) {
		this.elementsTable.addTransition(transition);
	}
	
	/**
	 * Removes a transition from a given position. Always shift other transitions left after removing a given transition, as transitions
	 * sequence must not have empty spaces.
	 * 
	 * @param position	the transition to be removed.
	 * @return			the removed transition.
	 */
	public Transition removeTransition(int position) {
		return this.elementsTable.removeTransition(position);
	}
		
	/*
	 * Camera Methods
	 */
	
	/**
	 * Sets the camera distance in the {@link PerformanceDrawer}.
	 * 
	 * @param distance		a positive floating point value, indicating the distance between the screen plane and the camera, in the z axis. 
	 */
	public void setCameraDistance(float distance) {
		if (distance > 0) {
			PerformanceDrawer.setEye(distance);
		}
	}
	
	/**
	 * @return	the distance between the screen plane and the camera, in the z axis.
	 */
	public float getCameraDistance() {
		return PerformanceDrawer.getEye();
	}
	
	/**
	 * Resets the camera position to the default values.
	 * @see PerformanceDrawer#resetCameraPosition(Performance, ProjectionPlane)
	 */
	public void resetCamera() {
		PerformanceDrawer.resetCameraPosition(this, plane);
	}
	
	/**
	 * Moves camera further away from the screen plane.
	 * 
	 * @param value		the distance to move the camera.
	 */
	public void moveCameraFurther(float value) {
		PerformanceDrawer.moveCameraFurther(value);
	}
	
	/**
	 * Moves the camera closer to the screen plane.
	 * 
	 * @param value		the distance to move the camera.
	 */
	public void moveCameraCloser(float value) {
		PerformanceDrawer.moveCameraCloser(value);
	}
	
	/**
	 * Modifies the flag which indicates whether the <code>PApplet</code> rendering algorithm should use perspectives or not.
	 * 
	 * @param value		<code>true</code> to indicate <code>PApplet</code> that perspectives should be used; <code>false</code> otherwise.
	 */
	public void setUsePerspective(boolean value) {
		PerformanceDrawer.setUsePerspective(value);
	}
	
	/**
	 * Sets the camera lens angle. For further details on camera lens angle, check {@link PApplet#perspective(float, float, float, float)}.
	 * 
	 * @param angle		the camera lens angle.
	 */
	public void setCameraLensAngle(float angle) {
		PerformanceDrawer.setLensAngle(angle);
	}
	
	/**
	 * @return		the camera lens angle. For further details on camera lens angle, check 
	 * 				{@link PApplet#perspective(float, float, float, float)}.
	 */
	public float getCameraLensAngle() {
		return PerformanceDrawer.getLensAngle();
	}
	
	/**
	 * Sums a given value to the current camera lens angle value. The inverse behaviour happens if the value is negative.
	 * 
	 * @param value		the value to be added to the current camera lens angle.
	 */
	public void widenCameraLensAngle(float value) {
		PerformanceDrawer.widenLensAngle(value);
	}
	
	/**
	 * Subtracts a given value from the current camera lens angle value. The inverse behaviour happens if the value is negative.
	 * 
	 * @param value		the value to be subtracted from the current camera lens angle.
	 */
	public void narrowCameraLensAngle(float value) {
		PerformanceDrawer.narrowLensAngle(value);
	}
	
	/*
	 * Paint Auxiliary Methods
	 */
	
	/**
	 * This method sets the coordinates system to a 2D specific coordinates system, with (0, 0) point in the middle of the screen; y axis
	 * pointing upward, and x axis pointing to the right.
	 * 
	 * NOTE: Every call of this method must be followed by a call to {@link Performance#end2D()}; otherwise, <code>PApplet</code> will
	 * complain of too many calls to {@link PApplet#pushMatrix()}, and a runtime exception will be thrown.
	 */
	public void begin2D() {
		super.pushMatrix();
		super.setMatrix(this.defaultMatrix);
		super.translate(super.displayWidth/2, super.displayHeight/2);
		super.scale(1, -1);
		super.hint(DISABLE_DEPTH_TEST);
	    super.noLights();
	}
	
	/**
	 * This method restores the Marine coordinate system. It should be called when {@link Performance#begin2D()} has already been called.
	 * Calling this method with no previous call to {@link Performance#begin2D()} may throw a runtime exception or cause unpredicted
	 * behaviour.
	 */
	public void end2D() {
		super.lights();
		super.hint(ENABLE_DEPTH_TEST);
		super.popMatrix();
	}
	
	/**
	 * This method sets the coordinates system to Processing default coordinates, with (0, 0) point in the to left corner of the screen;
	 * y axis pointing downward, and x axis pointing to the right. It is possible to translate Marine coordinates to Processing coordinates
     * by calling {@link Performance#toProcessingCoordinates(PositionVector)}.
     * 
     * NOTE: Every call of this method must followed by a call to {@link Performance#endProcessing()}; otherwise, <code>PApplet</code> will
	 * complain of too many calls to {@link PApplet#pushMatrix()}, and a runtime exception will be thrown.
	 */
	public void beginProcessing() {
		super.pushMatrix();
		super.setMatrix(this.defaultMatrix);
		super.hint(DISABLE_DEPTH_TEST);
	    super.noLights();
	}
	
	/**
	 * This method restores the Marine coordinate system. It should be called when {@link Performance#beginProcessing()} has already been 
	 * called. Calling this method with no previous call to {@link Performance#beginProcessing()} may throw a runtime exception or cause 
	 * unpredicted behaviour.
	 */
	public void endProcessing() {
		super.lights();
		super.hint(ENABLE_DEPTH_TEST);
		super.popMatrix();
	}
	
	/**
	 * Translates a given Marine coordinates position vector to Processing coordinates.
	 * 
	 * @param positionVector	a point in Marine coordinates system.
	 * @return					a point in Processing coordinates system.
	 */
	public PositionVector toProcessingCoordinates(PositionVector positionVector) {
		float x = positionVector.getX() - (super.displayWidth/2);
		float y = (-positionVector.getY()) - (super.displayHeight/2);
		float z = (-positionVector.getZ());
		return new PositionVector(x, y, z);
	}
	
	/*
	 * Getters and Setters
	 */

	/**
	 * @return		 a flag indicating whether <code>PApplet</code> graphic engine is using perspectives.
	 */
	public boolean isUsingPerspective() {
		return PerformanceDrawer.isUsingPerspective();
	}
	
	/**
	 * Modifies the projection plane used by the performance.
	 * 
	 * @param plane		the new projection plane.
	 */
	public void setPlane(ProjectionPlane plane) {
		this.plane = plane;
	}
	
	/**
	 * @return		the projection plane currently used by this instance.
	 */
	public ProjectionPlane getPlane() {
		return this.plane;
	}
	
	/**
	 * Modifies the background color of this instance.
	 * 
	 * @param backgroundColor		the new background color.
	 */
	public void setBackgroundColor(int backgroundColor) {
		this.backgroundColor = backgroundColor;
	}
	
	/**
	 * @return		the background color currently used.
	 */
	public int getBackgroundColor() {
		return this.backgroundColor;
	}
	
	/**
	 * Modifies the projection screen of this instance. Projection screens are indexed by 1. Therefore, your main monitor receives the
	 * index 1. The second monitor, receives the index 2, and so on.
	 * 
	 * @param screen		the index of the screen <code>PApplet</code> will draw in.
	 */
	public void setProjectionScreen(int screen) {
		this.projectionScreen = screen;
	}

	/**
	 * @return		the index of the screen <code>PApplet</code> is currently drawing in.
	 */
	public int getProjectionScreen() {
		return this.projectionScreen;
	}
	
	/**
	 * Modifies the default coordinates matrix. This method allows to set up a previously persisted performance, through Java Generics.
	 * 
	 * @param matrix	the default coordinates system matrix.
	 */
	public void setDefaultMatrix(PMatrix3D matrix) {
		this.defaultMatrix = matrix;
	}
	
	/**
	 * @return		the default coordinates system matrix.
	 */
	public PMatrix3D getDefaultMatrix() {
		return this.defaultMatrix;
	}
	
	/**
	 * Modifies the current element table.
	 * 
	 * @param table		the new element table.
	 */
	public void setElementTable(ElementTable table) {
		this.elementsTable = table;
	}
	
	/**
	 * @return		the current element table.
	 */
	public ElementTable getElementTable() {
		return this.elementsTable;
	}

	/**
	 * Prepares the element table for execution. Sets the elements' performance instance to <code>this</code> instance, and their
	 * input receiver instance to the given input receiver. Also, starts transition listeners.
	 * 
	 * @param inputReceiver		the current input receiver which will be used by elements.
	 */
	public void prepareElementsTable(IInputReceiver inputReceiver) {
		this.elementsTable.prepareElements(this, inputReceiver);
		this.elementsTable.startTransitionListener();
	}

}
