package mustic.scholz.marine.core.performance;

import mustic.scholz.marine.core.ConfigurationManager;
import mustic.scholz.marine.core.PerformanceFacade;
import mustic.scholz.marine.core.performance.element.builtin.CalibrationAgent;
import processing.core.PApplet;

/**
 * This class is intended to encapsulate UI control. It is implemented as a singleton, and provides a set of UI processing methods. Key 
 * pressings are forwarded to this class, which takes the appropriate actions.
 * 
 * @author Ricardo Scholz
 *
 */
public class UIController {
	
	/*
	 * Currently, LENS ANGLE setup is disabled for better testing and understanding.
	 */
	
	private static final float PPU_STEP 		= 5f;	// 5 pixels per unit steps
	private static float CAMERA_STEP 			= 10f;	//10 pixels steps
	private static float PROJECTION_PLANE_STEP 	= 0.05f;// 5 centimeters steps
//	private static float LENS_ANGLE_STEP		= 2 * PApplet.DEG_TO_RAD; // 2 degrees
	private static float DEVICE_ANGLE_STEP		= 5 * PApplet.DEG_TO_RAD; // 5 degrees

	private static UIController instance;

	private boolean refreshBackground = true;
	
	/**
	 * Singleton instance retrieval.
	 * 
	 * @return		the singleton instance.
	 */
	public static UIController getInstance() {
		if (instance == null) {
			instance = new UIController();
		}
		return instance;
	}
	
	// Public Methods

	/**
	 * @return		<code>true</code> if background is refreshed at each call of {@link PApplet#draw()}; <code>false</code> otherwise.
	 */
	public boolean isRefreshBackground() {
		return this.refreshBackground;
	}

	/**
	 * Modifies the internal refresh background flag, so that background is painted or not accordingly at each call of 
	 * {@link PApplet#draw()};
	 * 
	 * @param refreshBackground		<code>true</code> if background shold be refreshed at each draw cycle; <code>false</code> otherwise.
	 */
	public void setRefreshBackground(boolean refreshBackground) {
		this.refreshBackground = refreshBackground;
	}
	
	// Keyboard Input Handling
	
	/**
	 * Key pressed processing method. Filters the keys pressed and decides what actions should be taken. Widely used by 
	 * {@link CalibrationAgent} for camera and coordinates set up, load and save. Also used by other modules for specific tasks.
	 * 
	 * ========================================================================================
	 * General Settings
	 * ----------------------------------------------------------------------------------------
	 * 0 to 9			Forwarded as fire commands to the performance instance.
	 * Key 'q'			Saves current default calibration file.
	 * Key 'w'			Loads current default calibration file.
	 * Key 'r'			Turns on/off refresh background flag.
	 * Key 'o'			Turns on/off the use of perspective ("ortogonal/perspective switch").
	 * Key 'p'			Switch between projection planes: wall and floor.
	 * 
	 * ========================================================================================
	 * Input Device / Sensor Settings
	 * ========================================================================================
	 * Input Device Angle
	 * ----------------------------------------------------------------------------------------
	 * Key 'a'			Sets input device for horizontal alignment.
	 * Key 's'			Decreases input device angle.
	 * Key 'd'			Increases input device angle.
	 * Key 'f'			Sets input device for vertical alignment.
	 * ========================================================================================
	 * Input Device Scale (Pixels per Unit Ratio)
	 * ----------------------------------------------------------------------------------------
	 * Key 'g'			Resets input device pixel per unit of measure ratio.
	 * Key 'h'			Decreases input device pixel per unit of measure ratio.
	 * Key 'j'			Increases input device pixel per unit of measure ratio.
	 * ========================================================================================
	 * Input Device Translation
	 * ----------------------------------------------------------------------------------------
	 * l				Resets input device translation settings.
	 * UP				Moves projection plane upwards.
	 * DOWN				Moves projection plane downwards.
	 * LEFT				Moves projection plane forwards.
	 * RIGHT			Moves projection plane backwards.
	 * 
	 * ========================================================================================
	 * Camera Settings
	 * ========================================================================================
	 * Camera Distance
	 * ----------------------------------------------------------------------------------------
	 * Key 'x'			Resets camera distance.
	 * Key 'c'			Move camera closer.
	 * Key 'v'			Move camera further.
	 * ========================================================================================
	 * Camera Angle (not implemented)
	 * ----------------------------------------------------------------------------------------
	 * Key 'b'			Reset camera angle.
	 * Key 'n'			Narrow camera angle.
	 * Key 'm'			Widen camera angle.
	 * 
	 * @param key		the key pressed.
	 * @param keyCode	for special keys, the key code.
	 */
	public void keyPressed (char key, int keyCode) {
		
		PerformanceFacade facade = PerformanceFacade.getInstance();
		Performance performance = facade.getPerformance();
		
		if (key == '1' || key == '2' || key == '3' || key == '4' || key == '5'
				|| key == '6' || key == '7' || key == '8' || key == '9' || key == '0') {
			int command = Integer.parseInt(String.valueOf(key));
			performance.fireCommand(command);
		}
		
		//	q	Save current calibration
		else if (key == 'q') {
			facade.saveCurrentCalibrationToFile(ConfigurationManager.getInstance().getDefaultCalibrationFileAbsolutePath());
		//	w	Save current calibration
		} else if (key == 'w') {
			facade.loadCalibrationFile(ConfigurationManager.getInstance().getDefaultCalibrationFileAbsolutePath());
		}
		
		//	r	Background refresh
		else if (key == 'r') { // refresh Background
			this.refreshBackground = !this.refreshBackground;
		}
		
		//	o	Projection mode: orthogonal / perspective
		else if (key == 'o') {
			facade.usePerspective(!performance.isUsingPerspective());
		}		
		
		//	p	Projection plane: floor / wall
		else if (key == 'p') { // switch projection Plane
			this.switchProjectionPlane();
		}
		
		//	a	Input device angle: horizontal 
		else if (key == 'a') {
			facade.setInputDeviceHorizontalAlignment();
		//	s	Input device angle: decrease
		} else if (key == 's') {
			facade.increaseInputDeviceAngle(-DEVICE_ANGLE_STEP);	
		//	d	Input device angle: increase
		} else if (key == 'd') {
			facade.increaseInputDeviceAngle(DEVICE_ANGLE_STEP);
		//	f	Input device angle: vertical
		} else if (key == 'f') {
			facade.setInputDeviceVerticalAlignment();
		}
		
		//	g	Input device scale: reset pixels per unit ratio
		else if (key == 'g') {
			facade.resetPixelPerUnitRatio();
		//	h	Input device scale: decrease pixels per unit ratio
		} else if (key == 'h') {
			facade.increasePixelPerUnitRatio(-PPU_STEP);
		//	j	Input device scale: increase pixels per unit ratio
		} else if(key == 'j') {
			facade.increasePixelPerUnitRatio(PPU_STEP);
		}
		
		//	x	Camera: reset
		else if (key == 'r') {
			facade.resetCamera();
			
		//	c	Camera distance: move closer
		} else if (key == 'c') {
			facade.moveCameraCloser(CAMERA_STEP);
		//	v	Camera distance: move further
		} else if (key == 'v') {
			facade.moveCameraFurther(CAMERA_STEP);
		} 

//		//	n	Camera angle: narrow
//		else if (key == 'n') { //Widen camera angle
//			facade.narrowCameraLensAngle(LENS_ANGLE_STEP);
//		//	m	Camera angle: widen
//		} else if (key == 'm') {
//			facade.widenCameraLensAngle(LENS_ANGLE_STEP);
//		}

		//	<	Elements table: move backward
		else if (key == ',') { // "<"
			facade.getPerformance().getElementTable().moveBackward();
		//	>	Elements table: move forward
		} else if (key == '.') { // ">"
			facade.getPerformance().getElementTable().moveForward();
		}

		//	l	Reset translation
		else if (key == 'l') {
			facade.resetProjectionPlaneCoordinates();
		}
		
		if (key == PApplet.CODED) {
			switch (keyCode) {
			case PApplet.UP:
				PerformanceFacade.getInstance().moveProjectionPlaneUpward(PROJECTION_PLANE_STEP);
				break;
			case PApplet.DOWN:
				PerformanceFacade.getInstance().moveProjectionPlaneDownward(PROJECTION_PLANE_STEP);
				break;
			case PApplet.RIGHT:
				PerformanceFacade.getInstance().moveProjectionPlaneBackward(PROJECTION_PLANE_STEP);
				break;
			case PApplet.LEFT:
				PerformanceFacade.getInstance().moveProjectionPlaneForward(PROJECTION_PLANE_STEP);
				break;
			}
		}
	}

	private void switchProjectionPlane() {
		ProjectionPlane current = PerformanceFacade.getInstance().getPerformance().getPlane();
		if (ProjectionPlane.FLOOR.equals(current)) {
			PerformanceFacade.getInstance().projectionPlaneWall();
		} else if (ProjectionPlane.WALL.equals(current)) {
			PerformanceFacade.getInstance().projectionPlaneFloor();
		}
	}
}
