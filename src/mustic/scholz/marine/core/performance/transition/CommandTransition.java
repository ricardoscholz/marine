package mustic.scholz.marine.core.performance.transition;

/**
 * This is a concrete implementation of the abstract {@link Transition} class. It fires when one of the following keys are pressed:
 * 
 * 1. Space bar
 * 2. Enter (code 10)
 * 3. Return (code 13)
 * 
 * @author Ricardo Scholz
 *
 */
public class CommandTransition extends Transition {
	
	static final char ENTER		= 10;
	static final char RETURN    = 13;
	static final char SPACE     = 32;
	
	/**
	 * Default constructor.
	 */
	//TODO Adapt to remove TransitionTypeEnum, when model changes.
	public CommandTransition() {
		super(TransitionTypeEnum.COMMAND);
	}
	
	/**
	 * This method provides a way to programmatically indicate that a move forward command has been fired, and the transition should fire.
	 * This implementation sets the {@link Transition#fired} attribute to <code>true</code>. 
	 */
	public void processMoveForwardCommand() {
		super.fired = true;
	}
	
	/**
	 * Deals with keyboard inputs. Switches the {@link Transition#fired} attribute to <code>true</code> if:
	 * <code>key == ' ' || key == 10 || key == 13</code>
	 * Key 10 is ENTER and key 13 is RETURN.
	 * 
	 *  @param key			the key pressed.
	 *  @param keyCode		for coded keys, the key code.
	 */
	@Override
	public void processKeyboardInput(char key, int keyCode) {
		if (key == SPACE || key == ENTER || key == RETURN) {
			this.fired = true;
		}
	}

	/**
	 * Empty implementation.
	 */
	@Override
	public void start() {
		//do nothing.
	}
	
	/**
	 * Empty implementation.
	 */
	@Override
	public void pause() {
		//do nothing.
	}
	
	/**
	 * Returns this transition label: 'Command'. Internationalisation is not yet available.
	 */
	//TODO Internationalize.
	public String getLabel() {
		return "Command";
	}
}
