package mustic.scholz.marine.core.performance.transition;

import mustic.scholz.marine.core.utils.TimeUtil;

/**
 * This is a concrete implementation of the abstract {@link Transition} class. It fires when a given amount of time has passed since the
 * transition has started.
 * 
 * @author Ricardo Scholz
 */
public class TimerTransition extends Transition {

	private long startTime;
	private long duration;
	private long originalDuration;
	
	/**
	 * Default constructor.
	 */
	//TODO Adapt to remove TransitionTypeEnum, when model changes.
	public TimerTransition() {
		super(TransitionTypeEnum.TIMER);
	}
	
	/**
	 * Defines how long this transition should wait until it switches to "fired", after a call to {@link Transition#start()}.
	 * 
	 * @param hours				amount of hours to wait.
	 * @param minutes			amount of minutes to wait.
	 * @param seconds			amount of seconds to wait.
	 * @param milliseconds		amount of milliseconds to wait.
	 */
	public void setDuration(int hours, int minutes, int seconds, int milliseconds) {
		this.duration = TimeUtil.getTime(hours, minutes, seconds, milliseconds);
		this.originalDuration = this.duration;
	}
	
	//TODO Javadoc.
	public void setDuration(long duration) {
		this.duration = duration;
	}
	
	public void setOriginalDuration(long duration) {
		this.originalDuration = this.duration;
	}
	
	/**
	 * Empty implementation.
	 * 
	 * @param key				the key pressed.
	 * @param keyCode			for special keys, the key code.
	 */
	@Override
	public void processKeyboardInput(char key, int keyCode) {
		//do nothing.
	}

	/**
	 * Saves the current time when the transition has started, so the duration of this transition can be computed. Uses
	 * {@link System#currentTimeMillis()} to save {@link #startTime}.
	 */
	@Override
	public void start() {
		this.startTime = System.currentTimeMillis();
	}
	
	/**
	 * Pauses this transition stopwatch.
	 */
	@Override
	public void pause() {
		long interval = System.currentTimeMillis() - this.startTime;
		this.duration = this.duration - interval;
		if (this.duration < 0) {
			this.duration = 0;
		}
	}
	
	/**
	 * Resets this transition stopwatch.
	 */
	@Override
	public void reset() {
		super.reset();
		this.duration = this.originalDuration;
	}
	
	/**
	 * Updates this transition remaining time. Remaining time is not computed in a separate thread, but updated on demand.
	 * 
	 * @return		the up to date remaining time of this transition's stopwatch.
	 */
	public long updateRemainingTime() {
		long now = System.currentTimeMillis();
		long interval = now - this.startTime;
		this.duration = this.duration - interval;
		if (this.duration < 0) {
			this.duration = 0;
		}
		this.startTime = now;
		return this.duration;
	}
	
	/**
	 * Checks whether the amount of time passed since the last call to {@link #start()} is greater than the duration interval set in the
	 * last call to {@link #setDuration(int, int, int, int)}. In case it is, returns <code>true</code>. Otherwise, returns 
	 * <code>false</code>.
	 */
	@Override
	public boolean fired() {
		long interval = System.currentTimeMillis() - this.startTime;
		return (interval > this.duration) || super.fired();
	}
	
	/**
	 * Formats this transition original duration as 'hh:mm:ss'.
	 */
	public String getLabel() {
		return TimeUtil.getFormattedTime(this.originalDuration);
	}
	
	/**
	 * Returns the last computed remaining time of this transition. Remaining time is updated every time {@link #updateRemainingTime()} is
	 * called, as well as when this transition is paused.  
	 * 
	 * @return		the last computed remaining time of this transition;
	 */
	public long getDuration() {
		return this.duration;
	}
	
	/**
	 * @return		this transition original duration (set by user).
	 */
	public long getOriginalDuration() {
		return this.originalDuration;
	}
}
