package mustic.scholz.marine.core.performance.transition;

/**
 * This enumerator has been created to ease the implementation of a prototype UI. However, as transitions architecture evolved, it is no
 * longer needed. Therefore, a future refactoring will probably remove it from the framework implementation. Therefore, avoid relying on
 * this enumerator for any feature implementation.
 * 
 * @author Ricardo Scholz
 *
 */
public enum TransitionTypeEnum {

	NONE ("", 0),
	TIMER ("Timer", 1),
	COMMAND ("Command", 2),
	GESTURE ("Gesture", 3);
	
	private String name;
	
	private Integer index;

	private TransitionTypeEnum(String name, Integer index) {
		this.name = name;
		this.index = index;
	}
	
	/**
	 * @return		the transition type name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return		the transition type index.
	 */
	public Integer getIndex() {
		return index;
	}
	
	/**
	 * Two transition types are considered to be equal when their indexes are equal.
	 * 
	 * @param other		an other transition type to be compared to this instance.
	 * @return			<code>true</code> if <code>other</code> has the same index of this instance.
	 */
	public boolean equals(TransitionTypeEnum other) {
		return other != null && other.getIndex().equals(this.index);
	}

	/**
	 * Retrieves a transition type instance by its index.
	 * 
	 * @param index		the index of the transition to be retrieved.
	 * @return			an instance of a transition or <code>null</code> if there is no transition with such <code>index</code>. 
	 */
	public static TransitionTypeEnum getByIndex(int index) {
		TransitionTypeEnum result = null;
		if (index == NONE.index) {
			result = NONE;
		} else if (index == TIMER.index) {
			result = TIMER;
		} else if (index == COMMAND.index) {
			result = COMMAND;
		} else if (index == GESTURE.index) {
			result = GESTURE;
		}
		return result;
	}
}
