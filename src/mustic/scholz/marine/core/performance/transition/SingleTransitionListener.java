package mustic.scholz.marine.core.performance.transition;

import java.util.ArrayList;
import java.util.List;

import mustic.scholz.marine.core.ConfigurationManager;
import mustic.scholz.marine.core.performance.ITransitionCallbackProcessor;
import mustic.scholz.marine.core.performance.ITransitionListener;

/**
 * This implementation of {@link ITransitionListener} allows only one transition and one callback processor at a time. Therefore, the 
 * methods that add transitions or callback processors do not keep track of previously added transitions or callback processors. 
 * 
 * @author Ricardo Scholz
 *
 */
public class SingleTransitionListener implements ITransitionListener {

	private boolean running;
	
	private ITransitionCallbackProcessor callbackProcessor;	
	private Transition transition;

	/**
	 * Sets the current callback processor.
	 * 
	 * @param callbackProcessor		the new callback processor to be called when the {@link #transition} fires.
	 */
	@Override
	public void addCallbackProcessor(
			ITransitionCallbackProcessor callbackProcessor) {
		this.callbackProcessor = callbackProcessor;
	}

	/**
	 * Removes the current callback processors, setting the reference to <code>null</code>, if <code>callbackProcessor</code> instance
	 * is equal to the current callback processor, as of the implementation of {@link Object#equals(Object)}. If the parameter passed is
	 * different from the current callback processor, the call is ignored. Otherwise, the value of {@link #callbackProcessor} will be
	 * <code>null</code> when this method returns.
	 * 
	 * @param callbackProcessor		the callback processor to be removed.
	 */
	@Override
	public void removeCallbackProcessor(
			ITransitionCallbackProcessor callbackProcessor) {
		if (this.callbackProcessor != null && callbackProcessor != null && this.callbackProcessor.equals(callbackProcessor)) {
			this.callbackProcessor = null;
		}
	}

	/**
	 * @return		a list with the current callback processor (a list of size 1, therefore); or an empty list (a list of size zero), if
	 * 				the current callback processor is <code>null</code>.
	 */
	@Override
	public List<ITransitionCallbackProcessor> getCallbackProcessors() {
		List<ITransitionCallbackProcessor> result = new ArrayList<ITransitionCallbackProcessor>();
		if (this.callbackProcessor != null) {
			result.add(this.callbackProcessor);
		}
		return result;
	}

	/**
	 * Modifies the current transition this instance is listening to. If the <code>transition</code> parameter is not <code>null</code>,
	 * automatically starts the transition, by calling {@link Transition#start()}.  
	 */
	@Override
	public void addTransition(Transition transition) {
		this.transition = transition;
		if (this.transition != null) {
			this.transition.start();
		}
	}

	/**
	 * Removes the current transition this instance is listening to, if the <code>transition</code> parameter is equal to the current 
	 * transition, as of the implementation of {@link Transition#equals(Transition)}. 
	 */
	@Override
	public void removeTransition(Transition transition) {
		if (this.transition != null && transition != null && this.transition.equals(transition)) {
			this.transition = null;	
		}
	}

	/**
	 * Forwards keyboard inputs to the listened transitions.
	 * 
	 * @param key		the key pressed.
	 * @param keyCode	for coded keys, the key code.
	 */
	@Override
	public void processKeyboardInput(char key, int keyCode) {
		if (this.transition != null) {
			this.transition.processKeyboardInput(key, keyCode);
		}
	}
	
	/**
	 * This method executes a loop while an internal "running" flag is <code>true</code>. This loop has a mechanism to guarantee that
	 * transitions checking will not use all system resources. Therefore, a loop run is executed at predefined transition update intervals.
	 * 
	 * @see ConfigurationManager#getTransitionUpdateInterval()
	 */
	@Override
	public void run() {
		this.running = true;
		long interval;
		long updateInterval = ConfigurationManager.getInstance().getTransitionUpdateInterval();
		while (this.running) {
			interval = System.currentTimeMillis();
			this.checkTransitionsFireRules(this.transition);
			interval = System.currentTimeMillis() - interval;
			interval = updateInterval - interval;
			if (interval > 0) {
				try {
					Thread.sleep(interval);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Stops the transition checking loop. This will cause the thread to finish execution.
	 */
	@Override
	public void stop() {
		this.running = false;
	}
	
	// Private Methods
	
	private void checkTransitionsFireRules(Transition transition) {
		if (transition != null && transition.fired()) {
			this.callbackProcessor.transitionFired(transition);
		}
	}
}
