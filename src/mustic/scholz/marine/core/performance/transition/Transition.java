package mustic.scholz.marine.core.performance.transition;

import mustic.scholz.marine.core.performance.ElementTable;
import mustic.scholz.marine.core.performance.ITransitionCallbackProcessor;
import mustic.scholz.marine.core.performance.ITransitionListener;

/**
 * This is an abstract definition of a transition. A <code>Transition</code> is a kind of trigger that fires if a set of preconditions are 
 * met. The actual Marine Framework implementation uses transitions to allow {@link ElementTable} to move its internal active column pointer
 * forward.
 * 
 * The transitions architecture is based on callback processors. The {@link ITransitionListener} checks from times to times if any of its
 * transitions have fired. In case a transition has fired, it performs a callback to the {@link ITransitionCallbackProcessor}, so that
 * proper actions can be taken.
 * 
 * NOTE: The current model, which uses {@link TransitionTypeEnum}, is under investigation. It is possible that this enumerator is suppressed
 * on future versions of the framework. Therefore, there is no guarantee of backward compatibility for this class attribute.
 * 
 * @author Ricardo Scholz
 *
 */
//TODO [Refactoring] Remove TransitionTypeEnum from model.
public abstract class Transition {

	@Deprecated
	private TransitionTypeEnum type;
	
	/**
	 * Indicates whether this transition has fired or not. As transition firing may occur due to temporary events, such as key pressing,
	 * an attribute allows to "save" this information for use when {@link #fired()} method is called.
	 */
	protected boolean fired;

	/**
	 * Default constructor.
	 */
	public Transition() {
		this.fired = false;
	}
	
	/**
	 * Type dependent constructor.
	 * 
	 * @param type		the type of the actual implementing transition.
	 */
	@Deprecated
	public Transition(TransitionTypeEnum type) {
		this.type = type;
		this.fired = false;
	}
	
	/**
	 * @return		the type of this transition; be aware that this attribute is under investigation and may not be available in future
	 * 				versions of the framework.
	 */
	@Deprecated
	public TransitionTypeEnum getType() {
		return this.type;
	}
	
	/**
	 * Modifies the type of this transition. As transition types are actually defined by the underlying implementing class, calling this
	 * method may not be appropriate in most contexts.
	 *  
	 * @param type		the new transition type.
	 */
	@Deprecated
	public void setType(TransitionTypeEnum type) {
		this.type = type;
	}
	
	/**
	 * Compares this instance with other transition instance. Both instances are considered to be the same when their implementing classes
	 * are the same. The <code>fired</code> attribute is not taken into account, so a fired transition may be considered equal to an
	 * unfired transition.
	 * 
	 * @param other			another transition instance to compare with this instance.
	 * @return				<code>true</code> if both transitions are not <code>null</code> and have the same name (ignoring case).
	 */
	public boolean equals(Transition other) {
		return (other != null && this.getClass().getName().equalsIgnoreCase(other.getClass().getName()));
	}

	/**
	 * @return		<code>true</code> if the preconditions of this transition have been met.
	 */
	public boolean fired() {
		return this.fired;
	}
	
	/**
	 * Resets this transition state. Current implementation resets <code>fired</code> attribute to <code>false</code>.
	 */
	public void reset() {
		this.fired = false;
	}
	
	/**
	 * Allows the transition to process keyboard inputs. Keyboard inputs may be relevant to define if a given transition has fired or not.
	 * This is an abstract method and must be implemented by all subclasses, even if empty.
	 * 
	 * @param key		the key pressed.
	 * @param keyCode	for coded keys, the code of the key pressed.
	 */
	public abstract void processKeyboardInput(char key, int keyCode);
	
	/**
	 * This method allows the implementing subclass to get to know when the transition has started to actually run. This is useful for timer 
	 * transitions, for instance. 
	 */
	public abstract void start();
	
	/**
	 * Subclasses might want to perform some action when the playback is paused. This abstract method guarantees all transitions will
	 * implement a 'pause()' method to be called when playback is paused, even if they implement an empty method.
	 */
	public abstract void pause();
	
	/**
	 * All transitions must provide a label with a name or a distinctive information. Future internationalisation may change this approach.
	 * 
	 * @return		a label identifying the transition; usually for UI implementations.
	 */
	public abstract String getLabel();
}
