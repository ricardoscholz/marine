package mustic.scholz.marine.core.performance;

import mustic.scholz.marine.core.performance.transition.Transition;

/**
 * This interface defines the methods a transition callback processor should implement. Every implementing class should act as a flow
 * manager, deciding what to do when a given transition fires.
 * 
 * @author Ricardo Scholz
 *
 */
public interface ITransitionCallbackProcessor {

	/**
	 * The callback method to inform the implementing class that a given transition has fired.
	 * 
	 * @param transition	a transition that had its firing conditions fulfilled.
	 */
	public void transitionFired(Transition transition);
	
}
