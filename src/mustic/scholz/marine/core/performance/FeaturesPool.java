package mustic.scholz.marine.core.performance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mustic.scholz.marine.core.entity.JointEnum;
import mustic.scholz.marine.core.entity.PerformerPosition;
import mustic.scholz.marine.core.feature.Feature;
import mustic.scholz.marine.core.feature.FeatureFactory;

/**
 * This singleton class holds the current features and performer position values. It uses <code>HashMap</code>s to store the last received
 * full body features by index and joint based features by feature index and joint. A simple instance of <code>PerformerPosition</code> is 
 * kept as the last valid position received.  
 * 
 * @author Ricardo Scholz
 *
 */
public class FeaturesPool {

	private final int FEAT_INDEXED_MAP_INIT_CAP 		= 20;
	private final float FEAT_INDEXED_MAP_LOAD_FAC 		= 0.5f;
	private final int JOINT_INDEXED_MAP_INIT_CAP 		= 50;
	private final float JOINT_INDEXED_MAP_LOAD_FAC 		= 0.5f;
	
	private static FeaturesPool instance;
	
	//Contains full body and derived features
	private HashMap<String, Feature> featureIdIndexed;
	
	//Contains joint based features
	private HashMap<JointEnum, HashMap<String, Feature>> jointIndexed;
	
	//Performer position list
	private HashMap<Integer, PerformerPosition> performersMap;
	
	//First recognized performer
	private int firstPerformer;
	
	private FeaturesPool() {
		this.featureIdIndexed = new HashMap<String, Feature>(FEAT_INDEXED_MAP_INIT_CAP, FEAT_INDEXED_MAP_LOAD_FAC);
		this.jointIndexed = new HashMap<JointEnum, HashMap<String, Feature>>(JOINT_INDEXED_MAP_INIT_CAP, JOINT_INDEXED_MAP_LOAD_FAC);
		this.performersMap = new HashMap<Integer, PerformerPosition>();
	}
	
	/**
	 * Singleton instance retrieval.
	 * 
	 * @return		the singleton instance.
	 */
	public static FeaturesPool getInstance() {
		if (instance == null) {
			instance = new FeaturesPool();
		}
		return instance;
	}
	
	// Reading methods
	
	/**
	 * Returns the last valid performer position received, if it is not older than a given lifetime.
	 *  
	 * @param maxLifetime		the maximum age, in milliseconds, of the performer position information that matters.
	 * 
	 * @return					the last received performer position, if it is not older than <code>maxLifetime</code> milliseconds;
	 * 							<code>null</code> otherwise.
	 */
	public PerformerPosition getPerformerPosition(long maxLifetime) {
		return this.getPerformerPositionCheckTimestamp(this.performersMap.get(this.firstPerformer), maxLifetime);	
	}
	
	/**
	 * Returns the last valid performer position received, in a given index, if it is not older than a given lifetime.
	 * 
	 * @param maxLifetime		the maximum age, in milliseconds, of the performer position information that matters.
	 * @param index				the index of the recognized performer.
	 * @return					the last received performer position, if it is not older than <code>maxLifetime</code> milliseconds;
	 * 							<code>null</code> otherwise.
	 */
	public PerformerPosition getPerformerPosition(long maxLifetime, int index) {
		PerformerPosition result = null;
		if (this.performersMap != null) {
			result = getPerformerPositionCheckTimestamp(this.performersMap.get(index), maxLifetime);			
		}
		return result;
	}
	
	private PerformerPosition getPerformerPositionCheckTimestamp(PerformerPosition performer, long maxLifetime) {
		PerformerPosition result = null;
		if (performer != null) {
			long diff = System.currentTimeMillis() - performer.getTimestamp();
			if (diff <= maxLifetime) {
				result = performer;
			}
		}
		return result;
	}
	
	/**
	 * @return	the last received performer position information, no matter how old it is; this is equivalent to 
	 * <code>getPerformePosition(Long.MAX_VALUE)</code>.
	 */
	public PerformerPosition getPerformerPosition() {
		return this.getPerformerPosition(this.firstPerformer);
	}
	
	/**
	 * @param index		the index of the performer to be retrieved
	 * @return			the last received performer position information, no matter how old it is; this is equivalent to
	 * 					<code>getPerformerPosition(Long.MAX_VALUE, index)</code>.
	 */
	public PerformerPosition getPerformerPosition(int index) {
		return this.performersMap.get(index);
	}
	
	/**
	 * Retrieves all performances recognized by the sensor, which have non null references. Thus,
	 * this method guarantees that the list will not be null (although it might be empty), and 
	 * the elements within the list will not be null.
	 * 
	 * @return			all performers recognized by the sensor, discarding null references.
	 */
	public List<PerformerPosition> getAllNotNullPerformerPositions() {
		return this.getAllNotNullPerformerPositions(Long.MAX_VALUE);
	}
	
	/**
	 * Retrieves all performances recognized by the sensor which have non null references and 
	 * are younger than <code>maxLifetime</code>. Thus, this method guarantees that the list 
	 * will not be null (although it might be empty), and the elements within the list will 
	 * not be null.
	 * 
	 * @param maxLifetime		the maximum age, in milliseconds, of the performer position information that matters.
	 * @return					all performers recognized by the sensor, discarding null references.
	 */
	public List<PerformerPosition> getAllNotNullPerformerPositions(long maxLifetime) {
		List<PerformerPosition> result = new ArrayList<PerformerPosition>();
		Iterator<Map.Entry<Integer, PerformerPosition>> iterator = this.performersMap.entrySet().iterator();
		while (iterator.hasNext()) {
		    Map.Entry<Integer, PerformerPosition> pair = iterator.next();
		    PerformerPosition value = pair.getValue();
		    if (value != null) {
		    	long diff = System.currentTimeMillis() - value.getTimestamp();
				if (diff <= maxLifetime) {
					result.add(value);
				}
		    }
		}
		return result;
	}
	
	/**
	 * Retrieves the last version of the feature which matches a given ID and joint.
	 * 
	 * @param id			the feature identifier, as of {@link Feature#getId()}.
	 * @param joint			the joint to which the feature relates, if any; use null for full body features.
	 * @return				the last version of the feature, for the given joint, if one has already been received.
	 */
	public Feature getFeature(String id, JointEnum joint) {
		Feature result = this.getFeature(id);
		if (result == null && joint != null) {
			HashMap<String, Feature> feat = this.jointIndexed.get(joint);
			if (feat != null) {
				result = feat.get(id);
			}
		}
		return result;
	}
	
	/**
	 * Retrieves the last version of the feature which matches a given ID. If it is a joint based feature, result will be <code>null</code>.
	 * 
	 * @param id			the feature identifier, as of {@link Feature#getId()}.
	 * @return				the last version of the feature, if one has already been received.
	 */
	public Feature getFeature(String id) {
		return this.featureIdIndexed.get(id);
	}
	
	/**
	 * This method returns all features currently in the features pool (built in and custom). Be aware that <code>FeaturesPool</code> might 
	 * not have received all features you expect it to receive when you call this method. Although this method takes a lot of processing
	 * to retrieve the list, a cache implementation would need to augment processing during features update. As features are updated all the
	 * time, while this method may never be called, this was identified as a bad approach.
	 * 
	 * NOTE: This is an expensive computation method; consider using {@link FeatureFactory#buildAllBuiltInFeatures()}, if you need a list of 
	 * built in features, without their current values;
	 * 
	 * @return	a list of all features received by this pool since the beginning of the execution.
	 */
	public List<Feature> getAllFeatures() {
		List<Feature> result = new ArrayList<Feature>();
		Set<String> keys = featureIdIndexed.keySet();
		Feature temp;
		for (String key : keys) {
			temp = featureIdIndexed.get(key);
			if (!result.contains(temp)) {
				result.add(temp);	
			}
		}
		Set<JointEnum> jointKeys = jointIndexed.keySet();
		HashMap<String, Feature> tmpMap; 
		for (JointEnum jointKey : jointKeys) {
			tmpMap = jointIndexed.get(jointKey);
			keys = tmpMap.keySet();
			for (String key : keys) {
				temp = tmpMap.get(key);
				if (!result.contains(temp)) {
					result.add(temp);
				}
			}
		}
		return result;
	}
	
	// Writing methods
	
	/**
	 * Inserts or updates a given feature in the pools of features.
	 * 
	 * @param feature		the feature to be updated.
	 */
	public void updateFeature(Feature feature) {
		if (feature != null) {
			if (feature.getJoint() == null) {
				this.featureIdIndexed.put(feature.getId(), feature);
			} else {
				HashMap<String, Feature> map = this.jointIndexed.get(feature.getJoint());
				if (map == null) {
					map = new HashMap<String, Feature>(FEAT_INDEXED_MAP_INIT_CAP, FEAT_INDEXED_MAP_LOAD_FAC);
				}
				map.put(feature.getId(), feature);
				this.jointIndexed.put(feature.getJoint(), map);
			}
		}
	}
	
	/**
	 * UPdates the performer position variable associated to a given index.
	 * 
	 * @param performerPosition		the new performer position.
	 * @param index					the index to be associated to the new performer position.
	 */
	public void updatePerformerPosition(PerformerPosition performerPosition, int index) {
		if (performerPosition != null) {
			this.performersMap.put(index, performerPosition);
		}
	}
	
	/**
	 * Updates the index of the first recognized performer.
	 * 
	 * @param index		the index of the first recognized performer.
	 */
	public void setFirstPerformer(int index) {
		this.firstPerformer = index;
	}
	
	/**
	 * @return the index of the first performer recognized (from 0 to 5).
	 */
	public int getFirstPerformerIndex() {
		return this.firstPerformer;
	}
}
