package mustic.scholz.marine.core.performance;

import java.util.List;

import mustic.scholz.marine.core.performance.transition.Transition;

/**
 * Transition listeners are classes that manage transitions firing conditions, by checking weather these conditions have been fulfilled.
 * Implementing classes are also expected to forward keyboard inputs to the transitions it is listening to.
 * 
 * This interface implements <code>Runnable</code>, which means its implementing classes are supposed to run in a separate thread from the
 * main Marine thread.
 * 
 * @author Ricardo Scholz
 */
public interface ITransitionListener extends Runnable {

	/**
	 * Sets the object which will act as callback processor, managing the flow control when a transition is fired. 
	 * 
	 * @param callbackProcessor		any class that implements <code>ITransitionCallbackProcessor</code>.
	 */
	public void addCallbackProcessor(ITransitionCallbackProcessor callbackProcessor);

	/**
	 * Removes the callback processor. A transition listener without any callback processor may be of little use, as no action will be
	 * performed when a transition fires.
	 * 
	 * @param callbackProcessor		the instance of callback processor to be removed.
	 */
	public void removeCallbackProcessor(ITransitionCallbackProcessor callbackProcessor);
	
	/**
	 * @return	all callback processors this transition listener is forwarding the firing messages.
	 */
	public List<ITransitionCallbackProcessor> getCallbackProcessors();
	
	/**
	 * Adds a transition to be listened. The actual implementation may keep a list of transitions, or just the last added transition, so
	 * the behaviour of this method depends on the actual implementation.
	 * 
	 * @param transition	a transition to be listened.
	 */
	public void addTransition(Transition transition);
	
	/**
	 * Stops listening to a given transition.
	 * 
	 * @param transition	the transition to stop listening to.
	 */
	public void removeTransition(Transition transition);
	
	/**
	 * The actual implementations of this method are supposed to forward the keyboard input to the transitions, so they can decide weather
	 * that keyboard input will cause a transition fire or not.
	 *  
	 * @param key		the key pressed.
	 * @param keyCode	for special keys, the key code.
	 */
	public void processKeyboardInput(char key, int keyCode);
	
	/**
	 * Stops the transition listening thread.
	 */
	public void stop();
}
