package mustic.scholz.marine.core.performance.comm;

import java.util.HashMap;

/**
 * This class allows elements to exchange messages containing objects during performance.
 * It is implemented as a Singleton.
 * 
 * @author Ricardo Scholz.
 */
public class MessagesPool {

	private static MessagesPool instance;

	private HashMap<String, Message> pool;
	
	private MessagesPool() {
		this.pool = new HashMap<String, Message>();
	}
	
	/**
	 * Singleton access method.
	 * 
	 * @return the unique instance of the pool.
	 */
	public static MessagesPool getInstance() {
		if (instance == null) {
			instance = new MessagesPool();
		}
		return instance;
	}
	
	/**
	 * Retrieves a given message from the pool, erasing it for further reads. Does
	 * nothing if the message does not exist.
	 * 
	 * @param key	the message key
	 * @return		the message associated with the given key, or null if no message is found.
	 */
	public Message removeMessage(String key) {
		return this.pool.remove(key);
	}
	
	/**
	 * Retrieves a given message from the pool, erasing it for further reads, if it is no older than a 
	 * given lifetime, in milliseconds. Does nothing if the message does not exist or is older than the 
	 * lifetime.
	 * 
	 * @param key			the message key
	 * @param maxLifetime	the maximum lifetime of the message, in milliseconds.
	 * @return				the message, or null if there is no message with such key in the pool or the
	 * 						existing message is older than lifetime. 
	 */
	public Message removeMessage(String key, long maxLifetime) {
		return this.getOrRemoveMessage(key, maxLifetime, true);
	}
	
	/**
	 * Retrieves a given message from the pool, keeping it for further reads. Does
	 * nothing if the message does not exist.
	 * 
	 * @param key		the message key
	 * @return			the message associated with the given key, or null if no message is found.
	 */
	public Message getMessage(String key) {
		return this.pool.get(key);
	}
	
	/**
	 * Retrieves a given message from the pool, keeping it for further reads,  if it is no older than a 
	 * given lifetime, in milliseconds. Does nothing if the message does not exist or is older than the 
	 * lifetime.
	 * 
	 * @param key			the message key
	 * @param maxLifetime	the maximum lifetime of the message, in milliseconds.
	 * @return				the message, or null if there is no message with such key in the pool or the
	 * 						existing message is older than lifetime.
	 */
	public Message getMessage(String key, long maxLifetime) {
		return this.getOrRemoveMessage(key, maxLifetime, false);
	}
	
	private Message getOrRemoveMessage(String key, long maxLifetime, boolean remove) {
		Message message = this.pool.get(key);
		if (message != null) {
			long delay = System.currentTimeMillis() - message.getTimestamp();
			if (delay <= maxLifetime) {
				if (remove) {
					return this.pool.remove(key);
				} else {
					return message;
				}
			}
		}
		return null;
	}
	
	/**
	 * Inserts a new message in the pool, if the message is not <code>null</code>. Overwrites any existing message 
	 * associated with the same key. If the timestamp is zero, the timestamp of the message is set at the time of 
	 * insertion. If the message key is null, does not insert the message in the pool.
	 * 
	 * @param message
	 */
	public void putMessage(Message message) {
		if (message != null) {
			if (message.getTimestamp() == 0) {
				message.setTimestamp(System.currentTimeMillis());	
			}
			String key = message.getKey();
			if (key != null) {
				this.pool.put(key, message);
			}
		}
	}
}
