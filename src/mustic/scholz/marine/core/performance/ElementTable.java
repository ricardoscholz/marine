package mustic.scholz.marine.core.performance;

import mustic.scholz.marine.core.input.IInputReceiver;
import mustic.scholz.marine.core.performance.element.CustomElement;
import mustic.scholz.marine.core.performance.element.Element;
import mustic.scholz.marine.core.performance.transition.SingleTransitionListener;
import mustic.scholz.marine.core.performance.transition.Transition;

/**
 * This class models a grid of custom elements and manages the playback pointers, so that one column is active at a time. It is possible to 
 * move forward and backward along the rows, although normal behaviour would be moving forward. Also, it implements the 
 * {@link ITransitionCallbackProcessor} interface, meaning that transitions may move pointer forward when triggered.
 * 
 * There is an internal state machine, with three states: "idle", "playing" and "paused". The starting state is "idle".  This state does not
 * allow execution of flow control methods ({@link #moveBackward()}, {@link #moveForward()} and {@link #reset()}, for technical reasons. 
 * Indirectly, the transitions' mechanism will not fire flow control methods on this state, although other tasks may be executed.
 * The "paused" state does not update or repaint elements, so animation keeps still. However, flow methods do are executed, including
 * current position moves caused by transitions triggered.
 * The "playing" state updates and paints elements, and allows flow methods to be executed freely.
 * 
 * NOTE: The transitions model is not complete yet, and features related to this part of the model may be strongly refactored.
 * NOTE: The keyboard forwarding approach is not final, and features related to this part of the model may be strongly refactored.
 * 
 * @author Ricardo Scholz
 *
 */
public class ElementTable implements ITransitionCallbackProcessor {
	
	private final int STATE_IDLE 		= 0;
	private final int STATE_PLAYING 	= 1;
	private final int STATE_PAUSED 		= 2;

	private int state = STATE_IDLE;
	
	private int maxTracks;
	private int maxElementsPerTrack;
	private int currentPosition;
	
	private ITransitionListener transitionListener;
	
	private CustomElement[][] elements;
	private Transition[] transitions;
	
	private Performance performance;
	private IInputReceiver inputReceiver;
	
	/**
	 * Creates a new grid of custom elements, in which  <code>maxTracks</code> parameter is used to set the number of rows (called 
	 * "tracks"), and <code>maxElementsPerTrack</code> is used to set the number of columns. Elements may occupy more than one column in a 
	 * given track.
	 * 
	 * @param maxTracks					the number of tracks of this instance.
	 * @param maxElementsPerTrack		the number of custom elements each track will be capable to keep.
	 */
	public ElementTable(int maxTracks, int maxElementsPerTrack) {
		this.currentPosition = 0;
		this.maxTracks = maxTracks;
		this.maxElementsPerTrack = maxElementsPerTrack;
		this.elements = new CustomElement[this.maxTracks][this.maxElementsPerTrack];
		this.transitions = new Transition[this.maxElementsPerTrack];
	}
	
	/*
	 * Runtime control methods
	 */
	
	/**
	 * Method that is called before starting the playback, in order to prepare all elements and avoid delays when changing from one group
	 * of elements to the next group (next column). Internally, all elements subscribed to this instance will have its 
	 * {@link Element#setup()} method called once, when this method is called.
	 */
	public void setup() {
		CustomElement current;
		for (int i = 0; i < this.maxTracks; i++) {
			for (int j = 0; j < this.maxElementsPerTrack; j++) {
				current = this.elements[i][j];
				if (current != null && current.getElement() != null) {
					try {
						if (!current.getElement().isSetupRun()) {
								current.getElement().runSetup();
						}
					} catch (Throwable t) {
						t.printStackTrace(); //TODO Appropriately handle exceptions. 
					}
				}
			}
		}
	}
	
	/**
	 * Method that is called after exiting a playback, in order to free resources. Internally, all elements subscribed to this instance will
	 * have its {@link Element#destroy()} method called once, when this method is called. 
	 */
	public void destroy() {
		CustomElement current;
		for (int i = 0; i < this.maxTracks; i++) {
			for (int j = 0; j < this.maxElementsPerTrack; j++) {
				current = this.elements[i][j];
				if (current != null && current.getElement() != null) {
					try {
						current.getElement().destroy();
					} catch (Throwable t) {
						t.printStackTrace();
					}
				}
			}
		}
		this.stopTransitionListener();
	}
	
	/**
	 * When this method is called, every element in the current position (column) is updated and painted. Therefore, the methods
	 * {@link Element#update()} and {@link Element#paint()} (in this order) are called for the element in the higher track index, then for 
	 * the previous track, until the first track is reached. This means that when the element in track X is updated, element in track X+1 has
	 * already been updated and painted. Therefore, elements in lower numbered tracks will be painted over the elements in higher numbered
	 * tracks.
	 * 
	 * The update is only executed if the current state is "playing".
	 */
	public void update() {
		if (this.state == STATE_PLAYING) {
			CustomElement current;
			Element element;
			for (int i = this.maxTracks-1; i >= 0; i--) {
				this.defaultDrawingModes();
				current = this.elements[i][this.currentPosition];
				if (current != null) {
					element = current.getElement();
					if (element != null) {
						try {
							if (!element.isSetupRun()) {
								this.prepareElement(element);
								element.runSetup();
							}
							element.runUpdate();
							element.runPaint();
						} catch (Throwable t) {
							t.printStackTrace(); //TODO Appropriately handle exceptions!
						}
					}
				}
			}
		}
	}
	
	private void defaultDrawingModes() {
		this.performance.colorMode(Performance.RGB);
	}
	
	/*
	 * Callback methods
	 */
	
	/**
	 * Forwards the fire command to each element in the current index. Commands are only forwarded when the current state is "playing". 
	 * 
	 * NOTE: The keyboard forwarding approach is not final, and features related to this part of the model may be strongly refactored.
	 * 
	 * @param command	the command number to be forwarded.
	 */
	public void fireCommand(int command) {
		//TODO [Refactoring] Refactor keyboard command handling.
		if (this.state == STATE_PLAYING) {
			CustomElement current;
			for (int i = 0; i < this.maxTracks; i++) {
				current = this.elements[i][this.currentPosition];
				if (current != null && current.getElement() != null) {
					try {
						current.getElement().fireCommand(command);
					} catch (Throwable t) {
						t.printStackTrace(); //TODO Appropriately handle exceptions!
					}
				}
			}
		}
	}

	/**
	 * Moves forward when a {@link Transition} is fired, and updates the transition listener to listen to the next transition. For 
	 * performance reasons, this implementation does not check if the received transition is the current transition expected, as it trusts 
	 * the <code>SingleTransitionListener</code> implementation and its own transition management.
	 */
	@Override
	public void transitionFired(Transition transition) {
		this.moveForward();
		transition.reset();
	}
	
	/*
	 * Flow control methods
	 */
	
	/**
	 * If current state is "playing" or "paused", moves current position forward and updates the active transition at the transition
	 * listener. If the current position is the last column, goes to "idle" state and removes any active transition from the transition
	 * listener. If current state is "idle", ignores the command.
	 */
	public void moveForward() {
		if (this.state != STATE_IDLE) {
			this.transitionListener.removeTransition(this.transitions[this.currentPosition]);
			this.currentPosition++;
			if (this.currentPosition >= this.maxElementsPerTrack) {
				this.currentPosition = this.maxElementsPerTrack-1;
				this.state = STATE_IDLE;
			} else {
				this.transitionListener.addTransition(this.transitions[this.currentPosition]);
			}
		}
	}
	
	/**
	 * Moves the current position backward, until the first column, if the current state is "playing" or "paused". If in the first column, 
	 * or the current state is "idle", ignores the command.
	 */
	public void moveBackward() {
		if (this.state != STATE_IDLE) {
			this.transitionListener.removeTransition(this.transitions[this.currentPosition]);
			this.currentPosition--;
			if (this.currentPosition < 0) {
				this.currentPosition = 0;
				this.state = STATE_IDLE;
			} else {
				this.transitionListener.addTransition(this.transitions[this.currentPosition]);
			}
		}
	}
	
	/**
	 * Moves the current position to the first column and pauses the execution, if the current state is "playing" or "paused".
	 */
	public void reset() {
		if (this.state != STATE_IDLE) {
			this.transitionListener.removeTransition(this.transitions[this.currentPosition]);
			this.currentPosition = 0;
			this.state = STATE_PAUSED;
			this.resetElements();
			this.transitionListener.addTransition(this.transitions[this.currentPosition]);
		}
	}
	
	private void resetElements() {
		if (this.elements != null) {
			for (int i = 0; i < this.elements.length; i++) {
				for (int j = 0; j < this.elements[i].length; j++) {
					CustomElement customElement = this.elements[i][j];
					if (customElement != null) {
						Element element = customElement.getElement();
						if (element != null) {
							element.runSetup();
						}
					}
				}
			}
		}
	}
	
	/**
	 * Starts the execution, no matter what the current state is. The current state is set to "playing".
	 */
	public void play() {
		if (this.state == STATE_IDLE) {
			this.transitionListener.addTransition(this.transitions[this.currentPosition]);
		}
		this.state = STATE_PLAYING;
	}
	
	/**
	 * Pauses the execution, no matter what the current state is. The current state is set to "paused".
	 */
	public void pause() {
		this.state = STATE_PAUSED;
	}
	
	/**
	 * Suspends the execution, no matter what the current state is. When execution is suspended, transitions triggered are ignored, as well
	 * as any flow command (move forward, move backward and reset). The current state is set to "idle".
	 */
	public void suspend() {
		this.state = STATE_IDLE;
		this.transitionListener.removeTransition(this.transitions[this.currentPosition]);
	}
	
	/*
	 * Creation time control methods
	 */
	
	/**
	 * Adds a custom element to a given track.
	 * 
	 * @param element	the custom element to be added.
	 * @param track		the track index (starts from 0)
	 * @param position	the position index (starts from 0)
	 * @param shift		if <code>true</code>, shifts other custom elements right to make room for the new custom element;
	 * 					otherwise, overwrites custom element at that position.
	 */
	public void addElement(CustomElement element, int track, int position, boolean shift) {
		if (shift) {
			this.shiftRightFromElement(track, position);
		}
		this.elements[track][position] = element;
	}
	
	/**
	 * Removes a custom element from a given track.
	 * 
	 * @param track		the track to remove the custom element.
	 * @param position	the custom element position.
	 * @param shift		if <code>true</code>, shifts other custom elements left after removing the custom element;
	 * 					otherwise, keeps previous custom element position empty.
	 * @return			the removed custom element.
	 */
	public CustomElement removeElement(int track, int position, boolean shift) {
		CustomElement result = this.elements[track][position];
		this.elements[track][position] = null;
		if (shift) {
			this.shiftLeftFromElement(track, position);
		}
		return result;
	} 
	
	/**
	 * Shifts custom elements right in a given track, from a given position (inclusive). The custom element at the given position will be 
	 * null and last custom element of the track will be lost, after executing the shift.
	 * 
	 * @param track		the track in which shift will occur.
	 * @param position	the position from which the shift starts (custom elements at the left of this position won't be affected).
	 */
	public void shiftRightFromElement(int track, int position) {
		for (int i = this.maxElementsPerTrack-1; i > position; i--) {
			this.elements[track][i] = this.elements[track][i-1];
		}
		this.elements[track][position] = null;
	}
	
	/**
	 * Shifts custom elements left in a given track, from a given position (inclusive). The custom element at the given position will be 
	 * overwritten by its right neighbor and the last custom element of the track will be null, after executing the shift.
	 * 
	 * @param track		the track in which shift will occur.
	 * @param position	the position from which the shift starts (custom elements at the left of this position won't be affected).
	 */
	public void shiftLeftFromElement(int track, int position) {
		for (int i = position; i < this.maxElementsPerTrack-1; i++) {
				this.elements[track][i] = this.elements[track][i+1];
		}
		this.elements[track][this.maxElementsPerTrack-1] = null;
	}

	/**
	 * Adds a transition in a given position.
	 * 
	 * @param transition	the transition to be added.
	 * @param position		the position in which the transition will be added.
	 * @param shift			if <code>true</code>, shift other transitions right after adding this transition;
	 * 						otherwise, overwrites previous transition at that position.
	 */
	public void addTransition(Transition transition, int position, boolean shift) {
		if (shift) {
			this.shiftRightFromTransition(position);
		}
		this.transitions[position] = transition;
	}
	
	/**
	 * Adds a transition at the first null position, without shifting other transitions.
	 * 
	 * @param transition		the transition to be added.
	 */
	public void addTransition(Transition transition) {
		int position = this.getFirstNullPosition(this.transitions);
		this.addTransition(transition, position, false);
	}
	
	/**
	 * Removes a transition from a given position. Always shift other transitions left after removing a given transition, as transitions
	 * sequence must not have empty spaces.
	 * 
	 * @param position	the transition to be removed.
	 * @return			the removed transition.
	 */
	public Transition removeTransition(int position) {
		Transition result = this.transitions[position];
		this.shiftLeftFromTransition(position);
		return result;
	}
	
	/*
	 * Utilitary methods
	 */
	
	/**
	 * Shifts transitions right from a given position (inclusive). The transition at the given position will be null and last transition of 
	 * the sequence will be lost, after executing the shift.
	 * ATTENTION: this method leaves the transition sequence invalid, as transition at <code>position</code> will be null; it must be called
	 * within a method which deals with this inconsistency.
	 * 
	 * @param track		the track in which shift will occur.
	 * @param position	the position from which the shift starts (elements at the left of this position won't be affected).
	 */
	private void shiftRightFromTransition(int position) {
		for (int i = this.maxElementsPerTrack-1; i > position; i--) {
			this.transitions[i] = this.transitions[i-1];
		}
		this.transitions[position] = null;
	}
	
	/**
	 * Shifts transitions left from a given position (inclusive). The transition at the given position will be lost and last transition of 
	 * the sequence will be null, after executing the shift.
	 * 
	 * @param track		the track in which shift will occur.
	 * @param position	the position from which the shift starts (elements at the left of this position won't be affected).
	 */
	private void shiftLeftFromTransition(int position) {
		for (int i = position; i < this.maxElementsPerTrack-1; i++) {
			this.transitions[i] = this.transitions[i+1];
		}
		this.transitions[this.maxElementsPerTrack-1] = null;
	}
	
	/**
	 * Creates a new transition listener instance, of the type {@link SingleTransitionListener}, and starts it in a separate thread.
	 */
	public void startTransitionListener() {
		this.stopTransitionListener();
		this.transitionListener = new SingleTransitionListener();
		this.transitionListener.addCallbackProcessor(this);
		Thread t = new Thread(this.transitionListener);
		t.start();
	}
	
	/**
	 * Gently stops the transition listener thread, by forwarding a stop call to the transition listener.
	 */
	private void stopTransitionListener() {
		if (this.transitionListener != null) {
			this.transitionListener.stop();
		}
	}
	
	private int getFirstNullPosition(Object[] objectArray) {
		int i = -1;
		if (objectArray != null) {
			for (i = 0; i < objectArray.length; i++) {
				if (objectArray[i] == null) {
					return i;
				}
			}
		}
		return i;
	}
	
	/*
	 * Getters and Setters
	 */
	
	/**
	 * @return	the current state.
	 */
	public int getState() {
		return state;
	}

	/**
	 * Modifies the current state. Prefer using the state control methods: {@link #play()}, {@link #pause()} and {@link #suspend()}.
	 * 
	 * @param state		the new state.
	 */
	public void setState(int state) {
		this.state = state;
	}

	/**
	 * @return		the maximum number of tracks this grid supports.
	 */
	public int getMaxTracks() {
		return maxTracks;
	}

	/**
	 * @return		the maximum elements each track supports.
	 */
	public int getMaxElementsPerTrack() {
		return maxElementsPerTrack;
	}

	/**
	 * @return		the current position (column) being executed.
	 */
	public int getCurrentPosition() {
		return currentPosition;
	}

	/**
	 * Modifies the current position (column) being executed. No index validity is performed within this method. Prefer using the flow 
	 * control methods, in order to avoid {@link ArrayIndexOutOfBoundsException} throwing, or do the validations yourself. Consider using
	 * the flow control methods ({@link #moveBackward()}, {@link #moveForward()} and {@link #reset()}).
	 * 
	 * @param currentPosition		the new current position execution will move to.
	 */
	public void setCurrentPosition(int currentPosition) {
		this.currentPosition = currentPosition;
	}

	/**
	 * @return		the bidimensional array of custom elements of this table; some items might be null, as the table does not need to be 
	 * 				fully filled. The first index goes from 0 to {@link #maxTracks}, and the second index goes from 0 to 
	 * 				{@link #maxElementsPerTrack}.
	 */
	public CustomElement[][] getElements() {
		return elements;
	}

	/**
	 * @return		the array of transitions of this table. Its size is always {@link #maxElementsPerTrack}. Some items may be null, but
	 * 				null elements are supposed to occur at the end of the sequence of non-null elements. Therefore, the array can be divided
	 * 				into two parts: the left part with non null values (lower indexes), and the right part with null values (higher
	 * 				indexes). Any of those parts might be of size zero.
	 */
	public Transition[] getTransitions() {
		return transitions;
	}

	/**
	 * Before the actual execution, custom elements must be prepared for execution with a given {@link Performance} instance and a given
	 * {@link IInputReceiver} instance. This is needed because custom elements' cannot acquire valid instances of these classes at 
	 * instantiation time. Therefore, setting these instances for each custom element before actual execution is necessary. 
	 * 
	 * @param performance		the performance currently being used.
	 * @param inputReceiver		the input receiver currently being used.
	 */
	public void prepareElements(Performance performance, IInputReceiver inputReceiver) {
		this.performance = performance;
		this.inputReceiver = inputReceiver;
		CustomElement current = null;
		for (int i = 0; i < this.maxTracks; i++) {
			for (int j = 0; j < this.maxElementsPerTrack; j++) {
				current = this.elements[i][j];
				if (current != null) {
					this.prepareElement(current.getElement());
				}
			}
		}
	}
	
	private void prepareElement(Element element) {
		if (element != null) { 
			element.setPerformance(this.performance);
			element.setInputReceiver(this.inputReceiver);
		}
	}

	/**
	 * This method forwards key pressing messages to transitions. Currently, key pressing messages are not forwarded to elements. Elements
	 * receive numeric key pressing messages through the {@link ElementTable#fireCommand(int)} method.
	 * 
	 * @param key		the key pressed.
	 * @param keyCode	for special keys, the key code.
	 */
	public void keyPressed(char key, int keyCode) {
		this.transitionListener.processKeyboardInput(key, keyCode);
	}
	
}
