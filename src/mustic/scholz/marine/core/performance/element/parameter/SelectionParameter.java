package mustic.scholz.marine.core.performance.element.parameter;

import java.util.ArrayList;
import java.util.List;

import mustic.scholz.marine.core.performance.element.InputTypeEnum;
import mustic.scholz.marine.core.persistence.ParameterSerializer;
import mustic.scholz.marine.core.utils.ReflectionsUtil;

/**
 * Actual implementation of a parameter which holds a list of elements from which one of them should be selected for use. This
 * implementation uses Java Generics. For this reason, it is impossible to the persistence implementation to figure out how to persist your
 * list objects. Therefore, if you want to appropriately persist this parameter information, you must provide a {@link ParameterSerializer}
 * which first generic type is 'T', by calling {@link #setSerializer(ParameterSerializer)}. 
 * 
 * NOTE: Current implementation holds a hard dependency with the <code>core.persistence</code> package, due to the serialization issue; this
 * may be refactored in future versions of the framework, and backwards compatibility may not be guaranteed.
 * 
 * @author Ricardo Scholz
 */
public class SelectionParameter<T> extends Parameter {

	private T value;
	private T defaultValue;
	private List<T> list;
	private String listLabel;
	
	//TODO [Improvement] Hard dependency with core.persistence package not desirable.
	private ParameterSerializer<T, ?, ?> serializer;
	
	/**
	 * Empty default constructor.
	 */
	public SelectionParameter() {
		super();
	}
	
	/**
	 * Builds a new instance of a selection parameter, with the given attribute values.
	 * 
	 * @param input				the input type, one of {@link InputTypeEnum} options.
	 * @param id				the parameter ID, for fast parameter retrieval by the 
	 * 							{@link mustic.scholz.marine.core.performance.element.Element}.
	 * @param name				the parameter name.
	 * @param description		the parameter description.
	 * @param value				the actual parameter value; must be one of the values in <code>list</code>.
	 * @param defaultValue		the default parameter value; must be one of the values in <code>list</code>.
	 * @param list				a list of possible parameter values.
	 * @param listLabel			the attribute name which may be used to represent the values as a <code>String</code>.
	 * @param serializer		serializer for the selection parameter specific type.
	 */
	public SelectionParameter(InputTypeEnum input, Integer id, String name,
			String description, T value, T defaultValue, List<T> list, String listLabel,
			ParameterSerializer<T, ?, ?> serializer) {
		super(input, id, name, description);
		this.value = value;
		this.defaultValue = defaultValue;
		this.list = list;
		this.listLabel = listLabel;
		this.serializer = serializer;
	}

	/**
	 * @return		the current selected value.
	 */
	public T getValue() {
		return value;
	}

	/**
	 * Modifies the current selected value.
	 * 
	 * @param value		the new selected value; must be one of the instances of {@link #list}, although there is not an actual validation; 
	 * 					if it is not in the list, unexpected errors may occur.
	 */
	public void setValue(T value) {
		this.value = value;
	}

	/**
	 * @return		the current default selected value.
	 */
	public T getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Modifies the default selected value.
	 * 
	 * @param defaultValue		the default selected value; must be one of the instances of {@link #list}, although there is not an actual
	 * 							validation; if it is not in the list, unexpected errors may occur.
	 */
	public void setDefaultValue(T defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * @return		the list of possible values for this parameter.
	 */
	public List<T> getList() {
		return list;
	}

	/**
	 * Modifies the list of possible values. When modifying this list, make sure to update the selected value and default selected value, as
	 * they must be elements of this list, and there is no automatic validation for this constraint.
	 * 
	 * @param list		the new list of possible values for this parameter.
	 */
	public void setList(List<T> list) {
		this.list = list;
	}

	/**
	 * @return		the attribute which getter will provide a <code>String</code> value representing the selection elements.
	 */
	public String getListLabel() {
		return listLabel;
	}

	/**
	 * Modifies the attribute used to retrieve a <code>String</code> value representing the selection elements.
	 * 
	 * @param listLabel		the new attribute name, from class 'T', to be used as element label.
	 */
	public void setListLabel(String listLabel) {
		this.listLabel = listLabel;
	}
	
	/**
	 * Selection parameters accept generic types. Therefore, in order to allow persistence, a {@link ParameterSerializer} must be provided.  
	 * 
	 * @return		the current instance of parameter serialized this parameter is using.
	 */
	public ParameterSerializer<T, ?, ?> getSerializer() {
		return serializer;
	}
	
	/**
	 * Selection parameters accept generic types. Therefore, in order to allow persistence, a {@link ParameterSerializer} must be provided.
	 * This method allows to modify the parameter serializer used by this instance.
	 * 
	 * @param serializer		the new parameter serializer.
	 */
	public void setSerializer(ParameterSerializer<T, ?, ?> serializer) {
		this.serializer = serializer;
	}
	
	// Utilitary Methods
	
	/**
	 * @return		a list of <code>String</code>s of all element's labels; attempts to invoke the getter method of {@link #listLabel} of 
	 * 				each element in {@link #list}; if this is not possible, attempts to invoke a method named {@link #listLabel}; in case of
	 * 				failure, returns the {@link Object#toString()} result for that element. 
	 */
	public List<String> getListLabels() {
		List<String> result = new ArrayList<String>();
		for (T object : this.list) {
			Object labelObject = null;
			//Try listLabel getter method
			try {
				labelObject = ReflectionsUtil.invokeGetter(object, this.listLabel);
			} catch (Exception e1) {
				//Could not use listLabel getter method. Try listLabel method directly.
				try {
					labelObject = ReflectionsUtil.invokeMethod(object, this.listLabel);
				} catch (Exception e2) {
					//Could not find listLabel method. Use object.toString().
				}
			}
			if (labelObject != null) {
				result.add(labelObject.toString());
			} else {
				result.add(object.toString());
			}
		}
		return result;
	}
	
	/**
	 * Creates a new instance of <code>SelectionParameter</code> which is a clone of this one. Some attributes may be shared between
	 * instances, such as the serializer.
	 * 
	 *  @return 	a new instance of <code>SelectionParameter</code> with the same attribute values as this one.
	 */
	@Override
	public SelectionParameter<T> clone() {
		SelectionParameter<T> clone = new SelectionParameter<T>();
		super.clone(clone);
		clone.setValue(this.value);
		clone.setDefaultValue(this.defaultValue);
		clone.setList(this.list);
		clone.setListLabel(this.listLabel);
		clone.setSerializer(this.serializer);
		return clone;
	}
}
