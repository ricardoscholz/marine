package mustic.scholz.marine.core.performance.element.parameter;

import mustic.scholz.marine.core.performance.element.InputTypeEnum;

/**
 * Actual implementation of a parameter which holds a text. Text values are saved on {@link String} attributes. A text parameter holds a 
 * 'value' and a 'default value'.
 * 
 * @author Ricardo Scholz
 */
public class TextParameter extends Parameter {

	private String value;
	private String defaultValue;
	
	/**
	 * Empty default constructor.
	 */
	public TextParameter() {
		super();
	}
	
	/**
	 * Builds a new instance of a text parameter, with the given attribute values.
	 * 
	 * @param input				the input type, one of {@link InputTypeEnum} options.
	 * @param id				the parameter ID, for fast parameter retrieval by the 
	 * 							{@link mustic.scholz.marine.core.performance.element.Element}.
	 * @param name				the parameter name.
	 * @param description		the parameter description.
	 * @param value				a <code>String</code> holding the text value.
	 * @param defaultValue		a <code>String</code> holding the default color value.
	 */
	public TextParameter(InputTypeEnum input, Integer id, String name,
			String description, String value, String defaultValue) {
		super(input, id, name, description);
		this.value = value;
		this.defaultValue = defaultValue;
	}

	/**
	 * @return		the current text value hold by this instance.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Modifies the text value hold by this instance.
	 * 
	 * @param value		the new text value.
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return		the default text value hold by this instance.
	 */
	public String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Modifies the default text value hold by this instance.
	 * 
	 * @param defaultValue		the new default text value.
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	/**
	 * Clones this instance, creating new instances of inner objects in the chain.
	 * 
	 * @return		a new text parameter instance, which is a clone of this one.
	 */
	@Override
	public TextParameter clone() {
		TextParameter clone = new TextParameter();
		super.clone(clone);
		clone.setValue(this.value);
		clone.setDefaultValue(this.defaultValue);
		return clone;
	}
}
