package mustic.scholz.marine.core.performance.element.parameter;

import mustic.scholz.marine.core.performance.element.InputTypeEnum;

/**
 * Actual implementation of a parameter which holds a flag. Flag values are saved on primitive <code>boolean</code> attributes. A flag 
 * parameter holds a 'value' and a 'default value'.
 * 
 * @author Ricardo Scholz
 *
 */
public class FlagParameter extends Parameter {

	private boolean value;
	private boolean defaultValue;
	
	/**
	 * Empty default constructor.
	 */
	public FlagParameter() {
		super();
	}
	
	/**
	 * Builds a new instance of <code>FlagParameter</code>, with the given attribute values.
	 * 
	 * @param input				the input type, one of {@link InputTypeEnum} options.
	 * @param id				the parameter ID, for fast parameter retrieval by the 
	 * 							{@link mustic.scholz.marine.core.performance.element.Element}.
	 * @param name				the parameter name.
	 * @param description		the parameter description.
	 * @param value				a boolean holding the flag value.
	 * @param defaultValue		a boolean holding the flag default value.
	 */
	public FlagParameter(InputTypeEnum input, Integer id, String name,
			String description, boolean value, boolean defaultValue) {
		super(input, id, name, description);
		this.value = value;
		this.defaultValue = defaultValue;
	}

	/**
	 * @return		this instance current value.
	 */
	public boolean getValue() {
		return value;
	}

	/**
	 * Modifies the current flag value.
	 * 
	 * @param value		the new flag value.
	 */
	public void setValue(boolean value) {
		this.value = value;
	}

	/**
	 * @return		this instance default value.
	 */
	public boolean getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Modifies the default flag value.
	 * 
	 * @param defaultValue		the new default flag value.
	 */
	public void setDefaultValue(boolean defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	/**
	 * Clones this instance, creating new instances of inner objects in the chain.
	 * 
	 * @return		a new flag parameter instance, which is a clone of this one.
	 */
	@Override
	public FlagParameter clone() {
		FlagParameter clone = new FlagParameter();
		super.clone(clone);
		clone.setValue(this.value);
		clone.setDefaultValue(this.defaultValue);
		return clone;
	}
}
