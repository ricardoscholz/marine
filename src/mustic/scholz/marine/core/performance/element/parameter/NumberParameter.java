package mustic.scholz.marine.core.performance.element.parameter;

import mustic.scholz.marine.core.performance.element.InputTypeEnum;

/**
 * Actual implementation of a parameter which holds a numeric value. Numeric values are saved on primitive <code>float</code> attributes. A 
 * numeric parameter holds four attributes: a 'value', a 'default value', a 'minimum value' and a 'maximum value'. Although minimum and 
 * maximum values may be defined for a given numeric parameter instance, there is no actual verification of these limits when a new value
 * is set. These attributes are supposed to help in the use of UI elements, such as sliders, and during programming, for normalisation of
 * values.
 * 
 * @author Ricardo Scholz
 */
public class NumberParameter extends Parameter {

	private float value;
	private float defaultValue;
	private float minValue;
	private float maxValue;
	
	/**
	 * Empty default constructor.
	 */
	public NumberParameter() {
		super();
	}
	
	/**
	 * Builds a new instance of a numeric parameter, with the given attribute values.
	 * 
	 * @param input				the input type, one of {@link InputTypeEnum} options.
	 * @param id				the parameter ID, for fast parameter retrieval by the 
	 * 							{@link mustic.scholz.marine.core.performance.element.Element}.
	 * @param name				the parameter name.
	 * @param description		the parameter description.
	 * @param value				a floating number holding the value.
	 * @param defaultValue		a floating number holding the default value.
	 * @param minValue			a floating number holding the minimum expected value for this parameter.
	 * @param maxValue			a floating number holding the maximum expected value for this parameter.
	 */
	public NumberParameter(InputTypeEnum input, Integer id, String name, String description, float value, float defaultValue, 
			float minValue, float maxValue) {
		super(input, id, name, description);
		this.value = value;
		this.defaultValue = defaultValue;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	/**
	 * @return		this instance current value.
	 */
	public float getValue() {
		return value;
	}

	/**
	 * Modifies this instance current value.
	 * 
	 * @param value		the new value.
	 */
	public void setValue(float value) {
		this.value = value;
	}

	/**
	 * @return		this instance current default value.
	 */
	public float getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Modifies this instance default value.
	 * 
	 * @param defaultValue		the new default value.
	 */
	public void setDefaultValue(float defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * @return 		this instance current minimum value.
	 */
	public float getMinValue() {
		return minValue;
	}

	/**
	 * Modifies this instance current minimum value. No actual verification is made against the {@link #value} attribute, at any time.
	 *  
	 * @param minValue		the new minimum value expected for this parameter.
	 */
	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}

	/**
	 * @return		this instance current maximum value.
	 */
	public float getMaxValue() {
		return maxValue;
	}

	/**
	 * Modifies this instance current maximum value. No actual verification is made against the {@link #value} attribute, at any time.
	 *  
	 * @param maxValue		the new maximum value expected for this parameter.
	 */
	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}
	
	/**
	 * Clones this instance, creating new instances of inner objects in the chain.
	 * 
	 * @return		a new number parameter instance, which is a clone of this one.
	 */
	@Override
	public NumberParameter clone() {
		NumberParameter clone = new NumberParameter();
		super.clone(clone);
		clone.setValue(this.value);
		clone.setDefaultValue(this.defaultValue);
		clone.setMinValue(this.minValue);
		clone.setMaxValue(this.maxValue);
		return clone;
	}
	
}
