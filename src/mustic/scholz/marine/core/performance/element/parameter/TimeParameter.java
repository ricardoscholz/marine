package mustic.scholz.marine.core.performance.element.parameter;

import mustic.scholz.marine.core.performance.element.InputTypeEnum;

/**
 * Actual implementation of a parameter which holds a time duration value. Time duration values are saved on primitive <code>long</code> 
 * attributes, in the same format of {@link System#currentTimeMillis()}, meaning that time information is saved with milliseconds precision.
 * A time duration parameter holds a 'value' and a 'default value'.
 * 
 * @author Ricardo Scholz
 *
 */
//Include in Javadoc the possibility to use TimeUtil.java for time handling methods.
public class TimeParameter extends Parameter {

	private long value;
	private long defaultValue;
	
	/**
	 * Empty default constructor.
	 */
	public TimeParameter() {
		super();
	}
	
	/**
	 * Builds a new instance of a time duration parameter, with the given attribute values.
	 * 
	 * @param input				the input type, one of {@link InputTypeEnum} options.
	 * @param id				the parameter ID, for fast parameter retrieval by the
	 * 							{@link mustic.scholz.marine.core.performance.element.Element}.
	 * @param name				the parameter name.
	 * @param description		the parameter description.
	 * @param value				a floating point number holding the time value, as described in this class documentation header.
	 * @param defaultValue		a floating point number holding the default time value, as described in this class documentation header.
	 */
	public TimeParameter(InputTypeEnum input, Integer id, String name, String description, long value, long defaultValue) {
		super(input, id, name, description);
		this.value = value;
		this.defaultValue = defaultValue;
	}

	/**
	 * @return		the current time duration value hold by this instance.
	 */
	public long getValue() {
		return value;
	}

	/**
	 * Modifies the time duration value hold by this instance.
	 * 
	 * @param value		the new time duration value.
	 */
	public void setValue(long value) {
		this.value = value;
	}

	/**
	 * @return		the default time duration value hold by this instance.
	 */
	public long getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Modifies the default time duration value hold by this instance.
	 * 
	 * @param defaultValue		the new default time duration.
	 */
	public void setDefaultValue(long defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	/**
	 * Clones this instance, creating new instances of inner objects in the chain.
	 * 
	 * @return		a new time parameter instance, which is a clone of this one.
	 */
	@Override
	public TimeParameter clone() {
		TimeParameter clone = new TimeParameter();
		super.clone(clone);
		clone.setValue(this.value);
		clone.setDefaultValue(this.defaultValue);
		return clone;
	}
}
