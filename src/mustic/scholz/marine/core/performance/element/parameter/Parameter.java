package mustic.scholz.marine.core.performance.element.parameter;

import mustic.scholz.marine.core.performance.element.InputTypeEnum;

/**
 * This is an abstract implementation of a parameter. Elements use parameters to allow end-users to easily modify parameters within an 
 * element. There are several parameter actual implementation, with their particular features. This abstract implementation guarantees that
 * all parameters provide some common attributes, allowing some level of generic programming with parameters at UI level. 
 * 
 * NOTE: A better approach for internationalisation of parameter name and description is lacking; currently, these potential user accessible
 * text is hard coded. 
 * 
 * @author Ricardo Scholz
 */
//TODO [Refactoring] Use better approach for parameter name and description, suitable for internationalisation.
public abstract class Parameter implements Cloneable {

	private InputTypeEnum input;

	private Integer id;
	private String name;
	private String description;
	
	/**
	 * Default empty constructor.
	 */
	public Parameter() {
	}
	
	/**
	 * Builds a new instance of <code>Parameter</code>, with the given attribute values.
	 * 
	 * @param input			the input type of this instance.
	 * @param id			the ID of this instance, used for further recovery of parameter within element parameters hash map.
	 * @param name			the name of this parameter. 
	 * @param description	a description of this parameter.
	 */
	public Parameter(InputTypeEnum input, Integer id, String name, String description) { 
		this.input = input;
		this.id = id;
		this.name = name;
		this.description = description;
	}

	/**
	 * @return		the input type of this instance.
	 * @see mustic.scholz.marine.core.performance.element.InputTypeEnum
	 */
	public InputTypeEnum getInput() {
		return input;
	}

	/**
	 * Modifies the input type of this instance.
	 * 
	 * @param input		the new input type of this instance.
	 * @see mustic.scholz.marine.core.performance.element.InputTypeEnum
	 */
	public void setInput(InputTypeEnum input) {
		this.input = input;
	}

	/**
	 * @return		the ID of this instance.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Modifies the ID of this instance.
	 * @param id	the new ID of the instance.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return		the name of this instance; this information is potentially accessible to end-users.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Modifies the name of this instance; parameter names are potentially accessible to end-users.
	 * 
	 * @param name		the new name of the instance.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return		the description of this instance; this information is potentially accessible to end-users.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Modifies the description of this instance; parameter descriptions are potentially accessible to end-users.
	 * 
	 * @param description		the new description of the instance.
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Fills a parameter instance with this instance's attributes.
	 * 
	 * @param parameter		the parameter instance to fill with this instance's attributes.
	 */
	public void clone(Parameter parameter) {
		int primitiveId = this.id;
		parameter.setId(primitiveId);
		parameter.setInput(this.input);
		parameter.setDescription(this.description);
		parameter.setName(this.name);
	}
	
	/**
	 * All subclasses of this class MUST implement a public 'clone()' method with no arguments. This method is abstract
	 * so one can assume parameters will always implement this method in compilation time.
	 * 
	 * @return		a clone of the concrete parameter implementation.
	 */
	@Override
	public abstract Parameter clone();
	
}
