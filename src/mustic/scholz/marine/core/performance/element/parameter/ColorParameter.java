package mustic.scholz.marine.core.performance.element.parameter;

import mustic.scholz.marine.core.performance.element.InputTypeEnum;
import mustic.scholz.marine.core.utils.ColorUtil;

/**
 * Actual implementation of a parameter which holds a color. Color values are saved on primitive <code>int</code> attributes, in the format
 * 0xAARRGGBB, where AA=Alpha, RR=Red, GG=Green and BB=Blue. A color parameter holds a 'value' and a 'default value'.
 * 
 * @author Ricardo Scholz
 *
 */
public class ColorParameter extends Parameter {

	private int value;
	private int defaultValue;
	
	/**
	 * Empty default constructor.
	 */
	public ColorParameter() {
		super();
	}
	
	/**
	 * Builds a new instance of a color parameter, with the given attribute values.
	 * 
	 * @param input				the input type, one of {@link InputTypeEnum} options.
	 * @param id				the parameter ID, for fast parameter retrieval by the 
	 * 							{@link mustic.scholz.marine.core.performance.element.Element}.
	 * @param name				the parameter name.
	 * @param description		the parameter description.
	 * @param value				an integer holding the color value, as described in this class documentation header.
	 * @param defaultValue		an integer holding the default color value, as described in this class documentation header.
	 */
	public ColorParameter(InputTypeEnum input, Integer id, String name,
			String description, int value, int defaultValue) {
		super(input, id, name, description);
		this.value = value;
		this.defaultValue = defaultValue;
	}

	/**
	 * @return		the color this parameter is holding.
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Modifies the color this parameter is holding.
	 * 
	 * @param value		the new color value, in the format 0xAARRGGBB.
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * @return		the default color value of this instance.
	 */
	public int getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Modifies the default color of this instance.
	 * 
	 * @param defaultValue		the new default color value.
	 */
	public void setDefaultValue(int defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	// Utilitary Methods
	
	/**
	 * Allows modification of color attribute, by specifying alpha, red, green and blue channels separately.
	 *  
	 * @param alpha		the new alpha value (0 - 255, with 255 meaning fully opaque).
	 * @param red		the new red value.
	 * @param green		the new green value.
	 * @param blue		the new blue value.
	 */
	public void setColor(byte alpha, byte red, byte green, byte blue) {
		this.setAlpha(alpha);
		this.setRed(red);
		this.setGreen(green);
		this.setBlue(blue);
	}
	
	/**
	 * @return		retrieves the alpha value hold by this instance (0 - 255).
	 */
	public byte getAlpha() {
		return ColorUtil.getAlpha(this.value);
	}
	
	/**
	 * Modifies the alpha value hold by this instance, without modifying the RGB information.
	 * 
	 * @param alpha		the new alpha value (0-255).
	 */
	public void setAlpha(byte alpha) {
		this.value = ColorUtil.setAlpha(this.value, alpha);
	}
	
	/**
	 * @return		the red value hold by this instance.
	 */
	public byte getRed() {
		return ColorUtil.getRed(this.value);
	}
	
	/**
	 * Modifies the red value hold by this instance, without modifying the other RGB or alpha information.
	 * 
	 * @param red		the new red value (0-255).
	 */
	public void setRed(byte red) {
		this.value = ColorUtil.setRed(this.value, red);
	}
	
	/**
	 * @return		the green value hold by this instance.
	 */
	public byte getGreen() {
		return ColorUtil.getGreen(this.value);
	}
	
	/**
	 * Modifies the green value hold by this instance, without modifying the other RGB or alpha information.
	 * 
	 * @param green		the new green value (0-255).
	 */
	public void setGreen(byte green) {
		this.value = ColorUtil.setGreen(this.value, green);
	}
	
	/**
	 * @return		the blue value hold by this instance.
	 */
	public byte getBlue() {
		return ColorUtil.getBlue(this.value);
	}
	
	/**
	 * Modifies the blue value hold by this instance, without modifying the other RGB or alpha information.
	 * 
	 * @param blue		the new blue value (0-255).
	 */
	public void setBlue(byte blue) {
		this.value = ColorUtil.setBlue(this.value, blue);
	}
	
	/**
	 * Clones this instance, creating new instances of inner objects in the chain.
	 * 
	 * @return		a new color parameter instance, which is a clone of this one.
	 */
	@Override
	public ColorParameter clone() {
		ColorParameter clone = new ColorParameter();
		super.clone(clone);
		clone.setValue(this.value);
		clone.setDefaultValue(this.defaultValue);
		return clone;
	}
}
