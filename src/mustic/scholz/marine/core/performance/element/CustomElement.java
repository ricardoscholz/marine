package mustic.scholz.marine.core.performance.element;

/**
 * This class encapsulates an element with custom parameters, with a key and a name. This was first implemented in order to allow the
 * facade to save and retrieve the same element with different parameters configurations. Therefore, a bank of customised elements can be
 * implemented.
 * 
 * @author Ricardo Scholz
 */
public class CustomElement implements Cloneable {
	
	private Element element;
	private String name;
	private int key;
	private String directory;
	
	/**
	 * @param element	the element to be encapsulated. 
	 * @param name		the name of this particular configuration of the element.
	 * @param key		the key of this particular configuration of the element.
	 * @param directory	the directory under plug-ins' directory the original plug-in element is placed.
	 */
	public CustomElement(Element element, String name, int key, String directory) {
		this.element = element;
		this.name = name;
		this.key = key;
		this.directory = directory;
	}
	
	/**
	 * Constructor. Sets {@link #directory} attribute to <code>null</code>. Directory is used mainly to load icons images. Therefore,
	 * if using the core project directly, it may be unnecessary to compute the directory for every custom element created.
	 * 
	 * @param element	the element to be encapsulated. 
	 * @param name		the name of this particular configuration of the element.
	 * @param key		the key of this particular configuration of the element.
	 */
	public CustomElement(Element element, String name, int key) {
		this.element = element;
		this.name = name;
		this.key = key;
		this.directory = "";
	}
	
	/**
	 * @return		the encapsulated element instance, with its customised features set.
	 */
	public Element getElement() {
		return element;
	}
	
	/**
	 * Modifies the encapsulate element.
	 * 
	 * @param element		the new element to be encapsulated.
	 */
	public void setElement(Element element) {
		this.element = element;
	}
	
	/**
	 * @return		the name of this customised element instance.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Modifies the name of this customised element instance.
	 * 
	 * @param name		the new name of the customised element.
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return		the key of this customised element instance.
	 */
	public int getKey() {
		return key;
	}
	
	/**
	 * Modifies the key of this customised element instance.
	 * 
	 * @param key	the new key of the customised element.
	 */
	public void setKey(int key) {
		this.key = key;
	}
	
	/**
	 * @return	the relative directory of the original plug-in element, within plug-ins' directory.
	 */
	public String getDirectory() {
		return directory;
	}
	
	/**
	 * Modifies the relative directory of the original plug-in element, within plug-ins' directory.
	 * @param directory		the new relative directory.
	 */
	public void setDirectory(String directory) {
		this.directory = directory;
	}
	
	/**
	 * Clones this instance, creating new instances of inner objects in the chain, so a different customisation can be saved. Not all
	 * objects in the chain are cloned, as they can or should be shared among custom element instances.
	 * 
	 * @return		a new custom element instance, which is a clone of this one.
	 */
	@Override
	public CustomElement clone() {
		return new CustomElement(this.element.clone(), this.name, this.key, this.directory);
	}
}
