package mustic.scholz.marine.core.performance.element;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import mustic.scholz.marine.core.PerformanceFacade;
import mustic.scholz.marine.core.input.IInputReceiver;
import mustic.scholz.marine.core.performance.Performance;
import mustic.scholz.marine.core.performance.element.parameter.Parameter;
import mustic.scholz.marine.core.utils.ReflectionsUtil;

/**
 * This is the abstract element implementation. The <code>Element</code> class is the root class of all elements that a {@link Performance}
 * can load, insert into its elements' grid and play. This class provides a built-in parameters management, using a 
 * <code>HashMap</code> for faster retrieval of parameter values. Also, it defines some general attributes of any
 * element: name, description, version, author and icon path. Some of its methods are empty, and should be overridden appropriately: 
 * {@link Element#setup()}, {@link Element#update()}, {@link Element#paint()}, {@link Element#destroy()} and 
 * {@link Element#fireCommand(int, Object...)}.
 * 
 * @author Ricardo Scholz
 *
 */
public abstract class Element implements Cloneable {

	// Attributes
	
	/**
	 * The performance instance running this element. The facade will set it as appropriate when execution starts. This reference is
	 * necessary for painting purposes.
	 */
	protected Performance performance;
	
	/**
	 * The active input receiver instance when this element is running. The facade will set it as appropriate when execution starts. This
	 * reference is necessary for input receiver information retrieval by the implementing subclasses.
	 */
	protected IInputReceiver inputReceiver;
	
	private String description;
	private String version;
	private String author;
	private String name;
	
	private boolean setupRun;
	
	/**
	 * The implementing subclasses may access parameters directly through this attribute. Every attribute must have an element-unique key.
	 */
	protected HashMap<Integer, Parameter> parameters;
	
	// Constructors
	
	/**
	 * Default constructor. Initialises the parameters hash map, with an empty instance. 
	 */
	public Element() {
		this.parameters = new HashMap<Integer, Parameter>();
		this.setupRun = false;
	}
	
	// Getters and Setters
	
	/**
	 * The performance instance is used for the implementing subclasses to paint on screen. This attribute will be properly set by the
	 * {@link PerformanceFacade} when execution starts.  
	 * 
	 * @return		the current performance instance where this element is drawing in.
	 */
	public Performance getPerformance() {
		return performance;
	}

	/**
	 * Modifies the performance instance related to this element. This method is normally used by the {@link PerformanceFacade} when
	 * preparing the elements for execution. Unless you are not using the performance facade to manage your execution, do not set the
	 * performance instance manually.
	 * 
	 * @param performance		the new performance instance where this element will draw in.
	 */
	public void setPerformance(Performance performance) {
		this.performance = performance;
	}
	
	/**
	 * The input receiver holds important information which can be used by agents to augment their power of interaction. Therefore, this
	 * attributes holds the current {@link IInputReceiver} at execution time. It will be properly set by the {@link PerformanceFacade} when
	 * execution starts.
	 * 
	 * @return		the current input receiver instance from which this element may retrieve input device setup informations.
	 */
	public IInputReceiver getInputReceiver() {
		return inputReceiver;
	}
	
	/**
	 * Modifies the input receiver instance related to this element. This method is normally used by the {@link PerformanceFacade} when
	 * preparing the element for execution. Unless you are not using the performance facade to manage your execution, do not set the
	 * input receiver manually.
	 * 
	 * @param inputReceiver		the new input receiver instance.
	 */
	public void setInputReceiver(IInputReceiver inputReceiver) {
		this.inputReceiver = inputReceiver;
	}
	
	/**
	 * @return		the element's description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Modifies the element's description.
	 * 
	 * @param description		the new element's description.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return		the element's version.
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Modifies the element's version.
	 * 
	 * @param version		the new element's version.
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return		the element's author.
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * Modifies the element's author.
	 * 
	 * @param author		the new element's author.
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * The abstract element holds a hash map of element parameters. This allows implementing subclasses to store and retrieve their
	 * parameters with faster access than using a list of parameters. Every parameter is related to an integer key, so that parameters may
	 * be added, modified, retrieved or removed from the map in approximately constant time, when using the key. Each parameter must be
	 * associated with an unique key, for a given element.
	 * 
	 * @return		the element parameters' hash map.
	 */
	public HashMap<Integer, Parameter> getParameters() {
		return parameters;
	}

	/**
	 * Modifies the parameter's hash map. It is not likely that you will need to modify the whole parameters' map. Therefore, if you are
	 * calling this method, double check if it is really necessary for your approach, as creating and recreating the hash map may be more
	 * expensive than just adding and removing items.
	 * 
	 * @param parameters		the new hash map of parameters.
	 */
	public void setParameters(HashMap<Integer, Parameter> parameters) {
		this.parameters = parameters;
	}
	
	/**
	 * @return		the element's name.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Modifies the element's name.
	 * 
	 * @param name		the new element's name.
	 */
	public void setName(String name) {
		this.name = name;
	}
//	
//	/**
//	 * Modifies the flag which indicates whether setup has already run. Allows external control of setup running, as
//	 * internal control may not be safe, due to third party element programmers not calling <code>super.setup()</code>
//	 * within their <code>setup()</code> implementations.
//	 * 
//	 * @param setupRun		<code>true</code> if <code>setup()</code> method of this element has already been called; 
//	 * 						<code>false</code> otherwise.
//	 */
//	public void setSetupRun(boolean setupRun) {
//		this.setupRun = setupRun;
//	}
	
	/**
	 * Indicates that <code>setup()</code> method of this element has already been called. As this flag is not
	 * automatically set on call, there is no guarantee it is consistent.
	 * 
	 * @return	<code>true</code> if this element's <code>setup()</code> method has been called.
	 */
	public boolean isSetupRun() {
		return this.setupRun;
	}

	// Concrete Methods

	/**
	 * This method is executed once before the element start to run. It calls {@link #setup()} implementation and
	 * sets the <code>setupRun</code> flag <code>true</code>.
	 */
	public void runSetup() {
		this.setup();
		this.setupRun = true;
	}
	
	/**
	 * This method is executed once before the element start to run. Override it if you need to load any resources so that your
	 * implementing subclass may avoid higher processor usage during execution. However, make sure your <code>setup()</code> implementation
	 * does not take to long to run, as this may cause an exception in <code>PApplet</code>.
	 */
	protected void setup() {
	}
	
	/**
	 * This method runs every cycle. It calls {@link #update()} implementation.
	 */
	public void runUpdate() {
		this.update();
	}
	
	/**
	 * This method runs every cycle, and is supposed to update the element's world with the current data; most element's will need to
	 * override this method, to implement their own behaviour logic.
	 */
	protected void update() {}
	
	/**
	 * This method runs every cycle. It calls {@link #paint()} implementation.
	 */
	public void runPaint() {
		this.paint();
	}
	
	/**
	 * This method runs every cycle, after the {@link Element#update()} method is called. It is supposed to encapsulate code used to draw on 
	 * screen. If your element does not draw on screen, you do not need to override this method.
	 */
	protected void paint() {}
	
	/**
	 * This method is called once at the end of the element execution. It should be used to free any resources allocated by the implementing
	 * subclass. 
	 */
	public void destroy() {}
		
	/**
	 * This method is responsible to forward numeric commands to the elements. These commands may come from anywhere: a keyboard, a
	 * source code call, etc. 
	 * 
	 * NOTE: This model of element's dynamic control through numeric commands is not final yet, and it may be refactored with no guarantees
	 * of backward compatibility.
	 * 
	 * @param command		a numeric command.
	 * @param params		any command related parameters.
	 */
	public void fireCommand(int command, Object... params) {}
	
	/**
	 * This overridden implementation of {@link Object#equals(Object)} will return <code>true</code> when the object received as parameter
	 * is an element with the same name, author and version of this instance.
	 * 
	 * @param otherObj		an other object instance to be compared with this instance.
	 * @return				<code>true</code> if the name, author and version of the <code>otherObj</code> are the same of this instance;
	 * 						<code>false</code> otherwhise.
	 */
	@Override
	public boolean equals(Object otherObj) {
		Element other = null;
		if (otherObj instanceof Element) {
			other = (Element) otherObj;
		}
		
		if (other != null) {
			return (other.author != null && this.author != null
						&& other.author.equalsIgnoreCase(this.author)
					) && 
					(other.name != null && this.name != null
						&& other.name.equalsIgnoreCase(this.name)
					) &&
					(other.version != null && this.version != null
						&& other.version.equalsIgnoreCase(this.version));
		}
		return false;
	}
	
	/**
	 * Clones this instance, creating new instances of inner objects in the chain, so a different customisation can be saved. Not all
	 * objects in the chain are cloned, as they can or should be shared among element instances.
	 * 
	 * @return		a new element instance, which is a clone of this one.
	 */
	@Override
	public Element clone() {
		
		Element clone;
		try {
			clone = (Element) ReflectionsUtil.createInstance(this.getClass().getCanonicalName());
			this.cloneFields(clone);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			throw new RuntimeException("All " + Element.class.getCanonicalName() + " must implement an empty public constructor.");
		}
		
		return clone;
	}
	
	/**
	 * Updates a given element instance with this instance's attributes. This method is protected so classes inheriting from this class
	 * can implement their own <code>clone()</code> method, reusing this piece of code for superclass attributes.
	 * 
	 * @param clone		a clone of this element which fields should be filled with this instance's attributes. 
	 */
	protected void cloneFields(Element clone) {
		clone.setPerformance(this.performance);
		clone.setInputReceiver(this.inputReceiver);
		clone.setDescription(this.description);
		clone.setVersion(this.version);
		clone.setAuthor(this.author);
		clone.setName(this.name);
		
		HashMap<Integer, Parameter> newParameters = new HashMap<Integer, Parameter>();
		Set<Entry<Integer, Parameter>> entries = this.parameters.entrySet();
		for (Entry<Integer, Parameter> entry : entries) {
			int key = entry.getKey();
			Parameter value = entry.getValue().clone();
			newParameters.put(key, value);
		}
		clone.setParameters(newParameters);
	}
}
