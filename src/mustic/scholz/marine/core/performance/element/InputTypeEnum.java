package mustic.scholz.marine.core.performance.element;

/**
 * This enumeration has been defined to allow element programmers to define some element configuration UI behaviour. It enumerates the types
 * of input, so that every element parameter may indicate what should be the more appropriate input type UI element for configuration screen
 * purposes.
 * 
 * NOTE: It is sad that UI related code is living into a core package; therefore, this model may be changed with no guarantees of backward
 * compatibility if a better model is identified. 
 * 
 * @author Ricardo Scholz
 */
public enum InputTypeEnum {

	/**
	 * Defines a UI slider which accepts non-integer values.
	 */
	FLOAT_SLIDER (1),
	/**
	 * Defines a UI slider which accepts only integer values.
	 */
	INTEGER_SLIDER (2),
	/**
	 * Defines a UI text box.
	 */
	TEXT_BOX (3),
	/**
	 * Defines a UI element which opens the file explorer so a path to a file can be chosen.
	 */
	FILE_PATH(4),
	/**
	 * Defines a UI element which opens the file explorer so a path to a directory can be chosen.
	 */
	DIRECTORY_PATH(5),
	/**
	 * Defines a UI element which allows the user to pick one element of a list of elements.
	 */
	SELECTION_LIST (6),
	/**
	 * Defines a UI switch element, allowing the user to turn in or of some parameter.
	 */
	SWITCH (7),
	/**
	 * Defines a UI element which holds a time value.
	 */
	TIME (8),
	/**
	 * Defines a UI element which allows the user to chose a color.
	 */
	COLOR (9);
	
	private Integer id;
	
	private InputTypeEnum(Integer id) {
		this.id = id;
	}

	/**
	 * @return		this input type ID.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Modifies this input type ID.
	 * 
	 * @param id	the new input type ID.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Retrieves an instance of <code>InputTypeEnum</code> by its ID.
	 * 
	 * @param id	the ID of the input type enumerator desired.
	 * @return		the input type enumerator instance which matches the given ID.
	 */
	public static InputTypeEnum getById(int id) {
		InputTypeEnum result = null;
		if (id == FLOAT_SLIDER.id) {
			result = FLOAT_SLIDER;
		} else if (id == INTEGER_SLIDER.id) {
			result = INTEGER_SLIDER;
		} else if (id == TEXT_BOX.id) {
			result = TEXT_BOX;
		} else if (id == FILE_PATH.id) {
			result = FILE_PATH;
		} else if (id == DIRECTORY_PATH.id) {
			result = DIRECTORY_PATH;
		} else if (id == SELECTION_LIST.id) {
			result = SELECTION_LIST;
		} else if (id == SWITCH.id) {
			result = SWITCH;
		} else if (id == TIME.id) {
			result = TIME;
		} else if (id == COLOR.id) {
			result = COLOR;
		}
		return result;
	}
}
