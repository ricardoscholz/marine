package mustic.scholz.marine.core.performance.element.builtin.skeleton;

import mustic.scholz.marine.core.entity.JointEnum;

/**
 * Enumeration that maps all information of bones in a skeleton.
 *  
 * @author Ricardo Scholz
 */
public enum BoneEnum {

	BONE_LEFT_TOE_ANKLE (JointEnum.LEFT_TOE, JointEnum.LEFT_ANKLE, 0),
	BONE_RIGHT_TOE_ANKLE (JointEnum.RIGHT_TOE, JointEnum.RIGHT_ANKLE, 1),
	BONE_LEFT_ANKLE_KNEE (JointEnum.LEFT_ANKLE, JointEnum.LEFT_KNEE, 2),
	BONE_RIGHT_ANKLE_KNEE (JointEnum.RIGHT_ANKLE, JointEnum.RIGHT_KNEE, 3),
	BONE_LEFT_KNEE_HIP (JointEnum.LEFT_KNEE, JointEnum.LEFT_HIP, 4),
	BONE_RIGHT_KNEE_HIP (JointEnum.RIGHT_KNEE, JointEnum.RIGHT_HIP, 5),
	BONE_LEFT_HIP_HIPS (JointEnum.ABDOMEN, JointEnum.LEFT_HIP, 6),
	BONE_RIGHT_HIP_HIPS (JointEnum.ABDOMEN, JointEnum.RIGHT_HIP, 7),
	BONE_HIPS_ABDOMEN (JointEnum.HIPS, JointEnum.ABDOMEN, 8),
	BONE_LEFT_ABDOMEN_COLLAR (JointEnum.NECK, JointEnum.LEFT_COLLAR, 9),
	BONE_RIGHT_ABDOMEN_COLLAR (JointEnum.NECK, JointEnum.RIGHT_COLLAR, 10),
	BONE_LEFT_COLLAR_SHOULDER (JointEnum.LEFT_COLLAR, JointEnum.LEFT_SHOULDER, 11),
	BONE_RIGHT_COLLAR_SHOULDER (JointEnum.RIGHT_COLLAR, JointEnum.RIGHT_SHOULDER, 12),
	BONE_LEFT_SHOULDER_ELBOW (JointEnum.LEFT_SHOULDER, JointEnum.LEFT_ELBOW, 13),
	BONE_RIGHT_SHOULDER_ELBOW (JointEnum.RIGHT_SHOULDER, JointEnum.RIGHT_ELBOW, 14),
	BONE_LEFT_ELBOW_WRIST (JointEnum.LEFT_ELBOW, JointEnum.LEFT_WRIST, 15),
	BONE_RIGHT_ELBOW_WRIST (JointEnum.RIGHT_ELBOW, JointEnum.RIGHT_WRIST, 16),
	BONE_LEFT_WRIST_HAND (JointEnum.LEFT_WRIST, JointEnum.LEFT_HAND, 17),
	BONE_RIGHT_WRIST_HAND (JointEnum.RIGHT_WRIST, JointEnum.RIGHT_HAND, 18),
	BONE_ABDOMEN_CHEST (JointEnum.ABDOMEN, JointEnum.CHEST, 19),
	BONE_CHEST_NECK (JointEnum.CHEST, JointEnum.NECK, 20),
	BONE_NECK_HEAD (JointEnum.NECK, JointEnum.HEAD, 21);
	
	/**
	 * Total number of bones.
	 */
	public static final int NUMBER_OF_BONES = 22;
	
	private JointEnum from;
	
	private JointEnum to;
	
	private Integer index;

	private BoneEnum(JointEnum from, JointEnum to, Integer index) {
		this.from = from;
		this.to = to;
		this.index = index;
	}
	
	/**
	 * @return	the beginning joint of this bone.
	 */
	public JointEnum getFrom() {
		return from;
	}
	
	/**
	 * @return	the ending joint of this bone.
	 */
	public JointEnum getTo() {
		return to;
	}

	/**
	 * @return	the index of this bone.
	 */
	public Integer getIndex() {
		return index;
	}

	/**
	 * Retrieves a <code>BoneEnum</code> instance, given an index.
	 * 
	 * @param index		the index of the bone information to retrieve.
	 * @return			the <code>BoneEnum</code> instance with the given index; or <code>null</code> if the index is not found.
	 */
	public static BoneEnum getByIndex(int index) {
		if(BONE_LEFT_TOE_ANKLE.index == index) {
			return BONE_LEFT_TOE_ANKLE;
		} else if(BONE_RIGHT_TOE_ANKLE.index == index) {
			return BONE_RIGHT_TOE_ANKLE;
		} else if(BONE_LEFT_ANKLE_KNEE.index == index) {
			return BONE_LEFT_ANKLE_KNEE;
		} else if(BONE_RIGHT_ANKLE_KNEE.index == index) {
			return BONE_RIGHT_ANKLE_KNEE;
		} else if(BONE_LEFT_KNEE_HIP.index == index) {
			return BONE_LEFT_KNEE_HIP;
		} else if(BONE_RIGHT_KNEE_HIP.index == index) {
			return BONE_RIGHT_KNEE_HIP;
		} else if(BONE_LEFT_HIP_HIPS.index == index) {
			return BONE_LEFT_HIP_HIPS;
		} else if(BONE_RIGHT_HIP_HIPS.index == index) {
			return BONE_RIGHT_HIP_HIPS;
		} else if(BONE_HIPS_ABDOMEN.index == index) {
			return BONE_HIPS_ABDOMEN;
		} else if(BONE_LEFT_ABDOMEN_COLLAR.index == index) {
			return BONE_LEFT_ABDOMEN_COLLAR;
		} else if(BONE_RIGHT_ABDOMEN_COLLAR.index == index) {
			return BONE_RIGHT_ABDOMEN_COLLAR;
		} else if(BONE_LEFT_COLLAR_SHOULDER.index == index) {
			return BONE_LEFT_COLLAR_SHOULDER;
		} else if(BONE_RIGHT_COLLAR_SHOULDER.index == index) {
			return BONE_RIGHT_COLLAR_SHOULDER;
		} else if(BONE_LEFT_SHOULDER_ELBOW.index == index) {
			return BONE_LEFT_SHOULDER_ELBOW;
		} else if(BONE_RIGHT_SHOULDER_ELBOW.index == index) {
			return BONE_RIGHT_SHOULDER_ELBOW;
		} else if(BONE_LEFT_ELBOW_WRIST.index == index) {
			return BONE_LEFT_ELBOW_WRIST;
		} else if(BONE_RIGHT_ELBOW_WRIST.index == index) {
			return BONE_RIGHT_ELBOW_WRIST;
		} else if(BONE_LEFT_WRIST_HAND.index == index) {
			return BONE_LEFT_WRIST_HAND;
		} else if(BONE_RIGHT_WRIST_HAND.index == index) {
			return BONE_RIGHT_WRIST_HAND;
		} else if(BONE_ABDOMEN_CHEST.index == index) {
			return BONE_ABDOMEN_CHEST;
		} else if(BONE_CHEST_NECK.index == index) {
			return BONE_CHEST_NECK;
		} else if(BONE_NECK_HEAD.index == index) {
			return BONE_NECK_HEAD;
		}		
		return null;
	}
}
