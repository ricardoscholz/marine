package mustic.scholz.marine.core.performance.element.builtin.skeleton;

import mustic.scholz.marine.core.performance.Performance;
import processing.core.PApplet;

/**
 * This is a basic structure of the parts that form a {@link Skeleton} object. It concentrates all common code.
 * 
 * @author Ricardo Scholz
 */
public abstract class BasicElement {

	// Attributes
	
	/**
	 * The performance instance, used to draw elements on screen. It is <code>protected</code> so implementing subclasses can directly
	 * access it.
	 */
	protected Performance canvas;
	
	private int stroke;
	
	private float strokeAlpha;
	
	private int fill;
	
	private float fillAlpha;
	
	private boolean space3D;
	
	/**
	 * Default constructor.
	 * 
	 * @param performance		the performance which this element must use for drawing.
	 */
	public BasicElement(Performance performance) {
		this.canvas = performance;
		this.stroke = 0xFF000000;
		this.fill = 0xFF000000;
		this.strokeAlpha = 255;
		this.fillAlpha = 255;
	}
	
	/**
	 * @return	the performance instance used by this element for drawing.
	 */
	public PApplet getCanvas() {
		return this.canvas;
	}
	
	/**
	 * Modifies the performance instance used by this element for drawing.
	 * 
	 * @param canvas		the new performance instance.
	 */
	public void setCanvas(Performance canvas) {
		this.canvas = canvas;
	}
	
	/**
	 * @return	the stroke color used by this element.
	 */
	public int getStroke() {
		return this.stroke;
	}

	/**
	 * Modifies the stroke color to be used by this element, on future drawing methods calls.
	 * 
	 * @param rgb		the new color to be used, in 0xRRGGBB format, where RR, GG and BB must be between 00 and FF.
	 */
	public void setStroke(int rgb) {
		this.stroke = 0xFFFFFFFF & rgb;
	}

	/**
	 * @return	the stroke alpha used by this element.
	 */
	public float getStrokeAlpha() {
		return this.strokeAlpha;
	}
	
	/**
	 * Modifies the stroke alpha to be used by this element, on future drawing methods calls.
	 * 
	 * @param alpha		the new alpha to be used, from 0 to 255.
	 */
	public void setStrokeAlpha(float alpha) {
		this.strokeAlpha = alpha;
	}

	/**
	 * @return		the fill color used by this element.
	 */
	public int getFill() {
		return this.fill;
	}
	
	/**
	 * Modifies the fill color to be used by this element, on future drawing methods calls.
	 * 
	 * @param rgb		the new color to be used, in 0xRRGGBB format, where RR, GG and BB must be between 00 and FF.
	 */
	public void setFill(int rgb) {
		this.fill = 0xFFFFFFFF & rgb;
	}
	
	/**
	 * @return			the fill alpha used by this element.
	 */
	public float getFillAlpha() {
		return this.fillAlpha;
	}

	/**
	 * Modifies the fill alpha to be used by this element, on future drawing methods calls.
	 * 
	 * @param alpha		the new alpha to be used, from 0 to 255.
	 */
	public void setFillAlpha(float alpha) {
		this.fillAlpha = alpha;
	}
	
	/**
	 * @return		a flag indicating whether the element is using a three-dimensional space or not.
	 */
	public boolean getSpace3D() {
		return this.space3D;
	}
	
	/**
	 * Allows the element to use three-dimensional space, if <code>true</code>, or bi-dimensional space, if <code>false</code>.
	 * 
	 * @param space3D		<code>true</code> if element should use three-dimensional space from now on; <code>false</code> otherwise.	
	 */
	public void setSpace3D(boolean space3D) {
		this.space3D = space3D;
	}
	
	// Abstract Methods
	
	/**
	 * Abstract method. Must be overridden by any implementing subclass with drawing specific code.
	 */
	public abstract void paint();
	
	/**
	 * Abstract method. Must be overridden by any implementing subclass with logic updating specific code.
	 */
	public abstract void update();
	
	// Implemented Methods
	
	/**
	 * Prepares this element to be painted, by setting the stroke and fill color and alpha values.
	 */
	protected void prePaint() {
		this.canvas.stroke(this.stroke, this.strokeAlpha);
		this.canvas.fill(this.fill, this.fillAlpha);
	}
	
	/**
	 * Generic method which decreases a value, but does not allow it to be under zero, cropping to zero if needed.
	 * 
	 * @param current	the current value;
	 * @param amount	the amount to be decreased;
	 * @return			the current value minus the amount to be decreased, if result is greater than zero; or zero, otherwise.
	 */
	protected float decrementTillZero(float current, float amount) {
		float diff = current - amount;
		return diff >= 0 ? diff : 0 ;
	}
	
}
