package mustic.scholz.marine.core.performance.element.builtin.skeleton;

import mustic.scholz.marine.core.feature.JointPosition;
import mustic.scholz.marine.core.performance.Performance;

/**
 * This class represents a joint in a skeleton. It encapsulates a {@link JointPosition} instance, and is capable of painting a sphere on
 * that position, each cycle.
 * 
 * @author Ricardo Scholz
 */
public class Joint extends BasicElement {

	private JointPosition position;
	
	private float jointRadius = 2f;
	
	/**
	 * Default constructor.
	 * 
	 * @param canvas			the performance instance where this element will draw into.
	 * @param jointPosition		the joint position encapsulated by this element.
	 */
	public Joint(Performance canvas, JointPosition jointPosition) {
		super(canvas);
		this.position = jointPosition;
	}
	
	/**
	 * Modifies the joint position encapsulated by this joint.
	 * 
	 * @param jointPosition		the new joint position.
	 */
	public void setPosition(JointPosition jointPosition) {
		this.position = jointPosition;
	}
	
	/**
	 * @return		the current joint position encapsulated by this joint.
	 */
	public JointPosition getPosition() {
		return this.position;
	}
	
	/**
	 * Paints a sphere (if 3D activated) or an ellipse (if 2D activated) on the current joint position.
	 */
	@Override
	public void paint() {
		if(position != null) {
			super.prePaint();
			if(super.getSpace3D()) {
				super.canvas.pushMatrix();
				super.canvas.translate(position.getPosition().getX(), position.getPosition().getY(), position.getPosition().getZ());
				super.canvas.sphere(this.jointRadius);
				super.canvas.popMatrix();
			} else {
				super.canvas.ellipse(position.getPosition().getX(), position.getPosition().getY(), this.jointRadius, this.jointRadius);
			}
		}
	}

	/**
	 * Mandatory method override. Empty implementation. 
	 */
	@Override
	public void update() {
	}

	public void setRadius(float radius) {
		this.jointRadius = radius;
	}
}
