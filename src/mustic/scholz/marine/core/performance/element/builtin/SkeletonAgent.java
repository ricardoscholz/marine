package mustic.scholz.marine.core.performance.element.builtin;

import mustic.scholz.marine.core.entity.PerformerPosition;
import mustic.scholz.marine.core.performance.FeaturesPool;
import mustic.scholz.marine.core.performance.element.Element;
import mustic.scholz.marine.core.performance.element.InputTypeEnum;
import mustic.scholz.marine.core.performance.element.builtin.skeleton.Skeleton;
import mustic.scholz.marine.core.performance.element.parameter.ColorParameter;
import mustic.scholz.marine.core.performance.element.parameter.FlagParameter;
import mustic.scholz.marine.core.performance.element.parameter.NumberParameter;
import mustic.scholz.marine.core.performance.element.parameter.Parameter;
import mustic.scholz.marine.core.utils.ColorUtil;

/**
 * This element draws a skeleton on the screen, with the performer information available from the {@link FeaturesPool}. It is
 * useful for testing, debugging and tuning purposes. 
 * 
 * @author Ricardo Scholz
 */
public class SkeletonAgent extends Element {

	public static final int PARAM_MAX_LIFETIME 		= 0;
	public static final int PARAM_PAINT_SKELETON 	= 1;
	public static final int PARAM_PAINT_BOUNDARIES 	= 2;	
	public static final int PARAM_SPACE_3D 			= 3;
	
	public static final int PARAM_SHOW_INDEX_0		= 4;
	public static final int PARAM_COLOR_0			= 5;
	public static final int PARAM_SHOW_INDEX_1		= 6;
	public static final int PARAM_COLOR_1			= 7;
	
	public static final int PARAM_SHOW_INDEX_2		= 8;
	public static final int PARAM_COLOR_2			= 9;
	public static final int PARAM_SHOW_INDEX_3		= 10;
	public static final int PARAM_COLOR_3			= 11;
	
	public static final int PARAM_SHOW_INDEX_4		= 12;
	public static final int PARAM_COLOR_4			= 13;
	public static final int PARAM_SHOW_INDEX_5		= 14;
	public static final int PARAM_COLOR_5			= 15;
	
	public static final int PARAM_STROKE_WEIGHT		= 16;
	public static final int PARAM_JOINT_RADIUS		= 17;
	
	public static final int MAX_SIMULTANEOUS_PERFORMERS	= 6;
	
	private Skeleton[] skeleton = new Skeleton[MAX_SIMULTANEOUS_PERFORMERS];
	private long maxLifetime;
	private boolean paintSkeleton;
	private boolean paintBoundaries;
	private float strokeWeight;
	private float jointRadius;
	
	private PerformerPosition[] performer = new PerformerPosition[MAX_SIMULTANEOUS_PERFORMERS];
	
	private boolean[] showPerformer = new boolean[MAX_SIMULTANEOUS_PERFORMERS];
	
	/**
	 * Default constructor. Assumes that information received by {@link FeaturesPool} is never outdated.
	 */
	public SkeletonAgent() {
		super();
		
		super.setAuthor("Ricardo Scholz");
		super.setDescription("This element draws a skeleton on the screen, according to the information received from movement sensor.");
		super.setName("Built-in Skeleton Tracker");
		super.setVersion("1.1.0");
		
		Parameter maxLifetime = new NumberParameter(InputTypeEnum.INTEGER_SLIDER, PARAM_MAX_LIFETIME, "Info Timeout", 
				"Discard information received from sensor which is older than the specified time (in milliseconds). Use 0 (zero) if "
				+ "information must never be discarded.", 100, 100, 0, 2000);
		Parameter paintSkeleton = new FlagParameter(InputTypeEnum.SWITCH, PARAM_PAINT_SKELETON, "Show Skeleton", 
				"Shows the skeleton.", true, true);
		Parameter paintBoundaries = new FlagParameter(InputTypeEnum.SWITCH, PARAM_PAINT_BOUNDARIES, "Show Boundaries", 
				"Shows a box surrounding the skeleton.", false, false);
		Parameter space3D = new FlagParameter(InputTypeEnum.SWITCH, PARAM_SPACE_3D, "Use 3D Coordinates", 
				"If perspective is ON, uses 3D coordinates to draw skeleton on screen.", true, true);
		Parameter showIndex0 = new FlagParameter(InputTypeEnum.SWITCH, PARAM_SHOW_INDEX_0, "Show Performer 1", 
				"Show first performer recognized.", true, true);
		Parameter color0 = new ColorParameter(InputTypeEnum.COLOR, PARAM_COLOR_0, "Performer 1 Color", 
				"Color used to paint bones and joints of first performer recognized.", 0xFFFF0000, 0xFFFF0000);
		Parameter showIndex1 = new FlagParameter(InputTypeEnum.SWITCH, PARAM_SHOW_INDEX_1, "Show Performer 2", 
				"Show second performer recognized.", true, true);
		Parameter color1 = new ColorParameter(InputTypeEnum.COLOR, PARAM_COLOR_1, "Performer 2 Color", 
				"Color used to paint bones and joints of second performer recognized.", 0xFF00FF00, 0xFF00FF00);
		Parameter showIndex2 = new FlagParameter(InputTypeEnum.SWITCH, PARAM_SHOW_INDEX_2, "Show Performer 3", 
				"Show third performer recognized.", true, true);
		Parameter color2 = new ColorParameter(InputTypeEnum.COLOR, PARAM_COLOR_2, "Performer 3 Color", 
				"Color used to paint bones and joints of third performer recognized.", 0xFF0000FF, 0xFF0000FF);
		Parameter showIndex3 = new FlagParameter(InputTypeEnum.SWITCH, PARAM_SHOW_INDEX_3, "Show Performer 4", 
				"Show fouth performer recognized.", true, true);
		Parameter color3 = new ColorParameter(InputTypeEnum.COLOR, PARAM_COLOR_3, "Performer 4 Color", 
				"Color used to paint bones and joints of fourth performer recognized.", 0xFFFFFF00, 0xFFFFFF00);
		Parameter showIndex4 = new FlagParameter(InputTypeEnum.SWITCH, PARAM_SHOW_INDEX_4, "Show Performer 5", 
				"Show fifth performer recognized.", true, true);
		Parameter color4 = new ColorParameter(InputTypeEnum.COLOR, PARAM_COLOR_4, "Performer 5 Color", 
				"Color used to paint bones and joints of fifth performer recognized.", 0xFFFF00FF, 0xFFFF00FF);
		Parameter showIndex5 = new FlagParameter(InputTypeEnum.SWITCH, PARAM_SHOW_INDEX_5, "Show Performer 6", 
				"Show sixth performer recognized.", true, true);
		Parameter color5 = new ColorParameter(InputTypeEnum.COLOR, PARAM_COLOR_5, "Performer 6 Color", 
				"Color used to paint bones and joints of sixth performer recognized.", 0xFF00FFFF, 0xFF00FFFF);
		Parameter strokeWeight = new NumberParameter(InputTypeEnum.INTEGER_SLIDER, PARAM_STROKE_WEIGHT, "Bones Width", 
				"Defines how wide bones will be. Use zero to hide bones.", 20, 20, 0, 50);
		Parameter jointRadius = new NumberParameter(InputTypeEnum.INTEGER_SLIDER, PARAM_JOINT_RADIUS, "Joints Radius", 
				"Defines how big joints will be. Use zero to hide joints.", 3, 3, 0, 50);
		
		super.parameters.put(PARAM_MAX_LIFETIME, maxLifetime);
		super.parameters.put(PARAM_PAINT_SKELETON, paintSkeleton);
		super.parameters.put(PARAM_PAINT_BOUNDARIES, paintBoundaries);
		super.parameters.put(PARAM_SPACE_3D, space3D);
		super.parameters.put(PARAM_SHOW_INDEX_0, showIndex0);
		super.parameters.put(PARAM_COLOR_0, color0);
		super.parameters.put(PARAM_SHOW_INDEX_1, showIndex1);
		super.parameters.put(PARAM_COLOR_1, color1);
		super.parameters.put(PARAM_SHOW_INDEX_2, showIndex2);
		super.parameters.put(PARAM_COLOR_2, color2);
		super.parameters.put(PARAM_SHOW_INDEX_3, showIndex3);
		super.parameters.put(PARAM_COLOR_3, color3);
		super.parameters.put(PARAM_SHOW_INDEX_4, showIndex4);
		super.parameters.put(PARAM_COLOR_4, color4);
		super.parameters.put(PARAM_SHOW_INDEX_5, showIndex5);
		super.parameters.put(PARAM_COLOR_5, color5);
		super.parameters.put(PARAM_STROKE_WEIGHT, strokeWeight);
		super.parameters.put(PARAM_JOINT_RADIUS, jointRadius);
	}
	
	/**
	 * Sets up the skeleton and the auxiliary variables.
	 */
	@Override
	public void setup() {
		int color[] = new int[MAX_SIMULTANEOUS_PERFORMERS];
		color[0] = (int) ((ColorParameter) super.parameters.get(PARAM_COLOR_0)).getValue();
		color[0] = ColorUtil.setAlpha(color[0], (byte) 255);
		color[1] = (int) ((ColorParameter) super.parameters.get(PARAM_COLOR_1)).getValue();
		color[1] = ColorUtil.setAlpha(color[0], (byte) 255);
		color[2] = (int) ((ColorParameter) super.parameters.get(PARAM_COLOR_2)).getValue();
		color[2] = ColorUtil.setAlpha(color[0], (byte) 255);
		color[3] = (int) ((ColorParameter) super.parameters.get(PARAM_COLOR_3)).getValue();
		color[3] = ColorUtil.setAlpha(color[0], (byte) 255);
		color[4] = (int) ((ColorParameter) super.parameters.get(PARAM_COLOR_4)).getValue();
		color[4] = ColorUtil.setAlpha(color[0], (byte) 255);
		color[5] = (int) ((ColorParameter) super.parameters.get(PARAM_COLOR_5)).getValue();
		color[5] = ColorUtil.setAlpha(color[0], (byte) 255);
		
		boolean space3D = ((FlagParameter) super.parameters.get(PARAM_SPACE_3D)).getValue();
		for (int i = 0; i < MAX_SIMULTANEOUS_PERFORMERS; i++) {
			this.skeleton[i] = new Skeleton(super.performance, color[i], color[i], space3D);
		}
		
		this.maxLifetime = (int) ((NumberParameter) super.parameters.get(PARAM_MAX_LIFETIME)).getValue();
		this.paintSkeleton = ((FlagParameter) super.parameters.get(PARAM_PAINT_SKELETON)).getValue();
		this.paintBoundaries = ((FlagParameter) super.parameters.get(PARAM_PAINT_BOUNDARIES)).getValue();
		
		this.showPerformer[0] = ((FlagParameter) super.parameters.get(PARAM_SHOW_INDEX_0)).getValue();
		this.showPerformer[1] = ((FlagParameter) super.parameters.get(PARAM_SHOW_INDEX_1)).getValue();
		this.showPerformer[2] = ((FlagParameter) super.parameters.get(PARAM_SHOW_INDEX_2)).getValue();
		this.showPerformer[3] = ((FlagParameter) super.parameters.get(PARAM_SHOW_INDEX_3)).getValue();
		this.showPerformer[4] = ((FlagParameter) super.parameters.get(PARAM_SHOW_INDEX_4)).getValue();
		this.showPerformer[5] = ((FlagParameter) super.parameters.get(PARAM_SHOW_INDEX_5)).getValue();
		
		this.strokeWeight = ((NumberParameter) super.parameters.get(PARAM_STROKE_WEIGHT)).getValue();
		this.jointRadius = ((NumberParameter) super.parameters.get(PARAM_JOINT_RADIUS)).getValue();
	}
	
	/**
	 * Reads performer position and updates the skeleton instance.
	 */
	@Override
	public void update() {
		for (int i = 0; i < MAX_SIMULTANEOUS_PERFORMERS; i++) {
			this.readPerformerPosition(i);
			this.skeleton[i].update();
		}
	}

	/**
	 * Draws the skeleton on screen.
	 */
	@Override
	public void paint() {
		for (int i = 0; i < MAX_SIMULTANEOUS_PERFORMERS; i++) {
			if (this.performer[i] != null && this.showPerformer[i]) {
				if (this.paintSkeleton) {
					this.skeleton[i].setStrokeWeight(this.strokeWeight);
					this.skeleton[i].setJointRadius(this.jointRadius);
					this.skeleton[i].paint();
				}
				if (this.paintBoundaries) {
					this.paintBoundaries(i);
				}
			}
		}
	}
	
	private void paintBoundaries(int index) {
		if (this.performer != null && this.performer[index] != null) {
			float left = this.performer[index].getLeftmost().getX();
			float right = this.performer[index].getRightmost().getX();
			float bottom = this.performer[index].getLowermost().getY();
			float top = this.performer[index].getUppermost().getY();
			float front = this.performer[index].getForemost().getZ();
			float back = this.performer[index].getBackmost().getZ();
			
			super.performance.strokeWeight(this.strokeWeight);
			
			//bottom rectangle
			super.performance.line(left, bottom, front, right, bottom, front);
			super.performance.line(right, bottom, front, right, bottom, back);
			super.performance.line(right, bottom, back, left, bottom, back);
			super.performance.line(left, bottom, back, left, bottom, front);
			
			//top rectangle
			super.performance.line(left, top, front, right, top, front);
			super.performance.line(right, top, front, right, top, back);
			super.performance.line(right, top, back, left, top, back);
			super.performance.line(left, top, back, left, top, front);
			
			//sides
			super.performance.line(left, bottom, front, left, top, front);
			super.performance.line(right, bottom, front, right, top, front);
			super.performance.line(right, bottom, back, right, top, back);
			super.performance.line(left, bottom, back, left, top, back);
		}
	}

	private void readPerformerPosition(int index) {
		if (this.maxLifetime > 0) {
			this.performer[index] = FeaturesPool.getInstance().getPerformerPosition(this.maxLifetime, index);
		} else {
			this.performer[index] = FeaturesPool.getInstance().getPerformerPosition(index);
		}
		if (this.performer[index] != null) {
			this.skeleton[index].updateNodes(performer[index].getPositions());
		} else {
			this.skeleton[index].clearNodes();
		}
	}
	
	/**
	 * Sets the maximum lifetime of skeleton information to be retrieved from {@link FeaturesPool}.
	 * 
	 * @param maxLifetime		the maximum number of milliseconds past a skeleton information is considered to be up to date.
	 */
	public void setMaxLifetime(long maxLifetime) {
		this.maxLifetime = maxLifetime;
	}
	
	/**
	 * Sets the skeleton boundaries painting. If this flag is on, the skeleton will be painted within a box.
	 * 
	 * @param paintBoundaries		indicates whether a box around the skeleton should be painted or not.
	 */
	public void setPaintBoundaries(boolean paintBoundaries) {
		this.paintBoundaries = paintBoundaries;
	}

}
