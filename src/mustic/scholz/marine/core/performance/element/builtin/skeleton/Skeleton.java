package mustic.scholz.marine.core.performance.element.builtin.skeleton;

import mustic.scholz.marine.core.entity.JointEnum;
import mustic.scholz.marine.core.entity.PositionVector;
import mustic.scholz.marine.core.feature.JointPosition;
import mustic.scholz.marine.core.performance.Performance;

/**
 * This class represents a performer skeleton, capable of painting itself on a {@link Performance} instance.
 * 
 * @author Ricardo Scholz
 */
public class Skeleton extends BasicElement {

	private Joint[] joints;
	private Bone[] bones;
	private float strokeWeight;
	private float jointRadius;
	
	/**
	 * Constructor that builds a 3D skeleton.
	 * 
	 * @param canvas		the performance instance in which this element will be drawn into.
	 * @param stroke		the stroke color.
	 * @param fill			the fill color.
	 */
	public Skeleton(Performance canvas, int stroke, int fill) {
		this(canvas, stroke, fill, true);
	}
	
	/**
	 * Constructor that allows to choose between three-dimensional and bi-dimensional spaces.
	 * 
	 * @param canvas		the performance instance in which this element will be drawn into.
	 * @param stroke		the stroke color.
	 * @param fill			the fill color.
	 * @param space3D		a flag indicating that this skeleton will use three-dimensional space (if <code>true</code>), or bi-dimensional
	 * 						space (if <code>false</code>).
	 */
	public Skeleton(Performance canvas, int stroke, int fill, boolean space3D) {
		super(canvas);
		super.setSpace3D(space3D);
		super.setFill(fill);
		super.setStroke(stroke);
		this.initJoints();
		this.initBones();
	}

	private void initJoints(){
		this.joints = new Joint[JointEnum.NUMBER_OF_JOINTS];
		for (int i = 0; i < this.joints.length; i++) {
			this.joints[i] = new Joint(canvas, null);
			this.joints[i].setFill(super.getFill());
			this.joints[i].setFillAlpha(super.getFillAlpha());
			this.joints[i].setStroke(super.getStroke());
			this.joints[i].setStrokeAlpha(super.getStrokeAlpha());
			this.joints[i].setSpace3D(super.getSpace3D());
		}
	}
	
	private void initBones(){
		this.bones = new Bone[BoneEnum.NUMBER_OF_BONES];
		for (int i = 0; i < this.bones.length; i++) {
			this.bones[i] = new Bone(canvas, BoneEnum.getByIndex(i));
			this.bones[i].setFill(super.getFill());
			this.bones[i].setFillAlpha(super.getFillAlpha());
			this.bones[i].setStroke(super.getStroke());
			this.bones[i].setStrokeAlpha(super.getStrokeAlpha());
			this.bones[i].setSpace3D(super.getSpace3D());
		}
	}
	
	/**
	 * Paints the skeleton joints and bones on screen.
	 */
	@Override
	public void paint() {
		for(Joint joint : this.joints) {
			joint.setRadius(this.jointRadius);
			joint.paint();
		}
		for(Bone bone : this.bones) {
			bone.setStrokeWeight(this.strokeWeight);
			bone.paint();
		}
	}

	/**
	 * Updates the skeleton joints and bones. Currently, this method is empty, as {@link Joint#update()} and {@link Bone#update()} are
	 * empty implementations.
	 */
	@Override
	public void update() {
	}
	
	/**
	 * This method must be called when the skeleton positions have changed, so it can update its joints positions.
	 * 
	 * @param positions			an array of up to date positions.
	 */
	public void updateNodes(JointPosition[] positions) {
		for(JointPosition position : positions) {
			if (position != null) {
				this.joints[position.getJoint().getIndex()].setPosition(position);
				// TODO [Performance] Update bones has a loop; figure out less expensive approach.
				this.updateBones(position);
			}
		}
	}
	
	/**
	 * This method clear the joint position information of the skeleton, setting all of them to the point 
	 * (0, 0, <code>Float.MAX_VALUE</code>).
	 */
	public void clearNodes() {
		for(Joint joint : this.joints) {
			if (joint != null && joint.getPosition() != null) {
				joint.getPosition().setPosition(new PositionVector(0, 0, Float.MAX_VALUE));
			}
		}
	}
	
	/**
	 * Updates bones information, when a new joint position is received.
	 * 
	 * @param position		a new joint position received.
	 */
	public void updateBones(JointPosition position) {
		for(int i = 0; i < this.bones.length; i++) {
			this.bones[i].updateInfo(position);
		}
	}
	
	public void setStrokeWeight(float strokeWeight) {
		this.strokeWeight = strokeWeight;
	}
	
	public void setJointRadius(float jointRadius) {
		this.jointRadius = jointRadius;
	}

}
