package mustic.scholz.marine.core.performance.element.builtin.skeleton;

import mustic.scholz.marine.core.feature.JointPosition;
import mustic.scholz.marine.core.performance.Performance;

/**
 * Represents a link between two skeleton joints.
 * 
 * @author Ricardo Scholz
 *
 */
public class Bone extends BasicElement {

	private JointPosition from;
	private JointPosition to;
	
	private float strokeWeight = 2;
	
	/**
	 * Default constructor.
	 * 
	 * @param canvas	the performance instance where this element will draw into.
	 * @param info		the static information about this bone.
	 */
	public Bone(Performance canvas, BoneEnum info) {
		super(canvas);
		this.from = new JointPosition(info.getFrom().getIndex(), 0, 0, 0);
		this.to = new JointPosition(info.getTo().getIndex(), 0, 0, 0);
	}
	
	/**
	 * Modifies the joint position from which this bone departs.
	 * 
	 * @param from		the new joint position from which this bone departs.
	 */
	public void setFrom(JointPosition from) {
		this.from = from;
	}
	
	/**
	 * Modifies the joint position in which this bone arrives.
	 * 
	 * @param to		the new joint position in which this bone arrives.
	 */
	public void setTo(JointPosition to) {
		this.to = to;
	}
	
	/**
	 * Draws a line between the {@link #from} joint and the {@link to} joint. 
	 */
	@Override
	public void paint() {
		if(from != null && to != null) {
			super.prePaint();
			super.canvas.strokeWeight(this.strokeWeight);
			if(super.getSpace3D()) {
				super.canvas.line(from.getPosition().getX(), from.getPosition().getY(), from.getPosition().getZ(), 
						to.getPosition().getX(), to.getPosition().getY(), to.getPosition().getZ());
			} else {
				super.canvas.line(from.getPosition().getX(), from.getPosition().getY(), 
						to.getPosition().getX(), to.getPosition().getY());
			}
		}
	}

	/**
	 * Empty method, overridden because superclass method is abstract.
	 */
	@Override
	public void update() {
	}
	
	/**
	 * Updates the position of {@link from} and {@link to} joints, if they match the received <code>position</code>.
	 * 
	 * @param position		a joint position which will replace {@link from} or {@link to}, if its joint index is the same.
	 */
	public void updateInfo(JointPosition position) {
		if (position != null && position.getJoint().getIndex() == this.from.getJoint().getIndex()) {
			this.from = position;
		}
		if (position != null && position.getJoint().getIndex() == this.to.getJoint().getIndex()) {
			this.to = position;
		}
	}
	
	public void setStrokeWeight(float strokeWeight) {
		this.strokeWeight = strokeWeight;
	}
	
}
