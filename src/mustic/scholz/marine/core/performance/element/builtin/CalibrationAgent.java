package mustic.scholz.marine.core.performance.element.builtin;

import mustic.scholz.marine.core.entity.PerformerPosition;
import mustic.scholz.marine.core.performance.FeaturesPool;
import mustic.scholz.marine.core.performance.Performance;
import mustic.scholz.marine.core.performance.PerformanceDrawer;
import mustic.scholz.marine.core.performance.element.Element;
import mustic.scholz.marine.core.performance.element.builtin.skeleton.Skeleton;
import processing.core.PApplet;

/**
 * This built-in element provides a set of visual markers which help calibrating devices when they have been positioned in the stage. It
 * draws the following elements on screen:
 * 1. The (x, y, z) axis;
 * 2. A line with the size of one input device unit (on MS Kinect, it is expected to be 1 meter long);
 * 3. A set of three-dimensional boxes, one for each axis and one in the (0,0,0) point, in order to allow checking the orientation of the 
 * axis, and if 3D (perspective) is being properly drawn;
 * 4. The current calibration information;
 * 5. The keyboard commands for calibration; and
 * 6. The skeleton, if there is a performer moving around.
 * 
 * It also prints to the console the current calibration values, every {@link #PRINT_INTERVAL} frames (current value is 150, approximately
 * 5 seconds).
 * 
 * @author Ricardo Scholz
 */
//TODO [Refactoring] Evaluate architecture to read texts from language dependent files.
// Architecture must be able to set (through Facade) which language to use (use system.config for default language)
public class CalibrationAgent extends Element {
	
	private static final long PRINT_INTERVAL = 150; //in frames (30 fps)
	
	private static final float FONT_SIZE = 17;
	
	private Skeleton skeleton;
	
	private boolean drawAxes = true;			//Command 1
	private boolean drawUnitLine = true;		//Command 2
	private boolean draw3DCheck = true;			//Command 3
	private boolean drawCalibration = true;		//Command 4
	private boolean drawSkeleton = true;		//Command 5
	private boolean outputToConsole = true;		//Command 6
	
	/**
	 * This is an empty default constructor.
	 */
	public CalibrationAgent() {
		super.setAuthor("Ricardo Scholz");
		super.setDescription("This element provides a set of geometric figures to allow calibration of stage setup. Make sure the "
				+ "black line has 1 device unit (1 meter for Kinect).");
		super.setName("Calibration");
		super.setVersion("1.0.0");
	}
	
	/**
	 * Modifies the performance instance related to this element. Every time the performance instance is modified, a new {@link Skeleton}
	 * object must be created.
	 * 
	 * @param performance		the new performance instance where this element will draw in.
	 */
	@Override
	public void setPerformance(Performance performance) {
		super.setPerformance(performance);
		this.skeleton = new Skeleton(performance,  0xFF0000FF, 0xFF0000FF);
	}
	
	/**
	 * Reads performer position and updates the skeleton instance.
	 */
	@Override
	public void update() {
		this.readPerformerPosition();
		if (this.skeleton != null) {
			this.skeleton.update();
		}
	}
	
	/**
	 * Draws the visual markers and skeleton on screen. See class description for a list of markers this element provides.
	 */
	@Override
	public void paint() {
		this.drawAxes();
		this.drawUnitLine();
		this.draw3DCheck();
		this.drawSkeleton();
		this.drawCalibrationInfo();
		this.printCalibrationSettings();
	}
	
	private void drawAxes() {
		if (!this.drawAxes) return;
		super.performance.strokeWeight(2f);
		//X Axis (Red)
		super.performance.stroke(0xFFFF0000);
		super.performance.fill(0xFFFF0000);
		super.performance.strokeWeight(3);
		super.performance.line(-super.performance.displayWidth, 0, 0, super.performance.displayWidth, 0, 0);
		//Y Axis (Green)
		super.performance.stroke(0xFF00FF00);
		super.performance.fill(0xFF00FF00);
		super.performance.line(0, -super.performance.displayWidth, 0, 0, super.performance.displayWidth, 0);
		//Z Axis (Blue)
		super.performance.stroke(0xFF0000FF);
		super.performance.fill(0xFF0000FF);
		super.performance.line(0, 0, -super.performance.displayWidth, 0, 0, super.performance.displayWidth);
		//Origin (30cm box, black stroke, transparent gray fill)
		super.performance.stroke(0x33555555);
		super.performance.noFill();
		this.drawBox(0, 0, 0, 0.3f * super.inputReceiver.getPositionListenerPixelPerUnitRatio());
	}
	
	private void drawBox(float x, float y, float z, float size) {
		super.performance.pushMatrix();
		super.performance.translate(x, y, z);
		super.performance.box(size);
		super.performance.popMatrix();
	}
	
	private void drawUnitLine() {
		if (!this.drawUnitLine) return;
		//1 unit line parallel to X axis, at z = -0.5 unit, centralised with Z axis.
		//For MS Kinect, 1 unit = 1 meter.
		float halfUnit = 0.5f * super.inputReceiver.getPositionListenerPixelPerUnitRatio();
		super.performance.stroke(0xFFFF8800);
		super.performance.strokeWeight(5f);
		super.performance.line(-halfUnit, 0, -halfUnit, halfUnit, 0, -halfUnit);
	}
	
	private void draw3DCheck() {
		if (!this.draw3DCheck) return;
		float distance = 0.2f * super.inputReceiver.getPositionListenerPixelPerUnitRatio();
		super.performance.strokeWeight(2f);
		//Red box at X-axis
		super.performance.stroke(0xFFFF0000);
		super.performance.noFill();
		this.drawBox(300, 0, 0, distance);
		//Green box at Y-axis
		super.performance.stroke(0xFF00FF00);
		super.performance.noFill();
		this.drawBox(0, 300, 0, distance);
		//Blue box at Z-axis
		super.performance.stroke(0xFF0000FF);
		super.performance.noFill();
		this.drawBox(0, 0, 300, distance);
	}
	
	private void drawSkeleton() {
		if (!this.drawSkeleton) return;
		super.performance.strokeWeight(2f);
		if (this.skeleton != null) {
			this.skeleton.paint();
		}
	}
	
	private void drawCalibrationInfo() {
		super.performance.strokeWeight(2f);
		super.performance.begin2D();
		super.performance.pushMatrix();
		super.performance.scale(1, -1);
		super.performance.fill(0);
		String text = "";
		if (this.drawCalibration) {
			text += this.getCalibrationSettings() + "\n";
		}
		text += this.getCommands();
		
		super.performance.textSize(FONT_SIZE);
		super.performance.text(text, 
				-(super.performance.displayWidth/2) + 30, 
				-(super.performance.displayHeight/2) + 30);
		super.performance.popMatrix();
		super.performance.end2D();
	}
	
	private void printCalibrationSettings() {
		if (this.outputToConsole && super.performance.frameCount % PRINT_INTERVAL == 0) {
			System.out.println("=======================================================");
			System.out.println(this.getCalibrationSettings());
			System.out.println("=======================================================");
		}
	}
	
	private String getCalibrationSettings() {
		StringBuffer sb = new StringBuffer();
		sb.append("Calibration Settings");
		sb.append("\n    Input Device");
		sb.append("\n        Device Angle (rads / deg): " + super.inputReceiver.getInputDeviceAngle()
				+ " / " + PApplet.degrees(super.inputReceiver.getInputDeviceAngle()));
		sb.append("\n        PPU Ratio: " + super.inputReceiver.getPositionListenerPixelPerUnitRatio());
		sb.append("\n        Translation: " + super.inputReceiver.getPositionListenerTranslationCoordinateX()
				+ ", " + super.inputReceiver.getPositionListenerTranslationCoordinateY()
				+ ", " + super.inputReceiver.getPositionListenerTranslationCoordinateZ());
		sb.append("\n    Projection");
		sb.append("\n        Projection Plane: " + performance.getPlane());
		sb.append("\n        Camera Distance: " + PerformanceDrawer.getEye());
		sb.append("\n        Projection Mode Perspective: " + PerformanceDrawer.isUsingPerspective());
		sb.append("\n        Camera Angle (rads / deg): " + PerformanceDrawer.getLensAngle() 
				+ " / " + PApplet.degrees(PerformanceDrawer.getLensAngle()));
		return sb.toString();
	}
	
	private String getCommands() {
		StringBuffer sb = new StringBuffer();
//		sb.append("Commands");
//		sb.append("\n    1 = Show/Hide Axes");
//		sb.append("\n    2 = Show/Hide Unit Line");
//		sb.append("\n    3 = Show/Hide 3D Check Boxes");
//		sb.append("\n    4 = Show/Hide Capture Area (not implemented)");
//		sb.append("\n    5 = Show/Hide Calibration Info");
//		sb.append("\n    6 = Show/Hide Skeleton");
//		sb.append("\n\n");
		sb.append("Calibration Commands");
		sb.append("\n    General Settings");
		sb.append("\n        q      Saves current default calibration file");
		sb.append("\n        w      Loads current default calibration file");
		sb.append("\n        r      Turns background refresh ON/OFF");
		sb.append("\n        o      Switch between Orthogonal and Perspective");
		sb.append("\n        p      Switches between WALL and FLOOR");
		sb.append("\n    Input Device Angle");
		sb.append("\n        a      Horizontal alignment");
		sb.append("\n        s      Decreases device angle");
		sb.append("\n        d      Increases device angle");
		sb.append("\n        f      Vertical alignment");
		sb.append("\n    Input Device Scale (Pixels per Unit)");
		sb.append("\n        g      Resets PPU ratio");
		sb.append("\n        h      Increases PPU ratio");
		sb.append("\n        j      Decreases PPU ratio");
		sb.append("\n    Input Device Translation Coordinates");
		sb.append("\n        l      Resets translation coordinates");
		sb.append("\n        UP     Moves projection plan upward");
		sb.append("\n        DOWN   Moves projection plan downward");
		sb.append("\n        LEFT   Moves projection plan backward");
		sb.append("\n        RIGHT  Moves projection plan forward");
		sb.append("\n    Camera Settings");
		sb.append("\n        x      Resets camera settings");
		sb.append("\n    Camera Distance");
		sb.append("\n        c      Moves camera closer");
		sb.append("\n        v      Moves camera further");
//		sb.append("\n    Camera Lens Angle");
//		sb.append("\n        n      Narrows camera lens angle");
//		sb.append("\n        m      Widens camera lens angle");

		return sb.toString();
	}

	private void readPerformerPosition() {
		if (this.skeleton != null) {
			PerformerPosition performer = FeaturesPool.getInstance().getPerformerPosition();
			if (performer != null) {
				this.skeleton.updateNodes(performer.getPositions());
			} else {
				this.skeleton.clearNodes();
			}
		}
	}
	
	/**
	 * Process numeric keyboard commands. The following commands are accepted:
	 * 
	 * Key | Behaviour
	 *  1  | Switch on/off drawing of (x, y, z) axis;
	 *  2  | Switch on/off drawing of input device unit of measure line;
	 *  3  | Switch on/off drawing of 3D checking boxes;
	 *  4  | Switch on/off drawing of calibration information;
	 *  5  | Switch on/off drawing of skeleton;
	 *  6  | Switch on/off calibration information output to console.
	 */
	@Override
	public void fireCommand(int command, Object... param) {
		switch(command) {
			case 1:
				this.drawAxes = !this.drawAxes;
				break;
			case 2:
				this.drawUnitLine = !this.drawUnitLine;
				break;
			case 3:
				this.draw3DCheck = !this.draw3DCheck;
				break;
			case 4:
				this.drawCalibration = !this.drawCalibration;
				break;
			case 5:
				this.drawSkeleton = !this.drawSkeleton;
				break;
			case 6:
				this.outputToConsole = !this.outputToConsole;
				break;
		}
	}
}
