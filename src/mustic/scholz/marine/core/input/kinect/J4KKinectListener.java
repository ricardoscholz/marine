package mustic.scholz.marine.core.input.kinect;

import edu.ufl.digitalworlds.j4k.J4KSDK;
import edu.ufl.digitalworlds.j4k.Skeleton;
import mustic.scholz.marine.core.entity.PerformerPosition;
import mustic.scholz.marine.core.input.InputListener;

/**
 * Implements a listener for performer positions using J4K Library.
 * 
 * @author Ricardo Scholz
 *
 */
//TODO [Performance] Reuse a permanent Skeleton instance, and use "setJointPositions()" to
// update it, instead of creating new Skeleton instances every time a message is received.
public class J4KKinectListener extends J4KSDK implements IPKinectListener {
	
	private InputListener inputListener;
	
	private Skeleton[] skeletons;
	
	/**
	 * Default constructor.
	 */
	public J4KKinectListener() {
		super();
		this.skeletons = new Skeleton[super.getMaxNumberOfSkeletons()];
	}
	
	/**
	 * Sets the input listener for callback when a position message is received.
	 * 
	 * @param inputListener		the callback input listener.
	 */
	@Override
	public void setInputListener(InputListener inputListener) {
		this.inputListener = inputListener;
	}
	
	/**
	 * Called by J4K when a skeleton frame event is received.
	 * 
	 * @see J4KSDK#onSkeletonFrameEvent(boolean[], float[], float[], byte[])
	 */
	@Override
	public void onSkeletonFrameEvent(boolean[] skeleton_tracked, float[] positions, float[] orientations, byte[] joint_status) {
		boolean foundFirst = false;
		for(int i=0; i < this.skeletons.length; i++) {
			this.skeletons[i] = Skeleton.getSkeleton(i, skeleton_tracked, positions, orientations, joint_status, this);
			if (this.skeletons[i] != null && this.skeletons[i].isTracked()) {
				if (!foundFirst) {
					this.inputListener.setFirstPerformer(i);
					foundFirst = true;
				}
				PerformerPosition parsedPerformer = J4KSkeletonParser.parse(this.skeletons[i]);
				if (parsedPerformer != null) {
					this.inputListener.receivePositionMessage(parsedPerformer, i);
				}
			}
		}
	}

	/**
	 * Called by J4K when a color frame event is received. This implementation ignores these calls.
	 * 
	 * @see J4KSDK#onColorFrameEvent(byte[])
	 */
	@Override
	public void onColorFrameEvent(byte[] color_frame) {
	}

	/**
	 * Called by J4K when a depth frame event is received. This implementation ignores these calls.
	 * 
	 * @see J4KSDK#onDepthFrameEvent(short[], byte[], float[], float[])
	 */
	@Override
	public void onDepthFrameEvent(short[] depth_frame, byte[] body_index, float[] xyz, float[] uv) {
	}

	/**
	 * Starts <code>J4KSDK</code> skeleton listening, by calling {@link J4KSDK#start(int)}, passing the {@link J4KSDK#SKELETON} value.
	 */
	@Override
	public void startListening() {
		//this.start(J4KSDK.COLOR|J4KSDK.DEPTH|J4KSDK.SKELETON|J4KSDK.XYZ|J4KSDK.UV|J4KSDK.INFRARED);
		this.start(J4KSDK.SKELETON);
	}
}
