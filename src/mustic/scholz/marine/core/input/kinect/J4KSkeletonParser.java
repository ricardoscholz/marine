package mustic.scholz.marine.core.input.kinect;

import mustic.scholz.marine.core.entity.JointEnum;
import mustic.scholz.marine.core.entity.PerformerPosition;
import mustic.scholz.marine.core.feature.JointPosition;
import edu.ufl.digitalworlds.j4k.Skeleton;

/**
 * This class encapsulates the J4K skeleton messages parsing.
 * 
 * @author Ricardo Scholz
 */
public class J4KSkeletonParser {
	
	private static int[] mapping;
	
	static {
		mapping = new int[JointEnum.NUMBER_OF_JOINTS];
		mapping[JointEnum.LEFT_TOE.getIndex()] = Skeleton.FOOT_LEFT;
		mapping[JointEnum.RIGHT_TOE.getIndex()] = Skeleton.FOOT_RIGHT;
		mapping[JointEnum.LEFT_ANKLE.getIndex()] = Skeleton.ANKLE_LEFT;
		mapping[JointEnum.RIGHT_ANKLE.getIndex()] = Skeleton.ANKLE_RIGHT;
		mapping[JointEnum.LEFT_KNEE.getIndex()] = Skeleton.KNEE_LEFT;
		mapping[JointEnum.RIGHT_KNEE.getIndex()] = Skeleton.KNEE_RIGHT;
		mapping[JointEnum.LEFT_HIP.getIndex()] = Skeleton.HIP_LEFT;
		mapping[JointEnum.RIGHT_HIP.getIndex()] = Skeleton.HIP_RIGHT;
		mapping[JointEnum.HIPS.getIndex()] = Skeleton.SPINE_SHOULDER;
		mapping[JointEnum.ABDOMEN.getIndex()] = Skeleton.SPINE_BASE;
		mapping[JointEnum.LEFT_COLLAR.getIndex()] = Skeleton.SHOULDER_LEFT;
		mapping[JointEnum.RIGHT_COLLAR.getIndex()] = Skeleton.SHOULDER_RIGHT;
		mapping[JointEnum.LEFT_SHOULDER.getIndex()] = Skeleton.SHOULDER_LEFT;
		mapping[JointEnum.RIGHT_SHOULDER.getIndex()] = Skeleton.SHOULDER_RIGHT;
		mapping[JointEnum.LEFT_ELBOW.getIndex()] = Skeleton.ELBOW_LEFT;
		mapping[JointEnum.RIGHT_ELBOW.getIndex()] = Skeleton.ELBOW_RIGHT;
		mapping[JointEnum.LEFT_WRIST.getIndex()] = Skeleton.WRIST_LEFT;
		mapping[JointEnum.RIGHT_WRIST.getIndex()] = Skeleton.WRIST_RIGHT;
		mapping[JointEnum.LEFT_HAND.getIndex()] = Skeleton.HAND_LEFT;
		mapping[JointEnum.RIGHT_HAND.getIndex()] = Skeleton.HAND_RIGHT;
		mapping[JointEnum.CHEST.getIndex()] = Skeleton.SPINE_MID;
		mapping[JointEnum.NECK.getIndex()] = Skeleton.NECK;
		mapping[JointEnum.HEAD.getIndex()] = Skeleton.HEAD;
	}
	
	/**
	 * Parses a J4K <code>Skeleton</code> object to a <code>PerformerPosition</code> object.
	 * 
	 * @param skeleton	the J4K skeleton.
	 * @return			the Marine performer position.
	 */
	public static PerformerPosition parse(Skeleton skeleton) {
		PerformerPosition result = new PerformerPosition();
		result.setBodyOrientation(J4KSkeletonParser.parseBodyOrientation(skeleton.getBodyOrientation()));
		result.setPositions(J4KSkeletonParser.parseJointPositions(skeleton));
		result.setTimestamp(System.currentTimeMillis());
		return result;
	}
	
	private static JointPosition[] parseJointPositions(Skeleton skeleton) {
		JointPosition[] result = new JointPosition[JointEnum.NUMBER_OF_JOINTS];
		for (int i = 0; i < JointEnum.NUMBER_OF_JOINTS; i++) {
			result[i] = J4KSkeletonParser.parsePosition(skeleton, i); 
		}
		return result;
	}
	
	private static JointPosition parsePosition(Skeleton skeleton, int index) {
		JointPosition result = null;
		Integer joint = J4KSkeletonParser.mapping[index];
		if (joint != null) {
			if (skeleton.isJointTrackedOrInferred(joint)) {
				float x = skeleton.get3DJointX(joint) * -1; //Undo X axis automatic mirroring by MS Kinect
				float y = skeleton.get3DJointY(joint);
				float z = skeleton.get3DJointZ(joint);
				result = new JointPosition(index, x,y,z);
			}
		}
		return result;
	}
	
	private static float parseBodyOrientation(double orientation) {
		return (float) orientation;
	}
}
