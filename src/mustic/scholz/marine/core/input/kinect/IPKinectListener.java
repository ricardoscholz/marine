package mustic.scholz.marine.core.input.kinect;

import mustic.scholz.marine.core.input.InputListener;

/**
 * Microsoft Kinect Listener interface. This interface has been defined to allow easier modification of actual MS Kinect integration 
 * technology, removing technology specific dependency from <code>KinectInputListener</code>. 
 * 
 * @author Scholz
 *
 */
public interface IPKinectListener {
	
	/**
	 * Sets the input listener to be called when a message is received.
	 * 
	 * @param listener	the new input listener.
	 */
	public void setInputListener(InputListener listener);
	
	/**
	 * Prepares and starts running the concrete listener instance.
	 */
	public void startListening();

}
