package mustic.scholz.marine.core.input.kinect;

import mustic.scholz.marine.core.input.InputListener;

/**
 * Microsoft Kinect specific input listener. 
 * 
 * @author Ricardo Scholz
 *
 */
public class KinectInputListener extends InputListener {

	private IPKinectListener listener;

	/**
	 * Starts this listener.
	 */
	@Override
	public void startListening() {
		try {
			if (listener != null) {
				listener.startListening();
				System.out.println("Kinect listener " + listener.getClass().getName() + " ready.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This is a single listener implementation. Calls to this method will overwrite previous listeners added.
	 * 
	 * @param listener		the listener to use with this implementation; must be of type <code>IPKinectListene</code>, otherwise the call
	 * 						is ignored.
	 */
	@Override
	public void addListener(Object listener) {
		if (listener instanceof IPKinectListener) {
			this.listener = (IPKinectListener) listener;
		}
	}
	
	/**
	 * If the argument listener is equal to the current listener, removes the current listener.
	 * 
	 * @param listener		the listener to remove; if this is not the listener used by this instance, as of {@link Object#equals(Object)}
	 * 						result, ignores the method call.
	 * 						
	 * @see KinectInputListener#addListener(Object)
	 */
	@Override
	public void removeListener(Object listener) {
		if (this.listener != null && listener != null
				&& this.listener.equals(listener)) {
			this.listener = null;
		}
	}
}
