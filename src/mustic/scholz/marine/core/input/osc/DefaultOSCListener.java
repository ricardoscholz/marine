package mustic.scholz.marine.core.input.osc;

import java.util.Date;
import java.util.List;

import mustic.scholz.marine.core.input.InputListener;

import com.illposed.osc.OSCMessage;

/**
 * Concrete implementation of <code>IPOSCListener</code> which catches any message directed to the address selector specified in its
 * constructor, and prints to the console the message contents. 
 * 
 * @author Ricardo Scholz
 *
 */
public class DefaultOSCListener implements IPOSCListener {

	private String addressSelector;
	
	/**
	 * Constructor.
	 * 
	 * @param addressSelector	the address selector of the messages this instance will listen to.
	 */
	public DefaultOSCListener(String addressSelector) {
		this.addressSelector = addressSelector;
	}

	/**
	 * Callback method invoked when a message is received. It just prints to the console the received message content (address and
	 * parameters).
	 */
	@Override
	public void acceptMessage(Date msgDate, OSCMessage message) {
		System.out.println(message.getAddress());
		List<Object> arguments = message.getArguments();
		for (int i = 0; i < arguments.size(); i++) {
			System.out.println("    Param " + i + " : "
					+ arguments.get(i).getClass().getSimpleName() + " : " + arguments.get(i).toString());
		}
	}

	/**
	 * @return the address selector this instance is listening to.
	 */
	@Override
	public String getAddressSelector() {
		return this.addressSelector;
	}
	
	/**
	 * Sets the callback input listener. However, this concrete implementation does not forward the messages to the input listener. This
	 * method is empty, though.
	 */
	@Override
	public void setInputListener(InputListener inputListener) {
	}
}
