package mustic.scholz.marine.core.input.osc;

import mustic.scholz.marine.core.input.InputListener;

import com.illposed.osc.OSCListener;

/**
 * This interface specifies the contract OSC listeners must implement. Every listener must define an address selector and allow the
 * configuration of a <code>InputListener</code> for callback.
 * 
 * @author Ricardo Scholz
 *
 */
public interface IPOSCListener extends OSCListener {

	/**
	 * @return	the OSC address selector of this listener.
	 */
	public String getAddressSelector();
	
	/**
	 * Sets the callback input listener which will be called when position or features messages are received.
	 * 
	 * @param inputListener		the callback input listener.
	 */
	public void setInputListener(InputListener inputListener);
}
