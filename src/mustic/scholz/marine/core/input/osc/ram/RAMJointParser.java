package mustic.scholz.marine.core.input.osc.ram;

import mustic.scholz.marine.core.entity.JointEnum;

/**
 * This class encapsulates the RAM-DT joint names parsing.
 * 
 * @author Ricardo Scholz
 *
 */
public class RAMJointParser {

	/**
	 * Parses a RAM-DT joint name to a <code>JointEnum</code> index.
	 * 
	 * @param jointName		the name of the joint to parse.
	 * @return				the index of the joint, according to <code>JointEnum</code>.
	 */
	public static Integer findIndexByName(String jointName) {
		//Fix for misspelling of RAM-DT
		if (jointName.equalsIgnoreCase("adbomen")) {
			jointName = "abdomen";
		}
		return JointEnum.findByStrId(jointName).getIndex();
	}
}
