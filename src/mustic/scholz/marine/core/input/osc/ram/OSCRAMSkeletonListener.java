package mustic.scholz.marine.core.input.osc.ram;

import java.util.Date;
import java.util.List;

import mustic.scholz.marine.core.entity.PerformerPosition;
import mustic.scholz.marine.core.feature.JointPosition;
import mustic.scholz.marine.core.input.InputListener;
import mustic.scholz.marine.core.input.osc.IPOSCListener;

import com.illposed.osc.OSCMessage;

/**
 * This OSC listener implementation decodes performer position messages received according to the Reactor for Awareness in Motion Dance
 * Toolkit (RAM-DT) format.
 * See Reactor for Awareness in Motion, at http://interlab.ycam.jp/en/projects/ram.  
 * 
 * @author Ricardo Scholz
 */
public class OSCRAMSkeletonListener implements IPOSCListener {

	private static final String ADDRESS_SELECTOR = "/ram/skeleton";
	
	private static final int NUMBER_OF_NODE_ARGS = 8;
	
	private InputListener inputListener;
	
	/**
	 * Callback method invoked when a new message is received. This method forwards the decoded message as a <code>PerformerPosition</code>
	 * object to the <code>InputListener</code> subscribed to this instance.
	 * 
	 * @param timestamp		the time stamp of the message.
	 * @param msg			the OSC message received.
	 */
	@Override
	public void acceptMessage(Date timestamp, OSCMessage msg) {
		final int PERFORMER_INDEX = 0;
		this.inputListener.receivePositionMessage(this.decodePositionMessage(msg), PERFORMER_INDEX);
	}
	
	/**
	 * @return	the address selector of RAM-DT body position messages, defined at {@link OSCRAMSkeletonListener#ADDRESS_SELECTOR}, 
	 * currently with value "/ram/skeleton".
	 */
	@Override
	public String getAddressSelector() {
		return OSCRAMSkeletonListener.ADDRESS_SELECTOR;
	}
	
	/**
	 * Sets the input listener instance which will be used for callback when new feature messages are received.
	 * 
	 * @param inputListener		the new input listener to be used.
	 */
	@Override
	public void setInputListener(InputListener inputListener) {
		this.inputListener = inputListener;
	}
	
	private PerformerPosition decodePositionMessage(OSCMessage msg) {
		Integer argCount = 0;
		//discarded information: name (String)
		argCount++;
		Integer nodeCount = (Integer) msg.getArguments().get(argCount++);
		PerformerPosition position = new PerformerPosition();
		for(int i = 0; i < nodeCount; i++) {
			position.setPosition(this.readPositionVector(msg.getArguments(), argCount));
			argCount += NUMBER_OF_NODE_ARGS;
		}
		//discarded information: timestamp (float)
		return position;
	}
	
	private JointPosition readPositionVector(List<Object> msg, Integer startCount) {
		int argCount = startCount;
		JointPosition position = null;
		String name = (String) msg.get(argCount++);
		int index = RAMJointParser.findIndexByName(name);
		float x = (float) msg.get(argCount++);
		float y = (float) msg.get(argCount++);
		float z = (float) msg.get(argCount++);
		position = new JointPosition(index, x, y, z);
		//discarded information: angle, angle x, angle y, angle z
		argCount += 4;
		return position;
	}
}
