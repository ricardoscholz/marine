package mustic.scholz.marine.core.input.osc.ew;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mustic.scholz.marine.core.feature.Feature;
import mustic.scholz.marine.core.feature.FeatureFactory;
import mustic.scholz.marine.core.input.InputListener;
import mustic.scholz.marine.core.input.osc.IPOSCListener;

import com.illposed.osc.OSCMessage;

/**
 * This OSC listener implementation decodes messages of joint based features sent by EyesWeb features patch (see system configurations 
 * file).
 * 
 * @author Ricardo Scholz
 *
 */
public class EyesWebJointListener implements IPOSCListener {

	private static final String ADDRESS_SELECTOR = "/eyesweb/jointbased";
	
	private InputListener inputListener;
	
	/**
	 * Callback method invoked when a new message is received. This method forwards the decoded message as a list of <code>Feature</code>
	 * objects to the <code>InputListener</code> subscribed to this instance.
	 * 
	 * @param msgDate	the time stamp of the message.
	 * @param message	the OSC message received.
	 */
	@Override
	public void acceptMessage(Date msgDate, OSCMessage message) {
		this.inputListener.receiveFeatureMessage(this.decodeFeatureMessage(message));
	}
	
	/**
	 * @return	the address selector of EyesWeb full body features, defined at {@link EyesWebFullBodyListener#ADDRESS_SELECTOR}, currently
	 * with value "/eyesweb/jointbased".
	 */
	@Override
	public String getAddressSelector() {
		return ADDRESS_SELECTOR;
	}
	
	/**
	 * Sets the input listener instance which will be used for callback when new feature messages are received.
	 * 
	 * @param inputListener		the new input listener to be used.
	 */
	@Override
	public void setInputListener(InputListener inputListener) {
		this.inputListener = inputListener;
	}
	
	/**
	 * @return the callback input listener being used by this instance.
	 */
	public InputListener getInputListener() {
		return this.inputListener;
	}
	
	private List<Feature> decodeFeatureMessage(OSCMessage message) {
		List<Feature> result = new ArrayList<Feature>();
		if (ADDRESS_SELECTOR.equalsIgnoreCase(message.getAddress())) {
			List<Object> args = message.getArguments();
			String joint = (String) args.get(0);
			result.add(FeatureFactory.buildSmoothnessMagnitudeFeature((Float) args.get(1), joint));
			result.add(FeatureFactory.buildTangentialVelocityFeature((Float) args.get(1), joint));
			result.add(FeatureFactory.buildTangentialAccelerationFeature((Float) args.get(1), joint));
			result.add(FeatureFactory.buildTangentialJerkFeature((Float) args.get(1), joint));
			result.add(FeatureFactory.buildCurvatureIndex3DFeature((Float) args.get(1), joint));
			result.add(FeatureFactory.buildSmoothnessIndexFeature((Float) args.get(1), joint));
			result.add(FeatureFactory.buildDirectnessIndexFeature((Float) args.get(1), joint));
			result.add(FeatureFactory.buildDirectnessLengthFeature((Float) args.get(1), joint));
			result.add(FeatureFactory.buildCurvature3DFeature((Float) args.get(1), joint));
		}
		return result;
	}
}
