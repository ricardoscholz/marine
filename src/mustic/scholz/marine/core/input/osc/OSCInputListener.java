package mustic.scholz.marine.core.input.osc;

import java.util.ArrayList;
import java.util.List;

import mustic.scholz.marine.core.input.InputListener;

import com.illposed.osc.OSCPortIn;

/**
 * OSC messages input listener. This is a multiple listeners implementation, meaning that more than one OSC message type listener can be 
 * subscribed to this instance. These added listeners are java classes which encapsulate logic for specific messages parsing.
 *   
 * @author Ricardo Scholz
 *
 */
public class OSCInputListener extends InputListener {

	private Integer port = null;
	
	private List<IPOSCListener> listeners;

	/**
	 * Default constructor. Initialises the instance parameters.
	 * 
	 */
	public OSCInputListener() {
		super();
		this.listeners = new ArrayList<IPOSCListener>();
	}

	/**
	 * Starts listening to the default OSC port, as of {@link OSCPortIn#defaultSCOSCPort()}.
	 */
	//TODO [Improvement] Implement a way to listen to custom port.
	public void startListening() {
		try {
			int currPort = OSCPortIn.defaultSCOSCPort();
			if (port != null) {
				 currPort = port;
			}
			OSCPortIn receiver = new OSCPortIn(currPort);
			System.out.println("Binding OSC input listener to port " + currPort);
			for (IPOSCListener listener : this.listeners) {
				receiver.addListener(listener.getAddressSelector(), listener);
				System.out.println("OSC Listener " + listener.getAddressSelector() + " ready.");
			}
			receiver.startListening();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adds an actual listener to this instance. Listeners must be of type <code>IPOSCListener</code>, otherwise they are ignored.
	 * 
	 * @param listener		the listener to be added to this instance.
	 */
	@Override
	public void addListener(Object listener) {
		if (listener instanceof IPOSCListener) {
			this.listeners.add((IPOSCListener)listener);
		}
	}

	/**
	 * Removes the indicated listener from the list of input listeners, if it is found in this list, comparing by
	 * {@link Object#equals(Object)}.
	 * 
	 * @param listener		the listener to be removed.
	 */
	@Override
	public void removeListener(Object listener) {
		this.listeners.remove(listener);
	}
	
	/**
	 * Implementation of <code>Runnable</code> interface contract, for asynchronous run.
	 */
	@Override
	public void run() {
		this.startListening();
	}
}
