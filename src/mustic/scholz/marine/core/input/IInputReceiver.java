package mustic.scholz.marine.core.input;

import mustic.scholz.marine.core.entity.PerformerPosition;
import mustic.scholz.marine.core.feature.Feature;
import mustic.scholz.marine.core.feature.FeatureExtractor;

/**
 * This interface defines the methods an input receiver must implement. Input receivers are callback interfaces for performer position and
 * features when they are received by the hardware devices.
 * 
 * Input receivers are also expected to deal with device specific calibration information, such as default translation, pixels per unit 
 * ratio and device angle (in relation to the stage).
 * 
 * Implementing subclasses must be prepared to run asynchronously. 
 *  
 * @author Ricardo Scholz
 *
 */
public interface IInputReceiver extends Runnable {
	
	/*
	 * Position and Feature messages processing
	 */
	
	/**
	 * Callback method to process a performer position.
	 * 
	 * @param position	the performer position.
	 * @param index		the performer index, as some sensors might recognize more than one performer.
	 */
	public void processPerformerPosition(PerformerPosition position, int index);
	
	/**
	 * Callback method to process a feature message.
	 * 
	 * @param feature	the feature message.
	 */
	public void processFeatureMessage(Feature feature);
	
	/**
	 * Callback method to set the index of the first performer recognized.
	 * 
	 * @param index		the index of the first performer recognized.
	 */
	public void setFirstPerformer(int index);
	
	/*
	 * Custom Features Management
	 */
	
	/**
	 * Adds a custom feature to the internal pool, so every performer position message received will recompute the added feature and put it 
	 * in the <code>FeaturesPool</code>.
	 * 
	 * @param extractor		the <code>FeatureExtractor</code> instance, which implements the feature's computation algorithm.
	 */
	public void addFeatureExtractor(FeatureExtractor extractor);
	
	/**
	 * Removes a given custom feature from the internal pool. This feature will not be updated by future performer position messages 
	 * received by this instance. However, this method does not withdraw the last updated feature from the <code>FeaturesPool</code>.
	 * Therefore, its value will be kept constant until the end of the execution, or the custom feature extractor is added again to the 
	 * pool.
	 * 
	 * @param extractor		the feature extractor that implements the computation algorithm.
	 */
	public void removeFeatureExtractor(FeatureExtractor extractor);
	
	/*
	 * Calibration Methods
	 */
	
	// Translation
	
	/**
	 * Calibration method. Resets the translation coordinates to its default values. Translation will be applied to all position data 
	 * received by the actual hardware.
	 */
	public void resetPositionListenerTranslationCoordinates();
	
	/**
	 * Calibration method. Sets the translation coordinates to a given point.
	 * 
	 * @param x		the x axis value.
	 * @param y		the y axis value.
	 * @param z		the z axis value.
	 * @see IInputReceiver#resetPositionListenerTranslationCoordinates()
	 */
	public void setPositionListenerTranslationCoordinates(float x, float y, float z);
	
	/**
	 * @return 	the translation x axis coordinate.
	 */
	public float getPositionListenerTranslationCoordinateX();
	
	/**
	 * @return	the translation y axis coordinate.
	 */
	public float getPositionListenerTranslationCoordinateY();
	
	/**
	 * @return	the translation z axis coordinate.
	 */
	public float getPositionListenerTranslationCoordinateZ();
	
	// Pixels per Unit Ratio
	
	/**
	 * Calibration method. Resets the pixels per hardware unit of measure ratio to the default values. This ratio allows translation from 
	 * hardware unit of measure to Marine unit of measure (pixels). All position data received by the hardware will be translated to pixels.
	 */
	public void resetPositionListenerPixelsPerUnitRatio();
	
	/**
	 * Calibration method. Sets the pixels per hardware unit of measure ratio to a given value.
	 * 
	 * @param pixelsPerHardwareUnitOfMeasure	the amount of pixels each hardware unit of measure will be translated into.
	 * @see IInputReceiver#resetPositionListenerPixelsPerUnitRatio()
	 */
	public void setPositionListenerPixelsPerUnitRatio(float pixelsPerHardwareUnitOfMeasure);
	
	/**
	 * @return	the pixels per hardware unit of measure ratio.
	 */
	public float getPositionListenerPixelPerUnitRatio();
	
	// Device Angle
	
	/**
	 * Calibration method. Resets the input device angle to its default value. The input device angle is used to adjust coordinates systems
	 * between the coordinates received by the input device (sensitive to device angle in relation to the stage) and coordinates in the
	 * stage (fixed, independent of device and projector actual positioning). This is the angle between the input device z axis and the
	 * stage (Marine coordinates system) z axis, considering that (z,y) plan of both input device coordinates system and Marine coordinates
	 * system are coincident.
	 */
	public void resetInputDeviceAngle();

	/**
	 * Calibration method. Sets the input device angle to a given value, in radians.
	 * 
	 * @param angleRad	the new angle between the input device z axis and the stage z axis.
	 * @see IInputReceiver#resetInputDeviceAngle()
	 */
	public void setInputDeviceAngle(float angleRad);

	/**
	 * @return	the angle between the input device z axis and the stage z axis, in radians.
	 */
	public float getInputDeviceAngle();
	
}
