package mustic.scholz.marine.core.input;

import java.util.HashMap;
import java.util.List;

import mustic.scholz.marine.core.ConfigurationManager;
import mustic.scholz.marine.core.entity.PerformerPosition;
import mustic.scholz.marine.core.feature.Feature;

/**
 * This abstract class defines the contract for hardware specific input listeners. Input listeners are supposed to listen to position and
 * features messages, and call a given <code>IInputReceiver</code> callback methods. This abstract implementation saves the time stamps of
 * last received data of position and each of the features, and accepts a sampling timeout (in milliseconds), which causes it to ignore any 
 * data that arrives before the sampling timeout has expired, for each feature or position independently.
 * 
 * Actual InputListeners must be prepared to run asynchronously. 
 * 
 * @author Ricardo Scholz
 *
 */
public abstract class InputListener implements Runnable {

	/**
	 * The callback input receiver, which will process the messages received.
	 */
	protected IInputReceiver callback;
	
	private HashMap<Integer, Long> lastPositionReceived;
	private HashMap<String, Long> lastFeatureReceived;
	
	private long samplingTimeout;
	
	/**
	 * Default constructor. Reads the sampling timeout from the system configuration file.
	 */
	public InputListener() {
		this.lastPositionReceived = new HashMap<Integer, Long>();
		this.lastFeatureReceived = new HashMap<String, Long>();
		this.samplingTimeout = ConfigurationManager.getInstance().getSystemFeaturesSamplingTimeout();
	}
	
	/**
	 * Sets the input receiver which will be called back when a position or feature message is received.
	 * 
	 * @param callback	the callback input receiver.
	 */
	public void setCallbackProvider(IInputReceiver callback) {
		this.callback = callback;
	}
	
	/**
	 * Calls back the input receiver when a position message is received, so it can be processed.
	 * 
	 * @param position	the position message received.
	 * @param index		the index of the position received, as some sensors might recognize more than one performer.
	 */
	public void receivePositionMessage(PerformerPosition position, int index) {
		if (this.lastPositionReceived.get(index) == null || position.getTimestamp() - this.lastPositionReceived.get(index) > samplingTimeout) {
			this.callback.processPerformerPosition(position, index);
			this.lastPositionReceived.put(index, System.currentTimeMillis());
		}
	}
	
	/**
	 * Calls back the input receiver when feature messages are received, so they can be processed. For performance reasons, several features
	 * might be received at once.
	 * 
	 * @param features	the list of features received.
	 */
	public void receiveFeatureMessage(List<Feature> features) {
		long timestamp = System.currentTimeMillis();
		if (features != null) {
			for (Feature feature : features) {
				Long lastTimestamp = this.lastFeatureReceived.get(feature.getId());
				if (lastTimestamp == null 
						|| (timestamp - lastTimestamp > samplingTimeout)) {
					this.callback.processFeatureMessage(feature);
					this.lastFeatureReceived.put(feature.getId(), timestamp);
				}
			}
		}
	}
	
	public void setFirstPerformer(int index) {
		this.callback.setFirstPerformer(index);
	}
	
	/**
	 * Prepares and starts the concrete listeners, so they are able to receive hardware messages.
	 */
	public abstract void startListening();
	
	/**
	 * Adds a listener. Listeners are classes that may encapsulate protocol or hardware specific code, to simplify the concrete
	 * <code>InputListener</code> implementation. Type checking must be done by implementing subclass, as it is impossible at this level to 
	 * define a common interface for these specific listeners. The added listener may overwrite the previous one (single listener 
	 * implementation), or accumulate with the previous ones (multiple listeners implementation), according to the specific implementation.
	 * 
	 * @param listener	the actual listener to add.
	 */
	public abstract void addListener(Object listener);
	
	/**
	 * Removes a given listener.
	 * 
	 * @param listener		the listener to be removed.
	 * @see InputListener#addListener(Object)
	 */
	public abstract void removeListener(Object listener);
	
	/**
	 * <code>Runnable</code> interface method, for asynchronous execution.
	 */
	@Override
	public void run() {
		this.startListening();
	}
}
