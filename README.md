# marine: a framework for multimedia artistic interactivity experimentation #

marine is part of my PhD research, in which I investigate ways to improve the use of motion capture devices in performing arts and approximate developers and artists.

# How to Setup Developer Environment #

For detailed information about environment setup, please download "Developer's Guide" at 
http://marineframework.org/developers

# How to Talk to Me #

Ricardo Scholz | contact at marineframework dot org | [http://marineframework.org/](Link URL)